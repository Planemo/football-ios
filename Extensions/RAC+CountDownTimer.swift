//
//  RAC+CountDownTimer.swift
//  Football
//
//  Created by Mikhail Shulepov on 23/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import enum Result.NoError

public func countDownTimer(_ value: Int) -> Signal<Int, NoError> {
    return Signal<Int, NoError> { observer in
        var time = value
        let reduceTime = { () -> () in
            time -= 1
            observer.send(value: time)
            if time == 0 {
                observer.sendCompleted()
            }
        }
        let scheduler = QueueScheduler.main
        let interval = DispatchTimeInterval.seconds(1)
        let leeway = DispatchTimeInterval.microseconds(2)
        let disposable = scheduler.schedule(after: Date(), interval: interval, leeway: leeway, action: {
            reduceTime()
        })
        
        return CompositeDisposable([disposable!])
    }
}
