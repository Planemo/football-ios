//
//  UILabel+FontSize.swift
//  Football
//
//  Created by Mikhail Shulepov on 10/06/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension UILabel {
    func enlargeFontSizeBy(_ multiplier: CGFloat) {
        if let font = self.font {
            self.font = font.withSize(font.pointSize * multiplier)
        }
    }
}
