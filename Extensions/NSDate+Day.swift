//
//  NSDate+Day.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public extension Date {
    public func beginningOfDay() -> Date {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.year, .month, .day], from: self)
        return calendar.date(from: components)!
    }
    
    public func endOfDay() -> Date {
        var components = DateComponents()
        components.day = 1
        let date = (Calendar.current as NSCalendar).date(byAdding: components, to: self.beginningOfDay(), options: [])!
        return date.addingTimeInterval(-1)
    }
    
    public func isAfter(_ date: Date) -> Bool{
        return (self.compare(date) == ComparisonResult.orderedDescending)
    }
    
    public func isBefore(_ date: Date) -> Bool{
        return (self.compare(date) == ComparisonResult.orderedAscending)
    }
    
    // MARK: Getter
    
    /**
    Date year
    */
    public var year : Int {
        get {
            return getComponent(.year)
        }
    }
    
    /**
     Date month
     */
    public var month : Int {
        get {
            return getComponent(.month)
        }
    }
    
    /**
     Date weekday
     */
    public var weekday : Int {
        get {
            return getComponent(.weekday)
        }
    }
    
    /**
     Date weekMonth
     */
    public var weekMonth : Int {
        get {
            return getComponent(.weekOfMonth)
        }
    }
    
    
    /**
     Date days
     */
    public var days : Int {
        get {
            return getComponent(.day)
        }
    }
    
    /**
     Date hours
     */
    public var hours : Int {
        
        get {
            return getComponent(.hour)
        }
    }
    
    /**
     Date minuts
     */
    public var minutes : Int {
        get {
            return getComponent(.minute)
        }
    }
    
    /**
     Date seconds
     */
    public var seconds : Int {
        get {
            return getComponent(.second)
        }
    }
    
    /**
     Returns the value of the NSDate component
     
     - parameter component: NSCalendarUnit
     - returns: the value of the component
     */
    
    public func getComponent (_ component : Calendar.Component) -> Int {
        let calendar = Calendar.current
        return calendar.component(component, from: self)
    }
}
