//
//  RAC+Convenience.swift
//  Football
//
//  Created by Mikhail Shulepov on 30.11.15.
//  Copyright © 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift

extension SignalProducerProtocol {
    @discardableResult
    public func startWithValues(value: @escaping ((Value) -> ()), failed: @escaping ((Error) -> ())) -> Disposable {
        return startWithResult { result in
            switch result {
            case .success(let val):
                value(val)
            case .failure(let err):
                failed(err)
            }
        }
    }
}

/*
extension SignalProducer {
    public func start(next: ((Value) -> ()), failed: ((Error) -> ())? = nil, completed: (() -> ())? = nil, interrupted: (() -> ())? = nil) -> Disposable {
        return start(Observer(failed: failed, completed: completed, interrupted: interrupted, next: next))
    }
    
    public func start(failed: ((Error) -> ()), completed: (() -> ())? = nil, interrupted: (() -> ())? = nil) -> Disposable {
        return start(Observer(failed: failed, completed: completed, interrupted: interrupted, next: nil))
    }
}

extension Signal {
    public func observe(next: ((Value) -> ()), failed: ((Error) -> ())? = nil, completed: (() -> ())? = nil, interrupted: (() -> ())? = nil) -> Disposable? {
        return observe(Observer(failed: failed, completed: completed, interrupted: interrupted, next: next))
    }
    
    public func observe(failed: ((Error) -> ()), completed: (() -> ())? = nil, interrupted: (() -> ())? = nil) -> Disposable? {
        return observe(Observer(failed: failed, completed: completed, interrupted: interrupted, next: nil))
    }
}*/
