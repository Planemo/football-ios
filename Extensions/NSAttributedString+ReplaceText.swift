//
//  NSAttributedString+ReplaceText.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public extension NSAttributedString {
    public func attributedStringByReplacingTextWith(_ text: String) -> NSAttributedString {
        let mutableAttrString = self.mutableCopy() as! NSMutableAttributedString
        let range = NSMakeRange(0, mutableAttrString.string.utf16.count)
        mutableAttrString.replaceCharacters(in: range, with: text)
               
        return mutableAttrString
    }
    
    public func attributedStringByReplacingText(_ entry: String, withText text: String) -> NSAttributedString {
        if let found = self.string.range(of: entry, options: .caseInsensitive) {
            let rangeStart = self.string.distance(from: self.string.startIndex, to: found.lowerBound)
            let len = self.string.distance(from: found.lowerBound, to: found.upperBound)
            let nsrange = NSMakeRange(rangeStart, len)
            
            let mutableAttrString = self.mutableCopy() as! NSMutableAttributedString
            mutableAttrString.replaceCharacters(in: nsrange, with: text)
            return mutableAttrString
        }
        
        return self
    }
    
    public func attributedStringByReplacingTemplateTextWith(_ text: String) -> NSAttributedString {
        if self.string.utf16.count != text.utf16.count {
            NSLog("Warning: attributedStringByReplacingTemplateTextWith: lengt of strings should be equal")
            return self.attributedStringByReplacingTextWith(text)
        }
        let mutableAttrString = NSMutableAttributedString(string: text)
        let range = NSMakeRange(0, mutableAttrString.string.utf16.count)
        self.enumerateAttributes(in: range, options: []) { attrs, range, stop in
            NSLog("Insert attrs %@ at range %@", Array(attrs.keys).description, NSStringFromRange(range))
            mutableAttrString.addAttributes(attrs, range: range)
        }
        return mutableAttrString
    }
}

public extension NSMutableAttributedString {
    public func addAttributes(_ attributes: [String: Any], forSubstring substring: String) {
        if let range = self.string.range(of: substring, options: []) {
            let start = self.string.distance(from: self.string.startIndex, to: range.lowerBound)
            let len = self.string.distance(from: range.lowerBound, to: range.upperBound)
            let nsrange = NSMakeRange(start, len)
            self.addAttributes(attributes, range: nsrange)
        }
    }
}
