# README #

![appendix-13a_559x343.jpg](https://bitbucket.org/repo/XKMnnr/images/1010674594-appendix-13a_559x343.jpg)

### XML Samle structure ###


```
#!xml

<Games timestamp="2013-11-23T19:38:35">
<Game id="695019" away_team_id="8" away_team_name="Chelsea" competition_id="8" competition_name="English Barclays Premier League" game_date="2013-11-23T17:30:00" home_team_id="21" home_team_name="West Ham United" matchday="12" period_1_start="2013-11-23T17:30:27" period_2_start="2013-11-23T18:33:30" season_id="2013" season_name="Season 2013/2014">

<Event id="330261085" event_id="1" type_id="34" period_id="16" min="0" sec="0" team_id="8" outcome="1" x="0.0" y="0.0" timestamp="2013-11-23T16:37:53.628" last_modified="2013-11-23T19:02:00">

<Q id="709454605" qualifier_id="141" value="38.6"/>
<Q id="1693199433" qualifier_id="140" value="21.0"/>
<Q id="941017391" qualifier_id="212" value="17.3"/>
<Q id="1613203577" qualifier_id="213" value="4.6"/>
<Q id="1221628681" qualifier_id="56" value="Back"/>

</Event>

.....

<Event id="268788163" event_id="4" type_id="1" period_id="1" min="0" sec="3" player_id="18073" team_id="21" outcome="1" x="51.5" y="48.4" timestamp="2013-11-23T17:30:31.453" last_modified="2013-11-23T17:31:11">
</Event>

</Game>
<Games>


```

**<Event>**

1. outcome (0 - unsuccessfull, 1 - successfull)

* Pass
* Offside Pass
* Take On
* Foul
* Out
* Interception
* Penalty

Always set to 1

* Goal
* Miss
* Attempt Saved
* Aerial

2. assist (Will only appear on an event if this event led directly to a goal, value = 1)
3. keypass (Will only appear on an event if this event led directly to a shot off target, blocked or saved)

4. x (This is the length of the pitch where 0 the defensive goal line and 100 is the attacking goal line, value = 0...100)
5. y (value = 0...100)

**<Q>**

1. Pass End X (The x pitch coordinate for the end point of a pass)
2. Pass End Y (The y pitch coordinate for the end point of a pass)
3. Zone (Back, left, centre, right)
4. Length (Dynamic - length of pass in metres)
5. Angle (The angle the ball travels at during an event relative to the direction of play. Shown in radians. 0 to 6.28)
6. Direct
7. Long ball
8. Head pass
9. Corner taken
10. Attacking Pass (A pass in the opposition’s half of the pitch)
11. Left footed
12. Right footed
13. Other body part
14. Penalty
15. Own goal
16. Follows a Dribble (A goal followed a dribble by the goalscorer)
17. Individual Play (Player created the chance to shoot by himself, not assisted. For example he dribbled to create space for himself and shot.)
18. Save

### XML structure v.1 ###

```
#!xml

<Games timestamp="2013-11-23T19:38:35">
<Game id="695019" game_type="FIFA" away_team_name="Chelsea" competition_name="English Barclays Premier League" game_date="2013-11-23T17:30:00" home_team_name="West Ham United" matchday="12" period_1_start="2013-11-23T17:30:27" period_2_start="2013-11-23T18:33:30" season_id="2013" season_name="Season 2013/2014">

<Event id="330261085" event_id="1" type_id="34" min="0" sec="0" team_id="8" outcome="1" x="0.0" y="0.0" timestamp="2013-11-23T16:37:53.628">

<Q qualifier_id="pass_end_x" value="38.6"/>
<Q qualifier_id="pass_end_y" value="21.0"/>
<Q qualifier_id="length" value="17.3"/>
<Q qualifier_id="angle" value="4.6"/>
<Q qualifier_id="zone" value="Back"/>

</Event>

</Game>
<Games>


```
### Game ###

* Type [FIFA, NBA]
* League
* Country
* Season Name
* Stadium

### Team ###

* Name
* Emblem [Type: Image]
* List of Players

### Team XML ###
```
#!xml



<Teams>
<Team name="Morecambe">
<Player>Arjen Robben</Player>
</Team>
<Team name="Northampton Town">
<Player>Arjen Robben</Player>
</Team>
<Team name="Torquay United">
<Player>Arjen Robben</Player>
</Team>
<Team name="Portsmouth">
<Player>Arjen Robben</Player>
</Team>

</Teams>
```


### Player ###

* Name
* Number
* Portrait [Type: Image]
* Height
* Weight
* Position [Defender, Midfielder, Forward, Goalkeeper]
* Preferred Foot [Left, Right]
* Country [For example, Netherlands <...>]

### Player XML ###

```
#!xml


<Player>
<Name>Arjen Robben</Name>
<Portrait>PATH_TO_IMAGE</Portrait>
<Position>Midfield (General Winger)</Position>
<Stat Type="first_name">Arjen</Stat>
<Stat Type="last_name">Robben</Stat>
<Stat Type="birth_date">1984/00/23</Stat>
<Stat Type="weight">75</Stat>
<Stat Type="height">181</Stat>
<Stat Type="real_position">Midfield (General Winger)</Stat>
<Stat Type="real_position_side">Unknown</Stat>
<Stat Tyoe="preferred_foot">Left</Stat>
<Stat Type="birth_place">Bedum</Stat>
<Stat Type="nationality">Netherlands</Stat>
<Stat Type="international_caps">73</Stat>
<Stat Type="international_debut">Unknown</Stat>
<Stat Type="top_competition_debut">Unknown</Stat>

<Career>

<Individual_Honours>
<Honour Type="FA Cup Winner" honour_id="1">Season 2006/2007</Honour>
<Honour Type="England Premier League Champion" honour_id="8">Season 2005/2006</Honour>
<Honour Type="England Premier League Champion" honour_id="8">Season 2004/2005</Honour>
<Honour Type="UEFA Champions League Winner" honour_id="5">Season 2012/2013</Honour>
</Individual_Honours>

<International>
<CareerDetail games_played="73" goals="22" team_name="Netherlands Senior"/>
</International>
<Career>
<Player>
```