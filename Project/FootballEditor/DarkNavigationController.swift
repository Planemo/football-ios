//
//  DarkNavigationController.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class DarkNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        viewController.view.backgroundColor = UIColor.darkGrayColor()
    }
}

