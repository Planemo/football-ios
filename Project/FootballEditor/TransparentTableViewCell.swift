//
//  TransparentTableViewCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 30/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TransparentTableViewCell: UITableViewCell {

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clearColor()
        self.backgroundView?.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
    }

}
