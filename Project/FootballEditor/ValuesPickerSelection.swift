//
//  ValuesPickerSelection.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class ValuesPickerSelection: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var selectionCallback: ((String)->())?
    var values = [String]() {
        didSet {
            self.reloadAllComponents()
        }
    }
    
    init() {
        super.init(frame: CGRectZero)
        self.setup()
    }
    
    init(values: [String]) {
        super.init(frame: CGRectZero)
        self.values = values        
        self.setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        self.delegate = self
        self.dataSource = self
    }

    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.values.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.values[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectionCallback?(self.values[row])
    }
}
