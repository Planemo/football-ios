//
//  UIImage+Resize.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension UIImage {
    func resize(size: CGSize) -> UIImage {
        let oldSize = CGSize(width: self.size.width * self.scale, height: self.size.height * self.scale)
        let aspectRatio = oldSize.width / oldSize.height
        
        let boundedSize = CGSize(width: min(size.width, oldSize.width), height: min(size.height, oldSize.height))
        let newSize: CGSize = CGSize(width: boundedSize.height * aspectRatio, height: boundedSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 1.0)
        self.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage;
    }
}
