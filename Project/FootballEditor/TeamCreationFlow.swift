//
//  TeamCreationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveCocoa
import Alamofire
import PLSupport
import PLModal

class TeamCreationFlow: UITableViewController, ImageSelectorDelegate, UITextFieldDelegate {
    var formTemplateComposer: FormTemplateComposer?

    var teamLogo: UIImage? {
        didSet {
            self.formTemplateComposer?.teamLogo = self.teamLogo
        }
    }
    var teamName: String = "" {
        didSet {
            self.formTemplateComposer?.teamName = self.teamName
        }
    }
    
    var teamCountry: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if let formComposer = segue.destinationViewController as? FormTemplateComposer {
            self.formTemplateComposer = formComposer
            formComposer.teamLogo = self.teamLogo
            formComposer.teamName = self.teamName
            
        } else if let imageSelector = segue.destinationViewController as? ImageSelectorViewController {
            imageSelector.delegate = self
        }
    }
    
    func imageSelectorDidPickImage(image: UIImage) {
        self.teamLogo = image
    }
    
    @IBAction func onNameFieldUpdated(nameField: UITextField) {
        self.teamName = nameField.text ?? ""
    }
    
    @IBAction func onRegionFieldUpdated(regionField: UITextField) {
        self.teamCountry = regionField.text ?? ""
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func doCreateNewTeam() {
        let hud = MBProgressHUD.showGlobalHUD()
        
        let largeLogo = self.teamLogo!.resize(CGSize(width: 500, height: 500))
        let smallLogo = self.teamLogo!.resize(CGSize(width: 100, height: 100))
        
        let largeLogoData = UIImagePNGRepresentation(largeLogo)!
        let smallLogoData = UIImagePNGRepresentation(smallLogo)!
        
        FileUploader().uploadFileToFolder("Teams/Small", name: self.teamName, fileExtension: "png", data: smallLogoData)
            .then( FileUploader().uploadFileToFolder("Teams", name: self.teamName, fileExtension: "png", data: largeLogoData))
            .flatMap(.Merge) { logoUrl -> SignalProducer<String, NSError> in
                return self.createTeam(logoUrl)
            }
            .start(failed: { error in
                NSLog("Error submitting team: %@", error.description)
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
            }, completed: {
                hud.hideWithSuccessStatus(NSLocalizedString("Team Creation Success", comment: "Success"))
                self.navigationController?.popViewControllerAnimated(true)
            })
    }
    
    @IBAction func submit() {
        if self.teamLogo == nil || self.teamName.isEmpty || self.teamCountry.isEmpty || self.formTemplateComposer?.template == nil {
            MBProgressHUD.showHUDAddedTo(self.view, withError: "Not all fields are filled")
            return
        }
        
        let alert = PLAlertViewController()
        let actionCreate = AlertAction(title: "Create") {
            self.doCreateNewTeam()
        }
        let actionCancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Submit", message: "Are you sure you want to create new team?", actions: actionCreate, actionCancel)
    }
    
    func createTeam(logoUrl: String) -> SignalProducer<String, NSError> {
        NSLog("Create team...")
        func colorToRgbHexString(color: UIColor) -> String {
            let str = String(format: "%X", color.toARGB())
            return str.substringFromIndex(str.startIndex.advancedBy(2))
        }
        
        let template = self.formTemplateComposer!.template!
        let formTemplateData: [String: AnyObject] = [
            "template": (template.foregroundTemplate),
            "b_color": colorToRgbHexString(template.backgroundColor),
            "f_color": colorToRgbHexString(template.foregroundColor),
            "shadow": template.withShadow ? 1 : 0
        ]
        
        let teamData: [String: AnyObject] = [
            "name": self.teamName,
            "emblem_url": logoUrl,
            "country": self.teamCountry
        ]
        
        let data: [String: AnyObject] = [
            "form": formTemplateData,
            "team": teamData,
            "method": "create_team"
        ]
        
        return SignalProducer { observer, compositeDisposable in
            let url = "http://api.planemostd.com/football/api/content_editor.php"
            Alamofire.request(.POST, url, parameters: data, encoding: .JSON)
                .validate(statusCode: 200..<300)
                .responseString(encoding: nil) { response -> Void in
                    if let error = response.result.error {
                        NSLog("Error: %@", error.description)
                        observer.sendFailed(error)
                    } else {
                        let responseString = response.result.value!
                        observer.sendNext(responseString)
                        observer.sendCompleted()
                        NSLog("Created team with id: %@", responseString)
                    }
            }
        }
    }
    

}
