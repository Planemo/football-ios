//
//  NotificationSendViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/06/16.
//  Copyright © 2016 Planemo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD

class NotificationSendViewController: UIViewController {
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var actionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction
    func sendNotification() {
        guard let messageText = self.messageTextView.text where !messageText.isEmpty else {
            return;
        }
        
        guard let actionText = self.actionTextField.text where !actionText.isEmpty else {
            return;
        }
        
        let hud = MBProgressHUD.showGlobalHUD()
        let url = "http://api.planemostd.com/football/api/notifications_manager.php"
        let params: [String: AnyObject] = ["message": messageText, "action": actionText]
        Alamofire.request(.POST, url, parameters: params, encoding: .JSON).responseString { response -> Void in
            if let error = response.result.error {
                NSLog("%@", error.description)
                hud.hideWithErrorStatus(NSLocalizedString("Notification send error", comment: "Error"))
                //completion()
            } else {
                hud.hideWithSuccessStatus(NSLocalizedString("Notification sent!", comment: "Success"))
                //completion()
            }
            
        }
    }
}