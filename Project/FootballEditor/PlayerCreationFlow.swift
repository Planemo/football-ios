//
//  PlayerCreationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveCocoa
import Alamofire
import PLModal

class PlayerCreationFlow: UITableViewController, ImageSelectorDelegate, UITextFieldDelegate {
    @IBOutlet var birthDateField: UITextField! {
        didSet {
            let picker = UIDatePicker()
            picker.datePickerMode = UIDatePickerMode.Date
            picker.addTarget(self, action: #selector(PlayerCreationFlow.didPickBirthDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
            birthDateField.inputView = picker
        }
    }
    
    @IBOutlet var roleField: UITextField! {
        didSet {
            let positions = ["Goalkeeper", "Defender", "Midfielder", "Forward"].map {
                return NSLocalizedString($0, comment: "")
            }
            let picker = ValuesPickerSelection(values: positions)
            picker.selectionCallback = { [weak self] role in
                self?.role = role
                self?.roleField.text = role ?? ""
            }
            roleField.inputView = picker
        }
    }
    
    @IBOutlet var detailedPositionField: UITextField! {
        didSet {
            let positions = ["GK", "SW", "RB", "LB", "CB", "RWB", "LWB", "CDM",
                "RM", "CM", "LM", "RWM", "LWM", "CAM", "RF", "CF", "LF", "RS",
                "LS", "ST", "DM", "RW", "LW", "AM", "SS"]
            let picker = ValuesPickerSelection(values: positions)
            picker.selectionCallback = { [weak self] position in
                self?.detailedPosition = position
                self?.detailedPositionField.text = position
            }
            detailedPositionField.inputView = picker
        }
    }
    
    @IBOutlet var joinDateField: UITextField! {
        didSet {
            let picker = UIDatePicker()
            picker.datePickerMode = UIDatePickerMode.Date
            picker.addTarget(self, action: #selector(PlayerCreationFlow.didPickJoinDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
            joinDateField.inputView = picker
        }
    }
    
    @IBOutlet var numberField: UITextField!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var teamView: ShallowTeamView!
    
    private let dateFormatter = NSDateFormatter()
    
    var role: String?
    var portraitImage: UIImage?
    var birthDate: NSDate?
    var joinDate: NSDate?
    var detailedPosition: String?
    var team: Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFormatter.dateFormat = "YYYY MMM dd"
        self.teamView.team = nil
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let imageSelector = segue.destinationViewController as? ImageSelectorViewController {
            imageSelector.delegate = self
        }
    }
    
    func imageSelectorDidPickImage(image: UIImage) {
        self.portraitImage = image
    }
    
    private func doCreateNewPlayer() {
        let hud = MBProgressHUD.showGlobalHUD()
        let resizedImage = self.portraitImage!.resize(CGSize(width: 400, height: 400))
        let imageData = UIImagePNGRepresentation(resizedImage)!
        
        FileUploader().uploadFileToFolder("Players/\(self.team!.name)", fileExtension: "png", data: imageData)
            .flatMap(.Merge) { portraitURL -> SignalProducer<String, NSError> in
                return self.createPlayer(portraitURL)
            }.start(failed: { error in
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
                NSLog("Error creating player: %@", error.description)
            }, completed: {
                hud.hideWithSuccessStatus(NSLocalizedString("Player Creation Success", comment: "Success"))
                self.navigationController?.popViewControllerAnimated(true)
            })
    }

    @IBAction func proceed() {
        if (self.nameField.text?.isEmpty ?? true) || (self.role?.isEmpty ?? true) ||
           (self.portraitImage == nil) || (self.joinDate == nil) ||
           (self.team == nil) || (self.numberField.text?.isEmpty ?? true) || (self.detailedPosition == nil) {
                MBProgressHUD.showHUDAddedTo(self.view, withError: "Not all fields are filled")
                return
        }
        
        let alert = PLAlertViewController()
        let actionCreate = AlertAction(title: "Create") {
            self.doCreateNewPlayer()
        }
        let actionCancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Submit", message: "Are you sure you want to create new player?", actions: actionCreate, actionCancel)
    }
    
    func createPlayer(portraitURL: String) -> SignalProducer<String, NSError> {
        var playerData = [
            "name": self.nameField.text ?? "",
            "portrait_url": portraitURL,
            "role": self.role!
        ]
        if let birthDate = self.birthDate {
            playerData["birth_date"] = ISO8601DateParser().dateToString(birthDate)
        }
        
        let squadData = [
            "team_id": "\(self.team!.uid!)",
            "position": self.detailedPosition!,
            "join_date": ISO8601DateParser().dateToString(self.joinDate!),
            "number": self.numberField.text ?? ""
        ]

        let data: [String: AnyObject] = [
            "method": "create_player",
            "player": playerData,
            "squad": squadData
        ]
        
        return SignalProducer { observer, compositeDisposable in
            let url = "http://api.planemostd.com/football/api/content_editor.php"
            Alamofire.request(.POST, url, parameters: data, encoding: .JSON)
                .validate(statusCode: 200..<300)
                .responseString(encoding: nil) { response -> Void in
                    if let error = response.result.error {
                        observer.sendFailed(error)
                    } else {
                        let responseString = response.result.value!
                        observer.sendNext(responseString)
                        observer.sendCompleted()
                        NSLog("Created player with id: %@", responseString)
                    }
                }
        }
    }
    
    func didPickBirthDate(picker: UIDatePicker) {
        self.birthDate = picker.date
        self.birthDateField.text = self.dateFormatter.stringFromDate(picker.date)
    }
    
    func didPickJoinDate(picker: UIDatePicker) {
        self.joinDate = picker.date
        self.joinDateField.text = self.dateFormatter.stringFromDate(picker.date)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func selectTeam() {
        let hud = MBProgressHUD.showGlobalHUD()
        let teamsFilter = TeamsFilter.None
        DataProvider.activeProvider.requestShallowTeamsInfo(filter: teamsFilter)
            .observeOn(UIScheduler())
            .start(next: { teams in
                hud.hide(true)
                let teamPicker = Storyboards.Main.instantiateTeamsCollectionViewSelector()
                teamPicker.teams = teams
                teamPicker.selectedTeams = (self.team != nil) ? [self.team!] : []
                teamPicker.selectionDelegate = self
                self.navigationController?.pushViewController(teamPicker, animated: true)
                
            }, failed: { error in
                hud.hideWithErrorStatus(NSLocalizedString("Error Loading Teams", comment: "Error"))
            })
    }
}

extension PlayerCreationFlow: TeamsSelectionDelegate {
    func didSelectTeams(teams: [Team], selector: TeamsSelector) {
        self.team = teams.first
        self.teamView.team = self.team
        self.navigationController?.popViewControllerAnimated(true)
    }
}