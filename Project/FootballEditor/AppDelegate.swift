//
//  AppDelegate.swift
//  FootballEditor
//
//  Created by Mikhail Shulepov on 27/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Parse.PFUser
import ReactiveCocoa
import PLModal
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Fabric.with([Crashlytics()])
        
        Parse.setApplicationId(BuildConfig.Parse.ApplicationId, clientKey: BuildConfig.Parse.ClientKey)
        
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions ?? [:])
        //self.registerFofRemoteNotifications(application)
        self.launchSetup()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func registerFofRemoteNotifications(application: UIApplication) {
        let userNotificationTypes: UIUserNotificationType = [.Alert, .Badge, .Sound]
        
        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let installation = PFInstallation.currentInstallation()
        installation.channels = ["Editor"]
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
    }
}

// MARK: Setup
private extension AppDelegate {
    func launchSetup() {
        AppearanceConfigurator.configureAlerts()
        let serverConfig = ServerConfiguration()
              
        ParseLoginUtils.shared.currentUser.producer.startWithNext { pfuser in
            if let pfuser = pfuser {
                let linkedToFacebook = ParseLoginUtils.shared.facebookLoggedIn.value
                let user = LocalUser(userId: pfuser.objectId!, serverConfig: serverConfig, linkedToFacebook: linkedToFacebook)
                user.name = pfuser.displayName ?? "???"
                user.avatarURL = pfuser.avatarURL
                LocalUser.currentUser.value = user
                user.createOrUpdateUser()
            } else {
                LocalUser.currentUser.value = nil
            }
        }
        
        self.setupRemoteDataProvider(serverConfig)
    }
    
    func setupRemoteDataProvider(serverConfig: ServerConfiguration) {
        let engine = RemoteDataProvider(serverConfig: serverConfig)
        let provider = DataProvider(provider: engine)
        DataProvider.activeProvider = provider
    }
}