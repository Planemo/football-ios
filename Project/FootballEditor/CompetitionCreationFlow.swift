//
//  CompetitionCreationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveCocoa
import Alamofire
import PLSupport
import PLModal

class CompetitionCreationFlow: UIViewController, ImageSelectorDelegate, UITextFieldDelegate {
    @IBOutlet var textField: UITextField!
    @IBOutlet var regionField: UITextField!
    var selectedImage: UIImage?
    
    @IBAction func cancel() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func submit() {
        if self.selectedImage == nil || (self.textField.text?.isEmpty ?? true) || (self.regionField.text?.isEmpty ?? true) {
            MBProgressHUD.showHUDAddedTo(self.view, withError: "Not all fields are filled")
            return
        }
        
        let alert = PLAlertViewController()
        let actionCreate = AlertAction(title: "Create") {
            self.doCreateCompetition()
        }
        let actionCancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Submit", message: "Are you sure you want to create new competition?", actions: actionCreate, actionCancel)
    }
    
    private func doCreateCompetition() {
        let resizedImage = self.selectedImage!.resize(CGSize(width: 300, height: 300))
        let data = UIImagePNGRepresentation(resizedImage)
        
        guard let name = self.textField.text else {
            return
        }
        guard let region = self.regionField.text else {
            return
        }
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        FileUploader().uploadFileToFolder("Competitions", fileExtension: "png", data: data!)
            .flatMap(.Merge) { fileName -> SignalProducer<String, NSError> in
                return CompetitionCreationFlow.createCompetition(name, region: region, fileUrl: fileName)
            }
            .start(next: { value in
                
            }, failed: { error in
                NSLog("Error: \(error)")
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
                
            }, completed: {
                let competitionAddedFormat = NSLocalizedString("Competition %@ Added", comment: "Competition successfully added")
                let message = String(format: competitionAddedFormat, name)
                hud.hideWithSuccessStatus(message)
                self.cancel()
            })

    }
    
    private static func createCompetition(name: String, region: String, fileUrl: String) -> SignalProducer<String, NSError> {
        return SignalProducer { observer, compositeDisposable in
            let url = "http://api.planemostd.com/football/api/content_editor.php"
            let data = [
                "name": name,
                "logo": fileUrl,
                "region": region,
                "method": "create_competition"
            ]
            Alamofire.request(.POST, url, parameters: data, encoding: ParameterEncoding.URL)
                .validate(statusCode: 200..<300)
                .responseString(encoding: nil) { response -> Void in
                    if let error = response.result.error {
                        observer.sendFailed(error)
                    } else if let string = response.result.value {
                        observer.sendNext(string)
                        observer.sendCompleted()
                    } else {
                        let error = NSError(domain: "NoResponse", code: 3748, userInfo: nil)
                        observer.sendFailed(error)
                    }
                }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let imageSelector = segue.destinationViewController as? ImageSelectorViewController {
            imageSelector.delegate = self
        }
    }
    
    func imageSelectorDidPickImage(image: UIImage) {
        self.selectedImage = image
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
