//
//  TemplateCreationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal
import Color_Picker_for_iOS

protocol FormTemplateComposerDelegate: class {
    func didCreateFormTemplate(template: FormTemplate, creator: FormTemplateComposer)
}

class FormTemplateComposer: UIViewController {
    @IBOutlet var topTeamTemplate: TeamTemplateView!
    @IBOutlet var bottomTeamTemplate: TeamTemplateView!
    
    var template: FormTemplate! {
        didSet {
            self.topTeamTemplate.setupWithTemplate(self.template)
            self.bottomTeamTemplate.setupWithTemplate(self.template)
            self.delegate?.didCreateFormTemplate(self.template, creator: self)
        }
    }
    
    @IBOutlet var shadowSwitcher: UISwitch!
    @IBOutlet var templateNumberLabel: UILabel!
    
    @IBOutlet var backgroundColorButton: UIButton!
    @IBOutlet var foregroundColorButton: UIButton!
    
    var currentTemplate: Int = 1
    weak var delegate: FormTemplateComposerDelegate?
    
    var teamLogo: UIImage? {
        didSet {
            self.topTeamTemplate?.logoView?.image = teamLogo
            self.bottomTeamTemplate?.logoView?.image = teamLogo
        }
    }
    
    var teamName: String? {
        didSet {
            self.topTeamTemplate?.nameLabel?.text = teamName ?? "???"
            self.bottomTeamTemplate?.nameLabel?.text = teamName ?? "???"
        }
    }
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topTeamTemplate.team = nil
        self.bottomTeamTemplate.team = nil
        
        self.templateNumberLabel.text = "\(self.currentTemplate)"
        self.template = FormTemplate(
            backgroundColor: self.backgroundColorButton.backgroundColor!,
            foregroundTemplate: "\(self.currentTemplate)",
            foregroundColor: self.foregroundColorButton.backgroundColor!,
            withShadow: self.shadowSwitcher.on
        )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func switchShadow() {
        self.template = FormTemplate(backgroundColor: self.template.backgroundColor,
            foregroundTemplate: self.template.foregroundTemplate,
            foregroundColor: self.template.foregroundColor, withShadow: self.shadowSwitcher.on)
    }
    
    @IBAction func nextTemplate() {
        self.setupWithTemplateNumber(self.currentTemplate + 1)
    }
    
    @IBAction func prevTemplate() {
        self.setupWithTemplateNumber(self.currentTemplate - 1)
    }
    
    func setupWithTemplateNumber(number: Int) {
        var number = number
        if number > 12 {
            number = 1
        }
        if number < 1 {
            number = 12
        }
        self.currentTemplate = number
        let templateName = "\(number)"
        self.templateNumberLabel.text = templateName
        self.template = FormTemplate(backgroundColor: self.template.backgroundColor,
            foregroundTemplate: templateName,
            foregroundColor: self.template.foregroundColor, withShadow: self.template.withShadow)
    }
    
    @IBAction func pickBackgroundColor() {
        PLModal.shared.blackoutColor = UIColor.clearColor()
        
        let alert = PLAlertViewController()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            alert.appearance.width = 400
        } else {
            alert.appearance.width = 300
        }
        
        let colorPicker = alert.addContentViewFromNib("ColorPicker", bundle: NSBundle.mainBundle()) as! HRColorPickerView
        colorPicker.color = self.backgroundColorButton.backgroundColor!
        colorPicker.addTarget(self, action: #selector(FormTemplateComposer.didPickBackgroundColor(_:)), forControlEvents: UIControlEvents.ValueChanged)
        alert.show()
    }
    
    @IBAction func pickForegroundColor() {
        PLModal.shared.blackoutColor = UIColor.clearColor()
        
        let alert = PLAlertViewController()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            alert.appearance.width = 400
        } else {
            alert.appearance.width = 300
        }
        
        let colorPicker = alert.addContentViewFromNib("ColorPicker", bundle: NSBundle.mainBundle()) as! HRColorPickerView
        colorPicker.color = self.foregroundColorButton.backgroundColor!
        colorPicker.addTarget(self, action: #selector(FormTemplateComposer.didPickForegroundColor(_:)), forControlEvents: UIControlEvents.ValueChanged)
        alert.show()
    }
    
    func didPickBackgroundColor(picker: HRColorPickerView) {
        let color = picker.color
        
        self.backgroundColorButton.backgroundColor = color
        self.template = FormTemplate(backgroundColor: color,
            foregroundTemplate: self.template.foregroundTemplate,
            foregroundColor: self.template.foregroundColor, withShadow: self.template.withShadow)
    }
    
    func didPickForegroundColor(picker: HRColorPickerView) {
        let color = picker.color
        self.foregroundColorButton.backgroundColor = color
        self.template = FormTemplate(backgroundColor: self.template.backgroundColor,
            foregroundTemplate: self.template.foregroundTemplate,
            foregroundColor: color, withShadow: self.template.withShadow)
    }
}
