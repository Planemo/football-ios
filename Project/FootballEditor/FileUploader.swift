//
//  FileUploader.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Alamofire

class FileUploader: NSObject {
    func uploadFileToFolder(folder: String, fileExtension: String, data: NSData) -> SignalProducer<String, NSError> {
        return self.uploadFileToFolder(folder, name: nil, fileExtension: fileExtension, data: data)
    }
    
    func uploadFileToFolder(folder: String, name: String?, fileExtension: String, data: NSData) -> SignalProducer<String, NSError> {
        var url = "http://api.planemostd.com/football/api/content_editor.php?folder=\(folder)&extension=\(fileExtension)&method=upload_file"
        if let name = name {
            url += "&name=\(name)"
        }
        let escapedUrl = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let (signal, observer) = SignalProducer<String, NSError>.buffer(1)
        Alamofire.upload(Method.POST,  escapedUrl, data: data)
            .validate(statusCode: 200..<300)
            .responseString(encoding: NSUTF8StringEncoding) { response in
                if let error = response.result.error {
                    observer.sendFailed(error)
                    
                } else if let string = response.result.value {
                    observer.sendNext(string)
                    observer.sendCompleted()
                    
                } else {
                    let error = NSError(domain: "FileUploader", code: 438, userInfo: nil)
                    observer.sendFailed(error)
                }
        }
        return signal
    }
}
