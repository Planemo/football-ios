//
//  ViewController.swift
//  FootballEditor
//
//  Created by Mikhail Shulepov on 27/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    private var eventSubmittingFlow: EventSubmittingFlow?
    private var matchSubmittingFlow: MatchSubmittingFlow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func startNewEventSubmitting() {
        self.eventSubmittingFlow = EventSubmittingFlow(navigationController: self.navigationController!)
        self.eventSubmittingFlow?.startFlow()
    }
    
    @IBAction func startNewMatchSubmitting() {
        self.matchSubmittingFlow = MatchSubmittingFlow(navController: self.navigationController!)
        self.matchSubmittingFlow?.startFlow()
    }
    
    @IBAction func markMatchFinished() {
        self.eventSubmittingFlow = EventSubmittingFlow(navigationController: self.navigationController!)
        self.eventSubmittingFlow?.action = .Finish
        self.eventSubmittingFlow?.startFlow()
    }
    
    func stopBetsAccepting() {
        self.eventSubmittingFlow = EventSubmittingFlow(navigationController: self.navigationController!)
        self.eventSubmittingFlow?.action = .StopBetsAccepting
        self.eventSubmittingFlow?.startFlow()
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0): self.startNewMatchSubmitting()
        case (0, 1): self.startNewEventSubmitting()
        case (0, 2): self.markMatchFinished()
        case (0, 3): self.stopBetsAccepting()
            
        case (1, 0): break //competition via storyboard
        case (1, 1): break //new team
        case (1, 2): break //new player via storyboard
            
        case (2, 0): break //notification send via storyboard
            
        default: break
        }
    }
   

}

