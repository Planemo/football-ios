//
//  ImageSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

@objc protocol ImageSelectorDelegate {
    func imageSelectorDidPickImage(image: UIImage)
}

class ImageSelectorViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var imageView: UIImageView!
    weak var delegate: ImageSelectorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clearColor()
        
        self.imageView = UIImageView(frame: self.view.bounds)
        self.imageView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.imageView.image = UIImage(named: "ImageSelectionPlaceholder")
        self.view.addSubview(self.imageView)
        
        self.imageView.layer.borderWidth = 2
        self.imageView.layer.borderColor = UIColor.whiteColor().CGColor
        self.imageView.layer.cornerRadius = 5
      
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ImageSelectorViewController.didTap))
        self.imageView.userInteractionEnabled = true
        self.imageView.addGestureRecognizer(tapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didTap() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageView.image = image
        
        self.delegate?.imageSelectorDidPickImage(image)
    }
}
