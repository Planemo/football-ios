//
//  DateTimeSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveCocoa

public protocol MatchMeetupSelector {
    weak var selectionDelegate: MatchMeetupSelectorDelegate? { get set }
}

public protocol MatchMeetupSelectorDelegate: class {
    func didPickDateTime(date: NSDate, stadium: String?, selector: MatchMeetupSelector)
}

class TimeZonePickerDataSource: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    private let regionNames: [String]
    private let cityNames: [String: [String]]
    
    var selectionHandler: ((NSTimeZone) -> ())? = nil
    
    override init() {
        let knownTimeZoneNames = NSTimeZone.knownTimeZoneNames()
        let regionCityPairs = knownTimeZoneNames.map { name -> (String, String) in
            let components = name.componentsSeparatedByString("/")
            if components.count < 2 {
                return (name, name)
            }
            return (components[0], components[1])
        }
        
        self.regionNames = Array(Set(regionCityPairs.map { (region, _) -> String in
            return region
        })).sort {$0 < $1}
        
        var cityNames = regionCityPairs.groupBy{ (region, _) -> String in
            return region
        }.mapValues { (_, regionCityPairs: [(String, String)]) -> [String] in
            return regionCityPairs.map { _, city -> String in
                return city
            }
        }
        cityNames["GMT"] = TimeZonePickerDataSource.possibleGmtOffsets()
        self.cityNames = cityNames
        
        super.init()
    }
    
    private class func possibleGmtOffsets() -> [String] {
        return ["-12:00", "-11:00", "-10:00", "-09:30", "-09:00", "-08:00", "07:00", "-06:00", "-05:00",
        "-04:30", "-04:00", "-03:30", "-03:00", "-02:00", "-01:00", "+00:00", "+01:00", "+02:00",
        "+03:00", "+03:30", "+04:00", "+04:30", "+05:00", "+05:30", "+05:45", "+06:00", "+06:30",
            "+07:00", "+08:00", "+08:45", "+09:00", "+09:30", "+10:00", "+10:30", "+11:00", "+11:30",
        "+12:00", "+12:45", "+13:00", "+14:00"]
    }
    
    private func cityNamesForPicker(pickerView: UIPickerView) -> [String] {
        let selectedRegion = self.regionNames[pickerView.selectedRowInComponent(0)]
        return self.cityNames[selectedRegion]!
    }
    
    private func cityNameForPicker(pickerView: UIPickerView, row: Int) -> String {
        return self.cityNamesForPicker(pickerView)[row] ?? "-"
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return self.regionNames.count
        case 1:
            return self.cityNamesForPicker(pickerView).count
        default:
            return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return self.regionNames[row]
        case 1:
            return self.cityNameForPicker(pickerView, row: row)
        default:
            return nil
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            pickerView.reloadComponent(1)
        }
        
        let region = self.regionNames[pickerView.selectedRowInComponent(0)]
        let city = self.cityNameForPicker(pickerView, row: pickerView.selectedRowInComponent(1))
        let tzName: String
        if city.isEmpty {
            tzName = region
        } else if region == "GMT" {
            tzName = "GMT\(city)"
        } else {
            tzName = "\(region)/\(city)"
        }
        if let timezone = NSTimeZone(name: tzName) {
            self.selectionHandler?(timezone)
        }
    }
}

class MatchMeetupSelectorViewController: UIViewController, MatchMeetupSelector {
    weak var selectionDelegate: MatchMeetupSelectorDelegate? = nil
    
    @IBOutlet var dateField: UITextField!
    @IBOutlet var timeField: UITextField!
    @IBOutlet var timeZoneField: UITextField!
    @IBOutlet var stadiumField: UITextField!
    
    private var dateFormatter: NSDateFormatter!
    private var timeFormatter: NSDateFormatter!
    private var timeZoneFormatter: NSDateFormatter!
    private var debugDateFormatter: NSDateFormatter!
    
    private var datePicker: UIDatePicker!
    private var timePicker: UIDatePicker!
    private var timeZoneDataSource: TimeZonePickerDataSource!
    private var stadiumPicker: ValuesPickerSelection!
    
    private var timeZone: NSTimeZone! {
        didSet {
            self.timeZoneFormatter.timeZone = self.timeZone
            self.timeZoneField.text = self.timeZoneFormatter.stringFromDate(NSDate())
            
            self.debugDateFormatter.timeZone = self.timeZone
            
            self.printCurrentDate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        self.dateFormatter.dateFormat = "MMMM dd',' yyyy"
        
        self.timeFormatter = NSDateFormatter()
        self.timeFormatter.dateFormat = "HH:mm"
        
        self.timeZoneFormatter = NSDateFormatter()
        self.timeZoneFormatter.dateFormat = "ZZZZ"
        
        self.debugDateFormatter = NSDateFormatter()
        self.debugDateFormatter.dateFormat = "MMM dd YYY' at 'HH:mm ZZZZ"
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(MatchMeetupSelectorViewController.onDatePickerDateSelected(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.datePicker = datePicker
        self.dateField.inputView = datePicker
        self.onDatePickerDateSelected(datePicker)
        
        let timePicker = UIDatePicker()
        timePicker.datePickerMode = UIDatePickerMode.Time
        timePicker.minuteInterval = 5
        timePicker.locale = NSLocale(localeIdentifier: "ru_RU")
        timePicker.addTarget(self, action: #selector(MatchMeetupSelectorViewController.onDatePickerTimeSelected(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.timePicker = timePicker
        self.timeField.inputView = timePicker
        self.onDatePickerTimeSelected(timePicker)
        
        let timeZonePicker = UIPickerView()
        self.timeZoneDataSource = TimeZonePickerDataSource()
        self.timeZoneDataSource.selectionHandler = { [weak self] timezone in
            self?.timeZone = timezone
        }
        timeZonePicker.dataSource = self.timeZoneDataSource
        timeZonePicker.delegate = self.timeZoneDataSource
        self.timeZoneField.inputView = timeZonePicker
        
        self.timeZone = NSTimeZone.localTimeZone()
        
        self.stadiumPicker = ValuesPickerSelection()
        self.stadiumPicker.selectionCallback = { [weak self] value in
            if let strongSelf = self {
                strongSelf.stadiumField.inputView = nil
                strongSelf.stadiumField.text = value
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onDatePickerDateSelected(datePicker: UIDatePicker) {
        self.dateField.text = self.dateFormatter.stringFromDate(datePicker.date)
        self.printCurrentDate()
    }
    
    func onDatePickerTimeSelected(timePicker: UIDatePicker) {
        self.timeField.text = self.timeFormatter.stringFromDate(timePicker.date)
        self.printCurrentDate()
    }
    
    func onStadiumPickerSelectedStadium(stadiumPicker: UIPickerView) {

    }
    
    @IBAction func complete() {
        self.selectionDelegate?.didPickDateTime(self.date, stadium: self.stadiumField.text, selector: self)
    }
    
    private func printCurrentDate() {
        //NSLog("%@", self.debugDateFormatter.stringFromDate(self.date))
    }
    
    var date: NSDate {
        if self.isViewLoaded() {
            let date = self.datePicker.date
            let time = self.timePicker.date
            
            let dateComponents = NSDateComponents()
            
            dateComponents.calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
            dateComponents.era = date.getComponent(NSCalendarUnit.Era)
            dateComponents.year = date.getComponent(NSCalendarUnit.Year)
            dateComponents.month = date.getComponent(NSCalendarUnit.Month)
            dateComponents.day = date.getComponent(NSCalendarUnit.Day)
            
            dateComponents.hour = time.getComponent(NSCalendarUnit.Hour)
            dateComponents.minute = time.getComponent(NSCalendarUnit.Minute)
            
            dateComponents.timeZone = self.timeZone
            
            return dateComponents.date!
        }
        return NSDate()
    }
    
    @IBAction
    func selectExistingStadium() {
        if self.stadiumField.inputView != nil {
            switchInputView(self.stadiumField, inputView: nil)
            return
        }
        if !self.stadiumPicker.values.isEmpty {
            self.switchInputView(self.stadiumField, inputView: self.stadiumPicker)
            return
        }
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        DataProvider.activeProvider.requestStadiums().observeOn(UIScheduler()).start(next: {stadiums in
            NSLog("Stadiums: %@", stadiums)
            hud.hide(true)
            self.stadiumPicker.values = stadiums
            self.switchInputView(self.stadiumField, inputView: self.stadiumPicker)
            
        }, failed: { error in
            NSLog("Error: %@", error.description)
            hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
        })
    }
    
    func switchInputView(textField: UITextField, inputView: UIView?) {
        if textField.isFirstResponder() {
            textField.resignFirstResponder()
        }
        textField.inputView = inputView
        textField.becomeFirstResponder()
    }
}
