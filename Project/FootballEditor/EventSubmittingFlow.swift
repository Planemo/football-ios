//
//  EventSubmittingFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 13/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Alamofire
import MBProgressHUD
import PLModal
import PLSupport

class EventSubmittingFlow: NSObject {
    enum Action {
        case AddEvent
        case Finish
        case StopBetsAccepting
    }
    
    private let dataProvider = DataProvider.activeProvider
    private let navController: UINavigationController
    
    private var eventBuildingFlow: NavigationBasedBettingBuilder?
    
    var action = Action.AddEvent
    
    init(navigationController: UINavigationController) {
        self.navController = navigationController
        super.init()
    }
    
    func startFlow() {
        let hud = MBProgressHUD.showHUDAddedTo(self.navController.view, animated: true)
        
        let filters: [MatchFilter]
        switch self.action {
        case .AddEvent:
            fallthrough
        case .Finish:
            filters = [.Finished(false)]
        case .StopBetsAccepting:
            filters = [.BetsAcceptable]
        }
        
        self.dataProvider.requestMatchInfos(filters)
            .observeOn(UIScheduler())
            .start(next: { matches in
                hud.hide(true)
                self.chooseMatch(matches)
                
            }, failed: { error in
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
            })
    }
    
    private func chooseMatch(matches: [Game]) {
        let matchesVC = Storyboards.Main.instantiateMatchesViewController()
        matchesVC.title = NSLocalizedString("Choose Match", comment: "Choose a match")
        matchesVC.showFullDate = true
        matchesVC.selectionDelegate = self
        matchesVC.groupingStyle = .ByCompetition
        matchesVC.matches = matches
        self.navController.pushViewController(matchesVC, animated: true)
    }
    
    private func startEventBuilding(game: Game) {
        self.eventBuildingFlow = NavigationBasedBettingBuilder(navController: self.navController, contentEditor: true)
        self.eventBuildingFlow?.delegate = self
        self.eventBuildingFlow?.startBuilding(game)
    }
    
    private func submitEvents(events: [Event], forMatch match: Game) -> SignalProducer<String, NSError> {
        if let data = JsonEventParser().serializeWithExternalQualifications(events, atMatch: match) {
            return SignalProducer { observer, compositeDisposable in
                let url = "http://api.planemostd.com/football/api/events_submitter.php"
                let request = NSMutableURLRequest(URL: NSURL(string: url)!)
                request.HTTPBody = data
                request.HTTPMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                Alamofire.request(request).responseString(encoding: nil) { response -> Void in
                    if let error = response.result.error {
                        observer.sendFailed(error)
                    } else if let string = response.result.value {
                        observer.sendNext(string)
                        observer.sendCompleted()
                    } else {
                        let error = NSError(domain: "NoResponse", code: 3748, userInfo: nil)
                        observer.sendFailed(error)
                    }
                }
            }
        } else {
            let error = NSError(domain: "EventSubmittingError", code: 3848, userInfo: nil)
            return SignalProducer(error: error)
        }
    }
}

extension EventSubmittingFlow: MatchSelectorDelegate {
    func didSelectMatch(match: Game, selector: MatchSelector) {
        switch self.action {
        case .AddEvent: self.startEventBuilding(match)
        case .Finish:
            EventSubmittingFlow.markMatchFinished(match) { result in
                if result {
                    var matches = selector.matches
                    if let idx = matches.indexOf(match) {
                        matches.removeAtIndex(idx)
                    }
                    selector.matches = matches
                }
            }
        case .StopBetsAccepting:
            EventSubmittingFlow.stopMatchBetsAccepting(match) { result in
                if result {
                    var matches = selector.matches
                    if let idx = matches.indexOf(match) {
                        matches.removeAtIndex(idx)
                    }
                    selector.matches = matches
                }
            }
        }

    }
}

extension EventSubmittingFlow: BettingBuilderDelegate {
    func willStartBuildComponent(component: AbstractBettingBuildComponent, withViewController: UIViewController) {
        //nothing to do
    }
    
    func onBettingBuildCompleted(bettingType: BettingType, data: BettingBuildData) {
        if data.event == nil {
            return
        }
        
        let alert = PLAlertViewController()
        let actionCreate = AlertAction(title: "Submit") {
            let hud = MBProgressHUD.showHUDAddedTo(self.navController.view, animated: true)
            hud.labelText = NSLocalizedString("Submitting Process Title", comment: "Submitting...")
            self.submitEvents(data.game.events, forMatch: data.game)
                .observeOn(UIScheduler())
                .start( next: { result in
                    NSLog("Submitting result: %@", result)
                }, failed: { error in
                    NSLog("Submitting error: %@", error.description)
                    hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
                }, completed: {
                    hud.hide(true)
                    self.finishIfLast(data.game)
                })
        }
        let actionCancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Submit", message: "Are you sure you want to submit new events?", actions: actionCreate, actionCancel)
    }
    
    func finishIfLast(match: Game) {
        let alert = PLAlertViewController()
        alert.title = NSLocalizedString("Last Event Confirmation Title", comment: "Last event?")
        alert.addMessage(NSLocalizedString("Last Event Confirmation Message", comment: "Last event?"))
        alert.addAction(AlertAction(title: NSLocalizedString("Last Event Action Yes", comment: "YES")) {
            self.markMatchFinished(match)
        })
        alert.addAction(AlertAction(title: NSLocalizedString("Last Event Action No", comment: "NO")) {
            self.eventBuildingFlow?.close(true)
        })
        alert.show()
    }
    
    func markMatchFinished(match: Game) {
        EventSubmittingFlow.markMatchFinished(match) { result in
            self.eventBuildingFlow?.close(true)
        }
    }
    
    static func markMatchFinished(match: Game, completion: (Bool)->()) {
        let alert = PLAlertViewController()
        let finishMatch = AlertAction(title: "Finish it") {
            let hud = MBProgressHUD.showGlobalHUD()
            hud.labelText = NSLocalizedString("Finishing Match Process Title", comment: "Finishing match...")
            let url = "http://api.planemostd.com/football/api/event_submitter_finished.php"
            Alamofire.request(.POST, url, parameters: ["match_id": "\(match.uid!)"], encoding: .JSON).responseString { response -> Void in
                if response.result.error != nil {
                    hud.hideWithErrorStatus(NSLocalizedString("Finishing Match Error", comment: "Error"))
                    completion(false)
                } else {
                    hud.hideWithSuccessStatus(NSLocalizedString("Finishing Match Success", comment: "Succeess"))
                    completion(true)
                }
            }
        }
        let cancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Finish this match?",
            message: "\(match.gameDate)\n\(match.homeTeam.name) vs \(match.awayTeam.name)\nAll events added to this match?",
            actions: finishMatch, cancel)
    }
    
    
    static func stopMatchBetsAccepting(match: Game, completion: (Bool)->()) {
        let alert = PLAlertViewController()
        let stopMatch = AlertAction(title: "Stop Match") {
            let hud = MBProgressHUD.showGlobalHUD()
            hud.labelText = NSLocalizedString("Finishing Match Process Title", comment: "Finishing match...")
            let url = "http://api.planemostd.com/football/api/betting_manager.php"
            let params = ["method": "stop_bets_accepting", "match_id": "\(match.uid!)"]
            Alamofire.request(.POST, url, parameters: params, encoding: .JSON).responseString { response -> Void in
                if response.result.error != nil {
                    hud.hideWithErrorStatus(NSLocalizedString("Stopping Bets Accepting", comment: "Error"))
                    completion(false)
                } else {
                    hud.hideWithSuccessStatus(NSLocalizedString("Bets Accepting Stopped", comment: "Success"))
                    completion(true)
                }
                
            }
        }
        let cancel = AlertAction(title: "Cancel")
        alert.showWithTitle("Stop match?",
            message: "\(match.gameDate)\n\(match.homeTeam.name) vs \(match.awayTeam.name)",
            actions: stopMatch, cancel)
    }
    
}
