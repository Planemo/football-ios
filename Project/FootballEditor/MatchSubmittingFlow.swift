//
//  MatchSubmittingFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 28/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Alamofire
import MBProgressHUD
import PLModal
import PLSupport

class MatchSubmittingFlow: NSObject {
    private let dataProvider = DataProvider.activeProvider
    private let navController: UINavigationController
    private var initialViewController: UIViewController?
    
    private var date: NSDate?
    private var stadium: String?
    private var competition: Competition?
    private var teams = [Team]()
    
    init(navController: UINavigationController) {
        self.navController = navController
    }
    
    func startFlow() {
        self.initialViewController = self.navController.topViewController
        self.startDateTimeSelection()
    }
    
    private func startDateTimeSelection() {
        let dateTimeSelector = Storyboards.MainEditor.instantiateMatchMeetupSelectorViewController()
        dateTimeSelector.selectionDelegate = self
        self.navController.pushViewController(dateTimeSelector, animated: true)
    }
    
    private func startCompetitionSelection() {
        let competitionSelector = Storyboards.Main.instantiateCompetitionsViewController()
        competitionSelector.selectionDelegate = self
        self.navController.pushViewController(competitionSelector, animated: true)
    }
    
    private func startTeamsSelection(teams: [Team]) {
        let teamsVC = Storyboards.Main.instantiateTeamsCollectionViewSelector()
        teamsVC.selectionDelegate = self
        teamsVC.teams = teams
        teamsVC.allowsMultipleSelection = true
        self.navController.pushViewController(teamsVC, animated: true)
    }
    
    private func startTeamsSelection() {
        let hud = MBProgressHUD.showHUDAddedTo(self.navController.view, animated: true)
        self.dataProvider.requestShallowTeamsInfo(filter: TeamsFilter.None)
            .observeOn(UIScheduler())
            .start(next: { teams in
                hud.hide(true)
                self.startTeamsSelection(teams)
            }, failed: { error in
                hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
            })
    }
    
    private func askSubmit() {
        let alert = PLAlertViewController()
        let message = "\(self.date!.description)\n\(self.competition!.name)\n\(self.teams[0].name) vs \(self.teams[1].name)"
        let cancelAction = AlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"), style: .Cancel)
        let submitAction = AlertAction(title: NSLocalizedString("Submit", comment: "Submit")) {
            self.submit()
        }
        alert.showWithTitle(NSLocalizedString("Submit?", comment: "Submit?"), message: message, actions: [cancelAction, submitAction])
    }
    
    private func submit() {
        let match = Game(homeTeam: self.teams[0], awayTeam: self.teams[1], date: self.date!)
        match.competition = self.competition
        match.venue = self.stadium
        
        let data = JsonMatchParser().serialize(match)
        let submitter = SignalProducer<String, NSError> { observer, compositeDisposable in
            let url = "http://api.planemostd.com/football/api/match_submitter.php"
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPBody = data
            request.HTTPMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            Alamofire.request(request).responseString(encoding: nil) { response -> Void in
                if let error = response.result.error {
                    observer.sendFailed(error)
                } else if let string = response.result.value {
                    observer.sendNext(string)
                    observer.sendCompleted()
                } else {
                    let error = NSError(domain: "NoResponse", code: 3748, userInfo: nil)
                    observer.sendFailed(error)
                }
            }
        }
        let hud = MBProgressHUD.showHUDAddedTo(self.navController.view, animated: true)
        submitter.start(next: { response in
            hud.hide(true)
            self.didFinish()
            NSLog("%@", response)
        }, failed: { error in
            hud.hideWithErrorStatus(NSLocalizedString("Error", comment: "Error"))
        })
    }
    
    private func didFinish() {
        if let initialVC = self.initialViewController {
            self.navController.popToViewController(initialVC, animated: true)
        } else {
            self.navController.popToRootViewControllerAnimated(true)
        }
    }
}

extension MatchSubmittingFlow: TeamsSelectionDelegate {
    func didSelectTeams(teams: [Team], selector: TeamsSelector) {
        if teams.count == 2 {
            self.teams = teams
            self.askSubmit()
        }
    }
}

extension MatchSubmittingFlow: CompetitionSelectionDelegate {
    func didSelectCompetition(competition: Competition, selector: CompetitionSelector) {
        self.competition = competition
        self.startTeamsSelection()
    }
}

extension MatchSubmittingFlow: MatchMeetupSelectorDelegate {
    func didPickDateTime(date: NSDate, stadium: String?, selector: MatchMeetupSelector) {
        self.date = date
        self.stadium = stadium
        self.startCompetitionSelection()
    }
}
