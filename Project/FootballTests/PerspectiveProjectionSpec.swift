import Quick
import Nimble

class PerspectiveProjectionSpec: QuickSpec {
    override func spec() {
        let size = CGSize(width: 100, height: 100)
        //  1/-------\2
        // 3/_________\4
        let originalCorners = [
            CGPoint(x: 0, y: 0),
            CGPoint(x: 100, y: 0),
            CGPoint(x: 0, y: 100),
            CGPoint(x: 100, y: 100)
        ]
        let perspectiveCorners = [
            CGPoint(x: 100, y: 0),
            CGPoint(x: 300, y: 0),
            CGPoint(x: 0, y: 150),
            CGPoint(x: 400, y: 150)
        ]

        let perspectiveCornerValues = perspectiveCorners.map { point -> NSValue in
            return NSValue(CGPoint: point)
        }
        
        let projection = PerspectiveProjection(originalSize: size, transformedCorners: perspectiveCornerValues)
        
        describe("Original -> Perspective") {
            let originalTransformedCorners = originalCorners.map { originalCorner -> CGPoint in
                return projection.projectOriginalPoint(originalCorner)
            }
            for (originalTransformedCorner, perspectiveCorner) in Zip2(originalTransformedCorners, perspectiveCorners) {
                expect(originalTransformedCorner.x).to(beCloseTo(perspectiveCorner.x, within: 3))
                expect(originalTransformedCorner.y).to(beCloseTo(perspectiveCorner.y, within: 3))
            }
        }
        
        describe("Perspective -> Original") {
            let perspectiveTransformedCorners = perspectiveCorners.map { perspectiveCorner -> CGPoint in
                return projection.perspectivePointToOriginal(perspectiveCorner)
            }
            for (perspectiveTransformedCorner, originalCorner) in Zip2(perspectiveTransformedCorners, originalCorners) {
                expect(perspectiveTransformedCorner.x).to(beCloseTo(originalCorner.x, within: 3))
                expect(perspectiveTransformedCorner.y).to(beCloseTo(originalCorner.y, within: 3))
            }
        }
    }
}
