//
//  EventTypeCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 26/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class EventTypeCell: UICollectionViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    fileprivate var titleFont: UIFont!
    
    var eventType: BettingType! {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleFont = self.titleLabel.font
    }
    
    fileprivate func setup() {
        let title: NSAttributedString
        let icon: UIImage?
        switch self.eventType! {
        case .goal:
            title = self.createTitle("Goal")
            icon = UIImage(named: "icEventGoal")
            
        case .goalWithAssist:
            title = self.createTitle("Goal", withSubtitle: "+ Assistance")
            icon = UIImage(named: "icEventGoalWithAssistance")
        case .goalWithDribbling: self.titleLabel.text = "Goal + Dribbling"
            title = self.createTitle("Goal", withSubtitle: "+ Dribbling")
            icon = UIImage(named: "icEventGoalWithDribbling")

            //        case .Winner: self.titleLabel.text = "Winner"
            //        case .Score: self.titleLabel.text = "Score"
        }
        self.titleLabel.attributedText = title
        self.iconView.image = icon
    }
    
    fileprivate func createTitle(_ title: String, withSubtitle subtitle: String? = nil) -> NSAttributedString {
        let titleAttrs = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: self.titleFont,
        ] as [String : Any]
        
        if let subtitle = subtitle {
            let paragraph = NSMutableParagraphStyle()
            paragraph.lineHeightMultiple = 0.8
            paragraph.lineSpacing = -10
            let commonAttrs = [NSParagraphStyleAttributeName: paragraph]
            let ret = NSMutableAttributedString(string: "\(title)\n\(subtitle)", attributes: commonAttrs)
            ret.addAttributes(titleAttrs, forSubstring: title)
            
            let subtitleAttrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: self.titleFont.withSize(16)
            ]
            ret.addAttributes(subtitleAttrs, forSubstring: subtitle)
            return ret
            
        } else {
            return NSMutableAttributedString(string: title, attributes: titleAttrs)
        }
    }
}
