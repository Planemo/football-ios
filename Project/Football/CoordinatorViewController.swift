//
//  RootViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 04/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class CoordinatorViewController: SWRevealViewController {
    @IBInspectable var closedLeftMenuWidth: CGFloat = 50
    @IBInspectable var leftMenuOverdraw: CGFloat = 70

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var backButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.panGestureRecognizer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.rearViewRevealWidth = self.closedLeftMenuWidth
        self.rearViewRevealOverdraw = self.leftMenuOverdraw
        self.frontViewPosition = FrontViewPosition.Right
        
    }
}
