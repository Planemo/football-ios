//
//  ShareButton.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class ShareButton: UIBarButtonItem {
    @IBOutlet var viewController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.target = self
        self.action = #selector(ShareButton.share)
        if self.viewController == nil {
            NSLog("Warning: ShareButton: IBOutlet for viewController not set")
        }
    }
    
    func share() {
        let shareText = NSLocalizedString("ShareMessage", comment: "share")
        let link = BuildConfig.shareURL
        let items: [Any] = [shareText, link]
        
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
        if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
            if let popover = activityVC.popoverPresentationController {
                popover.barButtonItem = self
            }
        }
        self.viewController.present(activityVC, animated: true, completion: nil)
    }
}
