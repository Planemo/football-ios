//
//  TitleCollectionHeaderView.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TitleCollectionHeaderView: UICollectionReusableView {
    @IBOutlet var titleLabel: UILabel!
}

class IconTitleCollectionHeaderView: TitleCollectionHeaderView {
    @IBOutlet var iconView: UIImageView!
}

class TitleTableHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet var titleLabel: UILabel!
}

class IconTitleTableHeaderFooterView: TitleTableHeaderFooterView {
    @IBOutlet var iconView: UIImageView!
}

enum RoundingStyle {
    case header
    case footer
    case none
}

extension UITableViewHeaderFooterView {
    func roundWithStyle(_ roundingStyle: RoundingStyle, cornerRadius: CGFloat, insets: UIEdgeInsets) -> RoundedView {
        var roundedView: RoundedView!
        if let roundedBackground = self.backgroundView as? RoundedView {
            roundedView = roundedBackground
        }
        roundedView = RoundedView()
        self.backgroundView = roundedView
        
        switch roundingStyle {
        case .header:
            roundedView.roundedCorners = [.topLeft, .topRight]
        case .footer:
            roundedView.roundedCorners = [.bottomLeft, .bottomRight]
        case .none:
            roundedView.roundedCorners = []
        }
        roundedView.insets = insets
        roundedView.cornerRadius = cornerRadius
        
        return roundedView
    }
}
