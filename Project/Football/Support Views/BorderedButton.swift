//
//  BorderedButton.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class BorderedButton: UIButton {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = (self.titleLabel?.textColor ?? UIColor.white).cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
    }
}
