//
//  PLDatePicker.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public protocol DateTimeSelector {
    weak var dateTimeSelectionDelegate: DateTimeSelectorDelegate? { get set }
}

public protocol DateTimeSelectorDelegate: class {
    func didPickDateTime(_ date: Date, selector: DateTimeSelector)
}

open class PLDatePickerDayCell: UICollectionViewCell {
    @IBOutlet var dayOfWeekLabel: UILabel!
    @IBOutlet var dayOfMonthLabel: UILabel!
    @IBOutlet var selectionFrame: UIView!
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.isSelected = false
    }
    
    override open var isSelected: Bool {
        didSet {
            let foregroundColor: UIColor
            if self.isSelected {
                foregroundColor = UIColor.white
            } else {
                foregroundColor = UIColor(white: 0.9, alpha: 1.0)
            }
            self.dayOfWeekLabel.textColor = foregroundColor
            self.dayOfMonthLabel.textColor = foregroundColor
            self.selectionFrame.alpha = self.isSelected ? 1 : 0
        }
    }
}

open class PLDatePickerMonthHeader: UICollectionReusableView {
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
}

open class PLDatePickerCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, DateTimeSelector {
    @IBInspectable open var cellReuseId: String = "DayCell"
    @IBInspectable open var headerReuseId: String = "MonthHeader"
    @IBInspectable open var selectionFrameImage: UIImage?

    open weak var dateTimeSelectionDelegate: DateTimeSelectorDelegate?
    fileprivate var selectionFrame: UIView!
    
    fileprivate let dayOfWeekFormatter = DateFormatter()
    fileprivate let dayOfMonthFormatter = DateFormatter()
    fileprivate let monthFormatter = DateFormatter()
    fileprivate let yearFormatter = DateFormatter()
    
    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    fileprivate func initialize() {
        self.dayOfWeekFormatter.dateFormat = "EEE"
        self.dayOfMonthFormatter.dateFormat = "dd"
        self.monthFormatter.dateFormat = "MMM"
        self.yearFormatter.dateFormat = "YYYY"
        self.dataSource = self
        self.delegate = self
    }
    
    open var dates = [Date]() {
        didSet {
            self.selectedDate = nil
            self.rebuildSections()
        }
    }
    
    fileprivate(set) open var selectedDate: Date? = nil {
        didSet {
            if let date = self.selectedDate {
                self.dateTimeSelectionDelegate?.didPickDateTime(date, selector: self)
            }
        }
    }
    
    fileprivate var sections: Sections<Date, Date> = Sections() {
        didSet {
            if self.dataSource != nil && self.delegate != nil {
                self.reloadData()
            }
        }
    }
    
    fileprivate func rebuildSections() {
        self.sections = Sections(self.dates.groupBy { date -> Int in
            return date.month
            
        }.mapValues { month, dates -> Section<Date, Date> in
            return Section(header: dates.first!, items: dates)
        
        }.values.sorted { lhs, rhs -> Bool in
            let firstLhsDate = lhs.items.first!
            let firstRhsDate = rhs.items.first!
            return firstLhsDate.isBefore(firstRhsDate)
        })
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections.numberOfItemsInSection(section)
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dateCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellReuseId, for: indexPath) as! PLDatePickerDayCell
        let date = self.sections[indexPath]
        dateCell.dayOfWeekLabel.text = self.dayOfWeekFormatter.string(from: date).uppercased()
        dateCell.dayOfMonthLabel.text = self.dayOfMonthFormatter.string(from: date)
        return dateCell
    }
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.numberOfSections()
    }
    
    open func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: self.headerReuseId, for: indexPath)
                as! PLDatePickerMonthHeader
            let monthDate = self.sections.headerForSection(indexPath.section)
            header.monthLabel.text = self.monthFormatter.string(from: monthDate).uppercased()
            header.yearLabel.text = self.yearFormatter.string(from: monthDate)
            return header
        }
        return UICollectionReusableView()
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedDate = self.sections[indexPath]
    }
    
    open func selectDate(_ date: Date) {
        let indexPath = self.sections.indexPathForItem(date) { lhs, rhs -> Bool in
            let sameDay = lhs.days == rhs.days
            let sameMonth = lhs.month == rhs.month
            let sameYear = lhs.year == rhs.year
            return sameDay && sameMonth && sameYear
        }
        if let indexPath = indexPath {
            self.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
            self.selectedDate = date
        }
    }
}

public extension PLDatePickerCollectionView {
    public func fillDatesFromDate(_ date: Date, numberOfDays: Int) {
        var daysComponent = DateComponents()
        daysComponent.day = numberOfDays
        let calendar = Calendar.current
        let toDate = (calendar as NSCalendar).date(byAdding: daysComponent, to: date, options: [])!
        self.fillDatesFromDate(date, toDate: toDate)
    }
    
    public func fillDatesFromDate(_ date: Date, toDate: Date) {
        var dates = [date]
        var daysComponent = DateComponents()
        let calendar = Calendar.current
        var daysOffset = 0
        while (true) {
            daysOffset += 1
            daysComponent.day = daysOffset
            let nextDate = (calendar as NSCalendar).date(byAdding: daysComponent, to: date, options: [])!
            let diff = (calendar as NSCalendar).components([.day], from: nextDate, to: toDate, options: [])
            if diff.day! <= -1 {
                break
            }
            dates.append(nextDate)
        }
        self.dates = dates
    }
}
