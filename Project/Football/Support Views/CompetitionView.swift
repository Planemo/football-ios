//
//  CompetitionView.swift
//  Football
//
//  Created by Mikhail Shulepov on 13/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage

protocol CompetitionView: class {
    var titleLabel: UILabel! { get }
    var iconView: UIImageView! { get }
}

class CompetitionViewModel: NSObject {
    func setup(_ view: CompetitionView, competition: Competition) {
        view.titleLabel.text = competition.name
        
        //FIXME: url for regions should be retrieved from server
        if let url = competition.logoURL {
            if url.isFileURL {
                view.iconView.image = UIImage(contentsOfFile: url.absoluteString)
            } else {
                view.iconView.sd_setImage(with: url as URL!)
            }
        } else {
            view.iconView.image = nil
        }
    }
}

extension IconTitleCollectionHeaderView: CompetitionView {
    func setupWithCompetition(_ competition: Competition) {
        CompetitionViewModel().setup(self, competition: competition)
    }
}
