//
//  BetAmountSettingView.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLSupport
import ReactiveSwift

class BetAmountSettingView: UIView {
    @IBOutlet var betAmountLabel: UILabel!
    @IBOutlet var betIncrementor: UIButton!
    @IBOutlet var betDecrementor: UIButton!
    @IBOutlet var totalChipsLabel: UILabel!
    
    fileprivate let audioManager = AudioManager.defaultManager
    
    var userChips: Property<Int> = Property(LocalUser.currentUser.value!.chips) {
        didSet {
            self.totalChipsLabel?.text = "/\(userChips.value)"
            self.betDidChange()
        }
    }
    
    var betAmount = BuildConfig.minChallengeBet {
        didSet {
            if betAmount < BuildConfig.minChallengeBet {
                betAmount = BuildConfig.minChallengeBet
            }
            if betAmount > userChips.value {
                betAmount = userChips.value
            }
            self.betDidChange()
        }
    }
    
    fileprivate let steps = [10, 50, 100]
    fileprivate var step = 10
    
    fileprivate var actionSchedulerDisposable: Disposable?
    
    deinit {
        self.actionSchedulerDisposable?.dispose()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.step = steps.first!
        self.betDidChange()
        self.totalChipsLabel.text = "/\(userChips.value)"
    }
    
    fileprivate func chooseStep(_ count: Int) {
        let i = min(steps.count - 1, count / 10)
        step = steps[i]
    }
    
    fileprivate func betDidChange() {
        self.betDecrementor?.isEnabled = self.betAmount > BuildConfig.minChallengeBet
        self.betIncrementor?.isEnabled = self.betAmount < self.userChips.value
        self.betAmountLabel?.text = "\(self.betAmount)"
    }
    
    func incrementBet() {
        self.betAmount = ((self.betAmount + step) / step) * step
        
        audioManager.playSound("ChipsActions.mp3")
        
        if let incrementEnabled = self.betIncrementor?.isEnabled, incrementEnabled == false {
            stopAdjustingBet()
        }
    }
    
    @IBAction func startIncrementingBet() {
        self.stopAdjustingBet()
        
        self.incrementBet()
        
        let scheduler = QueueScheduler.main
        let largeDelay = Date(timeIntervalSinceNow: 0.4)
        let interval = DispatchTimeInterval.milliseconds(120)
        var incrementCount = 0
        self.actionSchedulerDisposable = scheduler.schedule(after: largeDelay, interval: interval) { [weak self] in
            self?.chooseStep(incrementCount)
            self?.incrementBet()
            incrementCount += 1
        }
    }
    
    func decrementBet() {
        self.betAmount = ((self.betAmount - step) / step) * step
        audioManager.playSound("ChipsActions.mp3")
        
        if let decrementEnabled = self.betDecrementor?.isEnabled, decrementEnabled == false {
            stopAdjustingBet()
        }
    }
    
    @IBAction func startDecrementingBet() {
        self.stopAdjustingBet()
        
        let scheduler = QueueScheduler.main
        self.decrementBet()
        var decrementCount = 0
        let largeDelay = Date(timeIntervalSinceNow: 0.4)
        let interval = DispatchTimeInterval.milliseconds(120)
        self.actionSchedulerDisposable = scheduler.schedule(after: largeDelay, interval: interval) { [weak self] in
            self?.chooseStep(decrementCount)
            self?.decrementBet()
            decrementCount += 1
        }
    }
    
    @IBAction func stopAdjustingBet() {
        if let disposable = self.actionSchedulerDisposable {
            self.actionSchedulerDisposable = nil
            disposable.dispose()
        }
        step = steps.first!
    }
}
