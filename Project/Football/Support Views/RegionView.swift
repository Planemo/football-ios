//
//  RegionCollectionHeaderView.swift
//  Football
//
//  Created by Mikhail Shulepov on 06/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage

protocol IRegionView: class {
    var titleLabel: UILabel! { get }
    var iconView: UIImageView! { get }
}

class RegionViewModel: NSObject {
    func setup(_ view: IRegionView, region: String) {
        view.titleLabel.text = region
        
        //FIXME: url for regions should be retrieved from server
        let baseURL = URL(string: "http://api.planemostd.com/football/files/Countries/")!
        let escapedRegion = region.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlPathAllowed)!
        if let url = URL(string: "\(escapedRegion).png", relativeTo: baseURL) {
            if (url as NSURL).isFileReferenceURL() {
                view.iconView.image = UIImage(contentsOfFile: url.absoluteString)
            } else {
                view.iconView.sd_setImage(with: url)
            }
        } else {
            NSLog("can't create url for region: %@, escaped: %@", region, escapedRegion)
        }
    }
}

class RegionView: UIView, IRegionView {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var iconView: UIImageView!
    
    func setupWithRegion(_ region: String) {
        RegionViewModel().setup(self, region: region)
    }
}

extension IconTitleCollectionHeaderView: IRegionView {
    func setupWithRegion(_ region: String) {
        RegionViewModel().setup(self, region: region)
    }
}

extension IconTitleTableHeaderFooterView: IRegionView {
    func setupWithRegion(_ region: String) {
        RegionViewModel().setup(self, region: region)
    }
}
