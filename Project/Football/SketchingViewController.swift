//
//  SketchingViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 09/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage
import PLSupport

protocol SketchingDelegate {
    func onSketchingComplete(_ viewController: SketchingViewController, withEvent: Event)
}

class GameFieldViewController: UIViewController {
    @IBOutlet var fieldView: GameFieldView!
    
    var game: Game! {
        didSet {
            self.initialSetup()
        }
    }
    var event: Event! {
        didSet {
            self.initialSetup()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    fileprivate func initialSetup() {
        if self.game == nil || self.event == nil || !self.isViewLoaded {
            return
        }
        let fieldDefinition = FieldDefinition()
        fieldDefinition.image = UIImage(named: "sketch_football_field_night.jpg")
        /*fieldDefinition.corners = [
        CGPoint(x: 0.0556, y: 0.206),
        CGPoint(x: 0.211, y: 0.606),
        CGPoint(x: 0.785, y: 0.606),
        CGPoint(x: 0.943, y: 0.206)
        ]
        fieldDefinition.contentModeAspectFill = false*/
        
        fieldDefinition.image = UIImage(named: "PlayField")
        fieldDefinition.corners = [
            CGPoint(x: 0.001, y: 0.001),
            CGPoint(x: 0.002, y: 0.999),
            CGPoint(x: 0.998, y: 0.999),
            CGPoint(x: 0.999, y: 0.001)
        ]
        fieldDefinition.contentModeAspectFill = false
        
        if let period = self.event.time?.period, let footballPeriod = FootballGamePeriod(rawValue: period) {
            switch footballPeriod {
            case .secondHalf, .secondExtraTime:
                self.fieldView.isTeamsSwitched = true
            default:
                break
            }
        }
        self.fieldView.fieldDefinition = fieldDefinition
        self.fieldView.game = game
        self.fieldView.editEvent(self.event)
    }
    
    func showCorrectEvent(_ targetEvent: Event, challengerResult: Event?) {
        let sketchedViews = self.fieldView.viewsForEvent(self.event)
        let targetEventViews = self.fieldView.addEvent(targetEvent)
        let challengerEventViews = (challengerResult == nil) ? [UIView]() : self.fieldView.addEvent(challengerResult!)
        
        for view in targetEventViews {
            view.alpha = 0
            view.tintColor = UIColor.fromARGB(0xFF1F3A93)
        }
        
        for view in challengerEventViews {
            view.alpha = 0
            view.tintColor = UIColor.fromARGB(0xFFD64541)
        }
        
        UIView.animate(withDuration: 1.0, animations: {
            for view in targetEventViews {
                view.alpha = 1.0
            }
            for view in sketchedViews {
                view.alpha = 0.25
            }
            for view in challengerEventViews {
                view.alpha = 0.5
            }
        })
    }
}

class SketchingViewController: GameFieldViewController {
    @IBOutlet var teamNameLabel: UILabel!
    @IBOutlet var teamEmblemView: UIImageView!
    @IBOutlet var teamInfoHolder: UIView!
    
    var sketchingDelegate: SketchingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.teamInfoHolder.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 2))
        self.teamEmblemView.transform = CGAffineTransform(rotationAngle: -CGFloat(M_PI / 2))
        self.initialSketchingSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.clipsToBounds = false        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.clipsToBounds = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.clipsToBounds = true
    }
    
    fileprivate func initialSketchingSetup() {
        self.teamNameLabel.text = self.event.relatedTeam.name
        self.teamEmblemView.sd_setImage(with: self.event.relatedTeam.logoURL as URL!)
    }
    
    @IBAction
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction
    func complete() {
        self.sketchingDelegate?.onSketchingComplete(self, withEvent: self.event)
    }
    
    func disableInteraction() {
        self.navigationItem.rightBarButtonItems = []
        self.view.isUserInteractionEnabled = false
    }
        
    func takeFieldSnapshot() -> UIImage {
        return self.fieldView.takeSnapshot()
    }
}



