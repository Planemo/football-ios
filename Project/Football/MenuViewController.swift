//
//  ViewController.swift
//  Football
//
//  Created by Mihail Shulepov on 12/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SVProgressHUD

class MenuViewController: UITableViewController {
    private class MenuItem {
        let title: String
        let segue: String
        let icon: String
        
        init(title: String, icon: String, segue: String) {
            self.title = title
            self.icon = icon
            self.segue = segue
        }
    }
    
    private struct Segues {
        static let Challenge = "Challenge"
        static let Profile = "Profile"
    }
    
    private let items: [MenuItem] = [
        MenuItem(title: "Profile", icon: "icProfile", segue: "Profile"),
        MenuItem(title: "Matches", icon: "icMatches", segue: "Matches"),
        MenuItem(title: "Store", icon: "icStore", segue: "Store"),
        MenuItem(title: "Challenge", icon: "icChallenge", segue: Segues.Challenge),
        MenuItem(title: "Invite", icon: "icInvite", segue: "Invite"),
        MenuItem(title: "Settings", icon: "icSettings", segue: "Settings"),
        MenuItem(title: "Feedback", icon: "icFeedback", segue: "Feedback"),
        MenuItem(title: "About", icon: "icInfo", segue: "About")
    ]
    
    private var challengeNavigationManager = ChallengeMakingNavigationFlow()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.performSegueWithIdentifier(Segues.Challenge, sender: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if self.tableView.indexPathForSelectedRow() == nil {
            let indexPath = NSIndexPath(forRow: 3, inSection: 0)
            self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .Bottom)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MenuItemCell
        let item = self.items[indexPath.row]
        cell.titleLabel.text = NSLocalizedString(item.title, comment: "Title")
        cell.iconView.image = UIImage(named: item.icon)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.items[indexPath.row]
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        self.performSegueWithIdentifier(item.segue, sender: cell)
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        let item = self.items[indexPath.row]
        let existedSegues = ["Profile", "Challenge", "Store", "Live"]
        if !contains(existedSegues, item.segue) {
            SVProgressHUD.showInfoWithStatus("Will be ready soon")
            return false
        }
        return true
    }    
}

