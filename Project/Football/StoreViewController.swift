//
//  StoreCollectionViewController.swift
//  
//
//  Created by Maxim Shmotin on 17/02/15.
//
//

import UIKit
import MBProgressHUD
import PLSupport
import ReactiveSwift

class SizeInvalidateCollectionViewLayout: UICollectionViewFlowLayout {
    fileprivate var oldSize = CGSize.zero
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if newBounds.size != oldSize {
            oldSize = newBounds.size
            return true
        }
        return false
    }
}

class StoreViewController: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
    
    fileprivate var products = [IAProduct]()
    fileprivate let store = IAStoreHelper.defaultHelper
    
    deinit {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .pad {
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let oldItemSize = layout.itemSize
            layout.itemSize = CGSize(width: oldItemSize.width * 1.7, height: oldItemSize.height * 1.6)
        }
        if let navController = self.navigationController, navController.viewControllers.count <= 1 {
            let backButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(StoreViewController.close))
            self.navigationItem.leftBarButtonItem = backButton
        }
        self.requestProducts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func requestProducts() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.store.products()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                switch event {
                case .value(let products):
                    hud.hide(animated: true)
                    self?.setupWithProducts(products)
                case .failed(_):
                    hud.setErrorStatus(NSLocalizedString("Error", comment: ""))
                default:
                    break
                }
            }
    }
    
    fileprivate func setupWithProducts(_ products: [IAProduct]) {
        self.products = products
        self.collectionView.reloadData()
    }
    
    @IBAction
    func restorePurchases() {
        IARestorer().restoreInView(self.view).start()
    }

    @IBAction
    func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension StoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = self.products[indexPath.item]
        IAProductBuyer().purchase(product, fromView: self.view).start()
    }
}

extension StoreViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    //center items horizontally and vertically
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: NSInteger) -> UIEdgeInsets {
            let viewSize = collectionView.bounds.size
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let itemSize = flowLayout.itemSize
            let spacing = flowLayout.minimumInteritemSpacing
            let itemsCount = CGFloat(collectionView.numberOfItems(inSection: 0))
            let totalWidth = itemsCount * itemSize.width + (itemsCount - 1) * spacing
            let hinset = max(0, (viewSize.width - totalWidth) / 2)
            let vinset = (viewSize.height - itemSize.height) / 2
            return UIEdgeInsetsMake(vinset, hinset, vinset, hinset)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! IAProductCell
        cell.product = self.products[indexPath.item]
        return cell
    }
}

extension StoreViewController {
    static func presentModallyFromViewController(viewController vc: UIViewController) {
        let navController = Storyboards.Main.instantiateStyledNavigationController()
        let shop = Storyboards.FootballMain.instantiateStoreViewController()
        navController.viewControllers = [shop]
        //                navController.modalTransitionStyle = UIModalPresentationStyle.FormSheet
        navController.modalTransitionStyle = .crossDissolve
        vc.present(navController, animated: true, completion: nil)
    }
}
