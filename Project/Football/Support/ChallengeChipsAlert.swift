//
//  ChallengeChipsAlert.swift
//  Football
//
//  Created by Mikhail Shulepov on 31/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal
import PLSupport

class ChallengeChipsAlert: PLAlertViewController {

    func showFromViewController(_ vc: UIViewController) {
        let restoreDate = Date() //LocalUser.currentUser.value!.challengeChipsRestoreDate ?? Date()
        let delta = restoreDate.timeIntervalSinceNow
        
        let timeUnit = TimeUnit.seconds(delta)
        let hours = Int(floor(TimeUnit(from: timeUnit, type: .hour).value))
        let minutes = Int(floor(TimeUnit(from: timeUnit, type: .minute).value))
        
        NSLog("Restoration date: %@, Delay: %f, hours: %d, minutes: %d", restoreDate.description, delta, hours, minutes)
        
        self.title = "Not enough chips"
        self.addMessage(String(format: "All chips will be restored in %d hours %d minutes", hours, minutes))
        self.addAction(AlertAction(title: "OK"))
        self.show()
    }
}
