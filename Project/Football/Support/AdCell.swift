//
//  AdCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 23/06/16.
//  Copyright © 2016 Planemo. All rights reserved.
//

import Foundation
import UIKit
import FBAudienceNetwork

class AdInjectedSection {
    var adPlacements: [Int: FBNativeAd] = [:]
    
    func isAdPlacement(_ indexPath: IndexPath) -> Bool {
        return adPlacements[indexPath.row] != nil
    }
    
    func getAdjustedIndexPath(_ indexPath: IndexPath) -> IndexPath {
        let countBelow = adPlacements.countWhere { (idx, _) in
            return idx < indexPath.row
        }
        
        return IndexPath(row: indexPath.row - countBelow, section: indexPath.section)
    }
    
    func getNativeAd(_ indexPath: IndexPath) -> FBNativeAd? {
        return self.adPlacements[indexPath.row]
    }
    
    func didRemoveRow(_ row: Int) {
        let oldAdPlacements = adPlacements
        adPlacements = [:]
        for (key, value) in oldAdPlacements {
            if key > row {
                adPlacements[key - 1] = value
            } else {
                adPlacements[key] = value
            }
        }
    }
}

class CustomTableViewAdCell: UITableViewCell {
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel?
    @IBOutlet var socialContextLabel: UILabel?
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var subtitleLabel: UILabel?
    
    @IBOutlet var coverMediaView: FBMediaView?
    
    fileprivate var adChoicesView: FBAdChoicesView!
    
    override func awakeFromNib() {
        self.iconImageView.layer.cornerRadius = 5
        self.iconImageView.layer.shouldRasterize = true
        self.iconImageView.layer.rasterizationScale = self.iconImageView.contentScaleFactor
        self.iconImageView.layer.masksToBounds = true
    }
    
    func setupWithNativeAd(_ nativeAd: FBNativeAd, viewController: UIViewController) {
        self.titleLabel.text = nativeAd.title
        self.bodyLabel?.text = nativeAd.body
        self.actionButton.setTitle(nativeAd.callToAction, for: .normal)
        
        self.socialContextLabel?.text = nativeAd.socialContext
        
        nativeAd.icon?.loadAsync{ [weak self] image in
            self?.iconImageView.image = image
        }
        
        self.coverMediaView?.nativeAd = nativeAd
        
        if let ad = adChoicesView?.nativeAd, ad != nativeAd {
            adChoicesView.removeFromSuperview()
            adChoicesView = nil
        }
        
        if adChoicesView == nil {
            adChoicesView = FBAdChoicesView(nativeAd: nativeAd, expandable: false)
            addSubview(adChoicesView)
            adChoicesView.updateFrameFromSuperview()
        }
        
        nativeAd.registerView(forInteraction: self, with: viewController)
    }
    
    override var frame: CGRect {
        didSet {
            if let adChoicesView = self.adChoicesView {
                adChoicesView.updateFrameFromSuperview()
            }
        }
    }
}


class TableViewAdCell: UITableViewCell {
    fileprivate var nativeAd: FBNativeAd?
    fileprivate var nativeAdView: FBNativeAdView!
    
    override var frame: CGRect {
        didSet {
            if let nativeAdView = self.nativeAdView {
                nativeAdView.frame = self.bounds
            }
        }
    }
    
    func setupWithNativeAd(_ nativeAd: FBNativeAd, type: FBNativeAdViewType, viewController: UIViewController) {
        if let selfNativeAd = self.nativeAdView, selfNativeAd == nativeAd {
            return
        }
        
        self.nativeAd = nativeAd
        
        if nativeAdView != nil {
            nativeAdView.removeFromSuperview()
        }
        
        nativeAdView = FBNativeAdView(nativeAd: nativeAd, with: type)
        nativeAdView.frame = self.bounds
        nativeAdView.viewController = viewController
        nativeAdView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, .flexibleHeight]
        self.contentView.addSubview(nativeAdView)
        
        nativeAd.registerView(forInteraction: self, with: viewController)
    }
}

class CustomCollectionViewAdCell: UICollectionViewCell {
    @IBOutlet var iconImageView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var bodyLabel: UILabel?
    @IBOutlet var socialContextLabel: UILabel?
    @IBOutlet var actionButton: UIButton?
    @IBOutlet var subtitleLabel: UILabel?
    @IBOutlet var callToActionLabel: UILabel?
    
    @IBOutlet var coverMediaView: FBMediaView?
    
    fileprivate var adChoicesView: FBAdChoicesView!
    
    override var frame: CGRect {
        didSet {
            if let adChoicesView = self.adChoicesView {
                adChoicesView.updateFrameFromSuperview()
            }
        }
    }
    
    override func awakeFromNib() {
        if let iconImageView = self.iconImageView {
            iconImageView.layer.cornerRadius = 5
            iconImageView.layer.shouldRasterize = true
            iconImageView.layer.rasterizationScale = iconImageView.contentScaleFactor
            iconImageView.layer.masksToBounds = true
        }
        
        self.contentView.layer.cornerRadius = 5.0
        self.contentView.layer.borderColor = UIColor.white.cgColor
        self.contentView.layer.borderWidth = 2.0
        self.contentView.layer.masksToBounds = true
        self.contentView.backgroundColor = UIColor.white
        self.backgroundColor = UIColor.clear
    }
    
    func setupWithNativeAd(_ nativeAd: FBNativeAd, viewController: UIViewController) {
        self.titleLabel?.text = nativeAd.title
        self.bodyLabel?.text = nativeAd.body
        self.actionButton?.setTitle(nativeAd.callToAction, for: .normal)
        
        self.socialContextLabel?.text = nativeAd.socialContext
        
        nativeAd.icon?.loadAsync{ [weak self] image in
            self?.iconImageView?.image = image
        }
        
        self.coverMediaView?.nativeAd = nativeAd
        self.coverMediaView?.isAutoplayEnabled = false
        self.callToActionLabel?.text = nativeAd.callToAction
        
        if let ad = adChoicesView?.nativeAd, ad != nativeAd {
            adChoicesView.removeFromSuperview()
            adChoicesView = nil
        }
        
        if adChoicesView == nil {
            adChoicesView = FBAdChoicesView(nativeAd: nativeAd, expandable: false)
            addSubview(adChoicesView)
            adChoicesView.updateFrameFromSuperview()
        }
        
        nativeAd.registerView(forInteraction: self, with: viewController)
    }
}
