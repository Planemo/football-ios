//
//  ChipsAlert.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal

class ChipsAlert: PLAlertViewController {
    func showFromViewController(_ vc: UIViewController) {
        self.title = "Not enough chips"
        self.addAction(AlertAction(title: "Cancel"))
        self.addAction(AlertAction(title: "Buy More") {
            StoreViewController.presentModallyFromViewController(viewController: vc)
        })
        self.show()
    }
}
