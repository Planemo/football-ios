//
//  ChipsButton.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift

class ChipsButton: UIButton {
    let deallocDisposable = CompositeDisposable()
    var userDisposable: Disposable?
    @IBInspectable var challengeChips: Bool = false
    
    deinit {
        self.userDisposable?.dispose()
        self.deallocDisposable.dispose()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    fileprivate func setup() {
        self.contentMode = UIViewContentMode.right
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        let chipsImage = UIImage(named: "icChips")
        self.setImage(chipsImage, for: .normal)
        self.setupWithLocalUser()
        
        let color: UIColor
        if self.challengeChips {
            color = UIColor(white: 0.9, alpha: 1.0)
        } else {
            color = UIColor(red: 0.082, green: 0.607, blue: 0.223, alpha: 1.0)
        }
        self.imageView?.tintColor = color
        self.tintColor = color
    }
    
    func setupWithLocalUser() {
        let disposable = LocalUser.currentUser.producer.startWithValues { [weak self] user in
            if let user = user {
                self?.setupWithUser(user)
            }
        }
        self.deallocDisposable.add(disposable)
    }
    
    func setupWithUser(_ user: LocalUser) {
        self.userDisposable?.dispose()
        
        let chips = challengeChips ? Property(user.challengeChips) : Property(user.chips)
        self.userDisposable = chips.producer
            .combinePrevious(chips.value)
            .startWithValues { [weak self] prev, current in
                if prev != current {
                    self?.showDifference(current - prev, targetValue: current)
                } else {
                    self?.setTitle("\(current)", for: .normal)
                }
            }
    }
    
    fileprivate func showDifference(_ difference: Int, targetValue: Int) {
        let targetTitle = "\(targetValue)"
        let completion = { () -> () in
            self.flipAnimationWith(targetTitle , color: UIColor.white, completion: nil)
        }
        let color = difference < 0 ? UIColor.red : UIColor.green
        let title = difference < 0 ? "\(difference)" : "+\(difference)"
        self.flipAnimationWith(title, color: color, completion: completion)
    }
    
    fileprivate func flipAnimationWith(_ title: String, color: UIColor, completion: (()->())?) {
        UIView.transition(with: self.titleLabel!, duration: 0.6, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            self.setTitle(title, for: .normal)
            self.setTitleColor(color, for: .normal)
        }, completion: { completed  in
            if completed {
                delay(0.6) {
                    completion?()
                }
            } else {
                completion?()
            }
        })
    }
}
