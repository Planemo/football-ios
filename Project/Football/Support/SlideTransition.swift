//
//  SlideTransition.swift
//  davinci
//
//  Created by Mihail Shulepov on 13/09/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation
import UIKit

///
open class SlideTransition: UIPercentDrivenInteractiveTransition {
    open var presenting = true
    open var delay: TimeInterval = 0.0
    
    public override init() {
        super.init()
    }
    
    fileprivate var direction: CGFloat {
        return presenting ? 1.0 : -1.0
    }
}

extension SlideTransition: UIViewControllerAnimatedTransitioning {
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let container = transitionContext.containerView

        container.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        let direction = self.direction
        let offset = fromVC.view.bounds.size.width
        
        var toVCCenter = fromVC.view.center
        toVCCenter.x += direction * offset
        toVC.view.center = toVCCenter
        
        UIView.animate(withDuration: 0.3, delay: self.delay, options: .curveLinear,
            animations: {
                toVC.view.center = fromVC.view.center
                var fromVCCenter = fromVC.view.center
                fromVCCenter.x -= direction * offset
                fromVC.view.center = fromVCCenter
                
            }) { finished in
                let completed = !transitionContext.transitionWasCancelled
                transitionContext.completeTransition(completed)
            }
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    public func animationEnded(_ transitionCompleted: Bool) {

    }
}
