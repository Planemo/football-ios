//
//  AlertViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal

class PLAlertViewController: AlertViewController {
    override func didLayoutButtons(_ buttons: [UIView], inView view: UIView, withStyle style: AlertViewController.ButtonsLayoutStyle) {
        self.addTopSeparatorInView(view)
        switch style {
        case .horizontal:
            self.addSeparatorsForHorizontalLayout(buttons, inView: view)
        case .vertical:
            self.addSeparatorsForVerticalLayout(buttons, inView: view)
        }
    }
    
    fileprivate func createSeparator() -> UIView {
        let ret = UIView()
        ret.backgroundColor = UIColor.white
        ret.translatesAutoresizingMaskIntoConstraints = false
        return ret
    }
    
    fileprivate func addTopSeparatorInView(_ view: UIView) {
        let separator = self.createSeparator()
        view.addSubview(separator)
        let mapping = ["separator": separator]
        let hformat = "H:|-(15)-[separator]-(15)-|"
        let vformat = "V:|-(0)-[separator(==1)]"
        let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: hformat, options: [], metrics: nil, views: mapping)
        let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: vformat, options: [], metrics: nil, views: mapping)
        view.addConstraints(hconstraints + vconstraints)
    }
    
    fileprivate func addSeparatorsForHorizontalLayout(_ buttons: [UIView], inView view: UIView) {
        let buttonsCount = buttons.count
        if buttonsCount < 2 {
            return
        }
        let secondButtons = buttons[1...buttonsCount - 1]
        for nextButton in secondButtons {
            let separator = self.createSeparator()
            view.addSubview(separator)
            let mapping = ["separator": separator, "button": nextButton]
            let hformat = "H:[separator(==1)]-(0)-[button]"
            let vformat = "V:|-(0)-[separator]-(10)-|"
            let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: hformat, options: [], metrics: nil, views: mapping)
            let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: vformat, options: [], metrics: nil, views: mapping)
            view.addConstraints(hconstraints + vconstraints)
        }
    }
    
    fileprivate func addSeparatorsForVerticalLayout(_ buttons: [UIView], inView view: UIView) {
        let buttonsCount = buttons.count
        if buttonsCount < 2 {
            return
        }
        let secondButtons = buttons[1...buttonsCount - 1]
        for nextButton in secondButtons {
            let separator = self.createSeparator()
            view.addSubview(separator)
            let mapping = ["separator": separator, "button": nextButton]
            let vformat = "V:[separator(==1)]-(0)-[button]"
            let hformat = "H:|-(15)-[separator]-(15)-|"
            let hconstraints = NSLayoutConstraint.constraints(withVisualFormat: hformat, options: [], metrics: nil, views: mapping)
            let vconstraints = NSLayoutConstraint.constraints(withVisualFormat: vformat, options: [], metrics: nil, views: mapping)
            view.addConstraints(hconstraints + vconstraints)
        }
    }
}
