//
//  StyledNavigationController.swift
//  Football
//
//  Created by Mikhail Shulepov on 01/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class EnlargedNavigationBar: UINavigationBar {
    fileprivate var height: CGFloat = 50
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    fileprivate func setup() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.setupForPads()
        } else {
            self.setupForPhones()
        }
    }
    
    fileprivate func setupForPhones() {
        self.height = 50
        if let font = UIFont(name: "OpenSans-Light", size: 24) {
            let attrs = [
                NSFontAttributeName: font,
                NSForegroundColorAttributeName: UIColor.white
            ]
            self.titleTextAttributes = attrs
        } else {
            NSLog("Font not found")
        }
    }
    
    fileprivate func setupForPads() {
        self.height = 60
        if let font = UIFont(name: "OpenSans-Light", size: 30) {
            let attrs = [
                NSFontAttributeName: font,
                NSForegroundColorAttributeName: UIColor.white
            ]
            self.titleTextAttributes = attrs
        } else {
            NSLog("Font not found")
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var ret = super.sizeThatFits(size)
        ret.height = self.height
        return ret
    }
}

class StyledNavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    @IBInspectable var backgroundImage: UIImage? {
        didSet {
            if self.isViewLoaded {
                self.updateBackgroundImage()
            }
        }
    }
    
    fileprivate var transitionContoller: SlideTransition!
    
    fileprivate var interactiveTransition = false
    fileprivate var popPanGesture: UIScreenEdgePanGestureRecognizer!
    
    fileprivate var backgroundImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateBackgroundImage()

        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.backgroundColor = UIColor.clear
        
        self.delegate = self
        self.transitionContoller = SlideTransition()
        
        self.popPanGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(StyledNavigationController.handlePanGesture(_:)))
        self.popPanGesture.edges = UIRectEdge.left
        self.popPanGesture.delegate = self
        self.popPanGesture.maximumNumberOfTouches = 1
        self.view.addGestureRecognizer(self.popPanGesture)
    }
    
    fileprivate func updateBackgroundImage() {
        if let backgroundImage = self.backgroundImage {
            if self.backgroundImageView == nil {
                let backgroundImageView = UIImageView(frame: self.view.bounds)
                backgroundImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill
                self.view.insertSubview(backgroundImageView, at: 0)
                self.backgroundImageView = backgroundImageView
            }
            self.backgroundImageView?.image = backgroundImage
            
        } else {
            self.backgroundImageView?.removeFromSuperview()
            self.backgroundImageView = nil
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.view.backgroundColor = UIColor.clear
    }
    
    func navigationController(_ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationControllerOperation,
        from fromVC: UIViewController,
        to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            switch operation {
            case .pop:
                self.transitionContoller.presenting = false
                return self.transitionContoller
            case .push:
                self.transitionContoller.presenting = true
                return self.transitionContoller
            case .none:
                break
            }
            return nil
    }
    
    func navigationController(_ navigationController: UINavigationController,
        interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
            if self.interactiveTransition {
                return animationController as? UIViewControllerInteractiveTransitioning
            }
            return nil
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let _ = gestureRecognizer as? UIScreenEdgePanGestureRecognizer {
            if self.viewControllers.count <= 1 {
                return false
            }
        }
        return true
    }
    
    dynamic func handlePanGesture(_ gesture: UIScreenEdgePanGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.interactiveTransition = true
            self.popViewController(animated: true)
            
        case .changed:
            let loc = gesture.location(in: self.view)
            let screenWidth = self.view.bounds.size.width
            let percent = loc.x / screenWidth
            self.transitionContoller.update(percent)
            
        case .ended:
            let direction: CGFloat = 1
            if gesture.velocity(in: self.view).x * direction >= 0 {
                self.transitionContoller.finish()
            } else {
                self.transitionContoller.cancel()
            }
            self.interactiveTransition = false
            
        case .cancelled:
            self.transitionContoller.cancel()
            self.interactiveTransition = false
            
        default:
            break
        }
    }
}
