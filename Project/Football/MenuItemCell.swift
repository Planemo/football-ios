//
//  MenuItemCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    @IBInspectable var selectionColor: UIColor = UIColor.redColor()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let selectionView = UIView()
        selectionView.backgroundColor = self.selectionColor
        self.selectedBackgroundView = selectionView
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
