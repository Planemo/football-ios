//
//  MainMenuViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 27/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import MBProgressHUD
import ReactiveCocoa
import PLModal
import PLSupport

class MainMenuViewController: UIViewController {
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var nameLabel: UILabel!
    
    fileprivate lazy var challengeNavigationManager = ChallengeMakingNavigationFlow()
    fileprivate var liveMatchBetFlow: LiveMatchBettingFlow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            nameLabel.font = nameLabel.font.withSize(nameLabel.font.pointSize * 1.5)
            
            for button in buttons {
                if let font = button.titleLabel?.font {
                    button.titleLabel?.font = font.withSize(font.pointSize * 2.0)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func makeChallenge() {
        self.challengeNavigationManager.startWithNavigationController(self.navigationController!)
    }
    
    @IBAction func makeLiveMatchBet() {
        self.liveMatchBetFlow = LiveMatchBettingFlow(navigationController: self.navigationController!)
        self.liveMatchBetFlow!.startFlow()
    }
    
    @IBAction func linkWithFacebook() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        LocalUser.linkOrLoginWithFacebook(from: self).start { event in
            switch event {
            case .failed(let error):
                NSLog("FB link error: \(error.description)")
                hud.hideWithErrorStatus("Error")
            case .completed:
                hud.hideWithSuccessStatus("Success")
            default:
                break
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if LocalUser.currentUser.value == nil {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            LocalUser.loadOrCreateLocalUser(serverConfig: ServerConfiguration()).start { event in
                switch event {
                case .failed(_):
                    hud.hideWithErrorStatus("Error...\nCheck your internet connection")
                case .completed:
                    hud.hide(animated: true)
                    delay(0.1) {
                        self.performSegue(withIdentifier: identifier!, sender: sender)
                    }
                default:
                    break
                }
            }
            
            return false
        }
        return true
    }
    
    @IBAction func inviteFriends() {
        SocialUtils.sendFacebookFriendsInvitations { }
    }
    
    @IBAction func share(_ button: UIButton) {
        let shareText = NSLocalizedString("ShareMessage", comment: "share")
        let link = BuildConfig.shareURL
        let items: [Any] = [shareText, link]
        
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
        if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
            if let popover = activityVC.popoverPresentationController {
                popover.sourceView = button
            }
        }
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func showLeaderboard() {
        GKHelper.helper().showLeaderboardWithName(BuildConfig.challengesLeaderboad, fromViewController: self)
    }
}
