//
//  LiveMatchEventBuildingFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveSwift
import Alamofire
import PLModal
import PLSupport

class LiveMatchBettingFlow: NSObject {
    fileprivate let dataProvider = DataProvider.activeProvider
    fileprivate let navController: UINavigationController
    fileprivate var rootVC: UIViewController?
    fileprivate var eventBuildingFlow: NavigationBasedBettingBuilder?
    
    init(navigationController: UINavigationController) {
        self.navController = navigationController
        super.init()
    }
    
    func startFlow() {
        self.rootVC = self.navController.topViewController
        let matchesVC = Storyboards.Main.instantiateMatchesScheduleViewController()
        matchesVC.selectionDelegate = self
        self.navController.pushViewController(matchesVC, animated: true)
    }
    
    func close() {
        if let rootVC = self.rootVC {
            self.navController.popToViewController(rootVC, animated: true)
        } else {
            self.navController.popToRootViewController(animated: true)
        }
    }
    
    fileprivate func startFlowWithMatch(_ game: Game) {
        self.eventBuildingFlow = NavigationBasedBettingBuilder(navController: self.navController, contentEditor: false)
        self.eventBuildingFlow?.delegate = self
        self.eventBuildingFlow?.startBuilding(game)
    }
    
    fileprivate func submitEvents(_ events: [Event], forMatch match: Game, betAmount: Int) -> SignalProducer<Int, NSError> {
        if let user = LocalUser.currentUser.value {
            return user.bettingsManager.createBet(betAmount, match: match, events: events).on(completed: {
                user.chips.value -= betAmount //them was decreased on server during request
            })
        }  else {
            let error = NSError(domain: "EventSubmittingError", code: 3748, userInfo: nil)
            return SignalProducer(error: error)
        }
    }
    
    func submit(_ data: BettingBuildData, betAmount: Int) {
        if let _ = data.event {
            let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
            hud.label.text = "Submitting..."
            self.submitEvents(data.game.events, forMatch: data.game, betAmount: betAmount)
                .observe(on: UIScheduler())
                .start { event in
                    switch event {
                    case .value(let result):
                        NSLog("Submitting result: %d", result)
                    case .failed(let error):
                        NSLog("Submitting error: %@", error.description)
                        hud.hideWithErrorStatus("Fail with error code: \(error.code)")
                    case .completed:
                        hud.hideWithSuccessStatus("Congratulations!\nYour bet was successfully created!")
                        self.close()
                    default:
                        break
                    }
                }
        }
    }
}

extension LiveMatchBettingFlow: MatchSelectorDelegate {
    func didSelectMatch(_ match: Game, selector: MatchSelector) {
        self.startFlowWithMatch(match)
    }
}

extension LiveMatchBettingFlow: BettingBuilderDelegate {
    func willStartBuildComponent(_ component: AbstractBettingBuildComponent, withViewController: UIViewController) {
        //nothing to do
    }
    
    func onBettingBuildCompleted(_ bettingType: BettingType, data: BettingBuildData) {
        let confirmation = BetConfirmationDialog()
        confirmation.match = data.game
        confirmation.event = data.event
        
        confirmation.show { betAmount in
            self.submit(data, betAmount: betAmount)
        }
    }
}
