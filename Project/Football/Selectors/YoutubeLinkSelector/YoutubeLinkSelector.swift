//
//  YoutubeLinkSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 15/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public protocol YoutubeLinkSelector: class {
    weak var linkSelectionDelegate: YoutubeLinkSelectorDelegate? { get set }
}

public protocol YoutubeLinkSelectorDelegate: class {
    func didSelectYoutubeLink(_ link: URL, selector: YoutubeLinkSelector)
}


