//
//  YoutubeLinkSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class YoutubeLinkSelectorViewController: UIViewController, YoutubeLinkSelector {
    @IBOutlet var youtubeLinkField: UITextField!
    weak var linkSelectionDelegate: YoutubeLinkSelectorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done() {
        let linkText = self.youtubeLinkField.text ?? ""
        if let link =  URL(string: linkText), link.absoluteString.utf8.count < 255 {
            self.linkSelectionDelegate?.didSelectYoutubeLink(link, selector: self)
        } else {
            UIAlertView(title: "Error", message: "Invalid youtube link or link too big (max len is 255)", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
