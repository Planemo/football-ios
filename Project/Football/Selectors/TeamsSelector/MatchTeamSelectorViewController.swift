//
//  MatchTeamSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class MatchTeamSelectorViewController: UIViewController, TeamsSelector {
    @IBOutlet var firstTeamView: ShallowTeamView!
    @IBOutlet var secondTeamView: ShallowTeamView!
    
    var selectedTeams: [Team] = []
    var allowsMultipleSelection: Bool = false
    
    weak var selectionDelegate: TeamsSelectionDelegate?
    var teams: [Team] = [Team]() {
        didSet {
            if self.isViewLoaded {
                self.updateTeams()
            }
        }
    }
    
    var game: Game? {
        didSet {
            if let game = self.game {
                self.teams = [game.homeTeam, game.awayTeam]
            } else {
                self.teams = []
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateTeams()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        //self.selectionDelegate?.prepareForSegue(segue, sender: sender)
    }
    
    fileprivate func updateTeams() {
        self.firstTeamView.team = self.teams.first
        self.secondTeamView.team = self.teams.last
    }
    
    @IBAction
    func didSelectFirstTeam() {
        self.selectionDelegate?.didSelectTeams([teams.first!], selector: self)
    }
    
    @IBAction
    func didSelectSecondTeam() {
        self.selectionDelegate?.didSelectTeams([teams.last!], selector: self)
    }
}
