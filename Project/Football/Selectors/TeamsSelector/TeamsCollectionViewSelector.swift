//
//  TeamsViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage
import FBAudienceNetwork


class TeamCell: UICollectionViewCell {
    @IBOutlet var teamView: ShallowTeamView!
    @IBOutlet var regionView: RegionView!
    @IBOutlet var logoHolder: UIView!
    
    @IBInspectable var selectedColor: UIColor?
    
    var team: Team! {
        didSet {
            self.setupWithTeam()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let selectedColor = selectedColor {
            self.selectedBackgroundView = UIView()
            self.selectedBackgroundView?.backgroundColor = selectedColor
        }
        
        if let logoView = self.logoHolder {
            logoView.layer.cornerRadius = logoView.bounds.size.width / 2
            logoView.layer.borderWidth = 2
            logoView.layer.borderColor = UIColor.white.cgColor
            logoView.layer.masksToBounds = true
        }
        
        if let iconView = self.regionView.iconView {
            iconView.layer.cornerRadius = iconView.bounds.size.width / 2
            iconView.layer.borderWidth = 1
            iconView.layer.borderColor = UIColor.white.cgColor
            iconView.layer.masksToBounds = true
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let nameFont = teamView.nameLabel.font {
                teamView.nameLabel.font = nameFont.withSize(nameFont.pointSize * 1.25)
            }
        }
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    fileprivate func setupWithTeam() {
        self.teamView.team = self.team
        if let country = self.team.country {
            self.regionView.isHidden = false
            self.regionView.setupWithRegion(country)
        } else {
            self.regionView.isHidden = true
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.updateSelectHightlightState()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.updateSelectHightlightState()
        }
    }
    
    fileprivate func updateSelectHightlightState() {
        var selectedState = false
        if self.isSelected != self.isHighlighted {
            selectedState = true // one of them is true
        } else {
            selectedState = false
        }
        self.logoHolder.alpha = selectedState ? 0.5 : 1.0
    }
}

class TeamsCollectionViewSelector: UICollectionViewController, TeamsSelector {
    weak var selectionDelegate: TeamsSelectionDelegate?
    
    var teams: [Team] = [] {
        didSet {
            self.rebuildSections()
        }
    }
    
    var selectedTeams: [Team] = [] {
        didSet {
            if self.isViewLoaded {
                self.updateSelection()
            }
        }
    }
    
    var allowsMultipleSelection: Bool = false {
        didSet {
            if let collectionView = self.collectionView {
                collectionView.allowsMultipleSelection = self.allowsMultipleSelection
            }
        }
    }
    
    fileprivate var adLayout = [Int: AdInjectedSection]()
    fileprivate var pendingAds = [FBNativeAd]()
    
    fileprivate var sections: Sections<String, Team> = Sections() {
        didSet {
            if self.isViewLoaded {
                self.collectionView?.reloadData()
            }
        }
    }
    
    enum GroupingStyle {
        case region
        case none
    }
    
    fileprivate var groupingStyle: GroupingStyle = .none {
        didSet {
            self.rebuildSections()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.allowsMultipleSelection = self.allowsMultipleSelection
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
            let oldItemSize = layout.itemSize
            layout.itemSize = CGSize(width: oldItemSize.width * 1.9, height: oldItemSize.height * 1.35)
        }
        
        if BuildConfig.adsEnabled {
            let ad = FBNativeAd(placementID: "1628457360721437_1786119468288558")
            ad.delegate = self
            ad.load()
            pendingAds.append(ad)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    fileprivate func rebuildSections() {
        switch self.groupingStyle {
        case .region:
            self.sections = Sections( Array(self.teams.groupBy { team -> String in
                return team.country ?? "???"
            }.mapValues { country, teams -> Section<String, Team> in
                return Section(header: country, items: teams)
            }.values))
        case .none:
            let teamsSortedByName = self.teams.sorted { lhs, rhs -> Bool in
                return lhs.name < rhs.name
            }
            self.sections = Sections([Section(header: "", items: teamsSortedByName)])
            self.insertAds()
        }
    }
    
    fileprivate func updateSelection() {
        if let collectionView = self.collectionView {
            if let indexPaths = collectionView.indexPathsForSelectedItems {
                for prevSelection in indexPaths {
                    collectionView.deselectItem(at: prevSelection, animated: false)
                }
            }
            
            let newSelection = self.selectedTeams.map { self.sections.indexPathForItem($0) }
            for path in newSelection {
                collectionView.selectItem(at: path, animated: false, scrollPosition: UICollectionViewScrollPosition())
            }
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.numberOfSections()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let adsCount: Int
        if let adPlacements = self.adLayout[section]?.adPlacements {
            adsCount = adPlacements.count
        } else {
            adsCount = 0
        }
        return self.sections.numberOfItemsInSection(section) + adsCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let adjustedIndexPath: IndexPath
        if let adLayout = self.adLayout[indexPath.section] {
                if adLayout.isAdPlacement(indexPath) {
                let adCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! CustomCollectionViewAdCell
                let ad = adLayout.getNativeAd(indexPath)!
                adCell.setupWithNativeAd(ad, viewController: self)
                return adCell
            }
            adjustedIndexPath = adLayout.getAdjustedIndexPath(indexPath)
        } else {
            adjustedIndexPath = indexPath
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ITeamCell", for: indexPath) as! TeamCell
        cell.team = self.sections[adjustedIndexPath]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! IconTitleCollectionHeaderView
            view.setupWithRegion(self.sections.headerForSection(indexPath.section))
            return view
        case UICollectionElementKindSectionFooter:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            return view
        default:
            break
        }
        return UICollectionReusableView()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let adjustedIndexPath: IndexPath
        if let adLayout = adLayout[indexPath.section] {
            adjustedIndexPath = adLayout.getAdjustedIndexPath(indexPath)
            if adLayout.isAdPlacement(indexPath) {
                return
            }
        } else {
            adjustedIndexPath = indexPath
        }
        let team = self.sections[adjustedIndexPath]
        self.selectedTeams.append(team)
        self.selectionDelegate?.didSelectTeams(self.selectedTeams, selector: self)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let adjustedIndexPath: IndexPath
        if let adLayout = adLayout[indexPath.section] {
            adjustedIndexPath = adLayout.getAdjustedIndexPath(indexPath)
            if adLayout.isAdPlacement(indexPath) {
                return
            }
        } else {
            adjustedIndexPath = indexPath
        }
        let team = self.sections[adjustedIndexPath]
        self.selectedTeams.remove(team)
        self.selectionDelegate?.didSelectTeams(self.selectedTeams, selector: self)
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension TeamsCollectionViewSelector: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch self.groupingStyle {
        case .none:
            return CGSize.zero
        default:
            return (self.collectionViewLayout as! UICollectionViewFlowLayout).headerReferenceSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        switch self.groupingStyle {
        case .none:
            return CGSize.zero
        default:
            return (self.collectionViewLayout as! UICollectionViewFlowLayout).footerReferenceSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let adLayout = adLayout[indexPath.section], adLayout.isAdPlacement(indexPath)  {
            return CGSize(width: 240, height: 180)
        }
        return CGSize(width: 240, height: 60)
    }
    
}

// MARK: FBNativeAdDelegate

extension TeamsCollectionViewSelector: FBNativeAdDelegate {
    func insertAds() {
        if let ad = pendingAds.first, adLayout.isEmpty {
            let adSection = AdInjectedSection()
            let idx = teams.count
            adSection.adPlacements[idx] = ad
            adLayout[0] = adSection
            self.collectionView!.insertItems(at: [IndexPath(item: idx, section: 0)])
        }
    }
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        if !self.teams.isEmpty {
            insertAds()
        }
    }
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        NSLog("Native ad did fail: %@", (error as NSError).description)
    }
}
