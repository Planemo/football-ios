//
//  TeamTemplateViewSetuper.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TeamTemplateView: ShallowTeamView {
    @IBOutlet var backgroundView: UIImageView!
    @IBOutlet var shadowView: UIImageView!
    @IBOutlet var foregroundView: UIImageView!
    
    @IBInspectable var top: Bool = true
    
    override func setupWithTeam() {
        super.setupWithTeam()
        
        if let template = self.team?.formTemplate {
            self.setupWithTemplate(template)
        }
    }
    
    func setupWithTemplate(_ template: FormTemplate) {
        self.backgroundView.tintColor = template.backgroundColor
        self.shadowView.isHidden = !template.withShadow
        self.foregroundView.tintColor = template.foregroundColor
        
        let prefix = self.top ? "TFT" : "BFT"
        let imageName = "\(prefix)_\(template.foregroundTemplate)"
        if let image = UIImage(named: imageName) {
            self.foregroundView.image = image
            
        } else {
            NSLog("Can't create image for team template: %@", imageName)
        }
    }
}

