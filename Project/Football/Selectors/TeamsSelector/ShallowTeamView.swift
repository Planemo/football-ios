//
//  TeamView.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class ShallowTeamView: UIView {
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBInspectable var smallLogo: Bool = true
    
    var team: Team? {
        didSet {
            self.setupWithTeam()
        }
    }
    
    func setupWithTeam() {
        if let team = self.team {
            self.setTeamName(team.name)
            let logoURL = self.smallLogo ? team.smallLogoURL : team.logoURL
            if let url = logoURL {
                if url.isFileURL {
                    self.logoView.image = UIImage(contentsOfFile: url.absoluteString)
                } else {
                    self.logoView.sd_setImage(with: url as URL!)
                }
                
            } else {
                //TODO: set placeholder
                self.logoView.image = nil
            }
        } else {
            self.setTeamName("???")
            //TODO: set placeholder
            self.logoView.image = nil
        }
    }
    
    fileprivate func setTeamName(_ name: String) {
        if let attrText = self.nameLabel.attributedText {
            self.nameLabel.attributedText = attrText.attributedStringByReplacingTextWith(name)
        } else {
            self.nameLabel.text = name
        }
    }
}
