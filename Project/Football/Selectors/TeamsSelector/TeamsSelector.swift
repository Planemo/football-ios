//
//  TeamSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

protocol TeamsSelector: class {
    var teams: [Team] { get set }
    var selectedTeams: [Team] { get set }
    var allowsMultipleSelection: Bool { get set }
    weak var selectionDelegate: TeamsSelectionDelegate? { get set }
}

protocol TeamsSelectionDelegate: class {
    func didSelectTeams(_ teams: [Team], selector: TeamsSelector)
}
