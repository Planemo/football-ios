//
//  FriendSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Foundation

public protocol UserSelector: class {
    var allowSelfSelection: Bool { get set }
    weak var userSelectionDelegate: UserSelectorDelegate? { get set }
}

public protocol UserSelectorDelegate: class {
    func didSelectUser(_ user: User, selector: UserSelector)
    func didSelectSpecialAction(_ barButton: UIBarButtonItem)
}
