//
//  ParseFriendCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage

class UserCell: UICollectionViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconView.layer.cornerRadius = 5
        self.iconView.layer.borderWidth = 2
        self.iconView.layer.borderColor = UIColor.white.cgColor
        self.iconView.layer.masksToBounds = true
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func setupWithUser(_ user: User) {
        self.nameLabel.text = user.name
        
        if let urlString = user.avatarURL, let url = URL(string: urlString) {
            self.iconView.sd_setImage(with: url)
        } else {
            self.iconView.image = UIImage(named: "AvatarPlaceholder")
        }
    }
}
