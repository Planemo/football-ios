//
//  ParseFriendsViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift
import MBProgressHUD
import PLSupport
import FBAudienceNetwork

class UsersViewController: UIViewController, UserSelector {
    weak var userSelectionDelegate: UserSelectorDelegate?
    var allowSelfSelection: Bool = true
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var loginView: UIView!
    @IBOutlet var invitesView: UIView!
    
    fileprivate var disposable = CompositeDisposable()
    
    fileprivate var nativeAds: [FBNativeAd] = []
    fileprivate let sectionLayout = AdInjectedSection()
    
    var users = [User]() {
        didSet {
            if self.isViewLoaded {
                if users.isEmpty {
                    self.showInvitesView()
                } else {
                    self.collectionView.reloadData()
                    self.loadAds()
                }
            }
        }
    }
    
    deinit {
        self.disposable.dispose()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loginDisposable = LocalUser.currentUser.producer.startWithValues { [weak self] user in
            if let user = user, user.isLoggedInWithFacebook {
                self?.showUsersView()
            } else {
                self?.showRequireLoginView()
            }
        }
        self.disposable.add(loginDisposable)
    }
    
    fileprivate func showRequireLoginView() {
        self.collectionView.isHidden = true
        self.loginView.isHidden = false
        self.invitesView.isHidden = true
    }
    
    fileprivate func showUsersView() {
        self.collectionView.isHidden = false
        self.loginView.isHidden = true
        self.invitesView.isHidden = true
        self.refreshUsersList()
    }
    
    fileprivate func showInvitesView() {
        self.invitesView.isHidden = false
        self.collectionView.isHidden = true
        self.loginView.isHidden = true
    }
    
    fileprivate func loadAds() {
        if BuildConfig.adsEnabled && self.nativeAds.isEmpty {
            let nativeAd = FBNativeAd(placementID: "1628457360721437_1785672568333248")
            nativeAd.delegate = self
            nativeAd.load()
            self.nativeAds.append(nativeAd)
        }
    }
    
    fileprivate func refreshUsersList() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        LocalUser.currentUser.value!.facebookFriends().map { [weak self] users -> [User] in
            if let allowSelfSelection = self?.allowSelfSelection, allowSelfSelection == true {
                return users + [LocalUser.currentUser.value!]
            }
            return users
        }.start { [weak self] event in
            switch event {
            case .value(let friends):
                self?.users = friends
            case .failed(let error):
                NSLog("Error loading friends: %@", error.description)
                hud.hideWithErrorStatus("Error")
            case .completed:
                hud.hide(animated: true)
            default:
                break
            }
        }
    }
    
    @IBAction func loginFacebook() {
        self.loginView.isHidden = true
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        LocalUser.linkOrLoginWithFacebook(from: self).start { event in
            switch event {
            case .failed(let error):
                NSLog("Error: %@", error.description)
                hud.hideWithErrorStatus("Error")
                self.loginView.isHidden = false
            case .completed:
                hud.hide(animated: true)
            default:
                break
            }
        }
    }
    
    @IBAction func inviteFriends() {
        SocialUtils.sendFacebookFriendsInvitations { }
    }
    
    @IBAction func specialAction(_ barButtonItem: UIBarButtonItem) {
        self.userSelectionDelegate?.didSelectSpecialAction(barButtonItem)
    }
}

extension UsersViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count + self.sectionLayout.adPlacements.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.sectionLayout.isAdPlacement(indexPath) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! CustomCollectionViewAdCell
            cell.setupWithNativeAd(self.sectionLayout.getNativeAd(indexPath)!, viewController: self)
            return cell
        }
        let adjustedIndexPath = self.sectionLayout.getAdjustedIndexPath(indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! UserCell
        let user = self.users[adjustedIndexPath.item]
        cell.setupWithUser(user)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if !self.sectionLayout.isAdPlacement(indexPath) {
            let adjustedIndexPath = self.sectionLayout.getAdjustedIndexPath(indexPath)
            let user = self.users[adjustedIndexPath.item]
            self.userSelectionDelegate?.didSelectUser(user, selector: self)
        }
    }
}

extension UsersViewController: FBNativeAdDelegate {
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        let idx = min(users.count, 4)
        self.sectionLayout.adPlacements[idx] = nativeAd
        self.collectionView.insertItems(at: [IndexPath(item: idx, section: 0)])
    }
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        NSLog("Native ad did fail: %@", (error as NSError).description)
    }

}
