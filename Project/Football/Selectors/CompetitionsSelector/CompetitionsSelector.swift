//
//  CompetitionsSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 26/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

protocol CompetitionSelector {
    var competitions: [Competition] { get set }
    weak var selectionDelegate: CompetitionSelectionDelegate? { get set }
}

protocol CompetitionSelectionDelegate: class {
    func didSelectCompetition(_ competition: Competition, selector: CompetitionSelector)
}
