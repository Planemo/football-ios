//
//  ChooseMatchCollectionViewController.swift
//  Football
//
//  Created by Maxim Shmotin on 17/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift
import MBProgressHUD
import PLSupport

class CompetitionsViewController: UICollectionViewController, CompetitionSelector {
    fileprivate let dataProvider = DataProvider.activeProvider
    
    weak var selectionDelegate: CompetitionSelectionDelegate?
    
    enum GroupStyle {
        case none
        case byRegion
    }
    
    var competitions: [Competition] = [] {
        didSet {
            self.updateSections()
        }
    }
    
    fileprivate var sections: Sections<String, Competition> = Sections() {
        didSet {
            if self.isViewLoaded {
                self.collectionView?.reloadData()
            }
        }
    }
    
    var groupingStyle: GroupStyle = .byRegion
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.competitions.isEmpty {
            self.requestCompetitions()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func requestCompetitions() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.dataProvider.requestCompetitions()
            .observe(on: UIScheduler())
            .startWithValues(value: { [weak self] competitions in
                hud.hide(animated: true)
                self?.competitions = competitions
                
            }, failed: { error in
                hud.hideWithErrorStatus("Error")
            })
    }
       
    fileprivate func updateSections() {
        switch self.groupingStyle {
        case .none:
            self.sections = Sections([Section(header: "All", items: self.competitions)])
        case .byRegion:
            sections = Sections( self.competitions
                .groupBy { $0.region ?? "Undefined" }
                .mapValues { (title, competitions) -> Section<String, Competition> in
                    return Section(header: title, items: competitions)
                }.values.sorted { (lhs: Section<String, Competition>, rhs) -> Bool in
                    return lhs.header.localizedCaseInsensitiveCompare(rhs.header) == .orderedAscending
                }
            )
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.numberOfSections()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections.numberOfItemsInSection(section)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CompetitionCell
        cell.competition = self.sections.itemForIndexPath(indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let section = self.sections[indexPath.section]
        switch self.groupingStyle {
        case .none:
            return UICollectionReusableView()
        case .byRegion:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CountryHeader", for: indexPath) as! IconTitleCollectionHeaderView
            headerView.titleLabel.text = section.header
            headerView.setupWithRegion(section.header)
            return headerView
        }
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let competition = self.sections[indexPath]
        self.selectionDelegate?.didSelectCompetition(competition, selector: self)
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension CompetitionsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch self.groupingStyle {
        case .none:
            return CGSize.zero
        default:
            return CGSize(width: collectionView.bounds.size.width, height: 30)
        }
    }
}
