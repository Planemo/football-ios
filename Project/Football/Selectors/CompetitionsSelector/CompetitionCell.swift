//
//  CompetitionCell.swift
//  Football
//
//  Created by Mihail Shulepov on 26/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import QuartzCore
import SDWebImage

class CompetitionCell: UICollectionViewCell {
    @IBOutlet var logoView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var effectsView: UIView!
    
    @IBInspectable var selectedColor: UIColor?
    var normalColor: UIColor?
    
    var competition: Competition! {
        didSet {
            self.setupCompetition()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.isOpaque = true
        self.contentView.backgroundColor = UIColor.clear
        self.effectsView.layer.borderWidth = 1
        self.effectsView.layer.borderColor = UIColor.black.cgColor
        self.effectsView.layer.cornerRadius = 3
        self.effectsView.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear

        self.normalColor = self.contentView.backgroundColor
    }
    
    fileprivate func setupCompetition() {
        CompetitionViewModel().setup(self, competition: self.competition)
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.contentView.backgroundColor = self.isHighlighted ? self.selectedColor : self.normalColor
        }
    }
}

extension CompetitionCell: CompetitionView {
    var titleLabel: UILabel! {
        return self.nameLabel
    }
    
    var iconView: UIImageView! {
        return self.logoView
    }
}
