//
//  PlayerCell.swift
//  Football
//
//  Created by Mikhail Shulepov on 09/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import QuartzCore
import SDWebImage

class PlayerCell: UICollectionViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var positionLabel: UILabel!
    @IBInspectable var placeholderImage: UIImage!
    
    var player: Player! {
        didSet {
            self.setupForPlayer()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let iconLayer = self.layer
        iconLayer.cornerRadius = 5
        iconLayer.borderColor = UIColor.white.cgColor
        iconLayer.borderWidth = 2
        iconLayer.masksToBounds = true
        iconLayer.shouldRasterize = true
        iconLayer.rasterizationScale = UIScreen.main.scale
    }
    
    fileprivate func setupForPlayer() {
        self.iconView?.sd_setImage(with: self.player.portrait as URL!, placeholderImage: self.placeholderImage)
        if let attrText = self.nameLabel?.attributedText {
            self.nameLabel?.attributedText = attrText.attributedStringByReplacingTextWith(self.player.name)
        }
        self.numberLabel?.text = "\(self.player.number)"
        if let role = self.player.role {
            self.positionLabel.text = role
        } else {
            self.positionLabel.text = ""
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.updateSelectHightlightState()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.updateSelectHightlightState()
        }
    }
    
    fileprivate func updateSelectHightlightState() {
        var selectedState = false
        if self.isSelected != self.isHighlighted {
            selectedState = true // one of them is true
        } else {
            selectedState = false
        }
        self.iconView?.alpha = selectedState ? 0.4 : 1.0
    }
}
