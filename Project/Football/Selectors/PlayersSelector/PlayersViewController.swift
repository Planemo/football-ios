//
//  PlayersViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 09/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController, PlayersSelector {
    @IBOutlet var collectionView: UICollectionView!
    weak var selectionDelegate: PlayersSelectionDelegate?
    
    var players: [Player] = [] {
        didSet {
            self.updateSections()
            self.selectedPlayers = []
        }
    }
    
    var selectedPlayers: [Player] = [] {
        didSet {
            if self.isViewLoaded {
                self.refreshPlayersSelection()
            }
        }
    }
    
    var allowsMultipleSelection: Bool = false {
        didSet {
            if self.isViewLoaded {
                self.collectionView.allowsMultipleSelection = self.allowsMultipleSelection
            }
        }
    }
    
    fileprivate var sections: Sections<String, Player> = Sections() {
        didSet {
            if self.isViewLoaded {
                self.collectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.allowsMultipleSelection = self.allowsMultipleSelection
        self.refreshPlayersSelection()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func updateSections() {
        self.sections = Sections(
            Array(self.players.groupBy { player -> String in
                return player.role ?? "??"
            }.mapValues { (role, players) -> Section<String, Player> in
                return Section(header: role, items: players)
            }.values)
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)        
        //self.selectionDelegate?.prepareForSegue(segue, sender: sender)
    }
    
    fileprivate func refreshPlayersSelection() {
        // clear collectionview selection
        if let selected = self.collectionView.indexPathsForSelectedItems {
            for selectedItem in selected {
                self.collectionView.deselectItem(at: selectedItem, animated: false)
            }
        }
        
        // update collectionview selection
        self.selectedPlayers.map {
            self.sections.indexPathForItem($0)
        }
        .filter { $0 != nil }.map { $0! }
        .each { (indexPath: IndexPath) in
            self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
        }
    }
    
    func deselectAll() {
        self.selectedPlayers.removeAll(keepingCapacity: false)
    }
}

extension PlayersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections.numberOfItemsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IPlayerCell", for: indexPath) as! PlayerCell
        cell.player = self.sections.itemForIndexPath(indexPath)
        return cell
    }
    
    /*func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "PositionHeader", forIndexPath: indexPath) as! TitleCollectionHeaderView
        header.titleLabel.text = self.sections.headerForSection(indexPath.section)
        return header
    }*/
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = self.sections[indexPath]
        self.selectedPlayers.append(player)
        self.selectionDelegate?.didSelectPlayers(self.selectedPlayers, selector: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let player = self.sections[indexPath]
        self.selectedPlayers.remove(player)
        self.selectionDelegate?.didDeselectPlayers([player], selector: self)
    }
}

