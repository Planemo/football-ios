//
//  PlayersSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

protocol PlayersSelector: class {
    weak var selectionDelegate: PlayersSelectionDelegate? { get set }
    
    var players: [Player] { get set }
    var allowsMultipleSelection: Bool { get set }
    var selectedPlayers: [Player] { get set }
    
    func deselectAll()
}

protocol PlayersSelectionDelegate: class {
    func didSelectPlayers(_ players: [Player], selector: PlayersSelector)
    func didDeselectPlayers(_ players: [Player], selector: PlayersSelector)
}
