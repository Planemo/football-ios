//
//  LiveMatchesCollectionViewCell.swift
//  Football
//
//  Created by Maxim Shmotin on 08/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit


class MatchCollectionViewCell: UICollectionViewCell {
    @IBOutlet var awayTeamView: TeamTemplateView?
    @IBOutlet var homeTeamView: TeamTemplateView?
    
    @IBOutlet var timeLabel: UILabel?
    @IBOutlet var secondTimeLabel: UILabel?
    
    var dateFormatter: DateFormatter?
    var secondDateFormatter: DateFormatter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.enlargeFontSize(awayTeamView)
            self.enlargeFontSize(homeTeamView)
            self.timeLabel?.enlargeFontSizeBy(1.5)
            self.secondTimeLabel?.enlargeFontSizeBy(1.33)
        }
    }
    
    func setupWithMatch(_ match: Game) {
        self.homeTeamView?.team = match.homeTeam
        self.awayTeamView?.team = match.awayTeam
        
        self.timeLabel?.text = self.dateFormatter?.string(from: match.gameDate as Date)
        self.secondTimeLabel?.text = self.secondDateFormatter?.string(from: match.gameDate as Date)

    }
    
    func enlargeFontSize(_ teamView: TeamTemplateView?) {
        teamView?.nameLabel?.enlargeFontSizeBy(1.5)
    }
}
