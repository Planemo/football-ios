//
//  MatchSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public protocol MatchSelector: class {
    weak var selectionDelegate: MatchSelectorDelegate? { get set }
    var matches: [Game] { get set }
}

public protocol MatchSelectorDelegate: class {
    func didSelectMatch(_ match: Game, selector: MatchSelector)
}
