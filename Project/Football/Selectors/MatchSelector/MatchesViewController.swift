//
//  LiveViewController.swift
//  Football
//
//  Created by Maxim Shmotin on 08/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public enum MatchGroupingStyle {
    case none
    case byCompetition
}

class MatchesViewController: UIViewController, MatchSelector {
    weak var selectionDelegate: MatchSelectorDelegate?
    
    @IBOutlet var collectionView: UICollectionView!
    
    var showFullDate = false {
        didSet {
            self.updateFormatters()
        }
    }
    
    var matches = [Game]() {
        didSet {
            self.rebuildSections()
        }
    }
    
    var groupingStyle: MatchGroupingStyle = .none {
        didSet {
            self.rebuildSections()
        }
    }
    
    var sections = Sections<String, Game>() {
        didSet {
            if self.isViewLoaded {
                self.collectionView.reloadData()
            }
        }
    }
    
    fileprivate let dateFormatter = DateFormatter()
    fileprivate var secondDateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .pad {
            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let oldItemSize = layout.itemSize
            layout.itemSize = CGSize(width: oldItemSize.width * 1.6, height: oldItemSize.height * 1.6)
        }
        self.updateFormatters()
    }
    
    fileprivate func rebuildSections() {
        let sortedMatches = self.matches.sorted { lhs, rhs -> Bool in
            return lhs.gameDate.isBefore(rhs.gameDate)
        }
        switch self.groupingStyle {
        case .none:
            self.sections = Sections ([
                Section(header: "", items: sortedMatches)
            ])
            
        case .byCompetition:
            self.sections = Sections (
                sortedMatches.groupBy { match -> String in
                    return match.competition?.name ?? "???"
                }.mapValues { competitionName, matches -> Section<String, Game> in
                    return Section(header: competitionName, items: matches)
                }.values.sorted { (lhs: Section<String, Game>, rhs) -> Bool in
                    return lhs.header.localizedCaseInsensitiveCompare(rhs.header) == .orderedAscending
                }
            )
        }
    }
    
    fileprivate func updateFormatters() {
        if showFullDate {
            dateFormatter.dateFormat = "dd MMM YYYY"
            secondDateFormatter.dateFormat = "hh:mm a"
        } else {
            dateFormatter.dateFormat = "hh:mm"
            secondDateFormatter.dateFormat = "a"
        }
    }
}

extension MatchesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections.numberOfItemsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchCell", for: indexPath) as! MatchCollectionViewCell
        cell.dateFormatter = self.dateFormatter
        cell.secondDateFormatter = self.secondDateFormatter
        cell.setupWithMatch(self.sections.itemForIndexPath(indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let match = self.sections.itemForIndexPath(indexPath)
        self.selectionDelegate?.didSelectMatch(match, selector: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! TitleCollectionHeaderView
            view.titleLabel.text = self.sections.headerForSection(indexPath.section)
            view.titleLabel.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 2))
            return view
        default:
            break
        }
        return UICollectionReusableView()
    }
}

extension MatchesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch self.groupingStyle {
        case .none:
            return CGSize.zero
        default:
            return (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).headerReferenceSize
        }
    }
}

