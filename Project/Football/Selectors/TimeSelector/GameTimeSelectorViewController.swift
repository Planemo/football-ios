//
//  TimeSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class GameTimeSelectorViewController: UIViewController, GameTimeSelector {
    @IBOutlet var pickerView: UIPickerView!

    weak var selectionDelegate: GameTimeSelectorDelegate?
    
    var periodsDefinition: [GamePeriodDefinition] = [] {
        didSet {
            self.pickerView?.reloadAllComponents()
        }
    }
    
    var selectedTime: GameTime? {
        let definitionIdx = self.pickerView.selectedRow(inComponent: 0)
        if definitionIdx < self.periodsDefinition.count {
            let period = self.periodsDefinition[definitionIdx].period
            let minutes = self.pickerView.selectedRow(inComponent: 1)
            let seconds = self.pickerView.selectedRow(inComponent: 2)
            return GameTime(period: period, minute: minutes, second: seconds)
        }
        return nil
    }
    
    @IBAction func complete() {
        if let time = self.selectedTime {
            self.selectionDelegate?.didSelectTime(time, selector: self)
        } else {
            NSLog("Error building time")
        }
    }
}

extension GameTimeSelectorViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0: return self.periodsDefinition.count //name
        case 1:
            let period = self.periodsDefinition[pickerView.selectedRow(inComponent: 0)]
            return period.maxDuration ?? 100
        case 2: return 60 //seconds
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let pickerWidth = pickerView.bounds.size.width
        switch component {
        case 0: return pickerWidth * 2 / 3
        case 1, 2: return pickerWidth / 6
        default: return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0: return self.periodsDefinition[row].name
        default: return "\(row)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            pickerView.reloadComponent(1)
        }
    }
}
