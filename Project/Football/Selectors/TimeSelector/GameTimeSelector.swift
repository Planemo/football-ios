//
//  TimeSelector.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public protocol GameTimeSelector {
    weak var selectionDelegate: GameTimeSelectorDelegate? { get set }
    var periodsDefinition: [GamePeriodDefinition] { get set }
}

public protocol GameTimeSelectorDelegate: class {
    func didSelectTime(_ time: GameTime, selector: GameTimeSelector)
}
