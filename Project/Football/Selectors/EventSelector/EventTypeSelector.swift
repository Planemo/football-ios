//
//  EventTypeSelectionDelegate.swift
//  Football
//
//  Created by Mikhail Shulepov on 26/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

enum BettingType: Int {
    case goal
    case goalWithAssist
    case goalWithDribbling
    
//    case Winner
//    case Score
    
    static var allTypes: [BettingType] {
        var types = [BettingType]()
        
        var n = 0
        while let type = BettingType(rawValue: n) {
            types.append(type)
            n += 1
        }
        
        return types
    }
}

protocol EventTypeSelector {
    weak var selectionDelegate: EventTypeSelectionDelegate? { get set }
}

protocol EventTypeSelectionDelegate: class {
    func didSelectEventType(_ eventType: BettingType, selector: EventTypeSelector)
}
