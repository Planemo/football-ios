//
//  EventSelectorViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 26/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class EventTypeSelectorViewController: UIViewController, EventTypeSelector {
    weak var selectionDelegate: EventTypeSelectionDelegate?
    
    let eventTypes = BettingType.allTypes
    
    convenience init() {
        self.init(nibName: "EventTypeSelectorViewController", bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        //self.selectionDelegate?.prepareForSegue(segue, sender: sender)
    }
}

extension EventTypeSelectorViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.eventTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! EventTypeCell
        cell.eventType = self.eventTypes[indexPath.item]
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let eventType = self.eventTypes[indexPath.item]
        self.selectionDelegate?.didSelectEventType(eventType, selector: self)
    }

}
