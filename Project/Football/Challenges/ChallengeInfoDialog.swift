//
//  ChallengeInfoViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal
import ReactiveSwift
import enum Result.NoError

class ChallengeInfoDialog: PLAlertViewController {
    // Teams views
    @IBOutlet var homeTeamView: TeamTemplateView!
    @IBOutlet var awayTeamView: TeamTemplateView!
    
    // Goal details labels
    @IBOutlet var shotPlayerLabel: UILabel?
    @IBOutlet var assistPlayerLabel: UILabel?
    @IBOutlet var goalTimeLabel: UILabel!
    
    // Match date labels
    @IBOutlet var matchDayLabel: UILabel!
    
    @IBOutlet var timeoutLabel: UILabel!
    
    var acceptAction: (() -> ())?
    var timeoutSignal: Signal<Int, NoError>!
    
    var challenge: (match: Game, event: Event)!
    
    override init() {
        super.init(nibName: "ChallengeInfoDialog", bundle: nil)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let match = challenge.match
        self.title = match.competition?.name ?? ""
        
        self.setupMatchTeams(match)
        self.setupGoalDetails(match)
        self.setupMatchDate(match)
        
        self.timeoutSignal .observeValues { value in
            self.timeoutLabel.text = "\(value)"
        }
        
        self.shotPlayerLabel?.superview?.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions(),
            animations: {
                self.shotPlayerLabel?.superview?.alpha = 1.0
            }, completion: nil)
        
        self.assistPlayerLabel?.superview?.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 1.2, options: UIViewAnimationOptions(),
            animations: {
                self.assistPlayerLabel?.superview?.alpha = 1.0
            }, completion: nil)
        
        self.goalTimeLabel?.superview?.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 2.2, options: UIViewAnimationOptions(),
            animations: {
                self.goalTimeLabel?.superview?.alpha = 1.0
            }, completion: nil)
        
        if BuildConfig.noTeamEmblems {
            self.homeTeamView.logoView.isHidden = true
            self.awayTeamView.logoView.isHidden = true
        }
    }
    
    fileprivate func setupMatchTeams(_ match: Game) {
        self.homeTeamView.team = match.homeTeam
        self.awayTeamView.team = match.awayTeam
    }
    
    
    fileprivate func setupGoalDetails(_ match: Game) {
        let shotPlayer = challenge.event.relatedPlayer
        self.shotPlayerLabel?.text = "\(shotPlayer.name) (\(shotPlayer.number))"
        
        if let assistEventId = challenge.event.relatedEventId, let assistEvent = match.eventWithId(assistEventId) {
            let assistPlayer = assistEvent.relatedPlayer
            if assistPlayer != shotPlayer {
                self.assistPlayerLabel?.text = "\(assistPlayer.name) (\(assistPlayer.number))"
            } else {
                self.assistPlayerLabel?.text = "-"
            }
        } else {
            self.assistPlayerLabel?.text = "-"
        }

        let time = challenge.event.time!
        let footballPeriod = Football().periods.filter { $0.period == time.period }.first!
        if let goalTimeLabel = self.goalTimeLabel {
            let mutableAttrString = goalTimeLabel.attributedText!.mutableCopy() as! NSMutableAttributedString
            
            if let timeRange = mutableAttrString.string.range(of: "{TIME}", options: []) {
                let start = mutableAttrString.string.distance(from: mutableAttrString.string.startIndex, to: timeRange.lowerBound)
                let len = mutableAttrString.string.distance(from: timeRange.lowerBound, to: timeRange.upperBound)
                let nsrange = NSMakeRange(start, len)
                mutableAttrString.replaceCharacters(in: nsrange, with: "\(time.minute)")
            }
            
            if let periodRange = mutableAttrString.string.range(of: "{PERIOD}", options: []) {
                let start = mutableAttrString.string.distance(from: mutableAttrString.string.startIndex, to: periodRange.lowerBound)
                let len = mutableAttrString.string.distance(from: periodRange.lowerBound, to: periodRange.upperBound)
                let nsrange = NSMakeRange(start, len)
                mutableAttrString.replaceCharacters(in: nsrange, with: footballPeriod.name.lowercased())
            }
            
            goalTimeLabel.attributedText = mutableAttrString
        }
    }
    
    fileprivate func setupMatchDate(_ match: Game) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY"
        self.matchDayLabel.text = formatter.string(from: match.gameDate as Date)
    }
    
    override func show() {
        let acceptAction = AlertAction(title: "Sketch how the goal was scored") {
            self.acceptAction?()
        }
        self.appearance.allowCloseOnTouchOutside = false
        //acceptAction.title
        self.addAction(acceptAction)
        super.show()
    }
}
