//
//  RechallengeNavigationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 11/09/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveCocoa
import PLSupport

class RechallengeNavigationFlow: NSObject {
    let dataProvider = DataProvider.activeProvider
    
    fileprivate var navController: UINavigationController!
    fileprivate var challenge: Challenge!
    
    func prepareForSegue(_ segue: UIStoryboardSegue, sender: AnyObject?) {}
    
    func startWithNavigationController(_ navigationController: UINavigationController, challenge: Challenge) {
        self.navController = navigationController
        self.challenge = challenge
        self.selectUserToChallenge()
    }
    
    func selectUserToChallenge() {
        let usersVC = Storyboards.Main.instantiateUsersViewController()
        usersVC.allowSelfSelection = false
        usersVC.userSelectionDelegate = self
        usersVC.title = "Challenge for \(self.challenge.request.title)"
        self.navController.pushViewController(usersVC, animated: true)
    }
}

extension RechallengeNavigationFlow: UserSelectorDelegate {
    func didSelectUser(_ user: User, selector: UserSelector) {
        let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
        if let localUser = LocalUser.currentUser.value {
            localUser.challengeManager.rechallenge(self.challenge, toUserWithId: user.userId)
                .start { event in
                    switch event {
                    case .failed(let error):
                        NSLog("Error challenging: \(error.description)")
                        hud.hideWithErrorStatus("Error")
                    case .completed:
                        hud.hideWithSuccessStatus("Challenge sent!")
                    default:
                        break
                    }
                }
        } else {
            hud.hideWithErrorStatus("Error")
            NSLog("User must be logged in to make challenge")
        }
    }
    
    func didSelectSpecialAction(_ barButton: UIBarButtonItem) {
        if let localUser = LocalUser.currentUser.value, let challenge = self.challenge {
            let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
            localUser.challengeManager.getDynamicChallengeLink(challenge: challenge)
                .startWithValues(value: { link in
                    hud.hide(animated: true)
                    let message = NSLocalizedString("ShareReChallengeMessage", comment: "share")
                    shareDynamicChallengeLink(link, message: message, viewController: self.navController.topViewController!, sourceView: barButton)
                    
                }, failed: { error in
                    NSLog("Error challenging: \(error.description)")
                    hud.hideWithErrorStatus("Error")
                })
        }
    }
}
