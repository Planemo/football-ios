//
//  ChallengeAcceptingNavigationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift
import PLModal
import PLSupport
import AVFoundation

class H2HChallengeAcceptingViewController: StyledNavigationController {
    var challenge: Head2HeadChallenge!
    var eventBuilderNavigation: NavigationBasedBettingBuilder?
    var shownFirstTime = true
    fileprivate var challengeManager: H2HChallengeManager!
    fileprivate var user: LocalUser!
    fileprivate var audioPlayer: AVAudioPlayer?
    
    var sketchTimeoutDisposable: Disposable?
    var sketchingVC: SketchingViewController?
    var titleAttrs: [String: AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    class func askStartChallenge(_ challenge: Head2HeadChallenge, localUser: LocalUser, fromVC: UIViewController, accepted: @escaping ()->()) {
        let involvedUsers = challenge.users.map { $0.user.userId }
        if !localUser.isCurrent() || involvedUsers.find(localUser.userId) == nil {
            return
        }
        
        let alertVC = PLAlertViewController()
        let acceptAction = AlertAction(title: "Accept the challenge") {
            let challengeUI = Storyboards.FootballMain.instantiateH2HChallengeAcceptingViewController()
            challengeUI.startWithChallengeIfCan(challenge, user: localUser, fromViewController: fromVC)
            accepted()
        }
        alertVC.addAction(AlertAction(title: "Later"))
        alertVC.addAction(acceptAction)
        
        let titleAttributes: [String: AnyObject] = [
            NSForegroundColorAttributeName: UIColor.red,
            NSFontAttributeName: alertVC.appearance.titleFont!
        ]
        let attrTitle = NSMutableAttributedString(string: "Challenge: \(challenge.title)",
            attributes: titleAttributes)
        
        alertVC.attributedTitle = attrTitle
        
        let otherUser = challenge.getOtherUserInfo(localUser).user
        if let avatar = otherUser.avatarURL, let avatarURL = URL(string: avatar) {
            let imageView = UIImageView()
            imageView.sd_setImage(with: avatarURL, placeholderImage: UIImage(named: "AvatarPlaceholder"))
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            let heightConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal,
                toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40.0)
            imageView.addConstraint(heightConstraint)
            alertVC.addContentView(imageView, sideSpacing: 0)
        }
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineHeightMultiple = 0.8
        let attributes: [String: AnyObject] = [
            NSForegroundColorAttributeName: UIColor.green,
            NSFontAttributeName: alertVC.appearance.messageFont!,
            NSParagraphStyleAttributeName: paragraph
        ]
        let attrMessage = NSMutableAttributedString(string: "\(otherUser.name)\nhas challenged you for \(challenge.betAmount) chips!",
            attributes: attributes)
        let challengerAttrs: [String: Any] = [
            NSFontAttributeName: alertVC.appearance.titleFont!
        ]
        attrMessage.addAttributes(challengerAttrs, forSubstring: otherUser.name)
        attrMessage.addAttributes(challengerAttrs, forSubstring: "\(challenge.betAmount)")
        alertVC.addAttributedMessage(attrMessage)
        alertVC.show()
    }
    
    func startWithChallengeIfCan(_ challenge: Head2HeadChallenge, user: LocalUser, fromViewController vc: UIViewController) {
        user.challengeChips.value -= challenge.betAmount
        self.startWithChallenge(challenge, user: user)
        vc.present(self, animated: true, completion: nil)
    }
    
    func startWithChallenge(_ challenge: Head2HeadChallenge, user: LocalUser) {
        self.user = user
        self.challengeManager = user.h2hChallengeManager
        challengeManager.acceptChallenge(challenge.uid)
        self.challenge = challenge
        self.startEventBuilding()
    }
    
    fileprivate func startEventBuilding() {
        let match = self.challenge.match!
        let prefilledData = BettingBuildData(game: match)
        let event = self.challenge.event!
        prefilledData.mainTeam = event.relatedTeam
        prefilledData.mainPlayer = event.relatedPlayer
        
        let bettingType: BettingType
        if let relatedEventId = event.relatedEventId, let relatedEvent = match.eventWithId(relatedEventId) {
            prefilledData.assistPlayer = relatedEvent.relatedPlayer
            bettingType = BettingType.goalWithAssist
        } else {
            bettingType = BettingType.goal
        }
        
        prefilledData.eventTime = event.time
        prefilledData.youtubeLink = URL(string: "http://google.com")
        
        self.eventBuilderNavigation = NavigationBasedBettingBuilder(navController: self, contentEditor: false)
        self.eventBuilderNavigation!.delegate = self
        self.eventBuilderNavigation!.startWithBettingType(bettingType, prefilledData: prefilledData)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //show info dialog and automatically close it after some time
        if shownFirstTime {
            let soundURL = Bundle.main.url(forResource: "SoccerNoise", withExtension: "mp3")!
            let player = try! AVAudioPlayer(contentsOf: soundURL)
            player.numberOfLoops = Int.max
            player.volume = AudioManager.defaultManager.soundsMuted ? 0 : AudioManager.defaultManager.soundsVolume
            player.prepareToPlay()
            player.play()
            self.audioPlayer = player
            
            let acceptTimeoutSignal = countDownTimer(15)
            
            let challengeInfoDialog = ChallengeInfoDialog()
            challengeInfoDialog.challenge = (match: self.challenge.match!, event: self.challenge.event!)
            challengeInfoDialog.timeoutSignal = acceptTimeoutSignal
            
            let timeoutDisposable = acceptTimeoutSignal.observeCompleted {
                challengeInfoDialog.dismiss()
                self.willStartSketching()
            }
            
            challengeInfoDialog.acceptAction = {
                timeoutDisposable?.dispose()
                self.willStartSketching()
            }
            challengeInfoDialog.show()
            shownFirstTime = false
        }
    }
    
    fileprivate func willStartSketching() {
        NSLog("Will start sketching")
        let sketchTimeoutSignal = countDownTimer(15)
        self.sketchTimeoutDisposable = sketchTimeoutSignal.observe{ event in
            switch event {
            case .value(let value):
                self.topViewController?.title = "\(value)"
            case .completed:
                let alert = PLAlertViewController()
                alert.didDismissAction = {
                    self.sketchingVC?.complete()
                }
                let proceedAction = AlertAction(title: "OK")
                alert.showWithTitle("Your time is over..", message: "", actions: [proceedAction])
            default:
                break
            }
        }
    }
}

extension H2HChallengeAcceptingViewController: BettingBuilderDelegate {
    func willStartBuildComponent(_ component: AbstractBettingBuildComponent, withViewController vc: UIViewController) {
        if let sketchingVC = vc as? SketchingViewController {
            self.titleAttrs = self.navigationBar.titleTextAttributes as [String : AnyObject]?
            var newTitleAttrs = self.navigationBar.titleTextAttributes ?? [:]
            newTitleAttrs[NSForegroundColorAttributeName] = UIColor(red: 0.588, green: 0.157, blue: 0.106, alpha: 1.0)
            newTitleAttrs[NSFontAttributeName] = UIFont.boldSystemFont(ofSize: UIDevice.current.userInterfaceIdiom == .pad ? 50 : 30)
            self.navigationBar.titleTextAttributes = newTitleAttrs
            self.sketchingVC = sketchingVC
        }
    }
    
    func onBettingBuildCompleted(_ bettingType: BettingType, data: BettingBuildData) {
        if let player = self.audioPlayer {
            player.stop()
            self.audioPlayer = nil
        }
        
        self.navigationBar.titleTextAttributes = self.titleAttrs
        self.sketchingVC?.title = ""
        
        self.sketchTimeoutDisposable?.dispose()
        self.sketchingVC?.disableInteraction()
        
        let involvedUser = self.challenge.users
        var otherUserEvent: Event?
        for user in involvedUser {
            if user.user.userId != self.user.userId {
                otherUserEvent = user.resultDetails
            }
        }
        
        self.sketchingVC?.showCorrectEvent(self.challenge.event!, challengerResult: otherUserEvent)
        delay(3) {
            self.checkResults(data)
        }
    }
    
    fileprivate func checkResults(_ data: BettingBuildData) {
        let resultEvent = data.event!
        let targetEvent = self.challenge.event!
        let result = Int(100.0 * self.compareEvent(resultEvent, toEvent: targetEvent, match: data.game))
        
        let userInfo = self.challenge.getCurrentUserInfo(self.user)!
        userInfo.resultDetails = data.event
        userInfo.result = result
        
        let compareVC = Storyboards.Main.instantiateH2HCompareViewController()
        compareVC.challenge = self.challenge
        compareVC.dismissAction = {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        let navController = Storyboards.Main.instantiateViewControllerWithIdentifier("StyledNavigationController") as! StyledNavigationController
        navController.viewControllers = [compareVC]
        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(navController, animated: true, completion: nil)
        self.challengeManager.completeChallenge(self.challenge, withResult: result, drawnEvent: resultEvent).start()
    }
    
    fileprivate func compareEvent(_ lhs: Event, toEvent rhs: Event, match: Game ) -> Float {
        let distance = Float(lhs.position.distanceTo(rhs.position))
        let fullHitDistance: Float = 5
        let zeroHitDistance: Float = 35

        let result: Float
        if distance < fullHitDistance {
            result = 1
        } else if distance > zeroHitDistance {
            result = 0.05
        } else {
            let linearResult = (distance - fullHitDistance) / (zeroHitDistance - fullHitDistance)
            result = 1.0 - pow(linearResult, 1.6)
        }
        
        NSLog("Event compare: %@ to %@ = %f with distance %f", NSStringFromCGPoint(lhs.position), NSStringFromCGPoint(rhs.position), result, distance)
        
        if let lhsRelatedId = lhs.relatedEventId, let rhsRelatedId = rhs.relatedEventId {
            if let lhsRelated = match.eventWithId(lhsRelatedId), let rhsRelated = match.eventWithId(rhsRelatedId) {
                let relatedResult = self.compareEvent(lhsRelated, toEvent: rhsRelated, match: match)
                let finalResult = (relatedResult + 2 * result) / 3 //first event has more weight
                NSLog("Final compare result: %f", finalResult)
                return finalResult
            }
        }
        
        return result
    }
}
