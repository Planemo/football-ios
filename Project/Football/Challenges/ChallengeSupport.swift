//
//  ChallengeSupport.swift
//  Football
//
//  Created by Mikhail Shulepov on 05.11.15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Foundation

func shareDynamicChallengeLink(_ link: URL, message: String, viewController: UIViewController, sourceView: UIBarButtonItem) {
    let items: [AnyObject] = [message as AnyObject, link as AnyObject]
    
    let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
    if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
        if let popover = activityVC.popoverPresentationController {
            popover.barButtonItem = sourceView
        }
    }
    viewController.present(activityVC, animated: true, completion: nil)
}
