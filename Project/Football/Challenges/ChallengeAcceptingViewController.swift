//
//  ChallengeAcceptingNavigationFlow.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift
import PLModal
import PLSupport
import AVFoundation

class ChallengeAcceptingViewController: StyledNavigationController {
    var challenge: Challenge!
    var eventBuilderNavigation: NavigationBasedBettingBuilder?
    var shownFirstTime = true
    fileprivate var challengeManager: ChallengeManager!
    fileprivate var user: LocalUser!
    fileprivate var audioPlayer: AVAudioPlayer?
    
    var sketchTimeoutDisposable: Disposable?
    var sketchingVC: SketchingViewController?
    var bettingAmount: Int = BuildConfig.minChallengeBet
    var titleAttrs: [String: AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func startWithChallengeIfCan(_ challenge: Challenge, user: LocalUser, fromViewController vc: UIViewController) {
        if  user.challengeChips.value >= self.bettingAmount {
            user.challengeChips.value -= self.bettingAmount
            self.startWithChallenge(challenge, user: user)
            vc.present(self, animated: true, completion: nil)
        } else {
            ChallengeChipsAlert().showFromViewController(vc)
        }
    }
    
    func startWithChallenge(_ challenge: Challenge, user: LocalUser) {
        self.user = user
        self.challengeManager = user.challengeManager
        challengeManager.startChallenge(challenge.request.challengeId)
        self.challenge = challenge
        self.startEventBuilding()
    }
    
    fileprivate func startEventBuilding() {
        let match = self.challenge.match
        let prefilledData = BettingBuildData(game: match)
        let event = self.challenge.event
        prefilledData.mainTeam = event.relatedTeam
        prefilledData.mainPlayer = event.relatedPlayer
        
        let bettingType: BettingType
        if let relatedEventId = event.relatedEventId, let relatedEvent = match.eventWithId(relatedEventId) {
            prefilledData.assistPlayer = relatedEvent.relatedPlayer
            bettingType = BettingType.goalWithAssist
        } else {
            bettingType = BettingType.goal
        }
        
        prefilledData.eventTime = event.time
        prefilledData.youtubeLink = URL(string: "http://google.com")
        
        self.eventBuilderNavigation = NavigationBasedBettingBuilder(navController: self, contentEditor: false)
        self.eventBuilderNavigation!.delegate = self
        self.eventBuilderNavigation!.startWithBettingType(bettingType, prefilledData: prefilledData)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //show info dialog and automatically close it after some time
        if shownFirstTime {
            let soundURL = Bundle.main.url(forResource: "SoccerNoise", withExtension: "mp3")!
            let player = try! AVAudioPlayer(contentsOf: soundURL)
            player.numberOfLoops = Int.max
            player.volume = AudioManager.defaultManager.soundsMuted ? 0 : AudioManager.defaultManager.soundsVolume
            player.prepareToPlay()
            player.play()
            self.audioPlayer = player
            
            let acceptTimeoutSignal = countDownTimer(15)
            
            let challengeInfoDialog = ChallengeInfoDialog()
            challengeInfoDialog.challenge = (match: self.challenge.match, event: self.challenge.event)
            challengeInfoDialog.timeoutSignal = acceptTimeoutSignal
            
            let timeoutDisposable = acceptTimeoutSignal.observeCompleted {
                challengeInfoDialog.dismiss()
                self.willStartSketching()
            }
            
            challengeInfoDialog.acceptAction = {
                timeoutDisposable?.dispose()
                self.willStartSketching()
            }
            challengeInfoDialog.show()
            shownFirstTime = false
        }
    }
    
    fileprivate func willStartSketching() {
        NSLog("Will start sketching")
        let sketchTimeoutSignal = countDownTimer(15)
        self.sketchTimeoutDisposable = sketchTimeoutSignal.observe { event in
            switch event {
            case .value(let value):
                self.topViewController?.title = "\(value)"
            case .completed:
                let alert = PLAlertViewController()
                alert.didDismissAction = {
                    self.sketchingVC?.complete()
                }
                let proceedAction = AlertAction(title: "OK")
                alert.showWithTitle("Your time is over..", message: "", actions: [proceedAction])
            default:
                break
            }
        }
        
        /*observe(next: { value in
            self.topViewController?.title = "\(value)"
            
        }, completed: {
            let alert = PLAlertViewController()
            alert.didDismissAction = {
                self.sketchingVC?.complete()
            }
            let proceedAction = AlertAction(title: "OK")
            alert.showWithTitle("Your time is over..", message: "", actions: [proceedAction])
        })*/
    }
}

extension ChallengeAcceptingViewController: BettingBuilderDelegate {
    func willStartBuildComponent(_ component: AbstractBettingBuildComponent, withViewController vc: UIViewController) {
        if let sketchingVC = vc as? SketchingViewController {
            self.titleAttrs = self.navigationBar.titleTextAttributes as [String : AnyObject]?
            var newTitleAttrs = self.navigationBar.titleTextAttributes ?? [:]
            newTitleAttrs[NSForegroundColorAttributeName] = UIColor(red: 0.588, green: 0.157, blue: 0.106, alpha: 1.0)
            newTitleAttrs[NSFontAttributeName] = UIFont.boldSystemFont(ofSize: UIDevice.current.userInterfaceIdiom == .pad ? 50 : 30)
            self.navigationBar.titleTextAttributes = newTitleAttrs
            self.sketchingVC = sketchingVC
        }
    }
    
    func onBettingBuildCompleted(_ bettingType: BettingType, data: BettingBuildData) {
        if let player = self.audioPlayer {
            player.stop()
            self.audioPlayer = nil
        }
        
        self.navigationBar.titleTextAttributes = self.titleAttrs
        self.sketchingVC?.title = ""
        
        self.sketchTimeoutDisposable?.dispose()
        self.sketchingVC?.disableInteraction()
        self.sketchingVC?.showCorrectEvent(self.challenge.event, challengerResult: self.challenge.challengerEvent)
        delay(3) {
            self.checkResults(data)
        }
    }
    
    fileprivate func checkResults(_ data: BettingBuildData) {
        let resultEvent = data.event!
        let targetEvent = self.challenge.event
        
        let compareVC = Storyboards.Main.instantiateCompareViewController()
        compareVC.challenge = self.challenge
        
        compareVC.dismissAction = {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        let result: Float = self.compareEvent(resultEvent, toEvent: targetEvent, match: data.game)
        compareVC.percentage = result
        compareVC.resultImage = self.sketchingVC!.takeFieldSnapshot()
        
        let navController = Storyboards.Main.instantiateViewControllerWithIdentifier("StyledNavigationController") as! StyledNavigationController
        navController.viewControllers = [compareVC]
        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(navController, animated: true, completion: {
            self.completeWithResult(result, resultEvent: resultEvent)
        })
    }
    
    fileprivate func completeWithResult(_ result: Float, resultEvent: Event) {
        self.challengeManager.completeChallenge(self.challenge, withSuccess: result >= 0.5, drawnEvent: resultEvent).start()
        let rewardMultiplier: Int
        switch result {
        case let x where x < 0.5:
            rewardMultiplier = 0
        case let x where x <= 0.6:
            rewardMultiplier = 1
        case let x where x <= 0.7:
            rewardMultiplier = 2
        default:
            rewardMultiplier = 3
        }
        if rewardMultiplier != 0 {
            self.user.challengeChips.value += rewardMultiplier * self.bettingAmount
        }
    }
    
    fileprivate func compareEvent(_ lhs: Event, toEvent rhs: Event, match: Game ) -> Float {
        let distance = Float(lhs.position.distanceTo(rhs.position))
        let fullHitDistance: Float = 5
        let zeroHitDistance: Float = 35

        let result: Float
        if distance < fullHitDistance {
            result = 1
        } else if distance > zeroHitDistance {
            result = 0.05
        } else {
            let linearResult = (distance - fullHitDistance) / (zeroHitDistance - fullHitDistance)
            result = 1.0 - pow(linearResult, 1.6)
        }
        
        NSLog("Event compare: %@ to %@ = %f with distance %f", NSStringFromCGPoint(lhs.position), NSStringFromCGPoint(rhs.position), result, distance)
        
        if let lhsRelatedId = lhs.relatedEventId, let rhsRelatedId = rhs.relatedEventId {
            if let lhsRelated = match.eventWithId(lhsRelatedId), let rhsRelated = match.eventWithId(rhsRelatedId) {
                let relatedResult = self.compareEvent(lhsRelated, toEvent: rhsRelated, match: match)
                let finalResult = (relatedResult + 2 * result) / 3 //first event has more weight
                NSLog("Final compare result: %f", finalResult)
                return finalResult
            }
        }
        
        return result
    }
}
