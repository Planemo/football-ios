//
//  ChallengeNavigationFlowManager.swift
//  Football
//
//  Created by Mikhail Shulepov on 24/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveSwift
import PLModal
import PLSupport

class ChallengeMakingNavigationFlow: NSObject {
    let dataProvider = DataProvider.activeProvider
   
    fileprivate var navController: UINavigationController!
    fileprivate var selectedTeam: Team?
    fileprivate var selectedUser: User?
    
    func prepareForSegue(_ segue: UIStoryboardSegue, sender: AnyObject?) {}
    
    func startWithNavigationController(_ navigationController: UINavigationController) {
        self.navController = navigationController
        let hud = MBProgressHUD.showAdded(to: navigationController.view, animated: true)
                
        dataProvider.requestShallowTeamsInfo(filter: TeamsFilter.challenge)
            .observe(on: UIScheduler())
            .startWithValues (value: { teams in
                hud.hide(animated: true)
                
                let teamsVC = Storyboards.Main.instantiateTeamsCollectionViewSelector()
                teamsVC.title = "Select team to challenge"
                teamsVC.teams = teams
                teamsVC.allowsMultipleSelection = false
                teamsVC.selectionDelegate = self
                navigationController.pushViewController(teamsVC, animated: true)
                
            }, failed: { error in
                hud.hideWithErrorStatus("Error")
            })
    }
    
    func makeChallengeWithTeam(_ team: Team) {
        self.selectedTeam = team
        self.selectUserToChallenge()
    }
    
    func selectUserToChallenge() {
        let usersVC = Storyboards.Main.instantiateUsersViewController()
        usersVC.allowSelfSelection = true
        usersVC.userSelectionDelegate = self
        usersVC.title = "Challenge for \(self.selectedTeam!.name)"
        self.navController.pushViewController(usersVC, animated: true)
    }
}

extension ChallengeMakingNavigationFlow: TeamsSelectionDelegate {
    func didSelectTeams(_ teams: [Team], selector: TeamsSelector) {
        if let team = teams.first {
            selector.selectedTeams = []
            self.makeChallengeWithTeam(team)
        }
    }
}

extension ChallengeMakingNavigationFlow: UserSelectorDelegate {
    func didSelectUser(_ user: User, selector: UserSelector) {
        self.selectedUser = user
        if let team = self.selectedTeam, let localUser = LocalUser.currentUser.value {
            if localUser.userId == user.userId {
                loadAndStartSelfChallenge(team)
            } else {
                chooseChallengeType()
                //TODO: present chooser
//                loadAndStartSelfChallenge(team)
//                loadAndStartHead2HeadChallenge(user, team: team, betAmount: Int)
            }

        } else {
            MBProgressHUD.showHUDAddedTo(self.navController.view, withError: "Error")
            NSLog("User must be logged in to make challenge")
        }
    }
    
    func didSelectSpecialAction(_ barButton: UIBarButtonItem) {
        if let localUser = LocalUser.currentUser.value, let team = self.selectedTeam {
            let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
            localUser.challengeManager.getDynamicChallengeLink(team: team)
                .startWithValues(value: { link in
                    hud.hide(animated: true)
                    let message = NSLocalizedString("ShareChallengeTeamMessage", comment: "share")
                    shareDynamicChallengeLink(link, message: message, viewController: self.navController.topViewController!, sourceView: barButton)
                    
                }, failed: { error in
                    NSLog("Error challenging: \(error.description)")
                    hud.hideWithErrorStatus("Error")
                })
        }
    }
    
    fileprivate func loadAndstartSimpleChallenge(_ user: User, team: Team) {
        let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
        LocalUser.currentUser.value!.challengeManager.challengeUser(user.userId, withTeam: team)
            .observe(on: UIScheduler())
            .start { event in
                switch event {
                case .failed(let error):
                    NSLog("Error challenging: \(error.description)")
                    hud.hideWithErrorStatus("Error")
                case .completed:
                    hud.hideWithSuccessStatus("Challenge sent!")
                default:
                    break
                }
            }
    }
    
    fileprivate func loadAndStartSelfChallenge(_ team: Team) {
        let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
        LocalUser.currentUser.value!.challengeManager.challengeSelf(team: team)
            .observe(on: UIScheduler())
            .start { event in
                switch event {
                case .value(let challenge):
                    self.startSelfChallenge(challenge)
                case .failed(let error):
                    NSLog("Error challenging: \(error.description)")
                    hud.hideWithErrorStatus("Error")
                case .completed:
                    hud.hide(animated: true)
                default:
                    break
                }
            }
        
    }
    
    fileprivate func startSelfChallenge(_ challenge: Challenge) {
        let challengeRequest = challenge.request
        let alert = BetSelectionAlert()
        alert.title = "Challenge: \(challengeRequest.title)"
        alert.addMessage("Do you want to start this challenge?")
        alert.addAction(AlertAction(title: "Later"))
        alert.addAction(AlertAction(title: "Start now") { [weak alert] in
            let challengeUI = Storyboards.FootballMain.instantiateChallengeAcceptingViewController()
            challengeUI.bettingAmount = alert!.betAmount
            self.navController.popToRootViewController(animated: false)
            challengeUI.startWithChallengeIfCan(challenge, user: LocalUser.currentUser.value!,
                fromViewController: self.navController)
        })
        alert.show()
    }
    
    fileprivate func loadAndStartHead2HeadChallenge(_ user: User, team: Team, betAmount: Int) {
        let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
        LocalUser.currentUser.value!.h2hChallengeManager.challengeUser(user.userId, betAmount: betAmount, withTeam: team)
            .observe(on: UIScheduler())
            .start { event in
                switch event {
                case .value(let challenge):
                    self.startHead2HeadChallenge(challenge)
                case .failed(let error):
                    NSLog("Error challenging: \(error.description)")
                    hud.hideWithErrorStatus("Error")
                case .completed:
                    hud.hide(animated: true)
                default:
                    break
                }
            }
    }
    
    fileprivate func startHead2HeadChallenge(_ challenge: Head2HeadChallenge) {
        let challengeUI = Storyboards.FootballMain.instantiateH2HChallengeAcceptingViewController()
        challengeUI.startWithChallengeIfCan(challenge, user: LocalUser.currentUser.value!, fromViewController: self.navController)
    }
    
    fileprivate func chooseChallengeType() {
        let alert = PLAlertViewController()
        alert.title = "Challenge type"
        let commonChallenge = AlertAction(title: "Common challenge") {
            self.loadAndstartSimpleChallenge(self.selectedUser!, team: self.selectedTeam!)
        }
        let h2hChallenge = AlertAction(title: "Head-to-Head challenge") {
            self.makeBetForH2HChallenge()
        }
        let cancel = AlertAction(title: "Cancel")
        alert.addActions(commonChallenge, h2hChallenge, cancel)
        alert.show()
    }
    
    fileprivate func makeBetForH2HChallenge() {
        let alert = BetSelectionAlert()
        alert.title = "Choose bet amount"
        alert.addAction(AlertAction(title: "Cancel"))
        alert.addAction(AlertAction(title: "Start") { [weak alert] in
            let betAmount = alert!.betAmount
            self.loadAndStartHead2HeadChallenge(self.selectedUser!, team: self.selectedTeam!, betAmount: betAmount)
        })

        alert.show()
    }
}

