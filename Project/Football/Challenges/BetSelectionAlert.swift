//
//  ChallengeNotificationAlert.swift
//  Football
//
//  Created by Mikhail Shulepov on 13/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveSwift

class BetSelectionAlert: PLAlertViewController {
    fileprivate var betSettingView: BetAmountSettingView!
    
    var betAmount: Int {
        return self.betSettingView?.betAmount ?? 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func show() {
        self.appearance.allowCloseOnTouchOutside = false
        self.betSettingView = self.addContentViewFromNib("BetAmountSettingView") as! BetAmountSettingView
        self.betSettingView.userChips = Property(LocalUser.currentUser.value!.challengeChips)
        super.show()
    }
}
