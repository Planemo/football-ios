//
//  CompareViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLSupport
import FBAudienceNetwork
import MBProgressHUD

class ChallengeCompareViewController: UIViewController {
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var percentageLabel: UILabel!
    @IBOutlet var youtubeLinkLabel: UILabel!
    @IBOutlet var motivationLabel: UILabel!
    @IBOutlet var resultImageView: UIImageView!
    @IBOutlet var rechallengeButton: UIButton!
    @IBOutlet var opponentNameLabel: UILabel!
    
    fileprivate var interstitial: FBInterstitialAd?
    fileprivate var isInterstitialLoaded = false
    fileprivate var showInterstitialWhenLoaded = false
    
    var dismissAction: (()->())? = nil
    
    var challenge: Challenge!
    
    var percentage: Float = 0.5
    var resultImage: UIImage! {
        didSet {
            if self.isViewLoaded {
                self.resultImageView.image = self.resultImage
            }
        }
    }
    var sound: String?
    
    fileprivate var targetEvent: Event {
        return challenge.event
    }
    
    fileprivate var rechallengeFlow: RechallengeNavigationFlow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resultImageView.image = self.resultImage       

        
//        if self.victory {
//            self.resultLabel.text = "You win"
//            self.resultLabel.textColor = UIColor.greenColor()
//        } else {
//            self.resultLabel.text = "You lose"
//            self.resultLabel.textColor = UIColor.redColor()
//        }
        
        let intPercentage = Int(100 * self.percentage)
        self.percentageLabel.text = "\(intPercentage)%"
        
        if targetEvent.youtubeLink == nil {
            self.youtubeLinkLabel.isHidden = true
        }
        let linkAttrs: [String: AnyObject] = [
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue as AnyObject,
            NSUnderlineColorAttributeName: UIColor.white
        ]
        self.youtubeLinkLabel.attributedText = NSAttributedString(string: self.youtubeLinkLabel.text!, attributes: linkAttrs)
        
        let motivationText: String
        switch intPercentage {
        case let x where x >= 90:
            let variants = ["Awesome!\nYou win!"]
            motivationText = variants.randomElement()!
        case let x where x >= 70:
            let variants = ["Good job!\nYou win!"]
            motivationText = variants.randomElement()!
        default:
            let variants = ["Don't worry, you were close!\nKeep trying!", "Don't worry, next time!",
                "Luck will be on your side next time.", "That's ok, keep trying.", "Better luck next time!",
                "Don't give up!", "Don't worry, you're on the road to victory!", "Good job, you will win on the next turn.",
                "Almost! Next result will better!", "Don't worry, it's hard to remember everything.",
                "Almost! Challenge your friends too!", "So close!\nChallenge your friends!"
            ]
            motivationText = variants.randomElement()!
        }
        self.motivationLabel.text = motivationText
        
        let sound: String
        if self.percentage < 0.5 {
            sound = "Lose.mp3"
        } else if self.percentage < 0.6 {
            let cherringSound = 1 + arc4random_uniform(3)
            sound = String(format: "Cheering0%d.mp3", cherringSound)
        } else {
            sound = "Win.mp3"
        }
        AudioManager.defaultManager.playSound(sound)
        self.sound = sound
        
        if self.challenge.challengerEvent != nil {
            self.opponentNameLabel.text = self.challenge.request.challenger.name
        } else {
            self.opponentNameLabel.superview!.isHidden = true
        }
        
        if BuildConfig.adsEnabled && self.percentage < 0.5 {
            self.interstitial = FBInterstitialAd(placementID: "1628457360721437_1784959308404574")
            self.interstitial!.delegate = self
            self.interstitial!.load()
        }
    }
    
    @IBAction func close() {
        if let sound = self.sound {
            AudioManager.defaultManager.stopSounds(sound)
        }
        
        if let interstitial = self.interstitial {
            if self.isInterstitialLoaded {
                interstitial.show(fromRootViewController: self)
            } else {
                self.showInterstitialWhenLoaded = true
                
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.isUserInteractionEnabled = true
            }
            return
        }
        
        if let dismissAction = self.dismissAction {
            dismissAction()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func openYoutube() {
        if let url = self.targetEvent.youtubeLink {
            let vc = YoutubeViewController.embedInNavigationController(url)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func share(_ button: UIButton) {
        let rightItem = self.navigationItem.rightBarButtonItem
        self.navigationItem.rightBarButtonItem = nil
        button.isHidden = true
        
        let snaphostView = self.navigationController!.view
        UIGraphicsBeginImageContextWithOptions((snaphostView?.bounds.size)!, false, UIScreen.main.scale);
        snaphostView?.drawHierarchy(in: (snaphostView?.bounds)!, afterScreenUpdates: true)
        let im = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        button.isHidden = false
        self.navigationItem.rightBarButtonItem = rightItem
        
        let url = BuildConfig.shareURL
        let shareController = UIActivityViewController(activityItems: [url, im], applicationActivities: nil)
        if let presentationController = shareController.popoverPresentationController {
            presentationController.sourceView = button
        }
        self.present(shareController, animated: true, completion: nil)
    }
    
    @IBAction func rechallenge() {
        if let _ = LocalUser.currentUser.value {
            rechallengeFlow = RechallengeNavigationFlow()
            rechallengeFlow.startWithNavigationController(self.navigationController!, challenge: self.challenge)
        }
    }
}

extension ChallengeCompareViewController: FBInterstitialAdDelegate {
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        self.isInterstitialLoaded = true
        if self.showInterstitialWhenLoaded == true {
            self.close()
        }
    }
    
    func interstitialAdWillClose(_ interstitialAd: FBInterstitialAd) {
        self.interstitial = nil
        self.close()
    }
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        self.interstitial = nil
    }
}
