//
//  SocialUtils.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import FBSDKShareKit
import MBProgressHUD
import PLSupport

open class SocialUtils: NSObject {
    open class func sendFacebookFriendsInvitations(_ completion: @escaping () -> Void) {
        if FBSDKAppInviteDialog().canShow() {
            let delegate = FBInviteDelegate(completion: completion)
            let content = FBSDKAppInviteContent()
            content.appLinkURL = URL(string: "https://fb.me/1646884058878767")!
            FBSDKAppInviteDialog.show(with: content, delegate: delegate)
        } else {
            completion()
        }
    }
}

class FBInviteDelegate: NSObject, FBSDKAppInviteDialogDelegate {
    let completion: () -> ()
    var strongSelf: FBInviteDelegate?
    
    init(completion: @escaping () -> ()) {
        self.completion = completion
        super.init()
        self.strongSelf = self
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable: Any]!) {
        if let results = results {
            if let completionGesture = results["completionGesture"] as? String, completionGesture == "cancel" {
                NSLog("Invite cancelled")
            } else {
                NSLog("Invite succeed")
                
                if let user = LocalUser.currentUser.value {
                    let rewardAmount = 50
                    user.challengeChips.value += rewardAmount
                    let hud = MBProgressHUD.showGlobalHUD()
                    hud.setInfoStatus("+\(rewardAmount) chips!")
                    hud.hide(animated: true, afterDelay: 1.5)
                }
                
            }
            NSLog("Invite results:\n%@", results.description)
        } else {
            NSLog("Invite results are nil")
        }

        self.completion()
        self.strongSelf = nil
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        self.completion()
        self.strongSelf = nil
    }
}
