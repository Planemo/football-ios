//
//  SegueNavigationDelegate.swift
//  Football
//
//  Created by Mikhail Shulepov on 24/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

protocol SegueNavigationDelegate: class {
    func prepareForSegue(_ segue: UIStoryboardSegue, sender: AnyObject?)
}
