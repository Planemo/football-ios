//
//  YoutubeLinkSelectionComponent.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class YoutubeLinkSelectionComponent: AbstractBettingBuildComponent, YoutubeLinkSelectorDelegate {
    fileprivate var selectorVC: YoutubeLinkSelectorViewController? = nil
    
    init() {
        super.init(labels: [BettingDefaultLabels.YoutubeLink])
    }
    
    var youtubeLink: URL? {
        get { return self.buildData.youtubeLink as URL? }
        set { self.buildData.youtubeLink = newValue }
    }
    
    override func viewControllerWithStoryboard() -> UIViewController {
        if selectorVC == nil {
            selectorVC = Storyboards.Main.instantiateYoutubeLinkSelectorViewController()
            selectorVC!.linkSelectionDelegate = self
        }
        return selectorVC!
    }
    
    func didSelectYoutubeLink(_ link: URL, selector: YoutubeLinkSelector) {
        self.youtubeLink = link
        self.complete()
    }
}
