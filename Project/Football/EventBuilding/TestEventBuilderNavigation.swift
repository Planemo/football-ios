//
//  TestEventBuilderNavigation.swift
//  Football
//
//  Created by Mikhail Shulepov on 26/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TestEventBuilderNavigation: NSObject {
    let navigationController: UINavigationController
    let game: Game
    
    var storyboard: UIStoryboard {
        return self.navigationController.storyboard!
    }
    
    var eventType: BettingType?
    var team: Team?
    var players = [Player]()
    
    init(game: Game, navigationController: UINavigationController) {
        self.game = game
        self.navigationController = navigationController
        super.init()
    }
    
    func start() {
        let eventSelector = self.storyboard.instantiateViewControllerWithIdentifier("EventSelector") as EventTypeSelectorViewController
        eventSelector.selectionDelegate = self
        eventSelector.title = "Select event"
        if self.navigationController.viewControllers.isEmpty {
            self.navigationController.viewControllers = [eventSelector]
        } else {
            self.navigationController.pushViewController(eventSelector, animated: true)
        }
    }
    
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //nothing to do
    }
    
    private func buildEvent() -> Event {
        let goalEvent = Event(type: .Goal,
            team: self.team!,
            player: self.players.first!,
            position: CGPoint(x: 80, y: 40))
        goalEvent.outcome = true
        
        if self.eventType! == BettingType.GoalWithAssist {
            let assisPathEvent = Event(type: .Pass,
                team: self.team!,
                player: self.players.last!,
                position: CGPoint(x: 58, y: 67))
            assisPathEvent.assist = true
            assisPathEvent.outcome = true
            goalEvent.qualifications.append(.Assisted(assisPathEvent))
        }
        return goalEvent
    }
    
    private func sketchEvent(event: Event) {
        let sketchVC = self.storyboard.instantiateViewControllerWithIdentifier("EventSketching") as SketchingViewController
        sketchVC.game = self.game
        sketchVC.event = event
        sketchVC.title = "Position players"
        self.navigationController.pushViewController(sketchVC, animated: true)
    }
}

extension TestEventBuilderNavigation: EventTypeSelectionDelegate {
    func didSelectEventType(eventType: BettingType, selector: EventTypeSelector) {
        if eventType != .Goal && eventType != .GoalWithAssist {
            return
        }
        self.eventType = eventType
        
        let teamSelector = self.storyboard.instantiateViewControllerWithIdentifier("MatchTeamSelector") as MatchTeamSelectorViewController
        teamSelector.selectionDelegate = self
        teamSelector.game = self.game
        teamSelector.title = "Which team will make a goal?"
        self.navigationController.pushViewController(teamSelector, animated: true)
    }
}

extension TestEventBuilderNavigation: TeamsSelectionDelegate {
    func didSelectTeams(teams: [Team], selector: TeamsSelector) {
        self.team = teams.first
        
        if let team = self.team {
            let playerSelector = self.storyboard.instantiateViewControllerWithIdentifier("PlayersSelector") as PlayersViewController
            playerSelector.selectionDelegate = self
            playerSelector.players = team.players
            playerSelector.allowsMultipleSelection = true
            playerSelector.title = "Who will make a goal?"
            self.navigationController.pushViewController(playerSelector, animated: true)
        }
    }
}

extension TestEventBuilderNavigation: PlayersSelectionDelegate {
    func didSelectPlayers(players: [Player], selector: PlayersSelector) {
        let viewController = selector as PlayersViewController //FIXME: cast to UIViewController instead
        switch players.count {
        case 0:
            viewController.title = "Who will make a goal?"
        default:
            viewController.title = "Who will assist?"
        }
        let requiredPlayersCount = self.eventType! == BettingType.Goal ? 1 : 2
        if players.count < requiredPlayersCount {
            return
        }
        viewController.title = "Who will make a goal?"
        viewController.deselectAll()
        self.players = players
        self.sketchEvent(self.buildEvent())
    }
}