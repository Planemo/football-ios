//
//  EventComposerComponent.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

/// Builds specified event with available data from BuildData
class EventComposerComponent: AbstractBettingBuildComponent, SketchingDelegate {
    fileprivate let assisted: Bool
    fileprivate let eventType: EventType
    
    fileprivate var team: Team {
        return self.buildData.teamForLabel(BettingDefaultLabels.Team)!
    }
    
    fileprivate var game: Game {
        return self.buildData.game
    }
    
    fileprivate var firstPlayer: Player {
        return self.buildData.playerForLabel(BettingDefaultLabels.Player)!
    }
    
    fileprivate var assistPlayer: Player {
        if let assistPlayer = self.buildData.playerForLabel(BettingDefaultLabels.AssistPlayer) {
            return assistPlayer
        } else {
            return self.firstPlayer
        }
    }
    
    fileprivate var buildedEvents = [Event]()
    
    init(eventType: EventType, assisted: Bool = false) {
        self.assisted = assisted
        self.eventType = eventType
        super.init(labels: [BettingDefaultLabels.Event])
    }
    
    override func resetAnyProgress() {
        super.resetAnyProgress()
        for event in self.buildedEvents {
            self.game.removeEvent(event)
        }
        self.buildedEvents = []
    }
    
    override func viewControllerWithStoryboard() -> UIViewController {
        let sketchingVC = Storyboards.Main.instantiateEventSketching()
        sketchingVC.game = self.buildData.game
        sketchingVC.event = self.prepareEvent()
        sketchingVC.sketchingDelegate = self
        return sketchingVC
    }
    
    fileprivate func prepareEvent() -> Event! {
        switch self.eventType {
        case .goal:
            if self.assisted {
                return self.makeGoalWithAssist()
            } else {
                return self.makeGoal()
            }
        case .pass:
            return self.makePass()
        }
    }
    
    fileprivate func makeGoal() -> Event! {
        let goalEvent = Event(type: .goal,
            team: self.team,
            player: self.firstPlayer,
            position: CGPoint(x: 80, y: 40))
        goalEvent.outcome = true
        self.registerEvent(goalEvent)
        return goalEvent
    }
    
    fileprivate func makeGoalWithAssist() -> Event! {
        let goalEvent = self.makeGoal()
        
        let assisPassEvent = Event(type: .pass,
            team: self.team,
            player: self.assistPlayer,
            position: CGPoint(x: 58, y: 67))
        assisPassEvent.assist = true
        assisPassEvent.outcome = true
        
        self.registerEvent(assisPassEvent)
        
        goalEvent?.addQualifierWithType(.assisted)
        goalEvent?.addQualifierWithType(.relatedEvent, value: assisPassEvent.uid)
        
        return goalEvent
    }
    
    fileprivate func registerEvent(_ event: Event) {
        if let time = self.buildData.eventTime {
            event.time = time
        }
        if let youtubeLink = self.buildData.youtubeLink {
            event.youtubeLink = youtubeLink
        }
        self.game.addEvent(event)
        self.buildedEvents.append(event)
    }
    
    fileprivate func makePass() -> Event! {
        return nil
    }
    
    func onSketchingComplete(_ viewController: SketchingViewController, withEvent event: Event) {
        self.buildData.event = event
        self.complete()
    }
}
