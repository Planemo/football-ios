//
//  NavigationBasedBettingBuilder.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD
import ReactiveSwift

protocol BettingBuilderDelegate {
    func onBettingBuildCompleted(_ bettingType: BettingType, data: BettingBuildData)
    func willStartBuildComponent(_ component: AbstractBettingBuildComponent, withViewController: UIViewController)
}

class NavigationBasedBettingBuilder: NSObject {
    fileprivate let navController: UINavigationController
    fileprivate let initialTopViewController: UIViewController?
    fileprivate let contentEditor: Bool
    
    fileprivate var data: BettingBuildData!
    fileprivate var bettingType: BettingType?
    
    var delegate: BettingBuilderDelegate?
    var components = [AbstractBettingBuildComponent]()
    
    //navigation handling
    fileprivate var recentlyPushed: UIViewController? = nil
    fileprivate var prevNavControllerDelegate: UINavigationControllerDelegate?
    var viewControllerComponentMap = [UIViewController: Int]()
    
    var currentComponentIdx: Int? {
        for (componentVC, idx) in self.viewControllerComponentMap {
            if componentVC === self.navController.topViewController {
                return idx
            }
        }
        return nil
    }
    
    deinit {
        self.navController.delegate = self.prevNavControllerDelegate
    }
    
    init(navController: UINavigationController, contentEditor: Bool) {
        self.contentEditor = contentEditor
        self.navController = navController
        self.initialTopViewController = navController.topViewController
        super.init()
        self.prevNavControllerDelegate = self.navController.delegate
        self.navController.delegate = self
    }
    
    func close(_ animated: Bool) {
        if let initialVC = self.initialTopViewController {
            self.navController.popToViewController(initialVC, animated: animated)
        } else {
            self.navController.viewControllers = []
        }
    }
    
    func startBuilding(_ game: Game) {
        self.startBuilding(BettingBuildData(game: game))
    }
    
    func startWithBettingType(_ type: BettingType, game: Game) {
        self.startWithBettingType(type, prefilledData: BettingBuildData(game: game))
    }
    
    func startBuilding(_ prefilledData: BettingBuildData) {
        self.data = prefilledData
        
        let typeSelector = Storyboards.Main.instantiateEventSelector()
        typeSelector.selectionDelegate = self
        if self.navController.viewControllers.isEmpty {
            self.navController.viewControllers = [typeSelector]
        } else {
            self.navController.pushViewController(typeSelector, animated: true)
        }
    }
    
    func startWithBettingType(_ type: BettingType, prefilledData: BettingBuildData) {
        self.bettingType = type
        self.data = prefilledData
        
        let componentsManager = self.contentEditor
            ? BettingBuildComponentsManager.editor
            : BettingBuildComponentsManager.main
        self.components = componentsManager.componentsForBettingType(type)
        
        for component in self.components {
            component.delegate = self
            component.buildData = self.data
        }
        
        self.viewControllerComponentMap.removeAll(keepingCapacity: true)
        self.startNextComponent()
    }
    
    fileprivate var nextNotReadyComponentIdx: Int? {
        let startFrom: Int
        if let currentComponentIdx = self.currentComponentIdx {
            startFrom = currentComponentIdx + 1
        } else {
            startFrom = 0
        }
        
        //next not ready component
        if startFrom < self.components.count {
            for nextComponentIdx in startFrom..<self.components.count {
                let nextComponent = self.components[nextComponentIdx]
                if !nextComponent.isReady {
                    return nextComponentIdx
                }
            }
            return nil //all component are ready
            
        }
        
        return nil //no next component
    }
    
    fileprivate func startNextComponent() {
        if let nextComponentIdx = self.nextNotReadyComponentIdx {
            let nextComponent = self.components[nextComponentIdx]
            let pushVC = { [weak self] () -> () in
                if let strongSelf = self {
                    let nextVC = nextComponent.viewControllerWithStoryboard()
                    strongSelf.recentlyPushed = nextVC
                    strongSelf.viewControllerComponentMap[nextVC] = nextComponentIdx
                    strongSelf.navController.pushViewController(nextVC, animated: true)
                    strongSelf.delegate?.willStartBuildComponent(nextComponent, withViewController: nextVC)
                }
            }
            if let prepareAction = nextComponent.prepare() {
                let hud = MBProgressHUD.showAdded(to: self.navController.view, animated: true)
                prepareAction.observe(on: UIScheduler())
                    .start { event in
                        switch event {
                        case .failed(let error):
                            NSLog("Error: %@", error.description)
                            hud.hideWithErrorStatus("Error")
                        case .completed:
                            pushVC()
                            hud.hide(animated: true)
                        default:
                            break
                        }
                    }
            } else {
                pushVC()
            }
            
        } else {
            self.complete()
        }
    }
    
    fileprivate func complete() {
        self.delegate?.onBettingBuildCompleted(self.bettingType!, data: self.data)
    }
    
    fileprivate func resetComponentAssociatedWithViewController(_ viewController: UIViewController) {
        if let componentIdx = self.viewControllerComponentMap[viewController] {
            let component = self.components[componentIdx]
            component.resetAnyProgress()
        }
    }
}

extension NavigationBasedBettingBuilder: EventTypeSelectionDelegate {
    func didSelectEventType(_ eventType: BettingType, selector: EventTypeSelector) {
        self.startWithBettingType(eventType, prefilledData: self.data)
    }
}

extension NavigationBasedBettingBuilder: BettingBuildComponentDelegate {
    func onComponentCompleted(_ component: AbstractBettingBuildComponent) {
        startNextComponent()
    }
}

extension NavigationBasedBettingBuilder: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.prevNavControllerDelegate?.navigationController?(navigationController, willShow: viewController, animated: animated)
        
        let pushedVCs = navigationController.viewControllers
        let activeVCs = Set(self.viewControllerComponentMap.keys)
        let poppedComponentVCs = activeVCs.subtracting(pushedVCs)
        
        for poppedVC in poppedComponentVCs {
            self.resetComponentAssociatedWithViewController(poppedVC)
            self.viewControllerComponentMap.removeValue(forKey: poppedVC)
        }
        
        // reset only if it will appear after pop (we should'n reset just pushed VCs as they may have prepared some data)
        if let recentlyPushed = self.recentlyPushed, recentlyPushed == viewController {
            self.recentlyPushed = nil
        } else {
            self.resetComponentAssociatedWithViewController(viewController)
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.prevNavControllerDelegate?.navigationController?(navigationController, didShow: viewController, animated: animated)
    }
       
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.prevNavControllerDelegate?.navigationController?(navigationController, interactionControllerFor: animationController)
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.prevNavControllerDelegate?.navigationController?(navigationController, animationControllerFor: operation, from: fromVC, to: toVC)
    }

}
