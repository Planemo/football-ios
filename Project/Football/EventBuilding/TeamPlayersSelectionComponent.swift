//
//  TeamPlayersSelectionComponent.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

// Component for selecting Player(s) from specified Team
// It takes Team with specified label from BuildData
// Then it selects required amount of players from players in specified Team
// After selection it fills players in BuildData
class TeamPlayersSelectionComponent: AbstractBettingBuildComponent, PlayersSelectionDelegate {
    fileprivate let dataProvider = DataProvider.activeProvider
    
    fileprivate var playersViewController: PlayersViewController?
    fileprivate let teamLabel: String
    fileprivate let titles: [String]
    
    fileprivate var team: Team! {
        get {
            return self.buildData.teamForLabel(self.teamLabel)!
        }
        set {
            self.buildData.insertValues([newValue], forLabels: [self.teamLabel])
        }
    }
    
    class func onePlayerMainTeam() -> TeamPlayersSelectionComponent {
        return TeamPlayersSelectionComponent(
            playerLabel: BettingDefaultLabels.Player,
            teamLabel: BettingDefaultLabels.Team
        )
    }
    
    convenience init(playerLabel: String, teamLabel: String = BettingDefaultLabels.Team) {
        self.init(playerLabels: [playerLabel], teamLabel: teamLabel, titles: ["Who made a goal?"])
    }
    
    init(playerLabels: [String], teamLabel: String = BettingDefaultLabels.Team, titles: [String]) {
        self.teamLabel = teamLabel
        self.titles = titles
        super.init(labels: playerLabels)
    }
    
    override func resetAnyProgress() {
        super.resetAnyProgress()
        if let vc = self.playersViewController {
            vc.selectedPlayers = []
            self.updateTitle()
        }
    }
    
    override func prepare() -> SignalProducer<Void, NSError>? {
        if self.team.players.isEmpty {
            let game = self.buildData.game
            let loadSignal = self.dataProvider.requestFullTeamsInfo([self.team], date: game.gameDate)
            return loadSignal.attemptMap { teams -> Result<Void, NSError> in
                if let team = teams.first, !team.players.isEmpty {
                    self.team = team
                    return Result.success()
                }
                let error = NSError(domain: "TeamPlayersSelectionComponent", code: 1001, userInfo: nil)
                return Result.failure(error)
            }
        } else {
            return nil
        }
    }
    
    override func viewControllerWithStoryboard() -> UIViewController {
        if playersViewController == nil {
            playersViewController = Storyboards.Main.instantiatePlayersSelector()
            playersViewController!.selectionDelegate = self
            if self.requiredLabels.count > 1 {
                playersViewController!.allowsMultipleSelection = true
            }
        }
        playersViewController!.players = team.players
        self.updateTitle()
        return playersViewController!
    }
    
    func didSelectPlayers(_ players: [Player], selector: PlayersSelector) {
        self.updateTitle()
        let requiredPlayersCount = self.requiredLabels.count
        if requiredPlayersCount == players.count {
            self.insertRequiredPlayers(players)
            self.complete()
        }
    }
    
    func didDeselectPlayers(_ players: [Player], selector: PlayersSelector) {
        self.buildData.removeObjects(players)
        self.updateTitle()
    }
    
    func updateTitle() {
        let selectedCount = playersViewController!.selectedPlayers.count
        if selectedCount < self.titles.count {
            let title = self.titles[selectedCount]
            playersViewController!.title = title
        }
    }
}
