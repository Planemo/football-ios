//
//  MatchTeamSelectionComponent.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

/// Component for selecting team from specified game
/// It selects either Home or Away team for specified game
/// When team is selected it fills BuildData
class MatchTeamSelectionComponent: AbstractBettingBuildComponent, TeamsSelectionDelegate {
    fileprivate var teamsViewController: MatchTeamSelectorViewController? = nil
    
    init() {
        super.init(labels: [BettingBuildData.DefaultLabels.Team])
    }
    
    func didSelectTeams(_ teams: [Team], selector: TeamsSelector) {
        self.insertRequiredTeams(teams)
        self.complete()
    }
    
    override func resetAnyProgress() {
        super.resetAnyProgress()
        if let vc = teamsViewController {
            vc.selectedTeams = []
        }
    }
    
    override func viewControllerWithStoryboard() -> UIViewController {
        if teamsViewController == nil {
            teamsViewController = Storyboards.Main.instantiateMatchTeamSelector()
            teamsViewController!.selectionDelegate = self
        }
        teamsViewController!.game = self.buildData.game
        return teamsViewController!
    }
}
