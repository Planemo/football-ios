//
//  EventBuildingCore.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import UIKit.UIViewController

open class BettingBuildData {
    public struct DefaultLabels {
        static let Player = "Player"
        static let AssistPlayer = "AssistPlayer"
        
        static let Team = "Team"
        static let SecondTeam = "SecondTeam"
        
        static let Event = "Event"
        
        static let Time = "Time"
        
        static let YoutubeLink = "YoutubeLink"
    }
    
    open let game: Game
    
    // Data about teams
    open var data = [String: AnyObject]()
    
    open var labels: [String] {
        return Array(self.data.keys)
    }
    
    public init(game: Game) {
        self.game = game
    }
    
    open func teamForLabel(_ label: String) -> Team? {
        return self.data[label] as? Team
    }
    
    open func playerForLabel(_ label: String) -> Player? {
        return self.data[label] as? Player
    }
    
    open var mainTeam: Team? {
        get { return self.teamForLabel(DefaultLabels.Team) }
        set {
            self.insertOrRemoveOptValue(newValue, forKey: DefaultLabels.Team)
        }
    }
    
    open var mainPlayer: Player? {
        get { return self.playerForLabel(DefaultLabels.Player) }
        set {
            self.insertOrRemoveOptValue(newValue, forKey: DefaultLabels.Player)
        }
    }
    
    open var assistPlayer: Player? {
        get { return self.playerForLabel(DefaultLabels.AssistPlayer) }
        set {
            self.insertOrRemoveOptValue(newValue, forKey: DefaultLabels.AssistPlayer)
        }
    }
    
    open var event: Event? {
        get {
            return self.data[DefaultLabels.Event] as? Event
        }
        set {
            self.insertOrRemoveOptValue(newValue, forKey: DefaultLabels.Event)
        }
    }
       
    open var eventTime: GameTime? {
        get {
            return self.data[DefaultLabels.Time] as? GameTime
        }
        set {
            self.insertOrRemoveOptValue(newValue, forKey: DefaultLabels.Time)
        }
    }
    
    open var youtubeLink: URL? {
        get {
            return self.data[DefaultLabels.YoutubeLink] as? URL
        }
        set {
            self.insertOrRemoveOptValue(newValue as AnyObject?, forKey: DefaultLabels.YoutubeLink)
        }
    }
    
    fileprivate func insertOrRemoveOptValue(_ value: AnyObject?, forKey key: String) {
        if let value: AnyObject = value {
            self.data[key] = value
        } else {
            self.data.removeValue(forKey: key)
        }
    }
    
    open func insertValues(_ values: [AnyObject], forLabels labels: [String]) {
        for (value, label) in zip(values, labels) {
            self.data[label] = value
        }
    }
    
    open func removeLabels(_ labels: [String]) {
        for label in labels {
            self.data.removeValue(forKey: label)
        }
    }
    
    open func removeObjects(_ objects: [AnyObject]) {
        var labels: [String] = []
        for (label, insertedObject) in self.data {
            for object in objects {
                if object === insertedObject {
                    labels.append(label)
                }
            }
        }
        self.removeLabels(labels)
    }
    
    open func hasLabel(_ label: String) -> Bool {
        return self.labels.contains(label)
    }
    
    open func hasLabels(_ labels: [String]) -> Bool {
        let selfLabels = self.labels
        for label in labels {
            if !selfLabels.contains(label) {
                return false
            }
        }
        return true
    }
    
    open func leftLabels(_ labels: [String]) -> [String] {
        return labels.filter { label -> Bool in
            return !self.hasLabel(label)
        }
    }
    
    open func clone() -> BettingBuildData {
        let cloneData = BettingBuildData(game: self.game)
        for (label, value) in self.data {
            cloneData.data[label] = value
        }
        return cloneData
    }
}

typealias BettingDefaultLabels = BettingBuildData.DefaultLabels

protocol BettingBuildComponentDelegate {
    func onComponentCompleted(_ component: AbstractBettingBuildComponent)
}

class AbstractBettingBuildComponent {
    let labels: [String]
    fileprivate var notPrefilledLabels = [String]()
    var buildData: BettingBuildData! = nil {
        didSet {
            self.notPrefilledLabels = self.buildData.leftLabels(self.labels)
        }
    }
    
    var delegate: BettingBuildComponentDelegate?
    
    init(labels: [String]) {
        self.labels = labels
    }
    
    var requiredLabels: [String] {
        return self.buildData.leftLabels(self.labels)
    }
    
    func complete() {
        self.delegate?.onComponentCompleted(self)
    }
    
    func completeIfReady() {
        if self.isReady {
            self.complete()
        }
    }
    
    var isReady: Bool {
        return self.requiredLabels.isEmpty
    }
    
    // Reset any selections
    func resetAnyProgress() {
        self.buildData.removeLabels(self.notPrefilledLabels)
    }
    
    // Load all necessary data (players, teams, etc..)
    func prepare() -> SignalProducer<Void, NSError>? {
        return nil
    }
    
    // Prepare view controllers for presenting actual data
    // Called just before presenting UI
    func viewControllerWithStoryboard() -> UIViewController {
        fatalError("viewControllerWithStoryboard must be overriden by subclasses")
    }
    
    func insertRequiredTeams(_ teams: [Team]) {
        self.buildData.insertValues(teams, forLabels: self.requiredLabels)
    }
    
    func insertRequiredPlayers(_ players: [Player]) {
        self.buildData.insertValues(players, forLabels: self.requiredLabels)
    }
}

class BettingBuildComponentsManager {
    class var main: BettingBuildComponentsManager {
        struct Static {
            static let Instance = BettingBuildComponentsManager()
        }
        return Static.Instance
    }
    
    class var editor: BettingBuildComponentsManager {
        struct Static {
            static let Instance = EventEditorComponentManager()
        }
        return Static.Instance
    }
    
    func componentsForBettingType(_ type: BettingType) -> [AbstractBettingBuildComponent] {
        switch type {
        case .goal:
            return [MatchTeamSelectionComponent(),
                TimeSelectionComponent(),
                TeamPlayersSelectionComponent.onePlayerMainTeam(),
                EventComposerComponent(eventType: .goal)]
            
        case .goalWithAssist:
            let titles = ["Select goalmaker player", "Select assistance player"]
            return [MatchTeamSelectionComponent(),
                TimeSelectionComponent(),
                TeamPlayersSelectionComponent(playerLabels: [BettingDefaultLabels.Player, BettingDefaultLabels.AssistPlayer], titles: titles),
                EventComposerComponent(eventType: .goal, assisted: true)]
            
        case .goalWithDribbling:
            let titles = ["Select player"]
            return [MatchTeamSelectionComponent(),
                TimeSelectionComponent(),
                TeamPlayersSelectionComponent(playerLabels: [BettingDefaultLabels.Player], titles: titles),
                EventComposerComponent(eventType: .goal, assisted: true)]
            
//        case .Score:
//            return []// [ScoreBuilderComponent()]
            
//        case .Winner:
//            return [MatchTeamSelectionComponent()]
        }
    }
}

private class EventEditorComponentManager: BettingBuildComponentsManager {
    override func componentsForBettingType(_ type: BettingType) -> [AbstractBettingBuildComponent] {
        var components = super.componentsForBettingType(type)
        let timeComponentIdx = components.index { component -> Bool in
            return (component as? TimeSelectionComponent) != nil
        }
        if let timeComponentIdx = timeComponentIdx {
            components.insert(YoutubeLinkSelectionComponent(), at: timeComponentIdx)
        }
        return components
    }
}
