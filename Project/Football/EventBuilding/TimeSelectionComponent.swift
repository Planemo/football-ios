//
//  TimeSelectionComponent.swift
//  Football
//
//  Created by Mikhail Shulepov on 15/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class TimeSelectionComponent: AbstractBettingBuildComponent, GameTimeSelectorDelegate {
    fileprivate var timeSelectorVC: GameTimeSelectorViewController? = nil
    
    init() {
        super.init(labels: [BettingDefaultLabels.Time])
    }
    
    var time: GameTime? {
        get { return self.buildData.eventTime }
        set { self.buildData.eventTime =  newValue }
    }
    
    override func viewControllerWithStoryboard() -> UIViewController {
        if timeSelectorVC == nil {
            timeSelectorVC = Storyboards.Main.instantiateTimeSelector()
            timeSelectorVC!.selectionDelegate = self
        }
        timeSelectorVC!.periodsDefinition = Football().periods
        return timeSelectorVC!
    }
    
    func didSelectTime(_ time: GameTime, selector: GameTimeSelector) {
        self.time = time
        self.complete()
    }
}
