//
//  BuildConfig.swift
//  Football
//
//  Created by Mikhail Shulepov on 16/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public struct BuildConfig {
    public static let Parse = (
        ApplicationId: "vlRtPPrATed38Wne5uN76xadftAmwF9v2YaccVfc",
        ClientKey: "ci6LUncpdgR22IeOYv2aWtzhouhCACLFKA1BqxTU"
    )
    
    public static let minChallengeBet = 20
    
    public static let shareURL = URL(string: "https://itunes.apple.com/us/app/draw-a-goal/id1000365998")!
    
    public static let gameCenterPrefix = "com.talhayan.football."
    public static let challengesLeaderboad = "challenges"
    public static let liveMatchesLeaderboard = "livematches"
    
    #if ADS
    public static let adsEnabled = true
    #else
    public static let adsEnabled = false
    #endif
    
    public static let DebugBuild = false
    public static let noTeamEmblems = false
}
