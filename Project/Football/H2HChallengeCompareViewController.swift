//
//  CompareViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLSupport
import FBAudienceNetwork
import MBProgressHUD

class H2HChallengeCompareViewController: UIViewController {
    @IBOutlet var selfResultLabel: UILabel!
    @IBOutlet var opponentResultLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    
    @IBOutlet var youtubeLinkLabel: UILabel!
    @IBOutlet var opponentNameLabel: UILabel!
    
    var dismissAction: (()->())? = nil
    
    var challenge: Head2HeadChallenge!
    
    var sound: String?
    
    var gameField: GameFieldViewController!
    
    fileprivate var interstitial: FBInterstitialAd?
    fileprivate var isInterstitialLoaded = false
    fileprivate var showInterstitialWhenLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let localUser = LocalUser.currentUser.value!
        
        let selfResults = self.challenge.getCurrentUserInfo(localUser)!
        selfResultLabel.text = "\(selfResults.result!)%"
        
        let opponentResults = self.challenge.getOtherUserInfo(localUser)
        self.opponentNameLabel.text = opponentResults!.user.name
        
        let sound: String?
        var isLose = false
        if let opponentResult = opponentResults?.result {
            localUser.h2hChallengeManager.setViewedChallengeResults(challenge.uid)
            
            opponentResultLabel.text = "\(opponentResult)%"
            if opponentResult < selfResults.result! {
                self.statusLabel.text = "Victory!"
                localUser.challengeChips.value += 2 * self.challenge.betAmount
                sound = "Win.mp3"
                self.statusLabel.textColor = UIColor.green
                
            } else if opponentResult > selfResults.result! {
                self.statusLabel.text = "Lose"
                sound = "Lose.mp3"
                self.statusLabel.textColor = UIColor.red
                isLose = true
                
            } else {
                self.statusLabel.text = "Draw"
                localUser.challengeChips.value += self.challenge.betAmount
                sound = "Win.mp3"
            }
            
        } else {
            opponentResultLabel.text = "???"
            statusLabel.text = "Waiting..."
            sound = nil
        }
        if let sound = sound {
            AudioManager.defaultManager.playSound(sound)
            self.sound = sound
        }
        
        
        gameField.game = self.challenge.match!
        gameField.event = selfResults.resultDetails
        gameField.showCorrectEvent(self.challenge.event!, challengerResult: opponentResults!.resultDetails)
        gameField.view.isUserInteractionEnabled = false
        
        
       /*
        if targetEvent.youtubeLink == nil {
            self.youtubeLinkLabel.hidden = true
        }
        let linkAttrs: [String: AnyObject] = [
            NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
            NSUnderlineColorAttributeName: UIColor.whiteColor()
        ]
        self.youtubeLinkLabel.attributedText = NSAttributedString(string: self.youtubeLinkLabel.text!, attributes: linkAttrs)
        */
        
        if BuildConfig.adsEnabled && isLose {
            self.interstitial = FBInterstitialAd(placementID: "1628457360721437_1784959308404574")
            self.interstitial!.delegate = self
            self.interstitial!.load()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let gameFieldVC = segue.destination as? GameFieldViewController {
            self.gameField = gameFieldVC
        }
    }
    
    @IBAction func close() {
        if let sound = self.sound {
            AudioManager.defaultManager.stopSounds(sound)
        }
        
        if let interstitial = self.interstitial {
            if self.isInterstitialLoaded {
                interstitial.show(fromRootViewController: self)
            } else {
                self.showInterstitialWhenLoaded = true
                
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.isUserInteractionEnabled = true
            }
            return
        }
        
        if let dismissAction = self.dismissAction {
            dismissAction()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func openYoutube() {
        if let url = self.challenge.event?.youtubeLink {
            let vc = YoutubeViewController.embedInNavigationController(url)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func share(_ button: UIButton) {
        let rightItem = self.navigationItem.rightBarButtonItem
        self.navigationItem.rightBarButtonItem = nil
        button.isHidden = true
        
        let snaphostView = self.navigationController!.view
        UIGraphicsBeginImageContextWithOptions((snaphostView?.bounds.size)!, false, UIScreen.main.scale);
        snaphostView?.drawHierarchy(in: (snaphostView?.bounds)!, afterScreenUpdates: true)
        let im = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        button.isHidden = false
        self.navigationItem.rightBarButtonItem = rightItem
        
        let url = BuildConfig.shareURL
        let shareController = UIActivityViewController(activityItems: [url, im], applicationActivities: nil)
        if let presentationController = shareController.popoverPresentationController {
            presentationController.sourceView = button
        }
        self.present(shareController, animated: true, completion: nil)
    }
    
}

extension H2HChallengeCompareViewController: FBInterstitialAdDelegate {
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        self.isInterstitialLoaded = true
        if self.showInterstitialWhenLoaded == true {
            self.close()
        }
    }
    
    func interstitialAdWillClose(_ interstitialAd: FBInterstitialAd) {
        self.interstitial = nil
        self.close()
    }
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        self.interstitial = nil
    }
}

