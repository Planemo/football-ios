//
//  MatchesScheduleViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import MBProgressHUD
import ReactiveSwift
import PLSupport

class MatchesScheduleViewController: UIViewController, DateTimeSelectorDelegate, MatchSelector {
    fileprivate let dataProvider = DataProvider.activeProvider

    weak var selectionDelegate: MatchSelectorDelegate? = nil
    
    @IBOutlet var matchesContainer: UIView!
    @IBOutlet var noMatchesView: UIView!
    
    @IBOutlet var datePicker: PLDatePickerCollectionView!
    var matchSelector: MatchSelector?
    
    var matchesLoading: Disposable?
    
    var matches: [Game] = [] {
        didSet {
            self.prepareSchedule(self.matches)
        }
    }
    var matchesSchedule = [Date: [Game]]()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let matchesSelector = segue.destination as? MatchSelector {
            matchesSelector.selectionDelegate = self
            self.matchSelector = matchesSelector
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noMatchesView.isHidden = true
        self.datePicker.dateTimeSelectionDelegate = self
        self.loadAllFutureMatches()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.datePicker.selectedDate == nil {
            self.datePicker.selectDate(Date())
        }
    }
    
    func didPickDateTime(_ date: Date, selector: DateTimeSelector) {
        self.matchSelector?.matches = self.matchesSchedule[date] ?? []
    }
    
    func loadAllFutureMatches() {
        self.matchesLoading?.dispose()
        
        let fromDate = Date()
        let filters: [MatchFilter] = [
            MatchFilter.dateInterval(fromDate, nil)
        ]
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading"
        self.matchesLoading = self.dataProvider.requestMatchInfos(filters)
            .observe(on: UIScheduler())
            .startWithResult { [weak self] result in
                switch result {
                case .success(let matches):
                    hud.hide(animated: true)
                    self?.matches = matches
                case .failure(_):
                    hud.hideWithErrorStatus("Error")
                }
            }
    }
    
    func prepareSchedule(_ matches: [Game]) {
        self.noMatchesView.isHidden = !matches.isEmpty

        self.matchesSchedule = matches.groupBy { match -> Date in
            return (match.gameDate.beginningOfDay() as Date)
        }
        self.datePicker.dates = self.matchesSchedule.keys.sorted { lhs, rhs -> Bool in
            return lhs.isBefore(rhs)
        }
        self.datePicker.selectDate(self.datePicker.dates.first ?? Date())
    }
}

extension MatchesScheduleViewController: MatchSelectorDelegate {
    func didSelectMatch(_ match: Game, selector: MatchSelector) {
        self.selectionDelegate?.didSelectMatch(match, selector: self)
    }
}
