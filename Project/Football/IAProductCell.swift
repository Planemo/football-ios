//
//  IAProductCell.swift
//  davinci
//
//  Created by Mihail Shulepov on 01/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import PLSupport

class IAProductCell: UICollectionViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    var product: IAProduct! {
        didSet {
            setupWithProduct(self.product)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .pad {
            amountLabel.font = amountLabel.font.withSize(amountLabel.font.pointSize * 1.5)
            priceLabel.font = priceLabel.font.withSize(priceLabel.font.pointSize * 1.5)
        }
    }
    
    fileprivate func setupWithProduct(_ product: IAProduct) {
        iconView.image = product.icon
        amountLabel.text = "\(product.value ?? 0)"
        priceLabel.text = product.localizedPrice
    }
}
