//
//  TutorialViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/04/16.
//  Copyright © 2016 Planemo. All rights reserved.
//

import Foundation
import UIKit
import EAIntroView

class TutorialViewController: UIViewController, EAIntroDelegate {
    @IBOutlet var introView: EAIntroView!
    
    override func viewDidLoad() {
        self.view.layoutIfNeeded()
        self.view.backgroundColor = UIColor(white: 0.3, alpha: 1.0)
        
        let titleYPos: CGFloat  = self.view.bounds.size.height * 0.95 //bottom space
        let descYPos = self.view.bounds.size.height * 0.9 //bottom space
        
        var pages = [EAIntroPage]()
        for i in 0...9 {
            let page = EAIntroPage()
            page.title = NSLocalizedString("TutorialTitle_\(i)", comment: "")
            page.desc = NSLocalizedString("TutorialDesc_\(i)", comment: "")
            let bundlePath = Bundle.main.path(forResource: "Tutorial/Tutorial_\(i)", ofType: "jpg")
            let img = UIImage(contentsOfFile: bundlePath!)
            page.titleIconView = UIImageView(image: img)
            let imgHeight = self.view.bounds.size.height * 0.65
            let imgWidth = imgHeight * img!.size.width / img!.size.height
            page.titleIconView.frame = CGRect(x: 0, y: 0, width: imgWidth, height: imgHeight)
            page.titlePositionY = titleYPos
            page.descPositionY = descYPos
            page.titleIconPositionY = 0.6 * (self.view.bounds.size.height - page.titleIconView.frame.size.height)
            pages.append(page)
        }
        introView.pages = pages
        introView.pageControlY = 30
        introView.delegate = self
        introView.skipButtonAlignment = EAViewAlignment.left
        introView.skipButtonY = self.view.bounds.size.height
        introView.skipButton.setTitle("Done", for: .normal)
    }
    
    func introDidFinish(_ introView: EAIntroView!) {
        self.dismiss(animated: true, completion: nil)
    }
}
