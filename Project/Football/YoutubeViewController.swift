//
//  YoutubeViewController.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Bolts
import PLModal

class YoutubeViewController: UIViewController, YTPlayerViewDelegate {
    var videoUrl: URL!
    
    class func embedInNavigationController(_ url: URL) ->  UINavigationController {
        let vc = YoutubeViewController()
        vc.videoUrl = url
        
        let navController = UINavigationController(rootViewController: vc)
        navController.navigationBar.barStyle = UIBarStyle.black
        navController.view.backgroundColor = UIColor.black
        return navController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        
        let button = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(YoutubeViewController.close))
        self.navigationItem.leftBarButtonItem = button
        
        let player = YTPlayerView(frame: self.view.bounds)
        player.backgroundColor = UIColor.black
        player.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        player.delegate = self
        self.view.addSubview(player)
        
        NSLog("URL: %@", self.videoUrl.absoluteString)
        let videoId = BFURL(url: self.videoUrl).targetQueryParameters["v"] as? String
        let timeParams = self.videoUrl.absoluteString.components(separatedBy: "#t=")
        let startTime: Int
        if timeParams.count > 0 {
            startTime = Int(timeParams.last!) ?? 0
        } else {
            startTime = 0
        }
        NSLog("VideoId=%@   startTime=%d", videoId ?? "nil", startTime)
        player.load(withVideoId: videoId!, playerVars: ["playsinline": 1, "start": startTime])
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case .playing:
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        case .paused, .ended, .unknown:
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        default:
            break
        }
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        NSLog("Error playback youtube video: %d", error.rawValue)
        let alertVC = PLAlertViewController()
        alertVC.appearance.allowCloseOnTouchOutside = false
        let proceedAction = AlertAction(title: "OK")
        alertVC.showWithTitle("Error",
            message: "Sorry, video wasn't found",
            actions: [proceedAction])
    }
    
    func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
