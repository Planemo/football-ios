//
//  ProfileViewController.swift
//  Football
//
//  Created by Maxim Shmotin on 17/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage
import ReactiveSwift
import MBProgressHUD
import PLModal
import PLSupport
import FBAudienceNetwork
import Result

class ChallengeCell: UITableViewCell {
    @IBOutlet var challengerLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var resultStatus: UIImageView!
    
    var dateFormatter: DateFormatter = DateFormatter()
    
    var challengeRequest: ChallengeRequest! {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dateFormatter.dateFormat = "dd MMM YYYY"
    }
    
    fileprivate func setup() {
        let challengerName = self.challengeRequest.challenger.name
        self.dateLabel.text = self.dateFormatter.string(from: self.challengeRequest.date as Date)
        self.challengerLabel.text = "Challenge from \(challengerName)"
        if let _ = challengeRequest.result {
            resultStatus.isHidden = false
        } else {
            resultStatus.isHidden = true
        }
    }
}

class H2HChallengeCell: UITableViewCell {
    @IBOutlet var challengerLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var resultStatus: UIImageView!
    
    var dateFormatter: DateFormatter = DateFormatter()
    
    var challenge: Head2HeadChallenge! {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dateFormatter.dateFormat = "dd MMM YYYY"
    }
    
    fileprivate func setup() {
        let selfUser: Head2HeadUserInfo! = self.challenge.getCurrentUserInfo(LocalUser.currentUser.value!)
        let otherUser: Head2HeadUserInfo! = self.challenge.getOtherUserInfo(LocalUser.currentUser.value!)
        self.dateLabel.text = self.dateFormatter.string(from: self.challenge.date as Date)
        if selfUser.result == nil {
            self.challengerLabel.text = "Challenge with \(otherUser.user.name)! Your turn!"
        } else if otherUser.result == nil {
            self.challengerLabel.text = "It's \(otherUser.user.name) turn!"
        } else {
            self.challengerLabel.text = "Challenge with \(otherUser.user.name)! View results!"
        }
    }
}

class BettingCell: UITableViewCell {
    @IBOutlet var homeTeamEmblem: UIImageView!
    @IBOutlet var awayTeamEmblem: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var resultsLabel: UILabel!
    
    var dateFormatter: DateFormatter = DateFormatter()
    
    var betInfo: BetInfo! {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dateFormatter.dateFormat = "dd MMM YYYY"
    }
    
    fileprivate func setup() {
        let match = betInfo.match
        self.homeTeamEmblem.sd_setImage(with: match.homeTeam.logoURL as URL!)
        self.awayTeamEmblem.sd_setImage(with: match.awayTeam.logoURL as URL!)
        self.dateLabel.text = self.dateFormatter.string(from: match.gameDate as Date)
        
        if let result = betInfo.result {
            self.resultsLabel.text = result > 0 ? "+\(result)" : "\(result)"
            if result > 0 {
                self.resultsLabel.textColor = UIColor.green
            } else {
                self.resultsLabel.textColor = UIColor.red
            }
        } else {
            self.resultsLabel.text = "???"
            self.resultsLabel.textColor = UIColor.orange
        }
    }
}


class ProfileViewController: UIViewController {
    @IBOutlet var loggedInView: UIView?
    @IBOutlet var notLoggedInView: UIView?
    
    @IBOutlet var userAvatar: UIImageView!
    @IBOutlet var userName: UILabel!
    
    @IBOutlet var tableView: UITableView!
    
    fileprivate let deallocDisposable = CompositeDisposable()
    
    deinit {
        self.deallocDisposable.dispose()
    }
    
    fileprivate enum SectionType {
        case bets, h2HChallenges, activeChallenges
    }
    
    fileprivate class SectionLayout: AdInjectedSection {
        let type: SectionType
        
        init(type: SectionType) {
            self.type = type
        }
    }
    
    fileprivate struct Sections {
        let proposedSectionsOrder = [SectionType.bets, .h2HChallenges, .activeChallenges]
        var sections: [SectionLayout] = []
        
        init() {
            sections.push(SectionLayout(type: SectionType.bets))
            sections.push(SectionLayout(type: SectionType.h2HChallenges))
            sections.push(SectionLayout(type: SectionType.activeChallenges))
        }
        
        var count: Int {
            return sections.count
        }
        
        func getSectionIndex(_ sectionType: SectionType) -> Int? {
            return sections.index { section -> Bool in
                return section.type == sectionType
            }
        }
        
        func getSectionType(_ idx: Int) -> SectionType {
            return sections[idx].type
        }
        
        func getSectionLayout(_ idx: Int) -> SectionLayout {
            return sections[idx]
        }
    }
    
    fileprivate var sections = Sections()
    
    fileprivate var activeChallenges = [ChallengeRequest]() {
        didSet {
            if self.isViewLoaded {
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate var betInfos = [BetInfo]() {
        didSet {
            if self.isViewLoaded {
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate var h2hChallenges = [Head2HeadChallenge]() {
        didSet {
            if self.isViewLoaded {
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate var nativeAd: FBNativeAd!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userAvatar.layer.cornerRadius = 5
        self.userAvatar.layer.borderWidth = 2
        self.userAvatar.layer.borderColor = UIColor.white.cgColor
        self.userAvatar.layer.masksToBounds = true
        
        let userDisposable = LocalUser.currentUser.producer.startWithValues { [weak self] user in
            if let strongSelf = self {
                let loggedIn = user?.isLoggedInWithFacebook ?? false
                strongSelf.loggedInView?.isHidden = !loggedIn
                strongSelf.notLoggedInView?.isHidden = loggedIn
                if let user = user {
                    strongSelf.setupUserInfo(user)
                }
                strongSelf.didUpdateUser()
            }
        }
        self.deallocDisposable.add(userDisposable)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    fileprivate func didUpdateUser() {
        self.activeChallenges = []
        if let user = LocalUser.currentUser.value {
            user.updateChipsAmount().start()
            
            user.challengeManager.uncompletedChallenges()
                .observe(on: UIScheduler())
                .startWithResult { [weak self] result in
                    if case let Result.success(requests) = result {
                        self?.activeChallenges = requests
                    }
                }
        
            user.bettingsManager.getBets(30)
                .observe(on: UIScheduler())
                .startWithResult { [weak self] result in
                    if case let Result.success(betInfos) = result {
                        self?.betInfos = betInfos
                    }
                }
            
            user.h2hChallengeManager.unviewedChallenges()
                .observe(on: UIScheduler())
                .startWithResult { [weak self] result in
                    if case let Result.success(challenges) = result {
                        self?.h2hChallenges = challenges.reversed()
                    }
                }
        }
        
        if BuildConfig.adsEnabled {
            delay(TimeUnit.seconds(1)) {
                self.nativeAd = FBNativeAd(placementID: "1628457360721437_1785560708344434")
                self.nativeAd.delegate = self
                self.nativeAd.load()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func inviteFriends() {
        SocialUtils.sendFacebookFriendsInvitations { }
    }
    
    @IBAction func login() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        LocalUser.linkOrLoginWithFacebook(from: self).start { event in
            switch event {
            case .failed(_):
                hud.hideWithErrorStatus("Failed to login...")
            case .completed:
                hud.hide(animated: true)
            default:
                break
            }
        }
    }
    
    fileprivate func setupUserInfo(_ user: User) {
        self.userName.text = user.name
        if let avatarURLString = user.avatarURL, let avatarURL = URL(string: avatarURLString) {
            self.userAvatar.sd_setImage(with: avatarURL)
        } else {
            self.userAvatar.image = UIImage(named: "AvatarPlaceholder")
        }
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = sections.getSectionLayout(indexPath.section)
        
        if section.isAdPlacement(indexPath) {
            return
        }
        
        let adjustedIndexPath = section.getAdjustedIndexPath(indexPath)
        switch section.type {
        case .bets:
            //TODO:
            break
        case .activeChallenges:
            self.askStartChallenge(adjustedIndexPath.row, indexPath: indexPath, tableView: tableView)
            
        case .h2HChallenges:
            self.didSelectH2HChallenge(adjustedIndexPath.row, indexPath: indexPath, tableView: tableView)
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionLayout = sections.getSectionLayout(section)
        switch sectionLayout.type {
        case .bets: return self.betInfos.count + sectionLayout.adPlacements.count
        case .h2HChallenges: return self.h2hChallenges.count + sectionLayout.adPlacements.count
        case .activeChallenges: return self.activeChallenges.count + sectionLayout.adPlacements.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        let sectionLayout = sections.getSectionLayout(indexPath.section)
        if sectionLayout.isAdPlacement(indexPath) {
            let adCell = tableView.dequeueReusableCell(withIdentifier: "CustomAdCell") as! CustomTableViewAdCell
            let ad = sectionLayout.getNativeAd(indexPath)!
            adCell.setupWithNativeAd(ad, viewController: self)
            return adCell
        }
        
        let adjustedIndexPath = sectionLayout.getAdjustedIndexPath(indexPath)
        switch sectionLayout.type {
        case .bets: cell = self.betInfoCellWithNumber(adjustedIndexPath.row, tableView: tableView)
        case .h2HChallenges: cell = self.h2hChallengeCellWithNumber(adjustedIndexPath.row, tableView: tableView)
        case .activeChallenges: cell = self.activeChallengeCellWithNumber(adjustedIndexPath.row, tableView: tableView)
        }
        cell.backgroundColor = UIColor.clear
        cell.backgroundView?.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionLayout = sections.getSectionLayout(indexPath.section)
        if sectionLayout.isAdPlacement(indexPath) {
            return 100
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionLayout = sections.getSectionLayout(indexPath.section)
        if sectionLayout.isAdPlacement(indexPath) {
            return 100
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionType = sections.getSectionType(section)
        switch sectionType {
        case .bets: return "Your Bets"
        case .h2HChallenges: return "Head-to-Head Challenges"
        case .activeChallenges: return "Active Challenges"
        }
    }
    
    func betInfoCellWithNumber(_ number: Int, tableView: UITableView) -> BettingCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IBettingCell") as! BettingCell
        cell.betInfo = self.betInfos[number]
        return cell
    }
    
    func activeChallengeCellWithNumber(_ number: Int, tableView: UITableView) -> ChallengeCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IChallengeCell") as! ChallengeCell
        cell.challengeRequest = self.activeChallenges[number]
        return cell
    }
    
    func h2hChallengeCellWithNumber(_ number: Int, tableView: UITableView) -> H2HChallengeCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IH2HChallengeCell") as! H2HChallengeCell
        cell.challenge = self.h2hChallenges[number]
        return cell
    }
}

private extension ProfileViewController {
    func askStartChallenge(_ idx: Int, indexPath: IndexPath, tableView: UITableView) {
        if LocalUser.currentUser.value!.challengeChips.value < BuildConfig.minChallengeBet {
            ChallengeChipsAlert().showFromViewController(self)
        } else {
            let challengeRequest = self.activeChallenges[idx]
            let alert = BetSelectionAlert()
            alert.title = "Challenge: \(challengeRequest.title)"
            alert.addMessage("Do you want to start this challenge?")
            alert.addAction(AlertAction(title: "Later"))
            alert.addAction(AlertAction(title: "Start now") { [weak alert] in
                self.loadAndStartChallenge(idx, indexPath: indexPath, tableView: tableView, betAmount: alert?.betAmount ?? 20)
            })
            alert.show()
        }
    }
    
    func loadAndStartChallenge(_ idx: Int, indexPath: IndexPath, tableView: UITableView, betAmount: Int) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = self.activeChallenges[indexPath.row]
        LocalUser.currentUser.value!.challengeManager.challengeWithId(request.challengeId)
            .observe(on: UIScheduler() )
            .start { event in
                switch event {
                case .value(let challenge):
                    hud.hide(animated: true)
                    
                    tableView.beginUpdates()
                    self.activeChallenges.remove(at: idx)
                    let sectionLayout = self.sections.getSectionLayout(indexPath.section)
                    sectionLayout.didRemoveRow(indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                    tableView.endUpdates()
                    
                    let challengeUI = Storyboards.FootballMain.instantiateChallengeAcceptingViewController()
                    challengeUI.bettingAmount = betAmount
                    challengeUI.startWithChallengeIfCan(challenge, user: LocalUser.currentUser.value!, fromViewController: self)
                case .failed(_):
                     hud.hideWithErrorStatus("Error")
                default:
                    break
                }
            }
    }
}

private extension ProfileViewController {
    func didSelectH2HChallenge(_ idx: Int, indexPath: IndexPath, tableView: UITableView) {
        //load full challenge info
        let shallowChallenge = self.h2hChallenges[indexPath.row]
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        LocalUser.currentUser.value!.h2hChallengeManager.challengeWithId(shallowChallenge.uid)
            .observe(on: UIScheduler())
            .startWithResult { result in
                switch result {
                case .success(let challenge):
                    if challenge.getCurrentUserInfo(LocalUser.currentUser.value!)!.result == nil {
                        self.startH2HChallenge(challenge, idx: idx, indexPath: indexPath, tableView: tableView)
                    } else {
                        self.viewH2HChallengeResults(challenge)
                    }
                    if challenge.isCompleted {
                        self.removeH2HChallenge(idx, indexPath: indexPath, tableView: tableView)
                    }
                    hud.hide(animated: true)
                    
                case .failure(_):
                    hud.hideWithErrorStatus("Error")
                }
            }
    }
    
    func startH2HChallenge(_ challenge: Head2HeadChallenge, idx: Int, indexPath: IndexPath, tableView: UITableView) {
        H2HChallengeAcceptingViewController.askStartChallenge(challenge, localUser: LocalUser.currentUser.value!, fromVC: self) {
            if challenge.getOtherUserInfo(LocalUser.currentUser.value!).result != nil {
                self.removeH2HChallenge(idx, indexPath: indexPath, tableView: tableView)
            }
        }
    }
    
    func removeH2HChallenge(_ idx: Int, indexPath: IndexPath, tableView: UITableView) {
        tableView.beginUpdates()
        self.h2hChallenges.remove(at: idx)
        let sectionLayout = self.sections.getSectionLayout(indexPath.section)
        sectionLayout.didRemoveRow(indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    func viewH2HChallengeResults(_ challenge: Head2HeadChallenge) {
        let compareVC = Storyboards.Main.instantiateH2HCompareViewController()
        compareVC.challenge = challenge
        
        let navController = Storyboards.Main.instantiateViewControllerWithIdentifier("StyledNavigationController") as! StyledNavigationController
        navController.viewControllers = [compareVC]
        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(navController, animated: true, completion: nil)
    }
}

extension ProfileViewController: FBNativeAdDelegate {
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        let targetPos = 4
        var rowsCount = 0
        var section = 0
        for s in 0..<self.tableView.numberOfSections {
            section = s
            rowsCount += self.tableView.numberOfRows(inSection: s)
            if rowsCount >= targetPos {
                break
            }
        }
        let rowsCountBelow = rowsCount - self.tableView.numberOfRows(inSection: section)
        let idx = min(rowsCount, targetPos - rowsCountBelow)
        
        NSLog("Did load native ad")
        let sectionLayout = self.sections.getSectionLayout(section)
        sectionLayout.adPlacements[idx] = nativeAd
        self.tableView.insertRows(at: [IndexPath(row: idx, section: section)], with: UITableViewRowAnimation.automatic)
    }
    
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        NSLog("Native ad did fail: %@", (error as NSError).description)
    }

}
