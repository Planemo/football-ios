//
//  ShadowView.swift
//  davinci
//
//  Created by Mihail Shulepov on 11/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

/// Wrapper around layer for designing shadow views in interface builder
@IBDesignable
public class ShadowView: UIView {
   
    @IBInspectable
    public var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set(newValue) {
            if BuildConfig.shadowsEnabled {
                self.layer.shadowOffset = newValue
            }
        }
    }
    
    @IBInspectable
    public var shadowColor: UIColor {
        get {
            return UIColor(CGColor: self.layer.shadowColor)
        }
        set(newValue) {
            if BuildConfig.shadowsEnabled {
                self.layer.shadowColor = newValue.CGColor
            } else {
                self.layer.borderColor = UIColor.darkGrayColor().CGColor
            }
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat {
        get {
            return self.layer.shadowRadius
        }
        set(newValue) {
            if BuildConfig.shadowsEnabled {
                self.layer.shadowRadius = newValue
            } else if newValue > 0.1 {
                self.layer.borderWidth = self.borderWidth
            }
        }
    }
    
    @IBInspectable
    public var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set(newValue) {
            if BuildConfig.shadowsEnabled {
                self.layer.shadowOpacity = newValue
            }
        }
    }
    
    @IBInspectable
    public var shouldRasterize: Bool {
        get {
            return self.layer.shouldRasterize
        }
        set(newValue) {
            if BuildConfig.shadowsEnabled {
                self.layer.shouldRasterize = newValue
            }
        }
    }
    
    private func updateShadowPath(bounds: CGRect) {
        //self.layer.shadowPath = UIBezierPath(rect: bounds).CGPath
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        if !BuildConfig.shadowsEnabled {
            self.opaque = true
        }
    }
    
   /* public override var frame: CGRect {
        set {
            if BuildConfig.shadowsEnabled {
                super.frame = newValue
            } else {
                let newFrame = CGRectInset(newValue, -2, -2)
                super.frame = newFrame
            }
        }
        get {
            return super.frame
        }
    }*/
    
    public override var bounds: CGRect {
        set {
            if BuildConfig.shadowsEnabled {
                super.bounds = newValue
            } else {
                let newFrame = CGRectInset(newValue, -self.borderWidth, -self.borderWidth)
                super.bounds = newFrame
            }
        }
        get {
            return super.bounds
        }
    }
    
    private var borderWidth: CGFloat {
        return 1.0
    }
}
