//
//  AppearanceConfigurator.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal

class AppearanceConfigurator: NSObject {
    class func configure() {       
        self.configureAlerts()
        self.configureNavigationControllerItems()
    }
    
    class func configureNavigationControllerItems() {
        let buttonTitleAttrs = [
            NSFontAttributeName: UIFont(name: "OpenSans-Light", size: 18)!,
            NSForegroundColorAttributeName: UIColor.white
        ]
        UIBarButtonItem.appearance().setTitleTextAttributes(buttonTitleAttrs, for: .normal)
    }
       
    class func configureAlerts() {
        PLModal.shared.cornerRadius = 2
        PLModal.shared.blackoutColor = UIColor(white: 0, alpha: 0.72)
        
        var appearance = PLAlertViewController.appearanceProxy
        self.configureAlertButtons(&appearance)
        
        appearance.cornerRadius = 0
        appearance.titleColor = UIColor.white
        appearance.messageColor = UIColor.white
        appearance.allowAsymmetricButtonsWidth = true
        
        PLAlertViewController.Appearance.defaultXib = "DefaultAlertViewController"
        appearance.backgroundColor = UIColor.clear
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            appearance.titleFont = UIFont(name: "AvenirNext-Medium", size: 26)!
            appearance.messageFont = UIFont(name: "AvenirNext-Regular", size: 18)!
            appearance.width = 350.0
            appearance.buttonsHeight = 44
            
        } else {
            appearance.titleFont = UIFont(name: "AvenirNext-Medium", size: 31)!
            appearance.messageFont = UIFont(name: "AvenirNext-Regular", size: 23)!
            appearance.width = 400
            appearance.buttonsHeight = 52
        }
        PLAlertViewController.appearanceProxy = appearance
    }
    
    fileprivate class func configureAlertButtons(_ alertAppearance: inout AlertViewController.Appearance) {
        let borderWidth: CGFloat = 1.0
        
        let btnImg = RectImageGenerator()
        btnImg.borderWidth = 0
        btnImg.cornerRadius = 0
        
        btnImg.fillColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        let normalBackground = btnImg.generate()
        
        btnImg.fillColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.08)
        let highlightedBackground = btnImg.generate()
        
        var font: UIFont!
        if UIDevice.current.userInterfaceIdiom == .phone {
            font = UIFont(name: "AvenirNext-Regular", size: 18)!
        } else {
            font = UIFont(name: "AvenirNext-Regular", size: 23)!
        }
        
        let defaultButtonProvider = UIButtonAlertButtonProvider { button, style in
            button.titleLabel?.font = font
            
            button.setTitleColor(UIColor.white, for: .normal)
            
            button.setBackgroundImage(normalBackground, for: .normal)
            button.setBackgroundImage(highlightedBackground, for: .highlighted)
        }
        
        alertAppearance.buttonsProvider = defaultButtonProvider
        alertAppearance.interButtonsMargins = borderWidth
    }

}
