//
//  BetConfirmationDialog.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import PLModal

class BetConfirmationDialog: PLAlertViewController {
    // Teams views
    @IBOutlet var homeTeamView: ShallowTeamView!
    @IBOutlet var awayTeamView: ShallowTeamView!
    
    // Form template views
    @IBOutlet var homeTeamBackView: UIImageView?
    @IBOutlet var homeTeamShadow: UIView?
    @IBOutlet var homeTeamForegroundItemView: UIImageView?
    
    @IBOutlet var awayTeamBackView: UIImageView?
    @IBOutlet var awayTeamShadow: UIView?
    @IBOutlet var awayTeamForegroundItemView: UIImageView?
    
    // Goal details labels
    @IBOutlet var shotPlayerLabel: UILabel?
    @IBOutlet var assistPlayerLabel: UILabel?
    @IBOutlet var goalTimeLabel: UILabel!
    
    var match: Game!
    var event: Event!
    
    // Match date labels
    @IBOutlet var matchDayLabel: UILabel!

    override init() {
        super.init(nibName: "BetConfirmationDialog", bundle: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.setupMatchTeams(match)
        self.setupGoalDetails(match)
        self.setupMatchDate(match)
    }
    
    fileprivate func setupMatchTeams(_ match: Game) {
        self.homeTeamView.team = match.homeTeam
        self.awayTeamView.team = match.awayTeam
        
        let homeFormTemplate = match.homeTeam.formTemplate
        self.homeTeamBackView?.tintColor = homeFormTemplate.backgroundColor
        self.homeTeamShadow?.isHidden = !homeFormTemplate.withShadow
        
        let awayFormTemplate = match.awayTeam.formTemplate
        self.awayTeamBackView?.tintColor = awayFormTemplate.backgroundColor
        self.awayTeamShadow?.isHidden = !awayFormTemplate.withShadow
        
        self.setupForeground(homeFormTemplate.foregroundTemplate, color: homeFormTemplate.foregroundColor, forTopTeam: true)
        self.setupForeground(awayFormTemplate.foregroundTemplate, color: awayFormTemplate.foregroundColor, forTopTeam: false)
    }
    
    func setupForeground(_ template: String, color: UIColor, forTopTeam top: Bool) {
        let itemView = top ? self.homeTeamForegroundItemView! : self.awayTeamForegroundItemView!
        
        let prefix = top ? "TFT" : "BFT"
        let imageName = "\(prefix)_\(template)"
        if let image = UIImage(named: imageName) {
            itemView.image = image
            itemView.tintColor = color
            
        } else {
            NSLog("Can't create image for team template: %@", imageName)
        }
    }
    
    fileprivate func setupGoalDetails(_ match: Game) {
        let shotPlayer = self.event.relatedPlayer
        self.shotPlayerLabel?.text = "\(shotPlayer.name) (\(shotPlayer.number))"
        
        if let assistEventId = self.event.relatedEventId, let assistEvent = match.eventWithId(assistEventId) {
            let assistPlayer = assistEvent.relatedPlayer
            if assistPlayer != shotPlayer {
                self.assistPlayerLabel?.text = "\(assistPlayer.name) (\(assistPlayer.number))"
            } else {
                self.assistPlayerLabel?.text = "-"
            }
        } else {
            self.assistPlayerLabel?.text = "-"
        }
        
        let time = self.event.time!
        let footballPeriod = Football().periods.filter { $0.period == time.period }.first!
        if let goalTimeLabel = self.goalTimeLabel {
            let mutableAttrString = goalTimeLabel.attributedText!.mutableCopy() as! NSMutableAttributedString
            
            if let timeRange = mutableAttrString.string.range(of: "{TIME}", options: []) {
                let start = mutableAttrString.string.distance(from: mutableAttrString.string.startIndex, to: timeRange.lowerBound)
                let len = mutableAttrString.string.distance(from: timeRange.lowerBound, to: timeRange.upperBound)
                let nsrange = NSMakeRange(start, len)
                mutableAttrString.replaceCharacters(in: nsrange, with: "\(time.minute)")
            }
            
            if let periodRange = mutableAttrString.string.range(of: "{PERIOD}", options: []) {
                let start = mutableAttrString.string.distance(from: mutableAttrString.string.startIndex, to: periodRange.lowerBound)
                let len = mutableAttrString.string.distance(from: periodRange.lowerBound, to: periodRange.upperBound)
                let nsrange = NSMakeRange(start, len)
                mutableAttrString.replaceCharacters(in: nsrange, with: footballPeriod.name.lowercased())
            }
            
            goalTimeLabel.attributedText = mutableAttrString
        }
    }
    
    fileprivate func setupMatchDate(_ match: Game) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY"
        self.matchDayLabel.text = formatter.string(from: match.gameDate as Date)
    }
    
    func show(_ accepted: @escaping (_ betAmount: Int)->()) {
        let betSettingView = self.addContentViewFromNib("BetAmountSettingView") as! BetAmountSettingView
        self.addAction(AlertAction(title: "Cancel", style: AlertActionStyle.cancel))
        self.addAction(AlertAction(title: "Yes, make bet!") {
            accepted(betSettingView.betAmount)
        })

        super.show()
    }

}
