//
//  PFUser+ModelUser.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Parse.PFUser

extension PFUser: User {
    private static let CoinsKey = "Coins"
    
    public var userId: String {
        return self.objectId ?? "EmptyId"
    }
    
    public var coins: Int {
        return (self.objectForKey(PFUser.CoinsKey) as? Int) ?? 0
    }
    
    public func incrementCoins(amount: Int) {
        self.incrementKey(PFUser.CoinsKey, byAmount: amount)
        self.saveEventually()
    }
    
    public func decrementCoins(amount: Int) {
        self.incrementKey(PFUser.CoinsKey, byAmount: -amount)
        self.saveEventually()
    }
    
    public var name: String {
        return self.displayName ?? "???"
    }
}
