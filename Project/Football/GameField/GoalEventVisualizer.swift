//
//  GoalEventVisualizer.swift
//  Football
//
//  Created by Mikhail Shulepov on 24/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveCocoa

class GoalEventsVisualizer: NSObject, GameEventEditor {
    fileprivate var goal: (event: Event, player: GamePlayerView)! = nil
    fileprivate var assist: (event: Event, player: GamePlayerView)? = nil
    
    fileprivate var editMode = false
       
    deinit {
        self.goal.player.removeFromSuperview()
        (self.assist?.player)?.removeFromSuperview()
    }
    
    func visualizeEvent(_ event: Event, onGameField gameField: GameFieldView) -> [UIView] {
        let game = gameField.game
        
        let goalMaker = event.relatedPlayer
        let team = event.relatedTeam
        let teamSpaceCoords = event.position
        let fieldSpaceCoords = gameField.convertTeamSpacePositionToFieldSpace(teamSpaceCoords, team: team)

        //create player view
        let goalMakerView = gameField.addPlayer(goalMaker, atFieldPosition: fieldSpaceCoords)!
        
        //visualize goal
        let gatesCoords = gameField.convertTeamSpacePositionToFieldSpace(CGPoint(x: 100, y: 50), team: team)
        let goalPathView = EventPathView(from: goalMakerView, to: gatesCoords, at: gameField)
        goalPathView.pathEndOffset = 1
        let pathViews = gameField.addEventPath(goalPathView)
        
        self.goal = (event: event, player: goalMakerView)
        
        goalMakerView.fieldSpacePositionSignalProducer
            .combinePrevious(CGPoint.zero)
            .startWithValues(value: { [weak gameField] (prevPosition, position) in
                gameField?.highlightSquareAtFieldPosition(position, prevPosition: prevPosition)
                }, failed: { _ in} )
        
        //check for assistance
        var assistViews = [UIView]()
        for qualifier in event.qualifications {
            switch qualifier.type {
            case .assisted:
                let relatedEvent = (game?.eventWithId(event.relatedEventId!))!
                assistViews += self.visualizeAssistance(relatedEvent, onGameField: gameField)
            default:
                break
            }
        }
        return [goalMakerView] + pathViews + assistViews
    }
    
    func visualizeAssistance(_ event: Event, onGameField gameField: GameFieldView) -> [UIView] {
        let player = event.relatedPlayer
        let team = event.relatedTeam
        let teamSpaceCoords = event.position
        let fieldSpaceCoords = gameField.convertTeamSpacePositionToFieldSpace(teamSpaceCoords, team: team)
        
        let shooterView = self.goal.player
        let playerView = gameField.addPlayer(player, atFieldPosition: fieldSpaceCoords)!
        let eventPath = EventPathView(from: playerView, to: shooterView)
        if self.goal.event.relatedPlayer == player {
            eventPath.shapeLayer().lineDashPattern = [10, 5]
        }
        let pathViews = gameField.addEventPath(eventPath)
       
        //place title labels depending on players vertical position
        playerView.viewSpacePositionSignalProducer.combineLatest(with: shooterView.viewSpacePositionSignalProducer)
            .startWithValues { [weak playerView, weak shooterView] assisterPos, shooterPos in
                if assisterPos.y > shooterPos.y {
                    playerView?.moveLabelToBottom()
                    shooterView?.moveLabelToTop()
                } else {
                    shooterView?.moveLabelToBottom()
                    playerView?.moveLabelToTop()
                }
            }
        
        playerView.fieldSpacePositionSignalProducer
            .combinePrevious(CGPoint.zero)
            .startWithValues { [weak gameField] (prevPosition, position) in
                gameField?.highlightSquareAtFieldPosition(position, prevPosition: prevPosition)
            }
        
        self.assist = (event: event, player: playerView)
        return [playerView] + pathViews
    }
    
    func editEvent(_ event: Event, onGameField gameField: GameFieldView) -> [UIView] {
        let views = self.visualizeEvent(event, onGameField: gameField)
        let goalEvent = goal.event
        
        self.goal.player.fieldSpacePositionSignalProducer
            .map { [weak gameField] fieldSpacePosition -> CGPoint in
                let team = goalEvent.relatedTeam
                return gameField?.convertFieldSpacePositionToTeamSpace(fieldSpacePosition, team: team) ?? fieldSpacePosition
            }.startWithValues { teamSpacePosition in
                goalEvent.position = teamSpacePosition
            }
        
        PlayerHandler.setup(self.goal.player)
        
        if let assist = self.assist {
            let assistEvent = assist.event
            assist.player.fieldSpacePositionSignalProducer
                .map { [weak gameField] fieldSpacePosition -> CGPoint in
                    let team = assistEvent.relatedTeam
                    return gameField?.convertFieldSpacePositionToTeamSpace(fieldSpacePosition, team: team) ?? fieldSpacePosition
                }.startWithValues { teamSpacePosition in
                    assistEvent.position = teamSpacePosition
                }

            PlayerHandler.setup(assist.player)
        }
        return views
    }
}
