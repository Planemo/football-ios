//
//  GamePlayerView.swift
//  Football
//
//  Created by Mikhail Shulepov on 16/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import SDWebImage

class GamePlayerView: GameFieldObject {
    @IBOutlet var playerIcon: UIImageView?
    @IBOutlet var numberLabel: UILabel?
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var titleCenterConstraint: NSLayoutConstraint!
    
    var player: Player! {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.playerIcon?.tintColor = UIColor.white
        self.moveLabelToBottom()
    }
    
    fileprivate func setup() {
        self.numberLabel?.text = "\(self.player.number)"
        if let attrText = self.nameLabel?.attributedText {
            self.nameLabel?.attributedText = attrText.attributedStringByReplacingTextWith(self.player.name)
        }
    }
    
    func moveLabelToTop() {
        self.titleCenterConstraint.constant = self.bounds.size.height * 0.6
    }
    
    func moveLabelToBottom() {
        self.titleCenterConstraint.constant = -self.bounds.size.height * 0.6
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        self.playerIcon?.tintColor = self.tintColor
        self.nameLabel?.textColor = self.tintColor
        self.numberLabel?.textColor = self.tintColor
    }
}
