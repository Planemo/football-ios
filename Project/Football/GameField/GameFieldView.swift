//
//  SketchingView.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import QuartzCore
import ReactiveSwift
import PLSupport
import SDWebImage
import enum Result.NoError

class FieldDefinition {
    var image: UIImage!
    var contentModeAspectFill = true
    var corners: [CGPoint]! //in image coordinates
    
    init() {
        
    }
}

// MARK: Frame of GameField
// It simplifies management of game field in perspective projection
// (converting coordinates from GameFieldSpace <-> PerspectiveViewSpace)
@objc class GameFieldFrame: NSObject {
    let lb, lt, rb, rt: CGPoint
    let offset: CGPoint
    fileprivate let perspectiveProjection: PerspectiveProjection
    
    init(cornerPoints: [CGPoint]) {
        precondition(cornerPoints.count == 4, "Must be specified 4 points")
        let sorted = cornerPoints.sorted { (lhs, rhs) -> Bool in
            return lhs.x < rhs.x
        }
        self.lb = sorted[0]
        self.lt = sorted[1]
        self.rt = sorted[2]
        self.rb = sorted[3]
        
        let offset = CGPoint(x: self.lb.x, y: self.lt.y)
        let gameFieldSize = CGSize(width: 100, height: 100)
        let pointValues = sorted
            .map { point -> CGPoint in return point - offset }
            .map { point -> NSValue in return NSValue(cgPoint: point) }
        self.offset = offset
        self.perspectiveProjection = PerspectiveProjection(originalSize: gameFieldSize, transformedCorners: pointValues)
    }
    
    // return points in order from left-bottom to right-bottom
    var corners: [CGPoint] {
        return [lb, lt, rt, rb]
    }
    
    var middleTop: CGPoint {
        return CGPoint(
            x: lt.x + 0.5 * (self.rt.x - self.lt.x),
            y: self.lt.y
        )
    }
    
    var middleBottom: CGPoint {
        return CGPoint(
            x: lb.x + 0.5 * (self.rb.x - self.lb.x),
            y: self.lb.y
        )
    }
    
    func clampInsideViewSpace(_ point: CGPoint) -> (viewSpace: CGPoint, fieldSpace: CGPoint) {
        if self.isInsideViewSpace(point) {
            let gsPoint = self.convertToGameFieldSpace(point)
            return (viewSpace: point, fieldSpace: gsPoint)
        } else {
            var gsPoint = self.convertToGameFieldSpace(point)
            gsPoint.x = max(min(gsPoint.x, 100), 0)
            gsPoint.y = max(min(gsPoint.y, 100), 0)
            return (viewSpace: self.convertFromGameFieldSpace(gsPoint), fieldSpace: gsPoint)
        }
    }
    
    func isInsideViewSpace(_ point: CGPoint) -> Bool {
        let points = self.corners
        var inside = false
        for (i, j) in zip([0,1,2,3], [3,0,1,2]) {
            let (first, second) = (points[i], points[j])
            let betweenY = (first.y >= point.y) != (second.y >= point.y)
            
            if betweenY {
                let maxX = (second.x - first.x) * (point.y - first.y) / (second.y - first.y) + first.x
                if point.x <= maxX {
                    inside = !inside
                }
            }
        }
        return inside
    }
    
    func convertFromGameFieldSpace(_ point: CGPoint) -> CGPoint {
        let vsPoint = self.perspectiveProjection.projectOriginalPoint(point)
        return self.offset + vsPoint
    }
    
    func convertToGameFieldSpace(_ viewPoint: CGPoint) -> CGPoint {
        let offsetedPoint = viewPoint - self.offset
        return self.perspectiveProjection.perspectivePoint(toOriginal: offsetedPoint)
    }
}


// MARK: GameObject
// Objects that located inside field
class GameFieldObject: UIView {
    var anchorPoint: CGPoint = CGPoint(x: 0.5, y: 0.5)
    fileprivate weak var gameField: GameFieldView!
    
    var components = [AnyObject]()
    
    var viewSpacePosition: CGPoint {
        get {
            return self._viewSpacePositionProperty.value
        }
        set {
            let (viewSpace, fieldSpace) = self.gameField.fieldFrame.value.clampInsideViewSpace(newValue)
            self._fieldSpacePositionProperty.value = fieldSpace
            self._viewSpacePositionProperty.value = viewSpace
            self.updateFrame()
        }
    }
    
    var fieldSpacePosition: CGPoint {
        get {
            return self._fieldSpacePositionProperty.value
        }
        set {
            self._fieldSpacePositionProperty.value = newValue
            self._viewSpacePositionProperty.value = self.gameField.fieldFrame.value.convertFromGameFieldSpace(newValue)
            self.updateFrame()
        }
    }
  
    init(anchorPoint: CGPoint, contentView: UIView, gameField: GameFieldView) {
        self.anchorPoint = anchorPoint
        self.gameField = gameField
        
        super.init(frame: contentView.frame)
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.translatesAutoresizingMaskIntoConstraints = true
        contentView.frame = self.bounds
        self.addSubview(contentView)
        self.translatesAutoresizingMaskIntoConstraints = true
        self.autoresizingMask = UIViewAutoresizing()
        self.isMultipleTouchEnabled = false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.translatesAutoresizingMaskIntoConstraints = true
        self.autoresizingMask = UIViewAutoresizing()
        self.isMultipleTouchEnabled = false
    }
    
    func updateViewSpacePosition() {
        self._viewSpacePositionProperty.value = self.gameField.fieldFrame.value.convertFromGameFieldSpace(self._fieldSpacePositionProperty.value)
        self.updateFrame()
    }
    
    fileprivate func updateFrame() {
        let size = self.bounds.size
        let origin = CGPoint(
            x: self.viewSpacePosition.x - size.width * anchorPoint.x,
            y: self.viewSpacePosition.y - size.height * (1 - anchorPoint.y)
        )
        self.frame = CGRect(origin: origin, size: size)
    }
    
    fileprivate let _viewSpacePositionProperty = MutableProperty(CGPoint.zero)
    fileprivate let _fieldSpacePositionProperty = MutableProperty(CGPoint.zero)
    
    var viewSpacePositionSignalProducer: SignalProducer<CGPoint, NoError> {
        return self._viewSpacePositionProperty.producer
    }
    
    var fieldSpacePositionSignalProducer: SignalProducer<CGPoint, NoError> {
        return self._fieldSpacePositionProperty.producer
    }
}

// MARK: Player Event Line (pass, goal, dribling)

class EventPathView: ShapeView {
    enum Style {
        case upper
        case lower
    }
    
    let from: SignalProducer<CGPoint, NoError>
    let to: SignalProducer<CGPoint, NoError>
    let style: Style
    
    var pathStartOffset: CGFloat = 10
    var pathEndOffset: CGFloat = 12
    var maxHeight: CGFloat = 25
    
    var color: UIColor {
        get {
            return UIColor(cgColor: self.shapeLayer().strokeColor!)
        }
        set {
            self.shapeLayer().strokeColor = newValue.cgColor
        }
    }
    
    //Accepts RACSignals that returns coordinates in View Space
    fileprivate init(from: SignalProducer<CGPoint, NoError>, to: SignalProducer<CGPoint, NoError>, style: Style = .upper) {
        self.from = from
        self.to = to
        self.style = style
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    init(from: GameFieldObject, to: GameFieldObject, style: Style = .upper) {
        self.from = from.viewSpacePositionSignalProducer
        self.to = to.viewSpacePositionSignalProducer
        self.style = style
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    init(from: GameFieldObject, to: CGPoint, at: GameFieldView, style: Style = .upper) {
        self.from = from.viewSpacePositionSignalProducer
        self.to = at.viewSpacePositionForFieldSpacePosition(to)
        self.style = style
        super.init(frame: CGRect.zero)
        self.setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setup() {
        self.from.combineLatest(with: self.to).start { [weak self] event in
            switch event {
            case .value(let from, let to):
                self?.updatePositions(from: from, to: to)
            case .completed:
                fallthrough
            case .interrupted:
                self?.removeFromSuperview()
            default:
                break
            }
        }
        
        self.shapeLayer().strokeColor = UIColor.white.cgColor
        self.shapeLayer().lineWidth = 3
        self.shapeLayer().lineCap = kCALineCapRound
        self.shapeLayer().lineJoin = kCALineJoinRound
        self.shapeLayer().fillColor = UIColor.clear.cgColor
        self.tintColor = UIColor.white
    }
    
    fileprivate func updatePositions(from: CGPoint, to: CGPoint) {
        switch self.style {
        case .upper:
            self.drawUpperLine(from: from, to: to)
        case .lower:
            self.drawLowerLine(from: from, to: to)
        }
    }
    
    fileprivate func drawUpperLine(from: CGPoint, to: CGPoint) {
        let origin = CGPoint(x: min(from.x, to.x), y: min(from.y, to.y))
        let rawPathStart = from - origin
        let rawPathEnd = to - origin
        let pathVector = (rawPathEnd - rawPathStart)
        let pathDir = pathVector.normalized()
        let pathStart = rawPathStart//rawPathStart + pathDir * pathStartOffset
        let pathEnd = rawPathEnd - pathDir * pathEndOffset
        
        let areaSize = CGSize(width: abs(pathStart.x - pathEnd.x), height: abs(pathStart.y - pathEnd.y))
        let pathDistance = pathStart.distanceTo(pathEnd)
        
        // draw path
        let heightDistanceMultiplier = min(1.0, pathDistance / 200.0)
        let direction: CGFloat = (pathStart.y < pathEnd.y) ? 1 : -1
        let directionSmooth: CGFloat = min(1.0, abs(pathStart.y - pathEnd.y) / 20)
        let height: CGFloat = (0.2 + 0.8 * heightDistanceMultiplier) * maxHeight * direction * directionSmooth
        
        let pathMiddle = (pathStart + pathEnd) / 2
        let pathNormal = CGPoint(x: -pathDir.y, y: pathDir.x)
        let topPoint = pathMiddle + CGPoint(x: pathNormal.x * height, y: pathNormal.y * height)
        
        let vec = pathVector / 5
        
        let path = CGMutablePath()
        path.move(to: pathStart)
        path.addCurve(to: pathEnd,
                      control1: CGPoint(x: topPoint.x - vec.x, y: topPoint.y - vec.y),
                      control2: CGPoint(x: topPoint.x + vec.x, y: topPoint.y + vec.y))
        
        //draw arrow
        let arrowSize: CGFloat = min(18, pathDistance * 0.3)
        let arrowOffsets = arrowSize * 0.65
        
        let endTopDir = (pathEnd - topPoint).normalized()
        let endTopNormal = CGPoint(x: endTopDir.y, y: -endTopDir.x)
        
        let arrowStartPoint = pathEnd - (endTopDir * arrowSize) - endTopNormal * (0.1 * height)
        let dir = (arrowStartPoint - pathEnd).normalized()
        let arrowNormal = CGPoint(x: dir.y, y: -dir.x)
        let firstArrowPoint = arrowStartPoint + (arrowNormal * arrowOffsets)
        let secondArrowPoint = arrowStartPoint - (arrowNormal * arrowOffsets)

        path.move(to: firstArrowPoint)
        path.addLine(to: pathEnd)
        path.addLine(to: secondArrowPoint)
        
        self.shapeLayer().path = path
        let offsetPercentage = self.pathStartOffset / pathDistance
        self.shapeLayer().strokeStart = offsetPercentage
//        self.shapeLayer().strokeEnd = 1.0 - offsetPercentage
        
        self.frame = CGRect(origin: origin, size: areaSize)
    }
    
    fileprivate func drawLowerLine(from: CGPoint, to: CGPoint) {
        let origin = CGPoint(x: min(from.x, to.x), y: min(from.y, to.y))
        let arrowStart = from - origin
        let arrowEnd = to - origin
        let areaSize = CGSize(width: abs(arrowStart.x - arrowEnd.x), height: abs(arrowStart.y - arrowEnd.y))
                
        let path = CGMutablePath()
        path.move(to: arrowStart)
        path.addLine(to: arrowEnd)
        self.shapeLayer().path = path
        
        self.frame = CGRect(origin: origin, size: areaSize)
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        self.shapeLayer().strokeColor = self.tintColor.cgColor
    }
}

// MARK: Event Visualizer

protocol GameEventVisualizer {
    func visualizeEvent(_ event: Event, onGameField: GameFieldView) -> [UIView]
}

protocol GameEventEditor: GameEventVisualizer {
    func editEvent(_ event: Event, onGameField: GameFieldView) -> [UIView]
}

private class HighlightSquare {
    var view: UIView
    var count: Int = 1
    var position: CGPoint
    
    init(view: UIView, position: CGPoint) {
        self.view = view
        self.position = position
    }
}

// MARK: FieldView
/// It responsible just for rendering (field, players and connections)

class GameFieldView: UIView {
    var fieldDefinition: FieldDefinition! {
        didSet {
            self.setup()
        }
    }
    
    var game: Game! {
        didSet {
            self.teams = (home: self.game.homeTeam, away: self.game.awayTeam)
        }
    }
    
    let fieldFrame = MutableProperty<GameFieldFrame!>(nil)
    fileprivate var leftTeamEmblem: UIImageView!
    fileprivate var rightTeamEmblem: UIImageView!
    
    fileprivate(set) var shadowsLayer: UIView!
    fileprivate(set) var gameObjectsLayer: UIView!
    fileprivate(set) var overlayLayer: UIView!
    @IBOutlet var backgroundView: UIImageView!
//    private(set) var debugLayer: ShapeView!

    fileprivate var eventVisualizers = [GameEventEditor]()
    fileprivate var eventViews = [Event: [UIView]]()
    fileprivate var highlightSquares = [String: HighlightSquare]()
    var squaresCount = CGSize(width: 12, height: 4)
    
    func addEvent(_ event: Event) -> [UIView] {
        let editor = GoalEventsVisualizer()
        self.eventVisualizers.append(editor)
        let views = editor.visualizeEvent(event, onGameField: self)
        self.eventViews[event] = views
        return views
    }
    
    func editEvent(_ event: Event) -> [UIView] {
        let editor = GoalEventsVisualizer()
        self.eventVisualizers.append(editor)
        let views = editor.editEvent(event, onGameField: self)
        self.eventViews[event] = views
        return views
    }
    
    func viewsForEvent(_ event: Event) -> [UIView] {
        return self.eventViews[event] ?? []
    }
    
    var isTeamsSwitched = false
    var teams: (home: Team, away: Team)! {
        didSet {
            if let teams = teams {
                if BuildConfig.noTeamEmblems {
                    self.leftTeamEmblem.sd_setImage(with: URL(string: "http://api.planemostd.com/football/files/Teams/fc1_default_logo.png"))
                    self.rightTeamEmblem.sd_setImage(with: URL(string: "http://api.planemostd.com/football/files/Teams/fc2_default_logo.png"))
                } else {
                    if self.isLeftTeam(teams.home) {
                        self.leftTeamEmblem.sd_setImage(with: teams.home.logoURL as URL!)
                        self.rightTeamEmblem.sd_setImage(with: teams.away.logoURL as URL!)
                        
                    } else {
                        self.rightTeamEmblem.sd_setImage(with: teams.home.logoURL as URL!)
                        self.leftTeamEmblem.sd_setImage(with: teams.away.logoURL as URL!)
                    }
                }
                
            } else {
                self.leftTeamEmblem.image = nil
                self.rightTeamEmblem.image = nil
            }
        }
    }
    
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var bounds: CGRect {
        didSet {
            self.refreshFieldFrame()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.leftTeamEmblem = UIImageView()
        self.rightTeamEmblem = UIImageView()
        for teamEmblemView in [self.leftTeamEmblem, self.rightTeamEmblem] {
            teamEmblemView?.contentMode = .scaleAspectFit
            teamEmblemView?.translatesAutoresizingMaskIntoConstraints = true
            teamEmblemView?.autoresizingMask = UIViewAutoresizing()
            teamEmblemView?.alpha = 0.33
            self.addSubview(teamEmblemView!)
        }
        
//        self.debugLayer = ShapeView(frame: self.bounds)
        self.shadowsLayer = UIView(frame: self.bounds)
        self.gameObjectsLayer = UIView(frame: self.bounds)
        self.overlayLayer = UIView(frame: self.bounds)
        for layerView in [self.shadowsLayer, self.gameObjectsLayer, self.overlayLayer] {
            layerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            layerView?.translatesAutoresizingMaskIntoConstraints = true
            layerView?.isUserInteractionEnabled = false
            self.addSubview(layerView!)
        }
        self.gameObjectsLayer.isUserInteractionEnabled = true
    }
    
    func viewSpacePositionForFieldSpacePosition(_ position: CGPoint) -> SignalProducer<CGPoint, NoError> {
        return self.fieldFrame.producer.map { (fieldFrame: GameFieldFrame!) -> CGPoint in
            if let frame = fieldFrame {
                return frame.convertFromGameFieldSpace(position)
            } else {
                return position
            }
        }
    }
    
    func isLeftTeam(_ team: Team) -> Bool {
        if isTeamsSwitched {
            return team.name == self.teams.away.name
        } else {
            return team.name == self.teams.home.name
        }
    }
    
    func convertTeamSpacePositionToFieldSpace(_ position: CGPoint, team: Team) -> CGPoint {
        if isLeftTeam(team) {
            return CGPoint(x: position.x, y: 100 - position.y)
        } else {
            return CGPoint(x: 100 - position.x, y: position.y)
        }
    }
    
    func convertFieldSpacePositionToTeamSpace(_ position: CGPoint, team: Team) -> CGPoint {
        if isLeftTeam(team) {
            return CGPoint(x: position.x, y: 100 - position.y)
        } else {
            return CGPoint(x: 100 - position.x, y: position.y)
        }
    }
    
    func takeSnapshot() -> UIImage {
        let bounds = self.bounds
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        self.drawHierarchy(in: bounds, afterScreenUpdates: true)
        let im = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let corners = self.fieldFrame.value.corners        
        let originX: CGFloat = corners.map { $0.x }.min()
        let originY: CGFloat = corners.map { $0.y }.min()
        let maxX: CGFloat = corners.map { $0.x }.max()
        let maxY: CGFloat = corners.map { $0.y }.max()
        let size = CGSize(width: maxX - originX, height: maxY - originY)
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale);
        im?.draw(at: CGPoint(x: -originX, y: -originY))
        let croppedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return croppedImage!
    }
}

// MARK: Players management

extension GameFieldView {
    func addObject(_ object: UIView, atFieldPosition position: CGPoint, anchorPoint: CGPoint) -> GameFieldObject {
        let vsPosition = self.fieldFrame.value.convertFromGameFieldSpace(position)
        let gameObject = GameFieldObject(anchorPoint: anchorPoint, contentView: object, gameField: self)
        gameObject.viewSpacePosition = vsPosition
        self.gameObjectsLayer.addSubview(gameObject)
        return gameObject
    }
    
    func addPlayer(_ player: Player, atFieldPosition position: CGPoint) -> GamePlayerView! {
        let nibViews = Bundle.main.loadNibNamed("GamePlayerView", owner: self, options: nil)
        if let playerView = nibViews?.first as? GamePlayerView {
            playerView.player = player
            playerView.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            playerView.gameField = self
            
            let vsPosition = self.fieldFrame.value.convertFromGameFieldSpace(position)
            playerView.viewSpacePosition = vsPosition
            self.gameObjectsLayer.addSubview(playerView)
            
            return playerView
        }
        assert(false)
        return nil
    }
    
    func addEventPath(_ eventPath: EventPathView) -> [UIView] {
        if eventPath.style == .upper {
            self.overlayLayer.addSubview(eventPath)
            //let shadow = EventPathView(from: eventPath.from, to: eventPath.to, style: .Lower)
            //shadow.color = UIColor(white: 0, alpha: 0.3)
            //self.shadowsLayer.addSubview(shadow)
            //return [eventPath, shadow]
            return [eventPath]
            
        } else {
            //self.shadowsLayer.addSubview(eventPath)
            //return [eventPath]
            return []
        }
    }
    
    func highlightSquareAtFieldPosition(_ position: CGPoint, prevPosition: CGPoint) {
        let tintColor = UIColor.green
        
        let prevSquarePosition = self.squarePositionForFieldPosition(prevPosition)
        let newSquarePosition = self.squarePositionForFieldPosition(position)
        if (prevSquarePosition.x == newSquarePosition.x && prevSquarePosition.y == newSquarePosition.y) {
            return
        }
        let prevSquareKey = "\(prevSquarePosition.x)_\(prevSquarePosition.y)"
        let newSquareKey = "\(newSquarePosition.x)_\(newSquarePosition.y)"
        
        if let prevSquare =  self.highlightSquares[prevSquareKey] {
            prevSquare.count -= 1
            if prevSquare.count <= 0 {
                self.highlightSquares.removeValue(forKey: prevSquareKey)
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    prevSquare.view.alpha = 0.0
                }, completion: { completed in
                    prevSquare.view.removeFromSuperview()
                })
            }
        }
        if let newSquare = self.highlightSquares[newSquareKey] {
            newSquare.count += 1
            return
        }
        
        let newSquareView = UIView()
        newSquareView.translatesAutoresizingMaskIntoConstraints = true
        newSquareView.backgroundColor = tintColor
        newSquareView.alpha = 0.3
        self.shadowsLayer.addSubview(newSquareView)
        
        let square = HighlightSquare(view: newSquareView, position: CGPoint(x: newSquarePosition.x, y: newSquarePosition.y))
        self.highlightSquares[newSquareKey] = square
        self.updateHighlightSquare(square)
    }
    
    fileprivate func updateHighlightSquare(_ square: HighlightSquare) {
        let squareDim = CGSize(width: 100 / self.squaresCount.width, height: 100 / self.squaresCount.height)
        let position = square.position
        let leftBottomPoint = CGPoint(x: position.x * squareDim.width, y: position.y * squareDim.height)
        let points = [
            leftBottomPoint + CGPoint(x: 0, y: squareDim.height), leftBottomPoint + CGPoint(x: squareDim.width, y: squareDim.height),
            leftBottomPoint, leftBottomPoint + CGPoint(x: squareDim.width, y: 0)
        ]
        self.setupFieldView(square.view, withPoints: points, viewSize: CGSize(width: 100, height: 100))
    }
    
    fileprivate func squarePositionForFieldPosition(_ position: CGPoint) -> (x: Int, y: Int) {
        let xPos = Int(floor((position.x / 101) * squaresCount.width))
        let yPos = Int(floor((position.y / 101) * squaresCount.height))
        return (x: xPos, y: yPos)
    }
}


// MARK: Setup & Configuration

private extension GameFieldView {
    func setup() {
        self.backgroundView.image = self.fieldDefinition.image
        self.backgroundView.contentMode = self.fieldDefinition.contentModeAspectFill ? .scaleAspectFill : .scaleAspectFit
        self.refreshFieldFrame()
    }
    
    func refreshFieldFrame() {
        self.layoutIfNeeded()
        
        let imgSize = self.fieldDefinition.image.size
        let selfSize = self.backgroundView.bounds.size
        
        NSLog("Frame: %@", NSStringFromCGRect(self.backgroundView.frame))
        
        let scale: CGFloat
        if self.fieldDefinition.contentModeAspectFill {
            scale = max(selfSize.width / imgSize.width, selfSize.height / imgSize.height)
        } else {
            scale = min(selfSize.width / imgSize.width, selfSize.height / imgSize.height)
        }
        let scaledImgSize = CGSize(width: imgSize.width * scale, height: imgSize.height * scale)

        let imgSpaceCorners = self.fieldDefinition.corners.map { original -> CGPoint in
            return CGPoint(x: original.x * imgSize.width, y: original.y * imgSize.height)
        }
        
        let backOrigin = self.backgroundView.frame.origin
        let imgOffsets = CGPoint(
            x: 0.5 * (scaledImgSize.width - selfSize.width),
            y: 0.5 * (scaledImgSize.height - selfSize.height)
        ) - CGPoint(x: backOrigin.x, y: -backOrigin.y)
        
        let selfSpaceFieldCorners = imgSpaceCorners.map { original -> CGPoint in
            return CGPoint(x: original.x * scale, y: original.y * scale)
        }.map { scaled -> CGPoint in
            return CGPoint(x: scaled.x - imgOffsets.x, y: scaled.y - imgOffsets.y)
        }.map { offseted -> CGPoint in
            return CGPoint(x: offseted.x, y: selfSize.height - offseted.y)
        }
        self.fieldFrame.value = GameFieldFrame(cornerPoints: selfSpaceFieldCorners)       
        
        self.setupTeamEmblems()
        self.updateGameObjectsPositionsAndTransform()
//        self.showFieldOutline()
    }
    
    func updateGameObjectsPositionsAndTransform() {
        for subview in self.gameObjectsLayer.subviews {
            if let gameObject = subview as? GameFieldObject {
                gameObject.updateViewSpacePosition()
            }
        }
        for square in self.highlightSquares.values {
            self.updateHighlightSquare(square)
        }
    }
    
    func setupTeamEmblems() {
        for emblemView in [self.leftTeamEmblem, self.rightTeamEmblem] {
            if emblemView?.superview == nil {
                self.addSubview(emblemView!)
            }
        }
        let emblemViewSize = CGSize(width: 110, height: 136)
        let lhsPoints = [
            CGPoint(x: 21, y: 72), CGPoint(x: 42, y: 72),
            CGPoint(x: 21, y: 28), CGPoint(x: 42, y: 28)
        ]
        self.setupFieldView(self.leftTeamEmblem, withPoints: lhsPoints, viewSize: emblemViewSize)
        
        let rhsPoints = [
            CGPoint(x: 58, y: 72), CGPoint(x: 79, y: 72),
            CGPoint(x: 58, y: 28), CGPoint(x: 79, y: 28)
        ]
        self.setupFieldView(self.rightTeamEmblem, withPoints: rhsPoints, viewSize: emblemViewSize)
    }
    
    func setupFieldView(_ fieldView: UIView, withPoints points: [CGPoint], viewSize: CGSize) {
        let vsPoints = points.map { point -> CGPoint in
            return self.fieldFrame.value.convertFromGameFieldSpace(point)
        }
        let offset = CGPoint(x: vsPoints[0].x, y: vsPoints[0].y)
        let convertedPoints = vsPoints.map { point -> CGPoint in
            return CGPoint(x: point.x - offset.x, y: point.y - offset.y)
        }.map { point -> NSValue in
            return NSValue(cgPoint: point)
        }
        let size = viewSize
        let emblemPerspective = PerspectiveProjection(
            originalSize: size,
            transformedCorners: convertedPoints)
        
        fieldView.layer.anchorPoint = CGPoint(x: 0, y: 0)
        fieldView.frame = CGRect(x: offset.x, y: offset.y, width: size.width, height: size.height)
        fieldView.layer.transform = emblemPerspective!.transform3D()
    }
    
    /*private func showFieldOutline() {
        let points = self.fieldFrame.value.corners
        let path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, points[0].x, points[0].y)
        CGPathAddLineToPoint(path, nil, points[1].x, points[1].y)
        CGPathAddLineToPoint(path, nil, points[2].x, points[2].y)
        CGPathAddLineToPoint(path, nil, points[3].x, points[3].y)
        CGPathCloseSubpath(path)
        
        let layer = self.debugLayer.shapeLayer()
        layer.path = path
        layer.strokeColor = UIColor.redColor().CGColor
        layer.lineWidth = 2
        layer.fillColor = UIColor.clearColor().CGColor
    }*/
}
