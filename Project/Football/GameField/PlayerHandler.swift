//
//  PlayerHandler.swift
//  Football
//
//  Created by Mikhail Shulepov on 24/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class PlayerHandler: NSObject {
    fileprivate weak var gameObject: GameFieldObject!
    
    fileprivate var previousTouchLocation: CGPoint?
    
    class func setup(_ gameObject: GameFieldObject) {
        let _ = PlayerHandler(gameObject: gameObject)
    }
    
    init(gameObject: GameFieldObject) {
        super.init()
        self.gameObject = gameObject
        
        self.gameObject.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(PlayerHandler.handleDrag(_:)))
        gameObject.addGestureRecognizer(panGesture)
        gameObject.components.append(self)
    }
    
    func handleDrag(_ gesture: UIPanGestureRecognizer) {
        let currentLocation = gesture.location(in: gameObject.superview!)
        if gesture.state == .began {
            previousTouchLocation = currentLocation
        }
        let _ = gameObject.superview!
        let delta = currentLocation - previousTouchLocation!
        previousTouchLocation = currentLocation
        
        let newPosition = gameObject.viewSpacePosition + delta
        
        // don't place player directly under the finger
        //if gameObject.frame.maxY - currentLocation.y < 54 {
        //    gameObject.position = newPosition + CGPoint(x: 0, y: 3)
        //} else {
        gameObject.viewSpacePosition = newPosition
        //}
    }
}
