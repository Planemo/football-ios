//
//  SketchingView.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import QuartzCore

class FieldDefinition {
    var image: UIImage!
    var corners: [CGPoint]! //in image coordinates
    
    init() {
        
    }
}

// MARK: Frame of GameField
// It simplifies management of game field in perspective projection
// (converting coordinates from GameFieldSpace <-> PerspectiveViewSpace)
struct GameFieldFrame {
    let lb, lt, rb, rt: CGPoint
    let offset: CGPoint
    private let perspectiveProjection: PerspectiveProjection
    
    init(cornerPoints: [CGPoint]) {
        precondition(cornerPoints.count == 4, "Must be specified 4 points")
        let sorted = cornerPoints.sorted { (lhs, rhs) -> Bool in
            return lhs.x < rhs.x
        }
        self.lb = sorted[0]
        self.lt = sorted[1]
        self.rt = sorted[2]
        self.rb = sorted[3]
        
        let offset = CGPoint(x: self.lb.x, y: self.lt.y)
        let gameFieldSize = CGSize(width: 100, height: 100)
        let pointValues = sorted
            .map { point -> CGPoint in return point - offset }
            .map { point -> NSValue in return NSValue(CGPoint: point) }
        self.offset = offset
        self.perspectiveProjection = PerspectiveProjection(originalSize: gameFieldSize, transformedCorners: pointValues)
    }
    
    // return points in order from left-bottom to right-bottom
    var corners: [CGPoint] {
        return [lb, lt, rt, rb]
    }
    
    var middleTop: CGPoint {
        return CGPoint(
            x: lt.x + 0.5 * (self.rt.x - self.lt.x),
            y: self.lt.y
        )
    }
    
    var middleBottom: CGPoint {
        return CGPoint(
            x: lb.x + 0.5 * (self.rb.x - self.lb.x),
            y: self.lb.y
        )
    }
    
    func clampInside(point: CGPoint) -> CGPoint {
        if self.isInside(point) {
            return point
        } else {
            var gsPoint = self.convertToGameFieldSpace(point)
            gsPoint.x = max(min(gsPoint.x, 100), 0)
            gsPoint.y = max(min(gsPoint.y, 100), 0)
            return self.convertFromGameFieldSpace(gsPoint)
        }
    }
    
    func isInside(point: CGPoint) -> Bool {
        let points = self.corners
        var inside = false
        for (i, j) in Zip2([0,1,2,3], [3,0,1,2]) {
            let (first, second) = (points[i], points[j])
            let betweenY = (first.y >= point.y) != (second.y >= point.y)
            
            if betweenY {
                let maxX = (second.x - first.x) * (point.y - first.y) / (second.y - first.y) + first.x
                if point.x <= maxX {
                    inside = !inside
                }
            }
        }
        return inside
    }
    
    func convertFromGameFieldSpace(point: CGPoint) -> CGPoint {
        let vsPoint = self.perspectiveProjection.projectOriginalPoint(point)
        return self.offset + vsPoint
    }
    
    func convertToGameFieldSpace(viewPoint: CGPoint) -> CGPoint {
        let offsetedPoint = viewPoint - self.offset
        return self.perspectiveProjection.perspectivePointToOriginal(offsetedPoint)
    }
}

protocol TouchHandler {
    var priority: Int { get }
    func handleTouch(touch: UITouch, onGameObject gameObject: GameObject) -> Bool
}

// MARK: GameObject
// Objects that located inside field
class GameObject: UIView {
    enum ObjectType {
        case GamePlayer(Player)
        case Target(Team)
        case Undefined
    }
       
    let type: ObjectType
    let anchorPoint = CGPoint(x: 0.5, y: 0.5)
    private weak var gameField: GameFieldView!
    var touchHandlers = [TouchHandler]()
    
    var position: CGPoint = CGPointZero {
        didSet {
            let clamped = self.gameField.fieldFrame.clampInside(position)
            if clamped != position {
                self.position = clamped
            }
            self.updateFrame()
        }
    }
    
    init(type: ObjectType, anchorPoint: CGPoint, contentView: UIView, gameField: GameFieldView) {
        self.anchorPoint = anchorPoint
        self.gameField = gameField
        self.type = type
        super.init(frame: contentView.frame)
        contentView.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        contentView.setTranslatesAutoresizingMaskIntoConstraints(true)
        contentView.frame = CGRect(origin: CGPointZero, size: contentView.bounds.size)
        self.addSubview(contentView)
        self.setTranslatesAutoresizingMaskIntoConstraints(true)
        self.autoresizingMask = .None
        self.multipleTouchEnabled = false
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrame() {
        let size = self.bounds.size
        let origin = CGPoint(
            x: self.position.x - size.width * anchorPoint.x,
            y: self.position.y - size.height * (1 - anchorPoint.y)
        )
        self.frame = CGRect(origin: origin, size: size)
    }
    
    func addTouchHandler(handler: TouchHandler) {
        self.touchHandlers.append(handler)
        self.touchHandlers.sort { lhs, rhs -> Bool in
            return lhs.priority > rhs.priority
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.handleTouch(touches.anyObject())
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        self.handleTouch(touches.anyObject())
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        self.handleTouch(touches.anyObject())
    }
    
    override func touchesCancelled(touches: NSSet!, withEvent event: UIEvent!) {
        self.handleTouch(touches.anyObject())
    }
    
    private func handleTouch(touch: AnyObject?) {
        if let touch = touch as? UITouch {
            for handler in self.touchHandlers {
                if handler.handleTouch(touch, onGameObject: self) {
                    break
                }
            }
        }
    }
}

// MARK: FieldView
/// It responsible just for rendering (field, players and connections)

class GameFieldView: UIView {
    var fieldDefinition: FieldDefinition! {
        didSet {
            self.setup()
        }
    }
    var fieldFrame: GameFieldFrame!
    var leftTeamEmblem: UIImageView!
    var rightTeamEmblem: UIImageView!
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var bounds: CGRect {
        didSet {
            self.setup()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.leftTeamEmblem = UIImageView()
        self.rightTeamEmblem = UIImageView()
        for teamEmblemView in [self.leftTeamEmblem, self.rightTeamEmblem] {
            teamEmblemView.contentMode = .ScaleAspectFit
            teamEmblemView.setTranslatesAutoresizingMaskIntoConstraints(true)
            teamEmblemView.autoresizingMask = .None
            teamEmblemView.alpha = 0.33
            self.addSubview(teamEmblemView)
        }
    }
}

// MARK: Players management

extension GameFieldView {
    func addObject(object: UIView, type: GameObject.ObjectType, atFieldPosition position: CGPoint, anchorPoint: CGPoint) -> GameObject {
        let vsPosition = self.fieldFrame.convertFromGameFieldSpace(position)
        let gameObject = GameObject(type: type, anchorPoint: anchorPoint, contentView: object, gameField: self)
        gameObject.position = vsPosition
        self.addSubview(gameObject)
        return gameObject
    }
}


// MARK: Setup & Configuration

private extension GameFieldView {
    private func setup() {
        for subview in self.subviews as [UIView] {
            if subview != self.leftTeamEmblem && subview != self.rightTeamEmblem {
                subview.removeFromSuperview()
            }
        }
        
        let fieldBackgroundView = UIImageView(image: self.fieldDefinition.image)
        fieldBackgroundView.contentMode = .ScaleAspectFill
        fieldBackgroundView.frame = self.bounds
        fieldBackgroundView.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        fieldBackgroundView.setTranslatesAutoresizingMaskIntoConstraints(true)
        self.insertSubview(fieldBackgroundView, atIndex: 0)
        
        self.refreshFieldFrame()
        self.showFieldOutline()
    }
    
    private func refreshFieldFrame() {
        let imgSpaceCorners = self.fieldDefinition.corners
        
        let imgSize = self.fieldDefinition.image.size
        let selfSize = self.bounds.size
        let scale = max(selfSize.width / imgSize.width, selfSize.height / imgSize.height)
        let scaledImgSize = CGSize(width: imgSize.width * scale, height: imgSize.height * scale)
        let imgOffsets = CGPoint(
            x: 0.5 * (scaledImgSize.width - selfSize.width),
            y: 0.5 * (scaledImgSize.height - selfSize.height)
        )
        let selfSpaceFieldCorners = imgSpaceCorners.map { original -> CGPoint in
                return CGPoint(x: original.x * scale, y: original.y * scale)
            }.map { scaled -> CGPoint in
                return CGPoint(x: scaled.x - imgOffsets.x, y: scaled.y - imgOffsets.y)
            }.map { offseted -> CGPoint in
                return CGPoint(x: offseted.x, y: selfSize.height - offseted.y)
            }
        self.fieldFrame = GameFieldFrame(cornerPoints: selfSpaceFieldCorners)
        
        let lhsPoints = [
            CGPoint(x: 17, y: 80), CGPoint(x: 42, y: 80),
            CGPoint(x: 17, y: 20), CGPoint(x: 42, y: 20)
        ]
        self.setupEmblem(self.leftTeamEmblem, withPoints: lhsPoints)
        
        let rhsPoints = [
            CGPoint(x: 58, y: 80), CGPoint(x: 83, y: 80),
            CGPoint(x: 58, y: 20), CGPoint(x: 83, y: 20)
        ]
        self.setupEmblem(self.rightTeamEmblem, withPoints: rhsPoints)
    }
    
    private func setupEmblem(emblem: UIImageView, withPoints points: [CGPoint]) {
        let vsPoints = points.map { point -> CGPoint in
            return self.fieldFrame.convertFromGameFieldSpace(point)
        }
        let offset = CGPoint(x: vsPoints[0].x, y: vsPoints[0].y)
        
        let convertedPoints = vsPoints.map { point -> CGPoint in
            return CGPoint(x: point.x - offset.x, y: point.y - offset.y)
        }.map { point -> NSValue in
            return NSValue(CGPoint: point)
        }
        let size = CGSize(width: 110, height: 136)
        let lhsEmblemPerspective = PerspectiveProjection(
            originalSize: size,
            transformedCorners: convertedPoints)
        
        emblem.layer.anchorPoint = CGPoint(x: 0, y: 0)
        emblem.frame = CGRectMake(offset.x, offset.y, size.width, size.height)
        emblem.layer.transform = lhsEmblemPerspective.transform3D()
    }
    
    private func showFieldOutline() {
        let points = self.fieldFrame.corners
        let path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, points[0].x, points[0].y)
        CGPathAddLineToPoint(path, nil, points[1].x, points[1].y)
        CGPathAddLineToPoint(path, nil, points[2].x, points[2].y)
        CGPathAddLineToPoint(path, nil, points[3].x, points[3].y)
        CGPathCloseSubpath(path)
        
        let view = ShapeView(frame: self.bounds)
        view.backgroundColor = UIColor.clearColor()
        
        let layer = view.layer as CAShapeLayer
        layer.path = path
        layer.strokeColor = UIColor.redColor().CGColor
        layer.lineWidth = 2
        layer.fillColor = UIColor.clearColor().CGColor
        
        view.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        view.setTranslatesAutoresizingMaskIntoConstraints(true)
        self.addSubview(view)
    }
}
