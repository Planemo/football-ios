//
//  SketchingGameFieldView.swift
//  Football
//
//  Created by Mikhail Shulepov on 17/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import ReactiveCocoa

class SketchingGameFieldView: GameFieldView {
    
    // MARK: Draggable behavior for GameObjects
    // Make GameObjects draggable
    class DragTouchHandler: NSObject, TouchHandler {
        var priority: Int { return 0 }
        
        func handleTouch(touch: UITouch, onGameObject gameObject: GameObject) -> Bool {
            if touch.phase != .Moved {
                return false
            }
            let superview = gameObject.superview!
            let currentLocation = touch.locationInView(superview)
            let previousLocation = touch.previousLocationInView(superview)
            let delta = CGPoint(
                x: currentLocation.x - previousLocation.x,
                y: currentLocation.y - previousLocation.y
            )
            self.moveGameObject(gameObject, byDelta: delta)
            return false
        }
        
        private func moveGameObject(gameObject: GameObject, byDelta delta: CGPoint) {
            gameObject.position += delta
        }
    }
    
    // MARK: Connectable behavior for GameObjects
    // GameObjects that can be connected to each other
    class ConnectionTouchHandler: NSObject, TouchHandler {
        private var isLongPressed = false
        private enum State {
            case None
            case Pending(time: NSTimeInterval, location: CGPoint)
            case Succeed
            case Failed
        }
        private var state = State.None
        
        var longTapDuration: NSTimeInterval = 0.5
        var priority: Int { return 10 }
        
        func handleTouch(touch: UITouch, onGameObject gameObject: GameObject) -> Bool {
            switch touch.phase {
            case .Began:
                let initialLocation = touch.locationInView(gameObject.superview!)
                let startTime = NSDate.timeIntervalSinceReferenceDate()
                self.state = .Pending(time: startTime, location: initialLocation)
                break
            case .Stationary:
                fallthrough
            case .Moved:
                return self.handleMoveTouch(touch, onGameObject: gameObject)
            default:
                //TODO: establish connection if available
                break
            }
            return false
        }
        
        private func handleMoveTouch(touch: UITouch, onGameObject gameObject: GameObject) -> Bool {
            let currentLocation = touch.locationInView(gameObject.superview!)
            switch self.state {
            case .Pending(let startTime, let startLocation):
                if currentLocation.distanceTo(startLocation) > 10 {
                    self.state = .Failed
                    return false
                }
                if NSDate.timeIntervalSinceReferenceDate() - startTime < self.longTapDuration {
                    return false
                }
                self.state = .Succeed
                return true
            case .Failed:
                return false
            case .Succeed:
                return true
            case .None:
                return false
            }
        }
    }
    
    // MARK: SketchingGameField Implementation
    func addPlayer(player: Player) {
        let nibViews = NSBundle.mainBundle().loadNibNamed("GamePlayerView", owner: self, options: nil)
        if let playerView = nibViews.first as? GamePlayerView {
            let size = CGSize(width: 35, height: 50)
            playerView.frame = CGRect(origin: CGPointZero, size: size)
            playerView.player = player
            
            let center = CGPoint(x: 50, y: 50)
            let anchor = CGPoint(x: 0.5, y: 0)
            let goType = GameObject.ObjectType.GamePlayer(player)
            let gameObject = self.addObject(playerView, type: goType, atFieldPosition: center, anchorPoint: anchor)
            gameObject.userInteractionEnabled = true
            gameObject.addTouchHandler(DragTouchHandler())
            gameObject.addTouchHandler(ConnectionTouchHandler())
        }
    }
}
