//
//  AppDelegate.swift
//  Football
//
//  Created by Mihail Shulepov on 09/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Branch
import ReactiveSwift
import MBProgressHUD
import PLModal
import FBSDKCoreKit
import PLSupport
import SDWebImage
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self, Branch.self])
        FIRApp.configure()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Branch.getInstance().initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: { params, error in
            if let error = error {
                NSLog("Branch init error: %@", (error as NSError).description)
                return
            }
            NSLog("Did init Branch: %@", params?.description ?? "no params")
            if let userDidClick = params?["+clicked_branch_link"] as? Bool, userDidClick == true {
                // This code will execute when your app is opened from a Branch deep link, which
                // means that you can route to a custom activity depending on what they clicked.
                // In this example, we'll just print out the data from the link that was clicked.
                //print("deep link data: ", params)
            }
        })

        self.launchSetup()
        
        self.registerFofRemoteNotifications(application)
        if let notificationPayload = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            delay(1) {
                self.handleNotification(notificationPayload)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        LocalUser.cacheCurrentUser()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: URL opening
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // For Branch to detect when a URI scheme is clicked
        Branch.getInstance().handleDeepLink(url)
        
        if FBSDKApplicationDelegate.sharedInstance().application(application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation) {
                return true
        }
        
        if url.scheme == "com.talhayun.football" {
            return true
            
        } else if url.scheme == "drawagoal" {
            let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            let items = (urlComponents?.queryItems)!
            var dict = [String: String]()
            for item in items {
                dict[item.name] = item.value ?? ""
            }
            if let requestId = dict["challenge"] {
                NSLog("Challenge: %@", requestId)

                let hud = MBProgressHUD.showGlobalHUD()
                if let localUser = LocalUser.currentUser.value {
                    localUser.challengeManager.acceptDynamicChallenge(requestId)
                        .observe(on: UIScheduler())
                        .startWithResult { result in
                            switch result {
                            case .success(let challenge):
                                self.startChallenge(challenge, localUser: localUser)
                                hud.hide(animated: true)
                            case .failure(let error):
                                NSLog("Error: %@", error.description)
                                hud.hideWithErrorStatus("Error")
                            }
                        }
                }
            }

            return true
        }
        
        return false
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // For Branch to detect when a Universal Link is clicked
        let handledByBranch = Branch.getInstance().continue(userActivity)
        return handledByBranch
    }
    
    //MARK: Notifications
    func registerFofRemoteNotifications(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //if user will be switched
        LocalUser.currentUser.producer
            .skipNil()
            .skipRepeats { lhs, rhs -> Bool in
                return lhs.userId == rhs.userId
            }
            .startWithValues { user in
                self.associateUserToInstallation()
            }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        NSLog("Fail to register remote notifications: %@", (error as NSError).description)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let payload = userInfo as? [String: Any] {
            self.handleNotification(payload)
        }
    }
    
    func notificationTokenDidChange(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        associateUserToInstallation()
    }
    
    func associateUserToInstallation() {
        guard let localUser = LocalUser.currentUser.value else {
            return
        }
        guard let token = FIRInstanceID.instanceID().token() else {
            NotificationCenter.default.addObserver(self, selector: #selector(notificationTokenDidChange(notification:)), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
            return
        }
        localUser.updateNotificationToken(token: token)
    }
}

private extension AppDelegate {
    func handleNotification(_ payload: [String: Any]) {
        NSLog("Did receive notification: %@", payload)

        guard let localUser = LocalUser.currentUser.value else {
            NSLog("Local user not ready yet");
            return
        }

        let getIntValue = { (key: String) -> Int? in
            if let val = payload[key] as? Int {
                return val
            }
            if let val = payload[key] as? String {
                return Int(val)
            }
            return nil
        }
        
        if let challengeId = getIntValue("challenge_id") {
            let challengeManager = localUser.challengeManager
            challengeManager.didReceiveChallengeWithId(challengeId)
            self.loadChallengeWithId(challengeId, localUser: localUser)
            
        } else if let h2hBetIt = getIntValue("h2h_bet_id") {
            self.loadAndStartHead2HeadChallengeWithId(h2hBetIt, localUser: localUser)

        } else if let h2hBetResult = getIntValue("h2h_bet_result") {
            self.viewH2HChallengeResults(localUser, challengeId: h2hBetResult)
        }
    }
    
    func loadChallengeWithId(_ challengeId: Int, localUser: LocalUser) {
        localUser.challengeManager.challengeWithId(challengeId)
            .observe(on: UIScheduler())
            .startWithResult {result in
                switch result {
                case .success(let challenge):
                    if challenge.request.started {
                        //nothing to do
                    } else {
                        self.startChallenge(challenge, localUser: localUser)
                    }
                case .failure(let error):
                     NSLog("Error challenge accepting: %@", error.description)
                }
            }
    }
    
    func startChallenge(_ challenge: Challenge, localUser: LocalUser) {
        if !localUser.isCurrent() {
            return
        }
        let alertVC = BetSelectionAlert()
        let acceptAction = AlertAction(title: "Accept the challenge") { [weak alertVC] in
            let rootVC = self.window!.rootViewController!
            let topVC = rootVC.topViewController()!
            let challengeUI = Storyboards.FootballMain.instantiateChallengeAcceptingViewController()
            challengeUI.bettingAmount = alertVC?.betAmount ?? 20
            challengeUI.startWithChallengeIfCan(challenge, user: localUser, fromViewController: topVC)
        }
        alertVC.addAction(AlertAction(title: "Later"))
        alertVC.addAction(acceptAction)
        
        let titleAttributes: [String: AnyObject] = [
            NSForegroundColorAttributeName: UIColor.red,
            NSFontAttributeName: alertVC.appearance.titleFont!
        ]
        let attrTitle = NSMutableAttributedString(string: "Challenge: \(challenge.request.title)",
            attributes: titleAttributes)
        
        alertVC.attributedTitle = attrTitle
        
        if let avatar = challenge.request.challenger.avatarURL, let avatarURL = URL(string: avatar) {
            let imageView = UIImageView()
            imageView.sd_setImage(with: avatarURL, placeholderImage: UIImage(named: "AvatarPlaceholder"))
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            let heightConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal,
                toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40.0)
            imageView.addConstraint(heightConstraint)
            alertVC.addContentView(imageView, sideSpacing: 0)
        }
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineHeightMultiple = 0.8
        let attributes: [String: AnyObject] = [
            NSForegroundColorAttributeName: UIColor.green,
            NSFontAttributeName: alertVC.appearance.messageFont!,
            NSParagraphStyleAttributeName: paragraph
        ]
        let attrMessage = NSMutableAttributedString(string: "\(challenge.request.challenger.name)\nhas challenged you!",
            attributes: attributes)
        let challengerAttrs: [String: Any] = [
            NSFontAttributeName: alertVC.appearance.titleFont!
        ]
        attrMessage.addAttributes(challengerAttrs, forSubstring: challenge.request.challenger.name)
        alertVC.addAttributedMessage(attrMessage)
        alertVC.show()
    }
    
    func showChallengeWasCompleted() {
        let alertVC = PLAlertViewController()
        alertVC.appearance.allowCloseOnTouchOutside = false
        let proceedAction = AlertAction(title: "OK")
        alertVC.showWithTitle("Already completed",
            message: "Challenge was already completed",
            actions: [proceedAction])
    }
    
    func loadAndStartHead2HeadChallengeWithId(_ challengeId: Int, localUser: LocalUser) {
        localUser.h2hChallengeManager.challengeWithId(challengeId)
            .observe(on: UIScheduler())
            .startWithResult { result in
                switch result {
                case .success(let challenge):
                    self.startH2HChallenge(challenge, localUser: localUser)
                case .failure(let error):
                    NSLog("Error challenge accepting: %@", error.description)
                }
            }
    }
    
    func startH2HChallenge(_ challenge: Head2HeadChallenge, localUser: LocalUser) {
        let rootVC = self.window!.rootViewController!
        let topVC = rootVC.topViewController()!
        H2HChallengeAcceptingViewController.askStartChallenge(challenge, localUser: localUser, fromVC: topVC) {
            
        }
    }
    
    func viewH2HChallengeResults(_ localUser: LocalUser, challengeId: Int) {
        localUser.h2hChallengeManager.challengeWithId(challengeId)
            .observe(on: UIScheduler())
            .startWithResult { result in
                switch result {
                case .success(let challenge):
                    let alertVC = PLAlertViewController()
                    alertVC.title = "Challenge completed"
                    alertVC.addMessage("\(challenge.getOtherUserInfo(localUser).user.name) completed his turn. View results!")
                    let cancel = AlertAction(title: "Later")
                    let view = AlertAction(title: "View") {
                        let compareVC = Storyboards.Main.instantiateH2HCompareViewController()
                        compareVC.challenge = challenge
                        
                        let navController = Storyboards.Main.instantiateViewControllerWithIdentifier("StyledNavigationController") as! StyledNavigationController
                        navController.viewControllers = [compareVC]
                        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        
                        let rootVC = self.window!.rootViewController!
                        let topVC = rootVC.topViewController()!
                        topVC.present(navController, animated: true, completion: nil)
                    }
                    alertVC.addActions(cancel, view)
                    alertVC.show()
                    
                case .failure(let error):
                    NSLog("Error challenge accepting: %@", error.description)
                }
            }
    }
}

// MARK: User Notifications

extension AppDelegate: UNUserNotificationCenterDelegate {
    
}

extension AppDelegate: FIRMessagingDelegate {
    public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        if let payload = remoteMessage.appData as? [String : Any] {
            handleNotification(payload)
        }
    }
}


// MARK: Setup
private extension AppDelegate {
    func launchSetup() {
        AudioManager.defaultManager.musicVolume = 1.0
        AudioManager.defaultManager.soundsVolume = 1.0
        
        AppearanceConfigurator.configure()
        IAPConfigurator.configure()
        let serverConfig = ServerConfiguration()
        self.setupRemoteDataProvider(serverConfig)
        self.configureGameCenter()
        LocalUser.loadOrCreateLocalUser(serverConfig: serverConfig).start()
    }
    
    func configureGameCenter() {
        GKHelper.startWithoutCache()
        GKHelper.helper().prefix = BuildConfig.gameCenterPrefix
        
        let userProducer = LocalUser.currentUser.producer.skipNil()
        let gkAuthProducer = GKHelper.helper().authenticated.producer
        let combined = gkAuthProducer.combineLatest(with: userProducer)
        
        combined.filter { (auth: Bool, _: LocalUser) in return auth } //if authenticated
            .map { (_: Bool, user: LocalUser) in return user }
            .delay(5, on: QueueScheduler.main) //wait some time for local user to update
            .startWithValues { user in
                user.challengeChips.producer.startWithValues { chipsAmount in
                    NSLog("Report challenges score \(chipsAmount)")
                    GKHelper.helper().reportScore(Int64(chipsAmount), forLeaderboardName: BuildConfig.challengesLeaderboad)
                }
                user.chips.producer.startWithValues { chipsAmount in
                    NSLog("Report livematches score \(chipsAmount)")
                    GKHelper.helper().reportScore(Int64(chipsAmount), forLeaderboardName: BuildConfig.liveMatchesLeaderboard)
                }
            }
    }
    
    func setupRemoteDataProvider(_ serverConfig: ServerConfiguration) {
        let engine = RemoteDataProvider(serverConfig: serverConfig)
        let provider = DataProvider(provider: engine)
        DataProvider.activeProvider = provider
    }
}

