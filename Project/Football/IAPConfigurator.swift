//
//  IAPConfigurator.swift
//  Football
//
//  Created by Mikhail Shulepov on 05/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import PLSupport

class IAPConfigurator: NSObject {
    class func configure() {
        let chipsProvider = { (product: IAProductDefinition) -> Void in
            LocalUser.currentUser.value?.changeChipsAmountBy(product.value!)
        }
        
        let chipsPack1 = IAProductDefinition.consumable {
            $0.icon = "ChipsPack_1"
            $0.value = 60
            $0.provideContentAction = chipsProvider
        }
        let chipsPack2 = IAProductDefinition.consumable {
            $0.icon = "ChipsPack_2"
            $0.value = 125
            $0.provideContentAction = chipsProvider
        }
        let chipsPack3 = IAProductDefinition.consumable {
            $0.icon = "ChipsPack_3"
            $0.value = 300
            $0.provideContentAction = chipsProvider
        }
        let chipsPack4 = IAProductDefinition.consumable {
            $0.icon = "ChipsPack_4"
            $0.value = 850
            $0.provideContentAction = chipsProvider
        }
        
        let bundleID = Bundle.main.bundleIdentifier!
        let storeHelper = IAStoreHelper.defaultHelper
        storeHelper.registerProduct("\(bundleID).ChipsPack_1", info: chipsPack1)
        storeHelper.registerProduct("\(bundleID).ChipsPack_2", info: chipsPack2)
        storeHelper.registerProduct("\(bundleID).ChipsPack_3", info: chipsPack3)
        storeHelper.registerProduct("\(bundleID).ChipsPack_4", info: chipsPack4)
    }
}
