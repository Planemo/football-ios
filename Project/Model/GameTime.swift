//
//  GameTime.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public typealias GamePeriod = Int

open class GameTime {
    open let period: GamePeriod
    open let minute: Int
    open let second: Int
    
    public init(period: GamePeriod, minute: Int, second: Int) {
        self.period = period
        self.minute = minute
        self.second = second
    }
}

open class GamePeriodDefinition {
    open let name: String
    open let maxDuration: Int?
    open let period: GamePeriod
    
    public init(name: String, period: GamePeriod, maxDuration: Int? = nil) {
        self.name = name
        self.maxDuration = maxDuration
        self.period = period
    }
}
