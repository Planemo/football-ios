//
//  JsonCompetitionParser.swift
//  Football
//
//  Created by Mihail Shulepov on 01/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonCompetitionParser: CompetitionParser {
    fileprivate struct Field {
        static let ID = "id"
        static let Name = "name"
        static let LogoUrl = "logo_url"
        static let Region = "region"
    }
    
    open func parse(_ data: Any) -> [Competition] {
        if let array = data as? [NSDictionary] {
            return parse(array)

        } else if let obj = data as? NSDictionary {
            if let competition = parse(obj) {
                return [competition]
            }
        }
        
        return []
    }
    
    open func parse(_ json: [NSDictionary]) -> [Competition] {
        return json.map { obj -> Competition? in
            return self.parse(obj)
        }.filter { $0 != nil }.map { $0! }
    }
    
    open func parse(_ json: NSDictionary) -> Competition? {
        if let name = json[Field.Name] as? String {
            let competition = Competition(name: name)
            competition.uid = json[Field.ID] as? Int
            competition.region = json[Field.Region] as? String
            if let logoUrl = json[Field.LogoUrl] as? String {
                competition.logoURL = URL(string: logoUrl)
            }
            return competition
        }
        return nil
    }
}
