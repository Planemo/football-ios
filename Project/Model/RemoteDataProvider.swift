//
//  RemoteDataProvider.swift
//  Football
//
//  Created by Mihail Shulepov on 28/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire

open class RemoteDataProvider: DataProviderEngine {
    open let serverConfig: ServerConfiguration

    fileprivate struct API {
        static let ParamQueryFor = (
            Name: "query_for",
            Value: (
                Competitions: "competitions",
                Teams: "teams",
                Matches: "matches",
                Stadiums: "stadiums"
            )
        )
        static let ParamFilterBy = (
            CompetitionIds: "competition_ids",
            CompetitionNames: "competition_names",
            MatchIds: "match_ids",
            TeamIds: "team_ids",
            TeamNames: "team_names",
            Country: "country",
            Challenge: "challenge",
            FromDate: "from_date",
            BeforeDate: "to_date",
            LiveOnly: "live_only",
            Finished: "finished",
            BetsAcceptable: "bets_acceptable"
        )
        static let ParamDate = "date"
    }
    
    public init(serverConfig: ServerConfiguration) {
        self.serverConfig = serverConfig
    }
    
    //TODO: cache some queries (competitions)
}

// MARK: Support
private extension RemoteDataProvider {
    func request(_ method: Alamofire.HTTPMethod, body: [String: String]) -> SignalProducer<Any, NSError> {
        return self.serverConfig.responseJSON(.ContentManager, method: method, body: body)
    }
    
    func getRequestWithParameters(_ params: [String: String]) -> SignalProducer<Any, NSError> {
        return request(.get, body: params)
    }
    
    func postRequestWithBody(_ body: [String: String]) -> SignalProducer<Any, NSError> {
        return request(.post, body: body)
    }
    
    var dateToStringFormatter: (Date)->String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mmZZZZ"
        return { date in
            return dateFormatter.string(from: date)
        }
    }
}

// MARK: Competitions
public extension RemoteDataProvider {
    public func requestCompetitions() -> SignalProducer<[Competition], NSError> {
        let params = [API.ParamQueryFor.Name: API.ParamQueryFor.Value.Competitions]
        return self.getRequestWithParameters(params)
            .observe(on: QueueScheduler())
            .map { json in
                return JsonCompetitionParser().parse(json)
            }
//            |> observeOn(QueueScheduler.mainQueueScheduler)
    }
}

// MARK: Teams
public extension RemoteDataProvider {
    public func requestShallowTeamsInfo(filter: TeamsFilter) -> SignalProducer<[Team], NSError> {
        var params = [API.ParamQueryFor.Name : API.ParamQueryFor.Value.Teams]
        switch filter {
        case .none:
            break
        case .relatedToCompetition(let competition):
            params[API.ParamFilterBy.CompetitionIds] = "\(competition.uid!)"
        case .relatedToMatch(let match):
            params[API.ParamFilterBy.MatchIds] = "\(match.uid!)"
        case .country(let country):
            params[API.ParamFilterBy.Country] = country
        case .challenge:
            params[API.ParamFilterBy.Challenge] = "true"
        }
        return getRequestWithParameters(params)
            .observe(on: QueueScheduler())
            .map { json in
                return JsonTeamParser().parse(json)
            }
//            |> observeOn(QueueScheduler.mainQueueScheduler)
    }
    
    public func requestFullTeamsInfo(_ teams: [Team], date: Date) -> SignalProducer<[Team], NSError> {
        let teamIds = teams.map {
            return "\($0.uid!)"
        }.joined(separator: ",")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" //exact date not necessary
        let params = [
            API.ParamQueryFor.Name : API.ParamQueryFor.Value.Teams,
            API.ParamFilterBy.TeamIds : teamIds,
            API.ParamDate : dateFormatter.string(from: date),
            "full": "true"
        ]
        return getRequestWithParameters(params)
            .observe(on: QueueScheduler())
            .map { json in
                return JsonTeamParser().parse(json)
            }
//            |> observeOn(QueueScheduler.mainQueueScheduler)
    }
    
    public func requestMatchInfos(_ filters: [MatchFilter]) -> SignalProducer<[Game], NSError> {
        let dateFormatter = self.dateToStringFormatter
        let initialParams = [
            API.ParamQueryFor.Name: API.ParamQueryFor.Value.Matches
        ]
        let params = filters.reduce(initialParams) { (acc: [String: String], filter: MatchFilter) in
            var acc = acc
            switch filter {
            case .dateInterval(let fromDate, let toDate):
                acc[API.ParamFilterBy.FromDate] = dateFormatter(fromDate)
                if let toDate = toDate {
                    acc[API.ParamFilterBy.BeforeDate] = dateFormatter(toDate)
                }
            case .involvedTeam(let team):
                if let teamId = team.uid {
                    acc[API.ParamFilterBy.TeamIds] = "\(teamId)"
                } else {
                    acc[API.ParamFilterBy.TeamNames] = team.name
                }
            case .inCompetition(let competition):
                if let competitionId = competition.uid {
                    acc[API.ParamFilterBy.CompetitionIds] = "\(competitionId)"
                } else {
                    acc[API.ParamFilterBy.CompetitionNames] = competition.name
                }
            case .country(let country):
                acc[API.ParamFilterBy.Country] = country
            case .matchId(let matchId):
                acc[API.ParamFilterBy.MatchIds] = "\(matchId)"
            case .liveOnly:
                acc[API.ParamFilterBy.LiveOnly] = "true"
            case .testData:
                acc["test_data"] = "true"
            case .finished(let finished):
                acc[API.ParamFilterBy.Finished] = finished ? "true" : "false"
            case .betsAcceptable:
                acc[API.ParamFilterBy.BetsAcceptable] = "true"
            }
            return acc
        }
        
        return getRequestWithParameters(params)
            .observe(on: QueueScheduler())
            .map { json in
                return JsonMatchParser().parse(json)
            }
    }
}

public extension RemoteDataProvider {
    public func requestStadiums() -> SignalProducer<[String], NSError> {
        let params = [
            API.ParamQueryFor.Name: API.ParamQueryFor.Value.Stadiums
        ]
        return getRequestWithParameters(params)
            .observe(on: QueueScheduler())
            .map { json in
                if let ret = json as? [String] {
                    return ret
                }
                return []
            }
        
    }
}
