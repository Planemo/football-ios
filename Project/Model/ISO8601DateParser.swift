//
//  SqlDateParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class ISO8601DateParser: DateParser {
    
    open func parseDate(_ dateString: String) -> Date? {
        let dateFormat = "yyyy-MM-dd"
        return parseDate(dateString, withFormat: dateFormat)
    }
    
    open func parseDateTime(_ timeString: String) -> Date? {
        let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        return parseDate(timeString, withFormat: dateFormat)
    }
    
    fileprivate func parseDate(_ dateString: String, withFormat format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: dateString)
    }
    
    //returns string formatted in time zone +0000
    open func dateTimeToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter.string(from: date)
    }
    
    open func dateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter.string(from: date)
    }
}
