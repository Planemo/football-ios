//
//  DataProvider.swift
//  Football
//
//  Created by Mihail Shulepov on 23/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift

open class MatchesFilter {
    open var startDate: Date?
    open var endDate: Date?
    
    open var name: String?
    
    open var teamName: String?
}

public enum TeamsFilter {
    case none
    case relatedToCompetition(Competition)
    case relatedToMatch(Game)
    case country(String)
    case challenge
}

public enum MatchFilter {
    case dateInterval(Date, Date?)  //restrict to matches at specified interval
    case involvedTeam(Team) //restrict to matches with specified team
    case inCompetition(Competition) //restrict to matches from specified competition
    case country(String) //country name/code
//    case Season(Int) //year
    case matchId(Int) //

    case liveOnly //return only ongoing matches
    
    case testData
    case finished(Bool)
    case betsAcceptable
}

public protocol DataProviderEngine {
    // return all possible competitions
    func requestCompetitions() -> SignalProducer<[Competition], NSError>
    
    // request shallow teams info
    func requestShallowTeamsInfo(filter: TeamsFilter) -> SignalProducer<[Team], NSError>
    
    // request full teams info (with players)
    func requestFullTeamsInfo(_ teams: [Team], date: Date) -> SignalProducer<[Team], NSError>
    
    // request shallow match info with shallow teams info (without players)
    func requestMatchInfos(_ filters: [MatchFilter]) -> SignalProducer<[Game], NSError>
    
    // query for match events
    //func requestMatchEvents(matchID: Int) -> RACSignal
    
    // support
    func requestStadiums() -> SignalProducer<[String], NSError>
}

open class DataProvider: NSObject, DataProviderEngine {
    fileprivate let provider: DataProviderEngine
    
    fileprivate struct Static {
        static var instance: DataProvider!
    }
    
    open class var activeProvider: DataProvider {
        get {
            return Static.instance
        }
        set {
            Static.instance = newValue
        }
    }
    
    public init(provider: DataProviderEngine) {
        self.provider = provider
    }
    
    open func requestCompetitions() -> SignalProducer<[Competition], NSError> {
        return self.provider.requestCompetitions()
    }
    
    open func requestShallowTeamsInfo(filter: TeamsFilter) -> SignalProducer<[Team], NSError> {
        return self.provider.requestShallowTeamsInfo(filter: filter)
    }

    open func requestFullTeamsInfo(_ teams: [Team], date: Date) -> SignalProducer<[Team], NSError> {
        return self.provider.requestFullTeamsInfo(teams, date: date)
    }
    
    open func requestMatchInfos(_ filters: [MatchFilter]) -> SignalProducer<[Game], NSError> {
        return self.provider.requestMatchInfos(filters)
    }
    
    open func requestStadiums() -> SignalProducer<[String], NSError> {
        return self.provider.requestStadiums()
    }
}
