//
//  JsonChallengeRequestParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 22/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonChallengeRequestParser: ChallengeRequestParser {
    fileprivate struct Field {
        static let ChallengeId = "id"
        static let UserId = "user_id"
        static let Started = "started"
        static let Result = "result"
        static let Time = "time"
        static let Title = "title"
    }
    
    open var dateParser = ISO8601DateParser()
    
    open func parse(_ data: Any) -> [ChallengeRequest] {
        if let jsonArray = data as? [NSDictionary] {
            return parseJsonArray(jsonArray)

        } else if let jsonObject = data as? NSDictionary, let challengeRequest = parseJsonObject(jsonObject) {
            return [challengeRequest]
        }
        
        return []
    }
    
    open func parseJsonObject(_ jsonObject: NSDictionary) -> ChallengeRequest? {
        let challenger = JsonUserParser(prefix: "challenger_").parseJsonObject(jsonObject)
        let challengeId = jsonObject[Field.ChallengeId] as? Int
        let date: Date?
        if let dateString = jsonObject[Field.Time] as? String {
            date = self.dateParser.parseDateTime(dateString)
        } else {
            date = nil
        }
        let result = jsonObject[Field.Result] as? Bool
        let started = (jsonObject[Field.Started] as? Bool) ?? false
        let title = (jsonObject[Field.Title] as? String) ?? ""
        
        if challengeId == nil {
            NSLog("Error challenge request parsing: challengeId not specified")
            return nil
        }
        
        if challenger == nil {
            NSLog("Error challenge request parsing: challenger not specified")
            return nil
        }
        return ChallengeRequest(uid: challengeId!, challenger: challenger!,
            date: date, started: started, result: result, title: title)
    }
    
    open func parseJsonArray(_ jsonArray: [NSDictionary]) -> [ChallengeRequest] {
        return jsonArray.map { jsonObject -> ChallengeRequest? in
            return self.parseJsonObject(jsonObject)
        }.filter { $0 != nil }.map { $0! }
    }
}
