//
//  Competition.swift
//  Football
//
//  Created by Mihail Shulepov on 25/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class Competition: NSObject {
    internal var uid: Int?
    
    open var region: String? //e.g. England, Germany or International

    open var name: String //e.g. English League Cup
    open var logoURL: URL?
    
    public init(name: String) {
        self.name = name
    }
}

extension Competition {
    open override var description: String {
        return "Competition (name: \(self.name))"
    }
    
    open override var debugDescription: String {
        let uid = self.uid ?? 0
        let region = self.region ?? "undefined"
        let logoURL = self.logoURL?.absoluteString ?? "none"
        return "\(uid) \(self.name); region: \(region); logo: \(logoURL)"
    }
}

public extension Competition {
    public func debugQuickLookObject() -> Any? {
        return self.debugDescription.replacingOccurrences(of: "; ", with: "\n") as AnyObject?
    }
}
