//
//  EventComparator.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public enum EventCompareOption {
    case players
    case positions
    case time
    
    public static func all() -> [EventCompareOption] {
        return [.players, .positions, .time]
    }
}

public enum EventComparsionResult {
//    public let result: Float
    
//    public let compareDetails: [EventCompareOption: Bool]
}

open class EventComparator {
    public typealias GridPosition = (x: Int, y: Int)
    open let lhs: Event
    open let rhs: Event
    open let game: Game
    
    open var minutesCompareBlock = 15 //split time for 15 minute blocks
    open var fieldGridSize = (x: 8, y: 4)
    
    public init(lhs: Event, rhs: Event, game: Game) {
        self.lhs = lhs
        self.rhs = rhs
        self.game = game
    }
    
    fileprivate func weightForOption(_ option: EventCompareOption) -> Float {
        switch option {
        case .players:
            return 1
        case .positions:
            return 1
        case .time:
            return 1
        }
    }
    
    open func compareWithOptions(_ options: [EventCompareOption]) {
        //TODO: what should be the result of compare?
        // players compare (main event + related event)
        // positions compare (main event + related event)
        // time compare
    }
    
    fileprivate func gridPositionForFieldPosition(_ position: CGPoint) -> GridPosition {
        let column = Int( floor( CGFloat(fieldGridSize.x) * position.x / 100 ) )
        let row = Int( floor ( CGFloat(fieldGridSize.y) * position.y / 100 ) )
        return (x: column, y: row)
    }
    
    fileprivate func comparePositions() -> Bool {
        let lhspos = self.gridPositionForFieldPosition(lhs.position)
        let rhspos = self.gridPositionForFieldPosition(rhs.position)
        
        let equalX = lhspos.x == rhspos.x || abs(lhs.position.x - rhs.position.x) < 5
        let equalY = lhspos.y == rhspos.y || abs(lhs.position.y - rhs.position.y) < 5
        return equalX && equalY
    }
    
    fileprivate func comparePlayers() -> Bool {
        if let lhsuid = lhs.relatedPlayer.uid, let rhsuid = rhs.relatedPlayer.uid {
            return lhsuid == rhsuid
        }
        return lhs.relatedPlayer == rhs.relatedPlayer
    }
    
    fileprivate func compareTime() -> Bool {
        if let lhsTime = lhs.time, let rhsTime = rhs.time  {
            let minutesBlock = self.minutesCompareBlock
            return lhsTime.period == rhsTime.period && lhsTime.minute / minutesBlock == rhsTime.minute / minutesBlock
        }
        return false
    }
    
}
