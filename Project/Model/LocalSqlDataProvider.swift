//
//  LocalSqlDataProvider.swift
//  Football
//
//  Created by Mihail Shulepov on 26/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveCocoa
import SQLite
import PLSupport

public class LocalSqlDataProvider: DataProviderEngine {
    public struct Error {
        public static let Domain = "com.planemo.LocalSqlDataProvider"
        public static let InsertErrorCode = 100
    }
    
    private struct Table {
        static let Competitions = (
            Name: "Competitions",
            Column: (
                ID: "id",
                Name: "name",
                Region: "region",
                Logo: "logo_url"
            )
        )
        
        static let Matches = (
            Name: "Matches",
            Column : (
                ID: "match_id",
                CompetitionID: "competition_id",
                Date: "date",
                HomeTeamID: "home_team_id",
                AwayTeamID: "away_team_id",
                Venue: "venue",
                Wheather: "wheather"
            )
        )
        
        static let Teams = (
            Name: "Teams",
            Column: (
                ID: "team_id",
                Name: "name",
                Emblem: "emblem_url",
                Country: "country"
            )
        )

        static let TeamSquads = (
            Name: "TeamSquads",
            Column: (
                TeamID: "team_id",
                PlayerID: "player_id",
                PlayerNumber: "number",
                JoinDate: "join_date",
                LeaveDate: "leave_date",
                Role: "role"
            )
        )
        
        static let Players = (
            Name: "Players",
            Column: (
                PlayerID: "player_id",
                Name: "name",
                Portrait: "portrait_url",
                BirthDate: "birth_date",
                Nationality: "nationality"
            )
        )
    }
    
    private let footballDB: Database
    public let dbPath: String
    
    public init(dbPath: String) {
        self.dbPath = dbPath
        self.footballDB = Database(dbPath)
    }
}

// MARK: Competitions
public extension LocalSqlDataProvider {
    public func requestCompetitions() -> RACSignal {
        let table = Table.Competitions
        
        let stmt = self.footballDB.prepare("SELECT \(table.Column.ID), \(table.Column.Name), \(table.Column.Region), \(table.Column.Logo) FROM \(table.Name)")
        let competitions = map(stmt) { (value: Statement.Element) -> Competition in
            let name = value[1] as? String ?? "UNDEFINED"
            let competition = Competition(name: name)
            competition.uid = value[0] as? Int
            competition.region = value[2] as? String
            if let logoURL = value[3] as? String {
                competition.logoURL = NSURL(string: logoURL)
            }
            return competition
        }
        return RACSignal.single(competitions)
    }
    
    public func insertOrUpdateCompetition(competition: Competition) -> RACSignal {
        let tableInfo = Table.Competitions
        
        let table = self.footballDB[tableInfo.Name]
        let uid = Expression<Int>(tableInfo.Column.ID)
        let name = Expression<String>(tableInfo.Column.Name)
        let region = Expression<String?>(tableInfo.Column.Region)
        let logo = Expression<String?>(tableInfo.Column.Logo)
        
        if let competitionUid = competition.uid {
            let dbCompetition = table.filter(uid == competitionUid)
            dbCompetition.update(
                name <- competition.name,
                region <- competition.region,
                logo <- competition.logoURL?.absoluteString
            )?
            
        } else if let insertID = table.insert(
            name <- competition.name,
            region <- competition.region,
            logo <- competition.logoURL?.absoluteString) {
                competition.uid = insertID

        } else {
            let error = NSError(domain: Error.Domain, code: Error.InsertErrorCode, userInfo: nil)
            return RACSignal.error(error)
        }
        return RACSignal.single(competition)
    }
}

// MARK: Support
private extension LocalSqlDataProvider {
    func buildColumnsString(columns: [String], withTablePrefix prefix: String? = nil) -> String {
        return ", ".join(
            columns.map { name -> String in
                if let prefix = prefix {
                    return "\(prefix).\(name)"
                }
                return name
            }
        )
    }
}


// MARK: Teams
public extension LocalSqlDataProvider {
    public func requestShallowTeamsInfo(#filter: TeamsFilter) -> RACSignal {
        // find teams ids
        var teamIds = [Int]()
        
        switch filter {
        case .RelatedToCompetition(let competition):
            let table = Table.Matches
            let stmt = self.footballDB.prepare("SELECT \(table.Column.HomeTeamID), \(table.Column.AwayTeamID) FROM \(table.Name) WHERE \(table.Column.CompetitionID)=\(competition.uid!)")
            teamIds = uniq( map(stmt) { (value: Statement.Element) -> [Int] in
                let homeTeamId = value[0] as Int
                let awayTeamId = value[1] as Int
                return [homeTeamId, awayTeamId]
                }.reduce([Int]()) { acc, val -> [Int] in
                    return acc + val
                })
            
        case .Country(let country):
            let table = Table.Teams
            let stmt = self.footballDB.prepare("SELECT \(table.Column.ID) FROM \(table.Name) WHERE \(table.Column.Country) LIKE '%\(country)%'")
            teamIds = map(stmt) { (value: Statement.Element) -> Int in
                return value[0] as Int
            }
        default: assert(false, "missed branches")
        }

        
        // request shallow teams info
        let teamIdsStr = ",".join(teamIds.map { "\($0)" })
        let stmt = self.footballDB.prepare("SELECT * FROM \(Table.Teams.Name) WHERE \(Table.Teams.Column.ID) IN (\(teamIdsStr))")
        let teams = map(stmt) { (value: Statement.Element) -> Team in
            let name = value[1] as String
            let team = Team(name: name)
            team.uid = value[0] as? Int
            if let emblem = value[2] as? String {
                team.logoURL = NSURL(string: emblem)
            }
            team.country = value[3] as? String
            return team
        }
        
        return RACSignal.single(teams)
    }
    
    public func requestFullTeamsInfo(teams: [Team], date: NSDate) -> RACSignal {
        let seconds = Int(date.timeIntervalSince1970)
        let teamIdsStr = ",".join( teams.map { "\($0.uid!)" } )

        let (tsq, tpl) = (Table.TeamSquads, Table.Players)
        
        let squadColumns = self.buildColumnsString([tsq.Column.PlayerID, tsq.Column.TeamID, tsq.Column.PlayerNumber, tsq.Column.Role], withTablePrefix: tsq.Name)
        let playerColumns = self.buildColumnsString([tpl.Column.Name, tpl.Column.Portrait, tpl.Column.BirthDate, tpl.Column.Nationality], withTablePrefix: tpl.Name)
        
        let selectColumns = "\(squadColumns), \(playerColumns)"
        let dateWhere = "\(tsq.Column.JoinDate) < \(seconds) AND (\(tsq.Column.LeaveDate) > \(seconds) OR \(tsq.Column.LeaveDate) IS NULL)"
        
        //SELECT fields FROM TeamSquads
        //JOIN Players ON TeamSquads.player_id=Players.player_id
        //WHERE team_id IN (teams_ids) AND join_date < date AND (leave_date > date OR leave_date IS NULL)
        let stmt = self.footballDB.prepare(
            "SELECT \(selectColumns) FROM \(Table.TeamSquads.Name)" +
            " JOIN \(tpl.Name) ON \(tsq.Name).\(tsq.Column.PlayerID)=\(tpl.Name).\(tpl.Column.PlayerID)" +
            " WHERE \(tsq.Column.TeamID) IN (\(teamIdsStr)) AND \(dateWhere)"
        )
        
        let teamsMap = teams.reduce([Int: Team]()) { (var acc, team) in
            team.players = []
            acc[team.uid!] = team
            return acc
        }
        
        for row in stmt {
            let name = row[4] as String
            let number = row[2] as Int
            let player = Player(name: name, number: number)
            player.role = row[3] as? String
            player.uid = row[0] as? Int
            if let portrait = row[5] as? String {
                player.portrait = NSURL(string: portrait)
            }
            let teamID = row[1] as Int
            let team = teamsMap[teamID]!
            team.players.append(player)
        }
        
        return RACSignal.single(teams)
    }

}