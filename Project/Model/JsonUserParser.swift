//
//  JsonUserParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonUserParser: UserParser {
    fileprivate struct Field {
        static let UserId = "user_id"
        static let Chips = "chips"
        static let Name = "name"
        static let AvatarURL = "avatar_url"
    }
    
    fileprivate let prefix: String
    
    public init(prefix: String = "") {
        self.prefix = prefix
    }
    
    open func parse(_ data: Any) -> User! {
        if let jsonDict = data as? NSDictionary {
            return self.parseJsonObject(jsonDict)
        }
        return nil
    }
    
    open func parseJsonObject(_ json: NSDictionary) -> User! {
        if let userId = json[self.prefix + Field.UserId] as? String {
            let user = User(userId: userId)
            user.name = (json[self.prefix + Field.Name] as? String) ?? "???"
            user.chips.value = (json[self.prefix + Field.Chips] as? Int) ?? 0
            user.avatarURL = json[self.prefix + Field.AvatarURL] as? String
            return user
        }
        NSLog("Error parsing user: user_id not specified")
        return nil
    }
}
