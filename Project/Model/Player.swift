//
//  Player.swift
//  Football
//
//  Created by Maxim Shmotin on 14/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class Player: NSObject {
    internal var uid: Int?
    internal var team_uid: Int?
    
    open var name: String
    open var number: Int
    open var portrait: URL? //url to local or remote file
    open var role: String? //role of the player, e.g. goalkeeper, defender, forward, midfielder in football
    
    open var detailedPosition: String?

    open var nationality: String? //country code
    open var birthDate: Date?
    
    public init(name: String, number: Int) {
        self.name = name
        self.number = number
    }
}

extension Player {
    open override var description: String {
        return "Player (name: \(self.name))"
    }
    
    open override var debugDescription: String {
        let uid = self.uid ?? 0
        let role = self.role ?? "--"
        return "\(uid): \(number) \(role) : \(self.name)"
    }
}

public extension Player {
    public func debugQuickLookObject() -> Any? {
        let role = self.role ?? "--"
        let portrait = self.portrait?.absoluteString ?? "none"
        let nationality = self.nationality ?? "undefined"
        let birthDate = self.birthDate?.description ?? "undefined"
        return "\(role) \(number) \(name)\nPortrait: \(portrait)\nNationality: \(nationality)\nBirth date: \(birthDate)" as AnyObject?
    }
}

extension Player {
    open override var hashValue: Int {
        return self.name.hashValue &+ self.number &+ (self.uid ?? 0) &+ (self.team_uid ?? 0)
    }
}

public func ==(lhs: Player, rhs: Player) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

//GK вратарь
//SW свободный з.
//RB правый з.
//LB левый з.
//CB центр з.
//RWB правый атакующий защитник
//LWB левый ат. з.
//CDM опорник
//RM рправый пз
//CM центр пз
//LM левый пз
//RWM правый атакующий пз
//LWM левый атакующий пз
//CAM атакующий пз
//RF правый отянутый форвард
//CF отянутый форвард
//LF левый отянутый форвард
//RS правый конечный форвард
//LS левый конечный форвард
//ST центр форвард
