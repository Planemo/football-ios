//
//  DataParsers.swift
//  Football
//
//  Created by Mihail Shulepov on 01/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

public protocol CompetitionParser {
    func parse(_ data: Any) -> [Competition]
}

public protocol TeamParser {
    func parse(_ data: Any) -> [Team]
}

public protocol PlayerParser {
    func parse(_ data: Any) -> [Player]
}

public protocol DateParser {
    func parseDate(_ dateString: String) -> Date?
    func parseDateTime(_ timeString: String) -> Date?
}

public protocol MatchParser {
    func parse(_ data: Any) -> [Game]
}

public protocol EventParser {
    func parse(_ data: Any, forMatch: Game) -> [Event]
}

public protocol ChallengeRequestParser {
    func parse(_ data: Any) -> [ChallengeRequest]
}

public protocol ChallengeParser {
    func parse(_ data: Any) -> Challenge?
}

public protocol Head2HeadChallengeParser {
    func parse(_ data: Any) -> [Head2HeadChallenge]
}

public protocol UserParser {
    func parse(_ data: Any) -> User!
}

public protocol TeamFormTemplateParser {
    func parse(_ data: Any) -> FormTemplate?
}

public protocol BetInfoParser {
    func parse(_ data: Any) -> [BetInfo]
}
