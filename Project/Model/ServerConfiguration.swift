//
//  ServerConfiguration.swift
//  Football
//
//  Created by Mikhail Shulepov on 17/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift

open class ServerConfiguration {
    public struct ErrorDomain {
        public static let Name = "ServerConfiguration"
        public static let Code = (
            UserNotSignedIn: 1000,
            InvalidSignature: 1001
        )
    }
    
    public enum Endpoint: String {
        case ContentManager = "api/content_manager_v2.php"
        case ChallengeManager = "api/challenges_v2.php"
        case UserManager = "api/user_manager_v2.php"
        case BettingsManager = "api/betting_manager.php"
        case H2HChallengeManager = "api/head2head_bets_manager.php"
        
        public func fullUrlString() -> String {
            return "http://api.planemostd.com/football" + "/" + self.rawValue
        }
        
        public func fullUrl() -> URL {
            return URL(string: self.fullUrlString())!
        }
    }
    
    open let appSecret = "lI85jFUi"
    fileprivate let requestManager: Alamofire.SessionManager
    
    //public let baseURL: NSURL = NSURL(string: "http://api.planemostd.com/football")!
    
    public init() {
        let sessionConfiguration = URLSessionConfiguration.default
        //            sessionConfiguration.URLCache = self.cache
        //            sessionConfiguration.requestCachePolicy = .ReturnCacheDataElseLoad
        sessionConfiguration.timeoutIntervalForRequest = 10
        self.requestManager = Alamofire.SessionManager(configuration: sessionConfiguration)
    }
    
    fileprivate var _cache: URLCache? = nil
    fileprivate var cache: URLCache {
        if _cache == nil {
            let cacheSizeMemory = 10 * 1024 * 1024
            let cacheSizeDisk = 20 * 1024 * 1024
            _cache = URLCache(memoryCapacity: cacheSizeMemory, diskCapacity: cacheSizeDisk, diskPath: "nsurlcache")
        }
        return _cache!
    }
    
    open func responseJSON(_ endpoint: Endpoint, method: HTTPMethod, body: [String: Any], userId: String? = nil, logging: Bool = false) -> SignalProducer<Any, NSError> {
        var body = body
        let ts = Int64(Date().timeIntervalSince1970)
        body["ts"] = "\(ts)"
        
        if let userKey = userId {
            body["user_id"] = userKey
            body["signature"] = self.makeSignature(endpoint.rawValue, body: self.flattenJson(body), userKey: userKey)
        }
        
        let requestSignalProducer = SignalProducer<Any, NSError> { observer, compositeDisposable in
            let request = self.requestManager.request(endpoint.fullUrlString(),
                                        method: method,
                                        parameters: body,
                                        encoding: method == .post ? JSONEncoding.default : URLEncoding.methodDependent,
                                        headers: nil)
            
            if logging, let body = request.request?.httpBody {
                NSLog("Url: %@\nBody: %@", endpoint.fullUrlString(), String(data: body, encoding: String.Encoding.utf8)!)
            }
            
            var stringData: String?
            request.validate(statusCode: 200..<300)
                .responseString(encoding: String.Encoding.utf8) { response in
                    if let data = response.result.value {
                        stringData = data
                        if response.result.error != nil {
                            NSLog("Response error data:\n%@", data )
                        }
                    }
                }
                .responseJSON { response in
                    if let error = response.result.error {
                        NSLog("Error: code: %d", response.response?.statusCode ?? -1)
                        NSLog("Error: URL: %@\n-> error: %@", response.request?.url?.absoluteString ?? "???", (error as NSError).description)
                        if method == .post {
                            NSLog("Error: Body: %@", body.description)
                        }
                        NSLog("Response: %@", stringData ?? "")                        
                        observer.send(error: error as NSError)
                    } else if let json = response.result.value {
                        observer.send(value: json)
                        observer.sendCompleted()
                    } else {
                        observer.sendCompleted()
                    }
                }
        }
        
        return requestSignalProducer.flatMapError { error -> SignalProducer<Any, NSError> in
            //if timeout error - retry once
            if error.code == Int(CFNetworkErrors.cfurlErrorTimedOut.rawValue) {
                return requestSignalProducer
            }
            return SignalProducer(error: error)
        }
    }
    
    fileprivate func flattenJson(_ json: [String: Any]) -> [String: String] {
        var result = [String: String]()
        for (key, value) in json {
            if let strValue = value as? String {
                result[key] = strValue
            }
        }
        return result
    }
    
    fileprivate func makeSignature(_ endpoint: String, body: [String: String], userKey: String) -> String {
        let paramsString = body.keys.sorted { lhs, rhs -> Bool in
            return lhs.compare(rhs) == ComparisonResult.orderedAscending
        }.map { (key: String) -> (String, String) in
            return (key, body[key]!)
        }.map { (key, value) -> String in
            return key + "=" + value
        }.joined(separator: "&")
        let fullstring = endpoint + "?" + paramsString
        return self.makeSignature(fullstring, key: userKey + self.appSecret)
    }
    
    fileprivate func makeSignature(_ string: String, key: String) -> String {
        let cKey = (key as NSString).cString(using: String.Encoding.utf8.rawValue)
        let cKeyLen = key.lengthOfBytes(using: String.Encoding.utf8)
        let cData = (string as NSString).cString(using: String.Encoding.utf8.rawValue)
        let cDataLen = string.lengthOfBytes(using: String.Encoding.utf8)
        
        let digestLen = Int(CC_SHA256_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        let algorithm = CCHmacAlgorithm(kCCHmacAlgSHA256)
        CCHmac(algorithm, cKey, cKeyLen, cData, cDataLen, result)
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        hash.replaceOccurrences(of: " ", with: "", options: [], range: NSMakeRange(0, hash.length))
        hash.replaceOccurrences(of: "<", with: "", options: [], range: NSMakeRange(0, hash.length))
        hash.replaceOccurrences(of: ">", with: "", options: [], range: NSMakeRange(0, hash.length))
        
        return String(hash)
    }
}
