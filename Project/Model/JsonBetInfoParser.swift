//
//  JsonBetInfoParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonBetInfoParser: BetInfoParser {
    open func parse(_ data: Any) -> [BetInfo] {
        if let jsonDict = data as? NSDictionary {
            let betsData = jsonDict["bets"]
            let sketchEventsData = jsonDict["sketch_events"]
            let realEventsData = jsonDict["real_events"]
            
            if let betsData = betsData, let matchesData = jsonDict["matches"] {
                let matches = JsonMatchParser().parse(matchesData)
                let sketchEvents = self.parseEvents(sketchEventsData, matches: matches)
                let realEvents = self.parseEvents(realEventsData, matches: matches)
                return self.parseBetsData(betsData, matches: matches, sketchEvents: sketchEvents, realEvents: realEvents)
            }
        }
        return []
    }
    
    fileprivate func parseEvents(_ events: Any?, matches: [Game]) -> [Game: [Event]]? {
        if let eventsArray = events as? [NSDictionary] {
            let matchesMap = matches.toDictionary { match in
                return (key: match.uid!, value: match)
            }
            
            let eventsParser = JsonEventParser()
            return eventsArray.groupBy { dict -> Int in
                return (dict[JsonEventParser.Field.MatchId] as? Int) ?? 0
            }.map { matchId, eventsData -> (Game, [NSDictionary]) in
                let match = matchesMap[matchId]!
                return (match, eventsData)
            }.map { match, eventsData -> (Game, [Event]) in
                let events = eventsParser.parse(eventsData, forMatch: match)
                match.removeAllEvents()
                return (match, events)
            }
        }
        
        return nil
    }
    
    fileprivate func parseBetsData(_ bets: Any, matches: [Game], sketchEvents: [Game: [Event]]?, realEvents: [Game: [Event]]?) -> [BetInfo] {
        if let betData = bets as? NSDictionary {
            if let betInfo = self.parseBetData(betData, matches: matches, sketchEvents: sketchEvents, realEvents: realEvents) {
                return [betInfo]
            }
        
        } else if let betsData = bets as? [NSDictionary] {
            return betsData.map { betData -> BetInfo? in
                return self.parseBetData(betData, matches: matches, sketchEvents: sketchEvents, realEvents: realEvents)
            }.filter { $0 != nil }.map { $0! }
        }
        
        return []
    }
    
    fileprivate func parseBetData(_ betData: NSDictionary, matches: [Game], sketchEvents: [Game: [Event]]?, realEvents: [Game: [Event]]?) -> BetInfo? {
        let matchId = betData["match_id"] as! Int
        let result = betData["result"] as? Float
        let betAmount = betData["bet_amount"] as! Int
        
        let match: Game! = matches.find { match in
            return match.uid! == matchId
        }
        
        if match == nil {
            return nil
        }
        
        let matchSketchEvents = sketchEvents?[match]
        let matchRealEvents = realEvents?[match]
        
        let betInfo = BetInfo(match: match, betAmount: betAmount, sketchEvents: matchSketchEvents, result: result, realEvents: matchRealEvents)
        betInfo.uid = betData["bet_id"] as? Int
        return betInfo
    }
}
