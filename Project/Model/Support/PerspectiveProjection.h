//
//  PerspectiveProjection.h
//  Football
//
//  Created by Mikhail Shulepov on 16/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface PerspectiveProjection : NSObject

- (instancetype)initWithOriginalSize:(CGSize)size transformedCorners:(NSArray *)corners;

- (CGPoint)projectOriginalPoint:(CGPoint)originalPoint;
- (CGPoint)perspectivePointToOriginal:(CGPoint)perspectivePoint;

- (CATransform3D)transform3D;

@end
