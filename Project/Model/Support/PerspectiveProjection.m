//
//  PerspectiveProjection.m
//  Football
//
//  Created by Mikhail Shulepov on 16/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

#import "PerspectiveProjection.h"

@class ArrayHolder;
static ArrayHolder *adjugate(CGFloat *m);
static ArrayHolder *multmm(CGFloat *a, CGFloat *b);
static ArrayHolder *multmv(CGFloat *m, CGFloat *v);
static ArrayHolder *basisToPoints(CGPoint p1, CGPoint p2, CGPoint p3, CGPoint p4);
static ArrayHolder *general2DProjection(
    CGPoint p1s, CGPoint p1d,
    CGPoint p2s, CGPoint p2d,
    CGPoint p3s, CGPoint p3d,
    CGPoint p4s, CGPoint p4d
);
static CGPoint project(CGFloat *m, CGPoint p);
static ArrayHolder *invertMatrix(CGFloat const * const m);


#pragma mark - Array Holder

@interface ArrayHolder : NSObject

@property (nonatomic) CGFloat *array;

@end

@implementation ArrayHolder

- (instancetype)initWithArraySize:(NSUInteger)size {
    if (self = [super init]) {
        self.array = malloc(sizeof(CGFloat) * size);
    }
    return self;
}

- (void)dealloc {
    free(self.array);
}

@end


#pragma mark - Perspective Projection

@interface PerspectiveProjection()

@property (strong, nonatomic) NSArray *corners;
@property (nonatomic) CGSize originalSize;

@property (nonatomic) ArrayHolder *projectionMatrix;
@property (nonatomic) ArrayHolder *inverseMatrix;

@end


@implementation PerspectiveProjection

- (instancetype)initWithOriginalSize:(CGSize)size transformedCorners:(NSArray *)corners; {
    if (self = [super init]) {
        self.corners = [corners sortedArrayUsingComparator:^(NSValue *lhs, NSValue *rhs){
            CGPoint lhsPoint = [lhs CGPointValue];
            CGPoint rhsPoint = [rhs CGPointValue];
            if (fabs(lhsPoint.y - rhsPoint.y) > 20) {
                if (lhsPoint.y < rhsPoint.y) {
                    return NSOrderedAscending;
                } else {
                    return NSOrderedDescending;
                }
            }
            if (lhsPoint.x < rhsPoint.x) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
        self.originalSize = size;
        self.projectionMatrix = nil;
        self.inverseMatrix = nil;
    }
    return self;
}

- (CGPoint)cornerPoint:(NSUInteger)idx {
    return [self.corners[idx] CGPointValue];
}

- (CGPoint)projectOriginalPoint:(CGPoint)originalPoint {
    return project(self.projectionMatrix.array, originalPoint);
}

- (CGPoint)perspectivePointToOriginal:(CGPoint)perspectivePoint {
    return project(self.inverseMatrix.array, perspectivePoint);
}

- (CATransform3D)transform3D {
    CGFloat *m = self.projectionMatrix.array;
    CATransform3D transform;
  /*  transform.m11 = m[0] / m[8];
    transform.m12 = m[3] / m[8];
    transform.m13 = 0;
    transform.m14 = m[6] / m[8];
    
    transform.m21 = m[1] / m[8];
    transform.m22 = m[4] / m[8];
    transform.m23 = 0;
    transform.m24 = m[7] / m[8];
    
    transform.m31 = 0;
    transform.m32 = 0;
    transform.m33 = 1;
    transform.m34 = 0;
    
    transform.m41 = m[2] / m[8];
    transform.m42 = m[5] / m[8];
    transform.m43 = 0;
    transform.m44 = m[8] / m[8]; */
    
    transform.m11 = m[0];
    transform.m12 = m[3];
    transform.m13 = 0;
    transform.m14 = m[6];
    
    transform.m21 = m[1];
    transform.m22 = m[4];
    transform.m23 = 0;
    transform.m24 = m[7];
    
    transform.m31 = 0;
    transform.m32 = 0;
    transform.m33 = 1;
    transform.m34 = 0;
    
    transform.m41 = m[2];
    transform.m42 = m[5];
    transform.m43 = 0;
    transform.m44 = m[8];
    
    return transform;
}

#pragma mark - Projection Matrix

- (ArrayHolder *)projectionMatrix {
    if (_projectionMatrix == NULL) {
        [self calculateProjectionMatrix];
    }
    return _projectionMatrix;
}

- (void)calculateProjectionMatrix {
    ArrayHolder *generalPro = general2DProjection(
            CGPointZero, [self cornerPoint:0],
            CGPointMake(self.originalSize.width, 0), [self cornerPoint:1],
            CGPointMake(0, self.originalSize.height), [self cornerPoint:2],
            CGPointMake(self.originalSize.width, self.originalSize.height), [self cornerPoint:3]
    );
    CGFloat *arr = generalPro.array;
    for (NSUInteger i = 0; i < 9; ++i) {
        arr[i] = arr[i] / arr[8];
    }
    self.projectionMatrix = generalPro;
}


#pragma mark - Inverse Matrix

- (ArrayHolder *)inverseMatrix {
    if (_inverseMatrix == NULL) {
        [self calculateInverseMatrix];
    }
    return _inverseMatrix;
}

- (void)calculateInverseMatrix {
    self.inverseMatrix = invertMatrix(self.projectionMatrix.array);
}

@end

#pragma mark - Support
// Compute the adjugate of m

static ArrayHolder *adjugate(CGFloat *m) {
    ArrayHolder *holder = [[ArrayHolder alloc] initWithArraySize: 9];
    CGFloat *dst = holder.array;
    dst[0] = m[4]*m[8]-m[5]*m[7];
    dst[1] = m[2]*m[7]-m[1]*m[8];
    dst[2] = m[1]*m[5]-m[2]*m[4];
    dst[3] = m[5]*m[6]-m[3]*m[8];
    dst[4] = m[0]*m[8]-m[2]*m[6];
    dst[5] = m[2]*m[3]-m[0]*m[5];
    dst[6] = m[3]*m[7]-m[4]*m[6];
    dst[7] = m[1]*m[6]-m[0]*m[7];
    dst[8] = m[0]*m[4]-m[1]*m[3];
    return holder;
}

static ArrayHolder *multmm(CGFloat *a, CGFloat *b) { // multiply two matrices
    ArrayHolder *holder = [[ArrayHolder alloc] initWithArraySize: 9];
    CGFloat *c = holder.array;
    for (NSUInteger i = 0; i != 3; ++i) {
        for (NSUInteger j = 0; j != 3; ++j) {
            CGFloat cij = 0;
            for (NSUInteger k = 0; k != 3; ++k) {
                cij += a[3*i + k]*b[3*k + j];
            }
            c[3*i + j] = cij;
        }
    }
    return holder;
}

static ArrayHolder *multmv(CGFloat *m, CGFloat *v) { // multiply matrix and vector
    ArrayHolder *holder = [[ArrayHolder alloc] initWithArraySize: 3];
    CGFloat *dst = holder.array;
    dst[0] = m[0]*v[0] + m[1]*v[1] + m[2]*v[2];
    dst[1] = m[3]*v[0] + m[4]*v[1] + m[5]*v[2];
    dst[2] = m[6]*v[0] + m[7]*v[1] + m[8]*v[2];
    return holder;
}

static ArrayHolder *basisToPoints(CGPoint p1, CGPoint p2, CGPoint p3, CGPoint p4) {
    CGFloat m[9];
    m[0] = p1.x; m[1] = p2.x; m[2] = p3.x;
    m[3] = p1.y; m[4] = p2.y; m[5] = p3.y;
    m[6] = 1;    m[7] = 1;    m[8] = 1;

    CGFloat v[3];
    v[0] = p4.x; v[1] = p4.y; v[2] = 1;
    
    ArrayHolder *adj = adjugate(m);
    
    ArrayHolder *adjv = multmv(adj.array, v);
    
    CGFloat mm[9];
    mm[0] = adjv.array[0]; mm[1] = 0; mm[2] = 0;
    mm[3] = 0; mm[4] = adjv.array[1]; mm[5] = 0;
    mm[6] = 0; mm[7] = 0; mm[8] = adjv.array[2];
    
    return multmm(m, mm);
}

static ArrayHolder *general2DProjection(
                             CGPoint p1s, CGPoint p1d,
                             CGPoint p2s, CGPoint p2d,
                             CGPoint p3s, CGPoint p3d,
                             CGPoint p4s, CGPoint p4d
                             ) {
    ArrayHolder *s = basisToPoints(p1s, p2s, p3s, p4s);
    ArrayHolder *d = basisToPoints(p1d, p2d, p3d, p4d);
    ArrayHolder *adj = adjugate(s.array);
    return multmm(d.array, adj.array);
}

static CGPoint project(CGFloat *m, CGPoint p) {
    CGFloat pos[3];
    pos[0] = p.x; pos[1] = p.y; pos[2] = 1;
    ArrayHolder *v = multmv(m, pos);
    return CGPointMake(v.array[0]/v.array[2], v.array[1]/v.array[2]);
}

static ArrayHolder *invertMatrix(CGFloat const * const m) {
    ArrayHolder *result = [[ArrayHolder alloc] initWithArraySize:9];
    
    double det = m[0] * (m[4] * m[8] - m[7] * m[5]) -
        m[1] * (m[3] * m[8] - m[5] * m[6]) +
        m[2] * (m[3] * m[7] - m[4] * m[6]);
    
    double invdet = 1 / det;
    
    CGFloat *minv = result.array; // inverse of matrix m
    minv[0] = (m[4] * m[8] - m[7] * m[5]) * invdet;
    minv[1] = (m[2] * m[7] - m[1] * m[8]) * invdet;
    minv[2] = (m[1] * m[5] - m[2] * m[4]) * invdet;
    minv[3] = (m[5] * m[6] - m[3] * m[8]) * invdet;
    minv[4] = (m[0] * m[8] - m[2] * m[6]) * invdet;
    minv[5] = (m[3] * m[2] - m[0] * m[5]) * invdet;
    minv[6] = (m[3] * m[7] - m[6] * m[4]) * invdet;
    minv[7] = (m[6] * m[1] - m[0] * m[7]) * invdet;
    minv[8] = (m[0] * m[4] - m[3] * m[1]) * invdet;
    
    return result;
}