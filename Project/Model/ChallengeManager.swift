//
//  Challenge.swift
//  Football
//
//  Created by Mikhail Shulepov on 17/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift
import Result
import PLSupport

//
// Deprecated: rewrite via h2hChallengeManager
//

open class ChallengeManager {
    open let serverConfig: ServerConfiguration
    
    fileprivate struct API {
        static let method = (
            name: "method",
            challenge: "challenge",
            rechallenge: "rechallenge",
            getCompletedChallengeRequests: "get_challenge_requests",
            completeChallenges: "complete_challenges",
            getChallenge: "get_challenge",
            startChallenge: "start_challenge",
            getUncompletedChallengesAmount: "get_uncompleted_challenges_amount",
            getUncompletedChallengeRequests: "get_uncompleted_challenge_requests",
            makeDynamicChallenge: "make_dynamic_challenge",
            acceptDynamicChallenge: "accept_dynamic_challenge"
        )
        
        static let challengeUserId = "challenge_user_id"
        static let challengeSelf = "self"
        static let challengeId = "challenge_id"
        static let challengeIds = "challenge_ids"
        static let teamId = "team_id"
        static let results = "results"
        static let limit = "limit"
        static let after = "after"
        static let resultDetails = "details"
        static let rechallengeOf = "rechallenge_of"
    }
    
    internal class ChallengeCompletionResult: NSObject, NSCoding {
        let challengeId: Int
        let successResult: Bool
        let drawnEventInfo: String?
        
        init(id: Int, successResult: Bool, drawnEventInfo: String?) {
            self.challengeId = id
            self.successResult = successResult
            self.drawnEventInfo = drawnEventInfo
        }
        
        required init(coder aDecoder: NSCoder) {
            self.challengeId = aDecoder.decodeInteger(forKey: "ChallengeId")
            self.successResult = aDecoder.decodeBool(forKey: "Result")
            self.drawnEventInfo = aDecoder.decodeObject(forKey: "EventInfo") as? String
        }
        
        func encode(with aCoder: NSCoder) {
            aCoder.encode(self.challengeId, forKey: "ChallengeId")
            aCoder.encode(self.successResult, forKey: "Result")
            if let eventInfo = self.drawnEventInfo {
                aCoder.encode(eventInfo, forKey: "EventInfo")
            }
        }
    }
    
    fileprivate let failedChallengeCompletions: PersistentProperty<[ChallengeCompletionResult]>
    fileprivate let _uncompletedChallengesCount: PersistentProperty<Int>
    fileprivate let userId: String
    open let uncompletedChallengesCount: Property<Int>
    
    public init(serverConfig: ServerConfiguration, userId: String) {
        self.serverConfig = serverConfig
        self.userId = userId
        
        let standardUd = UserDefaults.standard
        let udStorage = KeyValueStore(engine: UserDefaultsStore(userDefaults: standardUd))
        self._uncompletedChallengesCount = PersistentProperty<Int>.int(storage: udStorage, key: "UncompletedChallengesCount", defaultValue: 0)
        self.uncompletedChallengesCount = Property(self._uncompletedChallengesCount)
        
        self.failedChallengeCompletions = PersistentProperty<ChallengeCompletionResult>.array(storage: udStorage, key: "FailedCompletionsN")
        
        self.refreshUncompletedChallenges()
        self.resubmitFailedChallengesSubmissions().start()
    }
       
    fileprivate func request(_ method: HTTPMethod, body: [String: String]) -> SignalProducer<Any, NSError> {
        return self.serverConfig.responseJSON(.ChallengeManager, method: method, body: body, userId: self.userId)
    }
    
    open func getDynamicChallengeLink(team: Team) -> SignalProducer<URL, NSError> {
        let params: [String: String] = [
            API.method.name: API.method.makeDynamicChallenge,
            API.teamId: "\(team.uid!)"
        ]
        return self.request(.get, body: params).map { response in
            let requestUID = response as! String
            return URL(string: "http://www.api.planemostd.com/football/index.html?uid=\(requestUID)")!
        }
    }
    
    open func getDynamicChallengeLink(challenge: Challenge) -> SignalProducer<URL, NSError> {
        let params: [String: String] = [
            API.method.name: API.method.makeDynamicChallenge,
            API.rechallengeOf: "\(challenge.request.challengeId)"
        ]
        return self.request(.get, body: params).map { response in
            let requestUID = response as! String
            return URL(string: "http://www.api.planemostd.com/football/index.html?uid=\(requestUID)")!
        }
    }
    
    open func acceptDynamicChallenge(_ challenge: String) -> SignalProducer<Challenge, NSError> {
        let params: [String: String] = [
            API.method.name: API.method.acceptDynamicChallenge,
            "request_id": challenge
        ]
        return self.request(.post, body: params)
            .observe(on: QueueScheduler())
            .attemptMap { response in
                if let challenge = JsonChallengeParser().parse(response) {
                    return Result.success(challenge)
                } else {
                    let info = [NSLocalizedDescriptionKey: "Parse error"]
                    let error = NSError(domain: "ChallengeManager", code: 1002, userInfo: info)
                    return Result.failure(error)
                }
            }
    }
    
    open func challengeUser(_ userId: String, withTeam team: Team) -> SignalProducer<(), NSError> {
        let params: [String: String] = [
            API.method.name: API.method.challenge,
            API.challengeUserId: userId,
            API.teamId: "\(team.uid!)"
        ]
        return self.request(.post, body: params).map { response in
            return ()
        }
    }
    
    open func challengeSelf(team: Team) -> SignalProducer<Challenge, NSError> {
        let params: [String: String] = [
            API.method.name: API.method.challenge,
            API.challengeSelf: "true",
            API.teamId: "\(team.uid!)"
        ]
        return self.request(.post, body: params)
            .observe(on: QueueScheduler())
            .attemptMap { response in
                if let challenge = JsonChallengeParser().parse(response) {
                    return Result.success(challenge)
                } else {
                    let info = [NSLocalizedDescriptionKey: "Parse error"]
                    let error = NSError(domain: "ChallengeManager", code: 1002, userInfo: info)
                    return Result.failure(error)
                }
            }
    }
    
    open func rechallenge(_ challenge: Challenge, toUserWithId userId: String) -> SignalProducer<(), NSError> {
        let params: [String: String] = [
            API.method.name: API.method.rechallenge,
            API.challengeUserId: userId,
            API.challengeId: "\(challenge.request.challengeId)"
        ]
        return self.request(.post, body: params).map { response in
            return ()
        }
    }
    
    open func uncompletedChallenges() -> SignalProducer<[ChallengeRequest], NSError> {
        let params: [String: String] = [
            API.method.name: API.method.getUncompletedChallengeRequests
        ]
        return self.request(.get, body: params)
            .observe(on: QueueScheduler())
            .map { response in
                return JsonChallengeRequestParser().parse(response)
            }
    }
    
    open func challenges(_ limit: Int, after: ChallengeRequest? = nil) -> SignalProducer<[ChallengeRequest], NSError> {
        var params = [
            API.method.name: API.method.getCompletedChallengeRequests,
            API.limit: "\(limit)"
        ]
        if let afterChallengeId = after?.challengeId {
            params[API.after] = "\(afterChallengeId)"
        }
        return self.request(.get, body: params)
            .observe(on: QueueScheduler())
            .map { response in
                return JsonChallengeRequestParser().parse(response)
            }
    }
    
    open func challengeWithId(_ challengeId: Int) -> SignalProducer<Challenge, NSError> {
        let params = [
            API.method.name: API.method.getChallenge,
            API.challengeId: "\(challengeId)"
        ]
        return self.request(.get, body: params)
            .observe(on: QueueScheduler())
            .attemptMap { response in
                if let challenge = JsonChallengeParser().parse(response) {
                    return Result.success(challenge)
                } else {
                    let info = [NSLocalizedDescriptionKey: "Parse error"]
                    let error = NSError(domain: "ChallengeManager", code: 1002, userInfo: info)
                    return Result.failure(error)
                }
            }
    }
    
    open func startChallenge(_ challengeId: Int) {
        let params = [
            API.method.name: API.method.startChallenge,
            API.challengeId: "\(challengeId)"
        ]
        self.request(.post, body: params).start()
    }

    open func completeChallenge(_ challenge: Challenge, withSuccess success: Bool, drawnEvent: Event) -> SignalProducer<(), NSError> {
        self._uncompletedChallengesCount.value -= 1
        
        var drawnEvents = [drawnEvent]
        if let relatedDrawnEventId = drawnEvent.relatedEventId {
            if let relatedDrawnEvent = challenge.match.eventWithId(relatedDrawnEventId) {
                drawnEvents.append(relatedDrawnEvent)
            }
        }
        let resultInfoData = JsonEventParser().serializeWithInternalQualifications(drawnEvents, atMatch: challenge.match)
        let resultInfo = String(data: resultInfoData!, encoding: String.Encoding.utf8)
        let challengeCompletion = ChallengeCompletionResult(id: challenge.request.challengeId, successResult: success, drawnEventInfo: resultInfo)
        self.failedChallengeCompletions.value = self.failedChallengeCompletions.value + [challengeCompletion]
        return self.resubmitFailedChallengesSubmissions()
    }
    
    fileprivate func completeChallenges(_ challengeCompletions: [ChallengeCompletionResult]) -> SignalProducer<(), NSError> {
        let challengesString = challengeCompletions.map { "\($0.challengeId)" }.joined(separator: ",")
        let resultsString = challengeCompletions.map { $0.successResult ? "1" : "0" }.joined(separator: ",")
        let resultDetailsString = challengeCompletions.map { $0.drawnEventInfo ?? "null" }.joined(separator: "::<->::")
        let params = [
            API.method.name: API.method.completeChallenges,
            API.results: resultsString,
            API.challengeIds: challengesString,
            API.resultDetails: resultDetailsString
        ]
        return self.request(.post, body: params)
            .map { _ in
                return ()
            }.on (event: { event in
                switch event {
                case .failed(let error):
                    NSLog("error: %@", error.description)
                    self.failedChallengeCompletions.value = self.failedChallengeCompletions.value + challengeCompletions
                default:
                    break
                }
            })
    }
    
    open func didReceiveChallengeWithId(_ challengeId: Int) {
        self._uncompletedChallengesCount.value += 1
    }
    
    fileprivate func resubmitFailedChallengesSubmissions() -> SignalProducer<(), NSError> {
        let failedSubmissions = self.failedChallengeCompletions.value
        if !failedSubmissions.isEmpty {
            self.failedChallengeCompletions.value = []
            return self.completeChallenges(failedSubmissions)
        }
        return SignalProducer.empty
    }
    
    open func refreshUncompletedChallenges() {
        let params = [
            API.method.name: API.method.getUncompletedChallengesAmount
        ]
        self.request(.get, body: params).startWithValues(value: { json in
            if let val = json as? Int {
                self._uncompletedChallengesCount.value = val
            }
        }, failed: { _ in
            
        })
    }
}


open class ChallengeRequest {
    open let challengeId: Int
    open let challenger: User
    open let date: Date
    fileprivate let completionResult: Bool?
    open let started: Bool
    open let title: String
    
    open var result: Bool? {
        if let completionResult = self.completionResult {
            return completionResult
        }
        if started {
            return false
        }
        return nil
    }
    
    public init(uid: Int, challenger: User, date: Date? = nil, started: Bool, result: Bool? = nil, title: String) {
        self.challengeId = uid
        self.challenger = challenger
        self.date = date ?? Date()
        self.completionResult = nil
        self.started = started
        self.title = title
    }
}


open class Challenge {
    open let request: ChallengeRequest
    open let match: Game
    open let event: Event
    open let challengerEvent: Event?
    
    public init(match: Game, event: Event, request: ChallengeRequest, challengerEvent: Event?) {
        self.match = match
        self.event = event
        self.request = request
        self.challengerEvent = challengerEvent
    }
}
