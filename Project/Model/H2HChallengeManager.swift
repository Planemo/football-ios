//
//  Challenge.swift
//  Football
//
//  Created by Mikhail Shulepov on 17/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift
import Result
import PLSupport

open class H2HChallengeManager {
    open let serverConfig: ServerConfiguration
    
    fileprivate struct API {
        static let method = (
            name: "method",
            challenge: "create",
            completeChallenges: "submit_multiple_results",
            getChallenge: "get_info",
            acceptChallenge: "accept_bet",
            getUnviewedChallenges: "get_unviewed_bets",
            setViewedResults: "set_viewed_results"
        )
        
        static let challengeUserId = "challenge_user_id"
        static let challengeId = "bet_id"
        static let challengeIds = "bet_ids"
        static let teamId = "team_id"
        static let results = "results"
        static let resultDetails = "details"
        static let limit = "limit"
        static let after = "after"
        static let betAmount = "bet_amount"
    }
    
    internal class ChallengeCompletionResult: NSObject, NSCoding {
        let challengeId: Int
        let result: Int
        let drawnEventInfo: String?
        
        init(id: Int, result: Int, drawnEventInfo: String?) {
            self.challengeId = id
            self.result = result
            self.drawnEventInfo = drawnEventInfo
        }
        
        required init(coder aDecoder: NSCoder) {
            self.challengeId = aDecoder.decodeInteger(forKey: "ChallengeId")
            self.result = aDecoder.decodeInteger(forKey: "ResultPercentage")
            self.drawnEventInfo = aDecoder.decodeObject(forKey: "EventInfo") as? String
        }
        
        func encode(with aCoder: NSCoder) {
            aCoder.encode(self.challengeId, forKey: "ChallengeId")
            aCoder.encode(self.result, forKey: "ResultPercentage")
            if let eventInfo = self.drawnEventInfo {
                aCoder.encode(eventInfo, forKey: "EventInfo")
            }
        }
    }
    
    fileprivate let failedChallengeCompletions: PersistentProperty<NSArray>
    fileprivate let userId: String
    
    public init(serverConfig: ServerConfiguration, userId: String) {
        self.serverConfig = serverConfig
        self.userId = userId
        
        let standardUd = UserDefaults.standard
        let udStorage = KeyValueStore(engine: UserDefaultsStore(userDefaults: standardUd))
        
        self.failedChallengeCompletions = PersistentProperty<NSArray>.object(storage: udStorage, key: "FailedH2HCompletions", defaultValue: NSArray())
        
        self.resubmitFailedChallengesSubmissions().start()
    }
    
    fileprivate func request(_ method: HTTPMethod, body: [String: String]) -> SignalProducer<Any, NSError> {
        return self.serverConfig.responseJSON(.H2HChallengeManager, method: method, body: body, userId: self.userId)
    }
    
    open func challengeUser(_ userId: String, betAmount: Int, withTeam team: Team) -> SignalProducer<Head2HeadChallenge, NSError> {
        let params: [String: String] = [
            API.method.name: API.method.challenge,
            API.teamId: "\(team.uid!)",
            API.betAmount: "\(betAmount)",
            API.challengeUserId: "\(userId)"
        ]
        return self.request(.post, body: params)
            .observe(on: QueueScheduler())
            .attemptMap { response in
                if let challenge = JsonHead2HeadChallengeParser().parse(response).first {
                    return Result.success(challenge)
                } else {
                    let info = [NSLocalizedDescriptionKey: "Parse error"]
                    let error = NSError(domain: "ChallengeManager", code: 1002, userInfo: info)
                    return Result.failure(error)
                }
        }
    }
    
    open func setViewedChallengeResults(_ challengeId: Int) {
        let params: [String: String] = [
            API.method.name: API.method.setViewedResults,
            API.challengeId: "\(challengeId)"
        ]
        self.request(.post, body: params).start()
    }
    
    open func unviewedChallenges() -> SignalProducer<[Head2HeadChallenge], NSError> {
        let params: [String: String] = [
            API.method.name: API.method.getUnviewedChallenges
        ]
        return self.request(.get, body: params)
            .observe(on: QueueScheduler())
            .map { response in
                return JsonHead2HeadChallengeParser().parse(response)
            }
    }
    
   /* public func challenges(limit: Int, after: ChallengeRequest? = nil) -> SignalProducer<[Head2HeadChallenge], NSError> {
        var params = [
            API.method.name: API.method.getCompletedChallengeRequests,
            API.limit: "\(limit)"
        ]
        if let afterChallengeId = after?.challengeId {
            params[API.after] = "\(afterChallengeId)"
        }
        return self.request(.GET, body: params)
            .observeOn(QueueScheduler())
            .map { response in
                return JsonHead2HeadChallengeParser().parse(response)
            }
    }*/
    
    open func challengeWithId(_ challengeId: Int) -> SignalProducer<Head2HeadChallenge, NSError> {
        let params = [
            API.method.name: API.method.getChallenge,
            API.challengeId: "\(challengeId)"
        ]
        return self.request(.get, body: params)
            .observe(on: QueueScheduler())
            .attemptMap { response in
                if let challenge = JsonHead2HeadChallengeParser().parse(response).first {
                    return Result.success(challenge)
                } else {
                    let info = [NSLocalizedDescriptionKey: "Parse error"]
                    let error = NSError(domain: "ChallengeManager", code: 1002, userInfo: info)
                    return Result.failure(error)
                }
        }
    }
    
    open func acceptChallenge(_ challengeId: Int) {
        let params = [
            API.method.name: API.method.acceptChallenge,
            API.challengeId: "\(challengeId)"
        ]
        self.request(.post, body: params).start()
    }
    
    open func completeChallenge(_ challenge: Head2HeadChallenge, withResult result: Int, drawnEvent: Event) -> SignalProducer<(), NSError> {
        var drawnEvents = [drawnEvent]
        if let relatedDrawnEventId = drawnEvent.relatedEventId {
            if let relatedDrawnEvent = challenge.match!.eventWithId(relatedDrawnEventId) {
                drawnEvents.append(relatedDrawnEvent)
            }
        }
        let resultInfoData = JsonEventParser().serializeWithInternalQualifications(drawnEvents, atMatch: challenge.match!)
        let resultInfo = String(data: resultInfoData!, encoding: String.Encoding.utf8)
        let challengeCompletion = ChallengeCompletionResult(id: challenge.uid, result: result, drawnEventInfo: resultInfo)
        self.failedChallengeCompletions.value = self.failedChallengeCompletions.value.adding(challengeCompletion) as NSArray
        return self.resubmitFailedChallengesSubmissions()
    }
    
    fileprivate func completeChallenges(_ challengeCompletions: [ChallengeCompletionResult]) -> SignalProducer<(), NSError> {
        let challengesString = challengeCompletions.map { "\($0.challengeId)" }.joined(separator: ",")
        let resultsString = challengeCompletions.map { "\($0.result)" }.joined(separator: ",")
        let resultDetailsString = challengeCompletions.map { $0.drawnEventInfo ?? "null" }.joined(separator: "::<->::")
        let params = [
            API.method.name: API.method.completeChallenges,
            API.results: resultsString,
            API.challengeIds: challengesString,
            API.resultDetails: resultDetailsString
        ]
        return self.request(.post, body: params)
            .map { _ in
                return ()
            }.on (event: { event in
                switch event {
                case .failed(let error):
                    NSLog("error: %@", error.description)
                    self.failedChallengeCompletions.value = self.failedChallengeCompletions.value.addingObjects(from: challengeCompletions) as NSArray
                default:
                    break
                }
            })
    }
  
    fileprivate func resubmitFailedChallengesSubmissions() -> SignalProducer<(), NSError> {
        if let failedSubmissions = self.failedChallengeCompletions.value as? [ChallengeCompletionResult], !failedSubmissions.isEmpty {
            self.failedChallengeCompletions.value = NSArray()
            return self.completeChallenges(failedSubmissions)
        }
        return SignalProducer.empty
    }
}

open class Head2HeadUserInfo {
    open let user: User
    open var result: Int?
    open var resultDetails: Event?
    
    public init(user: User) {
        self.user = user
    }
}

open class Head2HeadChallenge {
    open let uid: Int
    
    open let match: Game?
    open let event: Event?
    
    open let title: String
    open let betAmount: Int
    open let date: Date
    
    open let users: [Head2HeadUserInfo]
    
    public init(uid: Int, match: Game?, event: Event?, title: String, betAmount: Int, date: Date, users: [Head2HeadUserInfo]) {
        self.uid = uid
        self.match = match
        self.event = event
        self.title = title
        self.betAmount = betAmount
        self.date = date
        self.users = users
    }
    
    open func getOtherUserInfo(_ currentUser: User) -> Head2HeadUserInfo! {
        for user in self.users {
            if user.user.userId != currentUser.userId {
                return user
            }
        }
        assert(false)
        return nil
    }
    
    open func getCurrentUserInfo(_ currentUser: User) -> Head2HeadUserInfo? {
        for user in self.users {
            if user.user.userId == currentUser.userId {
                return user
            }
        }
        return nil
    }
    
    open var isCompleted: Bool {
        return self.users.reduce(true) { value, user -> Bool in
            return value && (user.result != nil)
        }
    }
}
