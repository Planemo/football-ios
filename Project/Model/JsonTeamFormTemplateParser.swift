//
//  JsonTeamFormParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 04/05/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

open class JsonTeamFormTemplateParser: TeamFormTemplateParser {
    fileprivate struct Field {
        static let BackgroundColor = "b_color"
        static let ForegroundTemplate = "template"
        static let ForegroundColor = "f_color"
        static let Shadow = "shadow"
    }
    
    open func parse(_ data: Any) -> FormTemplate? {
        if let dict = data as? NSDictionary {
            return self.parseJsonObject(dict)
        }
        return nil
    }
    
    open func parseJsonObject(_ data: NSDictionary) -> FormTemplate? {
        let backgroundColorValue = data[Field.BackgroundColor] as? String
        let foregroundTemplate = data[Field.ForegroundTemplate] as? String
        let foregroundColorValue = data[Field.ForegroundColor] as? String
        let shadow = (data[Field.Shadow] as? Bool) ?? false
        
        if backgroundColorValue == nil || foregroundTemplate == nil || foregroundColorValue == nil {
            return nil
        }
        let backgroundColor = UIColor.fromHex(backgroundColorValue!)
        let foregroundColor = UIColor.fromHex(foregroundColorValue!)
        return FormTemplate(backgroundColor: backgroundColor, foregroundTemplate: foregroundTemplate!,
            foregroundColor: foregroundColor, withShadow: shadow)
    }
}
