//
//  JsonEventParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 14/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonEventParser: EventParser {
    public struct Field {
        static let Events = "events"
        static let Qualifications = "qualifications"
        
        static let InternalEventId = "internal_event_id"
        static let EventId = "event_id"
        static let MatchId = "match_id"
        static let EventType = "type"
        static let Period = "period"
        static let Minute = "min"
        static let Seconds = "sec"
        static let PlayerId = "player_id"
        static let TeamId = "team_id"
        static let Outcome = "outcome"
        static let PosX = "x"
        static let PosY = "y"
        static let Assist = "assist"
        static let Keypass = "keypass"
        static let YoutubeLink = "youtube_link"
        static let QualifierId = "id"
        static let QualifierType = "type"
        static let QualifierValue = "value"
        static let QualifierRelatedEventIdValue = "value_from_internal_id"
    }
    
    fileprivate let eventTypeToIntMap: [EventType: Int]
    fileprivate let intToEventTypeMap: [Int: EventType]
    
    fileprivate let qualifierTypeToIntMap: [QualifierType: Int]
    fileprivate let intToQualifierTypeMap: [Int: QualifierType]
    
    public init() {
        let eventTypesInts: [(EventType, Int)] = [
            (.goal, 16),
            (.pass, 1)
        ]
        var forwardEventTypesMap = [EventType: Int]()
        var reverseEventTypesMap = [Int: EventType]()
        for (type, value) in eventTypesInts {
            forwardEventTypesMap[type] = value
            reverseEventTypesMap[value] = type
        }
        self.eventTypeToIntMap = forwardEventTypesMap
        self.intToEventTypeMap = reverseEventTypesMap
        
        let qualifierTypesInts: [(QualifierType, Int)] = [
            (.assisted, 29),
            (.relatedEvent, 55),
            (.penalty, 9),
            (.ownGoal, 28)
        ]
        var forwardQualifierTypesMap = [QualifierType: Int]()
        var reverseQualifierTypesMap = [Int: QualifierType]()
        for (type, value) in qualifierTypesInts {
            forwardQualifierTypesMap[type] = value
            reverseQualifierTypesMap[value] = type
        }
        self.qualifierTypeToIntMap = forwardQualifierTypesMap
        self.intToQualifierTypeMap = reverseQualifierTypesMap
    }
    
    open func parse(_ data: Any, forMatch match: Game) -> [Event] {
        if let eventsJson = data as? [NSDictionary] {
            return self.parse(eventsJson, forMatch: match)
        }
        return []
    }
    
    open func parse(_ eventsJsonArray: [NSDictionary], forMatch match: Game) -> [Event] {
        return eventsJsonArray.map { json -> Event? in
            return self.parseEvent(json, forMatch: match)
        }.filter { $0 != nil }.map { $0! }
    }
    
    open func parseEvent(_ json: NSDictionary, forMatch match: Game) -> Event? {
        let id = (json[Field.EventId] as? Int) ?? -1
        let intType = (json[Field.EventType] as? Int) ?? -1
        let type = self.intToEventTypeMap[intType]
        let positionX = json[Field.PosX] as? CGFloat
        let positionY = json[Field.PosY] as? CGFloat
        let teamId = (json[Field.TeamId] as? Int) ?? -1
        let playerId = (json[Field.PlayerId] as? Int) ?? -1
        
        if type == nil {
            NSLog("Warning: can't create Event(%d) with int type: %d", id, intType)
            return nil
        }
        
        if positionX == nil || positionY == nil {
            NSLog("Warning: can't create Event(%d) - position not specified", id)
            return nil
        }
        
        let team = match.teamWithId(teamId)
        if team == nil {
            NSLog("Warning: can't create Event(%d) - can't find team(%d) in match(%d)", id, teamId, match.uid ?? -1)
            return nil
        }
        
        let player = team?.playeWithId(playerId)
        if player == nil {
            NSLog("Warning: can't create Event(%d) - can't find player(%d) in team(%d)", id, playerId, teamId)
            return nil
        }
        
        let position = CGPoint(x: positionX!, y: positionY!)
        let event = Event(type: type!, team: team!, player: player!, position: position)
        event.uid = id

        if let qualificationsJsonArray = json[Field.Qualifications] as? [NSDictionary] {
            event.qualifications = qualificationsJsonArray.map { json -> Event.Qualifier? in
                return self.parseQualification(json)
            }.filter { $0 != nil }.map { $0! }
        }
        
        if let period = json[Field.Period] as? Int,
            let sec = json[Field.Seconds] as? Int,
            let minute = json[Field.Minute] as? Int {
                event.time = GameTime(period: period, minute: minute, second: sec)
        }
        
        event.outcome = (json[Field.Outcome] as? Bool) ?? false
        event.assist = (json[Field.Assist] as? Bool) ?? false
        event.keypass = (json[Field.Keypass] as? Bool) ?? false
        if let link = json[Field.YoutubeLink] as? String {
            event.youtubeLink = URL(string: link)
        }
        
        return event
    }
    
    open func parseQualification(_ json: NSDictionary) -> Event.Qualifier? {
        let value = json[Field.QualifierValue]
        let intType = json[Field.QualifierType] as? Int
        if let intType = intType, let type = self.intToQualifierTypeMap[intType] {
            let qualifier = Event.Qualifier(type: type, value: value)
            qualifier.uid = json[Field.QualifierId] as? Int
            return qualifier
        } else {
            NSLog("Warning: failed to create qualifier with int type: %d", intType ?? -1)
        }
        return nil
    }
    
    open func toJsonWithExternalQualifications(_ events: [Event], atMatch match: Game) -> [String: Any] {
        var json = [String: Any]()
        
        json[Field.Events] = self.serializeEventsData(events, match: match, withQualifications: false)
        json[Field.Qualifications] = self.serializeQualificationsData(events) as AnyObject?
        
        return json
    }
    
    open func serializeWithExternalQualifications(_ events: [Event], atMatch match: Game) -> Data! {
        let json = self.toJsonWithExternalQualifications(events, atMatch: match)
        
        do {
            return try JSONSerialization.data(withJSONObject: json, options: [])
        } catch let error {
            NSLog("Unexpected events json writing error: \(error)")
            fatalError("Events serialization error")
        }
    }
    
    fileprivate func toJsonWithInternalQualifications(_ events: [Event], atMatch match: Game) -> [[String: Any]] {
        return self.serializeEventsData(events, match: match, withQualifications: true)
    }
    
    open func serializeWithInternalQualifications(_ events: [Event], atMatch match: Game) -> Data! {
        let json = self.toJsonWithInternalQualifications(events, atMatch: match)
        
        do {
            return try JSONSerialization.data(withJSONObject: json, options: [])
        } catch let error {
            NSLog("Unexpected events json writing error: \(error)")
            fatalError("Events serialization error")
        }
    }
    
    fileprivate func serializeEventsData(_ events: [Event], match: Game, withQualifications: Bool) -> [[String: Any]] {
        return events.map { event -> [String: Any] in
            var keyValueMap = [String: Any]()
            
            keyValueMap[Field.MatchId] = match.uid!
            keyValueMap[Field.EventType] = self.eventTypeToIntMap[event.type]
            
            if let time = event.time {
                keyValueMap[Field.Period] = time.period
                keyValueMap[Field.Minute] = time.minute
                keyValueMap[Field.Seconds] = time.second
            }
            
            if let youtubeLink = event.youtubeLink?.absoluteString {
                keyValueMap[Field.YoutubeLink] = youtubeLink
            }
            
            keyValueMap[Field.PlayerId] = event.relatedPlayer.uid!
            keyValueMap[Field.TeamId] = event.relatedTeam.uid!
            keyValueMap[Field.Outcome] = event.outcome
            keyValueMap[Field.PosX] = Int(event.position.x)
            keyValueMap[Field.PosY] = Int(event.position.y)
            keyValueMap[Field.Assist] = event.assist
            keyValueMap[Field.Keypass] = event.keypass
            
            if withQualifications {
                keyValueMap[Field.Qualifications] = self.serializeQualificationsDataForEvent(event)
                keyValueMap[Field.EventId] = event.uid!
            } else {
                keyValueMap[Field.InternalEventId] = event.uid!
            }
            
            return keyValueMap
        }
    }
    
    fileprivate func serializeQualificationsData(_ events: [Event]) -> [[String: Any]] {
        return events.flatMap { event -> [(Event, Event.Qualifier)] in
            return event.qualifications.map { qualifier -> (Event, Event.Qualifier) in
                return (event, qualifier)
            }
        }.map { event, qualifier -> [String: Any] in
            var keyValueMap = [String: Any]()
            keyValueMap[Field.InternalEventId] = event.uid!
            keyValueMap[Field.QualifierType] = self.qualifierTypeToIntMap[qualifier.type]
            if let value = qualifier.value {
                switch qualifier.type {
                case .relatedEvent:
                    keyValueMap[Field.QualifierRelatedEventIdValue] = value
                default:
                    keyValueMap[Field.QualifierValue] = value
                }
            }
            return keyValueMap
        }
    }

    fileprivate func serializeQualificationsDataForEvent(_ event: Event) -> [[String: Any]] {
        return event.qualifications.map { qualifier -> [String: Any] in
            var keyValueMap = [String: Any]()
            keyValueMap[Field.QualifierType] = self.qualifierTypeToIntMap[qualifier.type]
            if let value: Any = qualifier.value {
                keyValueMap[Field.QualifierValue] = value
            }
            return keyValueMap
        }
    }
}
