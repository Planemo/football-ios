//
//  Event.swift
//  Football
//
//  Created by Maxim Shmotin on 14/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import CoreGraphics

// Main Event Type
public enum EventType: Int {
    case goal /// All goals
    case pass /// Any pass attempted from one player to another, free kicks, corners, throw-ins, goal kicks and goal assists
}

public enum QualifierType: Int {
    //        case PassEndPosition(CGPoint) // The (x,y) pitch coordinate for the end of a path (0..100)
    
    case assisted //Indicates that there was a pass (assist) from another player to set up the goal opportunity (check related event)
    
    case relatedEvent
    
    // Body part descriptors
    //        case PartOfBody(BodyPart)
    
    // Shot descriptors
    case penalty //When attempt on goal was a penalty kick. ALSO used on Event type 'Foul' to indicate a penalty was awarded
    //        case PenaltyShotOut //Penalty period - is it differs from Penalty?
    case ownGoal //Own goal. Note: Use the inverse coordinates of the goal location
    
    // Pattern of Play
    //        case RegularPlay //Shot during open play as opposed to from a set play
    //        case SetPlay //Shot occurred from a crossed free kick
}

open class Event {
    open var uid: Int!
    
    open let type: EventType
    
    open var time: GameTime?
    open var position: CGPoint // (0..100)
    
    open var relatedPlayer: Player //player related to the event
    open let relatedTeam: Team //team related to the event
    
    open var assist: Bool = false //whether the event has led to a goal
    open var keypass: Bool = false //whether the event has led to unsuccessful shot
    
    open var outcome: Bool = false //whether the event was successfull
    
    open var qualifications = [Qualifier]()
    
    open var youtubeLink: URL?
    
    open var relatedEventId: Int? {
        for qualifier in self.qualifications {
            switch qualifier.type {
            case .relatedEvent:
                return qualifier.intValue
            default:
                break
            }
        }
        return nil
    }
    
    public init(type: EventType, team: Team, player: Player, position: CGPoint) {
        self.type = type
        self.relatedTeam = team
        self.relatedPlayer = player
        self.position = position
    }
    
    // Body part associated with event
    public enum BodyPart: Int {
        case head
        case leftFoot
        case rightFoot
        case feet
        case hands
        case other
    }
    
    open class Qualifier {
        open var uid: Int? = nil
        open let type: QualifierType
        open let value: Any?

        public init(type: QualifierType, value: Any? = nil) {
            self.type = type
            self.value = value
        }
        
        open var intValue: Int? {
            return value as? Int
        }
        
        open var floatValue: Float? {
            return value as? Float
        }
        
        open var boolValue: Bool? {
            return value as? Bool
        }
    }
    
    @discardableResult
    open func addQualifierWithType(_ type: QualifierType, value: Any? = nil) -> Qualifier {
        let qualifier = Qualifier(type: type, value: value)
        self.qualifications.append(qualifier)
        return qualifier
    }

}
/*
extension Event.Qualifier: Equatable, Hashable {
    public var hashValue: Int {
        switch self {
        case .PassEndPosition(let pos):
            let floatHash = 1.0 + 13 * pos.x + 17 * pos.y
            return Int(round(floatHash) % CGFloat(Int.max))
        case .Assisted: return 2
        case .RelatedEvent(let event): return 23 &+ 11 &* event.hashValue
        case .PartOfBody(let bodyPart): return 37 + 31 * bodyPart.hashValue
        case .Penalty: return 3
        case .PenaltyShotOut: return 4
        case .OwnGoal: return 5
        case .RegularPlay: return 6
        case .SetPlay: return 7
        }
    }
}

public func ==(lhs: Event.Qualifier, rhs: Event.Qualifier) -> Bool {
    return lhs.hashValue == rhs.hashValue
}
*/

extension Event: Equatable, Hashable {
    public var hashValue: Int {
        let uintHash = UInt(bitPattern: ObjectIdentifier(self))
        return Int(uintHash % UInt(Int.max))
    }
}

public func ==(lhs: Event, rhs: Event) -> Bool {
    return lhs.hashValue == rhs.hashValue
}
