//
//  Football.swift
//  Football
//
//  Created by Mikhail Shulepov on 09/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

// Just to name periods in Football

public enum FootballGamePeriod: Int {
    case firstHalf = 1
    case secondHalf
    case firstExtraTime
    case secondExtraTime
    case penaltyShootOut
}

open class Football: NSObject {
    public enum PlayerRole: String {
        case Goalkeeper = "Goalkeeper"
        case Defender = "Defender"
        case Midfielder = "Midfielder"
        case Forward = "Forward"
    }
    
    public enum PlayerDetailedPosition: String {
        // Goalkeepers
        case GK = "GK"
        
        // Defenders
        case SW = "SW" // Sweeper
        case CB = "CB" // Centre-back
        case FB = "FB" // Full-back, either LB or RB
        case LB = "LB", RB = "RB" // Full-back position, left or right side
        case WB = "WB" // Wing-back, either LWB or RWB
        case LWB = "LWB", RWB = "RWB" // Wing-back position, left or right side
        
        // Midfielders
        case CM = "CM" // Centre midfield
        case DM = "DM" // Defensive midfield
        case AM = "AM" // Attacking midfield
        case WM = "WM" // Wide midfield, either LM or RM
        case LM = "LM", RM = "RM" // Wide midfield, left or right side
        
        // Forwards
        case CF = "CF" // Centre forward
        case SS = "SS" // Second striker
        case LW = "LW", RW = "RW" // Left / right winger
    }
    
    fileprivate func playerRoleForDetailedPosition(_ detailedPosition: PlayerDetailedPosition) -> PlayerRole! {
        let rolesMap: [PlayerRole: [PlayerDetailedPosition]] = [
            .Goalkeeper: [.GK],
            .Defender: [.SW, .CB, .FB, .LB, .RB, .WB, .LWB, .RWB],
            .Midfielder: [.CM, .DM, .AM, .WM, .LM, .RM],
            .Forward: [.CF, .SS, .LW, .RW]
        ]
        for (role, positions) in rolesMap {
            if positions.contains(detailedPosition) {
                return role
            }
        }
        return nil
    }

    open var periods: [GamePeriodDefinition] {
        return [
            GamePeriodDefinition(name: "First half", period: 1, maxDuration: 60),
            GamePeriodDefinition(name: "Second half", period: 2, maxDuration: 60),
            GamePeriodDefinition(name: "First period of extra time", period: 3, maxDuration: 20),
            GamePeriodDefinition(name: "Second period of extra time", period: 4, maxDuration: 20),
            GamePeriodDefinition(name: "Penalty shoot out", period: 5, maxDuration: nil),
        ]
    }
}
