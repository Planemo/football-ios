//
//  JsonTeamParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonTeamParser: TeamParser {
    fileprivate struct Field {
        static let ID = "team_id"
        static let Name = "name"
        static let EmblemUrl = "emblem_url"
        static let Country = "country"
        static let Players = "players"
    }
    
    open func parse(_ data: Any) -> [Team] {
        if let array = data as? [NSDictionary] {
            return parse(array)
            
        } else if let obj = data as? NSDictionary {
            if let team = parse(obj) {
                return [team]
            }
        }
        return []
    }
    
    open func parse(_ json: [NSDictionary]) -> [Team] {
        return json.map { obj -> Team? in
            return self.parse(obj)
        }.filter { $0 != nil }.map { $0! }
    }
    
    open func parse(_ json: NSDictionary) -> Team? {
        if let name = json[Field.Name] as? String {
            let team = Team(name: name)
            team.uid = json[Field.ID] as? Int
            team.country = json[Field.Country] as? String
            if BuildConfig.noTeamEmblems {
                team.logoURL = URL(string: "http://api.planemostd.com/football/files/Teams/fc_default_logo.png")
            } else {
                if let emblemUrl = json[Field.EmblemUrl] as? String {
                    team.logoURL = URL(string: emblemUrl)
                }
            }
            
            if let players: Any = json[Field.Players] {
                team.players = JsonPlayerParser().parse(players)
            }
            if let formTemplate = JsonTeamFormTemplateParser().parseJsonObject(json) {
                team.formTemplate = formTemplate
            } else {
                NSLog("Form template not specified for team: %d - %@", team.uid ?? -1, name)
            }
            return team
        }
        return nil
    }
}
