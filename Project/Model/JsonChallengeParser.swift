//
//  JsonChallengeParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 18/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonChallengeParser: ChallengeParser {
    open func parse(_ data: Any) -> Challenge? {
        if let jsonDict = data as? NSDictionary {
            let matchData = jsonDict["match"]!
            
            if let match = JsonMatchParser().parse(matchData).first {
                let challengeData = jsonDict["challenge"] as! NSDictionary
                return parseChallengeJson(challengeData, match: match)
            }
        }
        return nil
    }
    
    fileprivate func parseChallengeJson(_ json: NSDictionary, match: Game) -> Challenge? {
        let request = JsonChallengeRequestParser().parseJsonObject(json)
        if request == nil {
            NSLog("Challenge parse error: can't parse challenge request")
            return nil
        }
        
        let info = json["info"] as! NSDictionary
        let eventId = info["event_id"] as! Int
        
        let event = match.eventWithId(eventId)
        if event == nil {
            NSLog("Challenge parse error: can't create challenge becase event(%d) not found in match(%d)", eventId, match.uid!)
            return nil
        }
        
        var challengerEvent: Event? = nil
        if let challengerResultDetails = json["result_details"] as? [NSDictionary] {
            let events = JsonEventParser().parse(challengerResultDetails, forMatch: match)
            match.addEvents(events)

            for event in events {
                if event.relatedEventId != nil || events.count == 1 {
                    challengerEvent = event
                    break
                }
            }
        }
        
        return Challenge(match: match, event: event!, request: request!, challengerEvent: challengerEvent)
    }
}
