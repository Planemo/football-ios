//
//  LocalUser.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/12/2016.
//  Copyright © 2016 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import PLSupport
import Result

open class LocalUser: User {
    fileprivate static let DefaultChallengeChipsAmount = 1000
    fileprivate let serverConfig: ServerConfiguration
    
    open static let currentUser: MutableProperty<LocalUser?> = MutableProperty(nil)
    
    open let challengeManager: ChallengeManager
    open let h2hChallengeManager: H2HChallengeManager
    open let bettingsManager: BettingsManager
    
    open let challengeChips: MutableProperty<Int>
    
    open var facebookId: String?
    open var notificationToken: String?
    
    fileprivate let pendingChipsSubmission: PersistentProperty<Int>
    
    fileprivate func makeRequestWithBody(_ params: [String: String]) -> SignalProducer<Any, NSError> {
        return self.serverConfig.responseJSON(.UserManager, method: .post, body: params, userId: self.userId)
    }
    
    fileprivate let disposable = CompositeDisposable()
    
    deinit {
        NSLog("LocalUser deinit")
        disposable.dispose()
    }
    
    public class func createNewUser(serverConfig: ServerConfiguration) -> SignalProducer<LocalUser, NSError> {
        let params = [
            "method": "create_user"
        ]
        let infoRequest = serverConfig.responseJSON(.UserManager, method: .post, body: params, userId: nil)
        
        return infoRequest.attemptMap { jsonData -> Result<LocalUser, NSError> in
            if let jsonDict = jsonData as? [String: Any] {
                let newLocalUser = LocalUser(userInfo: jsonDict, serverConfig: serverConfig)
                LocalUser.currentUser.value = newLocalUser
                self.cacheCurrentUser()
                return Result.success(newLocalUser)
            } else {
                let error = NSError(domain: "com.planemo.football.user", code: 2842, userInfo: nil)
                return Result.failure(error)
            }
        }
    }
    
    public class func loadOrCreateLocalUser(serverConfig: ServerConfiguration) -> SignalProducer<LocalUser, NSError> {
        //try load local saved data
        if let cachedData = self.loadCachedLocalUserInfo() {
            let user = LocalUser(userInfo: cachedData, serverConfig: serverConfig)
            self.currentUser.value = user
        }
        
        if let localUser = self.currentUser.value {
            localUser.updateUserInfo()
            return SignalProducer(value: localUser)
        } else {
            return createNewUser(serverConfig: serverConfig)
        }
    }
    
    init(userInfo: [String: Any], serverConfig: ServerConfiguration) {
        let userId = userInfo["user_id"] as! String
        self.challengeManager = ChallengeManager(serverConfig: serverConfig, userId: userId)
        self.h2hChallengeManager = H2HChallengeManager(serverConfig: serverConfig, userId: userId)
        self.bettingsManager = BettingsManager(serverConfig: serverConfig, userId: userId)
        self.serverConfig = serverConfig
        
        let kvsEngine = UserDefaultsStore(userDefaults: UserDefaults.standard)
        let kvs = KeyValueStore(engine: kvsEngine)
        self.pendingChipsSubmission = PersistentProperty<Int>.int(storage: kvs, key: "PendingSpihc" + userId, defaultValue: 0)
        
        self.challengeChips = MutableProperty(LocalUser.DefaultChallengeChipsAmount)
        
        super.init(userId: userId)
        
        self.setupWithInfo(userInfo: userInfo)
    }
    
    private func setupWithInfo(userInfo: [String: Any]) {
        self.name = userInfo["name"] as! String
        self.avatarURL = userInfo["avatar_url"] as? String
        self.facebookId = userInfo["facebook_id"] as? String
        self.chips.value = userInfo["chips"] as! Int
        self.notificationToken = userInfo["notification_token"] as? String
    }
    
    open func isCurrent() -> Bool {
        if let localUser = LocalUser.currentUser.value {
            return localUser.userId == self.userId
        }
        return false
    }
    
    open func changeChipsAmountBy(_ amount: Int) {
        self.chips.value += amount
        self.pendingChipsSubmission.value += amount
        self.submitPendingChips()
    }
    
    fileprivate func submitPendingChips() {
        if self.pendingChipsSubmission.value == 0 {
            return
        }
        let submissionAmount = self.pendingChipsSubmission.value
        let params = [
            "method": "update_chips",
            "user_id": userId,
            "amount": "\(submissionAmount)"
        ]
        self.makeRequestWithBody(params).startWithFailed { error in
            self.pendingChipsSubmission.value += submissionAmount
        }
        self.pendingChipsSubmission.value = 0
    }
    
    open func updateChipsAmount() -> SignalProducer<Int, NSError> {
        let params = [
            "method": "get_user_chips",
            "user_id": userId
        ]
        return SignalProducer { observer, compositeDisposable in
            self.makeRequestWithBody(params).start { event in
                switch event {
                case .value(let responseStr):
                    self.chips.value = ((responseStr as? Int) ?? 0) + self.pendingChipsSubmission.value
                    self.submitPendingChips()
                    observer.send(value: self.chips.value)
                    NSLog("Chips amount: %d, challenge chips amount: %d", self.chips.value, self.challengeChips.value)
                case .failed(let error):
                    observer.send(error: error)
                case .interrupted:
                    observer.sendInterrupted()
                case .completed:
                    observer.sendCompleted()
                }
            }
        }
    }
    
    public func updateNotificationToken(token: String) {
        if let currentToken = self.notificationToken, currentToken == token {
            return
        }
        let params = [
            "method": "update_notification_token",
            "notification_token": token
        ]
        makeRequestWithBody(params).start()
    }
    
    private func updateUserInfo() {
        let params = [
            "method": "get_user_info"
        ]
        makeRequestWithBody(params).startWithValues(value: { infoObj in
            if let userInfo = infoObj as? [String: Any] {
                self.setupWithInfo(userInfo: userInfo)
            }
        }, failed: { _ in
            
        })
    }
    
    private class var cacheFilePath: URL {
        let documentsDirectory = URL(fileURLWithPath: UIApplication.documentsDirectory)
        return documentsDirectory.appendingPathComponent("LocalUserFF.dd")
    }
    
    public class func cacheCurrentUser() {
        if let user = self.currentUser.value {
            var info = [String: Any]()
            info["name"] = user.name
            info["user_id"] = user.userId
            info["chips"] = user.chips.value
            info["challenge_chips"] = user.challengeChips.value
            if let avatarUrl = user.avatarURL {
                info["avatar_url"] = avatarUrl
            }
            if let facebookId = user.facebookId {
                info["facebook_id"] = facebookId
            }
            if let notificationToken = user.notificationToken {
                info["notification_token"] = notificationToken
            }
            
            let data = NSKeyedArchiver.archivedData(withRootObject: info)
            do {
                try data.write(to: self.cacheFilePath, options: .atomicWrite)
            } catch let error {
                NSLog("Write error: %@", (error as NSError).description)
            }
        }
    }
    
    private class func loadCachedLocalUserInfo() -> [String: Any]? {
        do {
            let data = try Data(contentsOf: self.cacheFilePath)
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: Any]
        } catch let error {
            NSLog("Read error: %@", (error as NSError).description)
            return nil
        }
    }
}

extension LocalUser {
    public func queryUsers(withIds ids: [String]) -> SignalProducer<[User], NSError> {
        let userIdsStr = ids.joined(separator: ",")
        let params = [
            "method": "query_users",
            "user_ids": userIdsStr
        ]
        return self.makeRequestWithBody(params).attemptMap { response -> Result<[[String: Any]], NSError> in
            if let infoArray = response as? [[String: Any]] {
                return Result.success(infoArray)
            } else {
                let error = NSError(domain: "com.planemo.football.user", code: 98347, userInfo: nil)
                return Result.failure(error)
            }
        }.map { userInfos -> [User] in
            return userInfos.map { userInfo in
                let userId = userInfo["user_id"] as! String
                let user = User(userId: userId)
                user.avatarURL = userInfo["avatar_url"] as? String
                user.chips.value = (userInfo["chips"] as? Int) ?? 0
                user.name = userInfo["name"] as! String
                return user
            }
        }
    }
}
