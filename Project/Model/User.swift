//
//  User.swift
//  Football
//
//  Created by Mikhail Shulepov on 25/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift

open class User {
    open let userId: String
    
    open let chips: MutableProperty<Int> = MutableProperty(0)
    
    open var name = "???"
   
    open var avatarURL: String? = nil
    
    public init(userId: String) {
        self.userId = userId
    }
}

