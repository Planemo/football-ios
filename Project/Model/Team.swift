//
//  Team.swift
//  Football
//
//  Created by Maxim Shmotin on 14/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import UIKit


open class FormTemplate {
    
    open let backgroundColor: UIColor
    open let withShadow: Bool
    open let foregroundTemplate: String
    open let foregroundColor: UIColor
    
    public init(backgroundColor: UIColor, foregroundTemplate: String, foregroundColor: UIColor, withShadow: Bool) {
        self.backgroundColor = backgroundColor
        self.foregroundTemplate = foregroundTemplate
        self.foregroundColor = foregroundColor
        self.withShadow = withShadow
    }
}

open class Team: NSObject {
    internal var uid: Int?
    
    open var name: String
    open var country: String?
    open var logoURL: URL?
    open var smallLogoURL: URL? {
        if let logoURL = self.logoURL?.absoluteString {
            let smallLogoUrlStr = logoURL.replacingOccurrences(of: "files/Teams", with: "files/Teams/Small")
            return URL(string: smallLogoUrlStr)
        }
        return nil
    }
    open var players = [Player]()

    fileprivate var _formTemplate: FormTemplate?
    open var formTemplate: FormTemplate {
        get {
            if _formTemplate == nil {
                _formTemplate = Team.createDummyFormTemplate()
            }
            return _formTemplate!
        }
        set {
            _formTemplate = newValue
        }
    }
    
    public init(name: String) {
        self.name = name
    }
    
    open func playeWithId(_ id: Int) -> Player? {
        return self.players.filter {
            if let playerId = $0.uid, playerId == id {
                return true
            }
            return false
        }.first
    }
    
    fileprivate class func createDummyFormTemplate() -> FormTemplate {
        let br = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let bg = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let bb = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let bcolor = UIColor(red: br, green: bg, blue: bb, alpha: 1.0)
        let withShadow: Bool = arc4random_uniform(2) == 0
        
        let templateNumber = 1 + arc4random_uniform(12)
        let templateName = "\(templateNumber)"
        
        let fr = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let fg = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let fb = CGFloat(arc4random()) / CGFloat(UINT32_MAX)
        let fcolor = UIColor(red: fr, green: fg, blue: fb, alpha: 1.0)
        
        return FormTemplate(backgroundColor: bcolor, foregroundTemplate: templateName, foregroundColor: fcolor, withShadow: withShadow)
    }
}

extension Team {
    open override var description: String {
        return "Team (name: \(self.name))"
    }
    
    open override var debugDescription: String {
        let uid = self.uid ?? 0
        let country = self.country ?? "undefined"
        let logoURL = self.logoURL?.absoluteString ?? "none"
        return "\(uid): \(self.name); Players count: \(players.count); Region: \(country); Logo: \(logoURL)"
    }
}

public extension Team {
    public func debugQuickLookObject() -> Any? {
        let basic = self.debugDescription.replacingOccurrences(of: "; ", with: "\n")
        let players = "\nPlayers:\n" + self.players.map { player -> String in
            return "\t" + player.debugDescription
        }.joined(separator: "\n")
        return basic + players
    }
}

extension Team {
    open override var hashValue: Int {
        return self.name.hashValue
    }
}

public func ==(lhs: Team, rhs: Team) -> Bool {
    return lhs.hashValue == rhs.hashValue
}
