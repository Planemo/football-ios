//
//  JsonPlayerParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 03/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonPlayerParser: PlayerParser {
    fileprivate struct Field {
        static let ID = "player_id"
        static let TeamID = "team_id"
        static let Name = "name"
        static let BirthDate = "birth_date"
        static let Nationality = "nationality"
        static let PortraitURL = "portrait_url"
        static let Number = "number"
        static let Role = "role"
        static let DetailedPosition = "position"
    }
    
    open let dateParser: DateParser = ISO8601DateParser()
    
    open func parse(_ data: Any) -> [Player] {
        if let array = data as? [NSDictionary] {
            return parse(array)
            
        } else if let obj = data as? NSDictionary {
            if let team = parse(obj) {
                return [team]
            }
        }
        return []
    }
    
    open func parse(_ json: [NSDictionary]) -> [Player] {
        return json.map { obj -> Player? in
            return self.parse(obj)
        }.filter { $0 != nil }.map { $0! }
    }
    
    open func parse(_ json: NSDictionary) -> Player? {
        if let name = json[Field.Name] as? String {
            let number = json[Field.Number] as! Int
            let player = Player(name: name, number: number)
            player.uid = json[Field.ID] as? Int
            player.team_uid = json[Field.TeamID] as? Int
            player.nationality = json[Field.Nationality] as? String
            player.role = json[Field.Role] as? String
            player.detailedPosition = json[Field.DetailedPosition] as? String
            
            if let birthDate = json[Field.BirthDate] as? String {
                player.birthDate = self.dateParser.parseDate(birthDate)
            }
            
            if let portraitUrl = json[Field.PortraitURL] as? String {
                player.portrait = URL(string: portraitUrl)
            }
            
            return player
        }
        return nil
    }
}
