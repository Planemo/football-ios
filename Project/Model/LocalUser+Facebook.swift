//
//  LocalUser+Facebook.swift
//  Football
//
//  Created by Mikhail Shulepov on 19/12/2016.
//  Copyright © 2016 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import FBSDKLoginKit
import Result

public extension LocalUser {
    public var isLoggedInWithFacebook: Bool {
        if let accessToken = FBSDKAccessToken.current() {
            let now = Date()
            return now.isBefore(accessToken.expirationDate)
        } else {
            return false
        }
    }
    
    public class func linkOrLoginWithFacebook(from viewController: UIViewController) -> SignalProducer<LocalUser, NSError> {
        return SignalProducer<FBSDKAccessToken, NSError> { observer, disposable in
            //login with facebook to get facebook id
            let lm = FBSDKLoginManager()
            let permissions = ["public_profile", "user_friends"]
            lm.logIn(withReadPermissions: permissions, from: viewController) { result, error in
                if let error = error {
                    observer.send(error: error as NSError)
                } else if let token = result?.token {
                    observer.send(value: token)
                    observer.sendCompleted()
                } else {
                    let error = NSError(domain: "com.planemo.football.localuser", code: 7382, userInfo: nil)
                    observer.send(error: error)
                }
            }
        }.flatMap(.latest) { _ -> SignalProducer<FBSDKProfile, NSError> in
            return self.loadFBSDKProfile()
            
        }.flatMap(.latest) { facebookProfile -> SignalProducer<LocalUser, NSError> in
            return self.linkOrLoginWithFacebook(facebookProfile: facebookProfile)
            
        }.on(value: { newLocalUser in
            LocalUser.currentUser.value = newLocalUser
        })
    }
    
    fileprivate class func linkOrLoginWithFacebook(facebookProfile: FBSDKProfile) -> SignalProducer<LocalUser, NSError> {
        let avatarUrl = facebookProfile.imageURL(for: FBSDKProfilePictureMode.square, size: CGSize(width: 300, height: 300)).absoluteString
        let params: [String: String] = [
            "method": "try_link_with_facebook",
            "facebook_user_id": facebookProfile.userID,
            "name": facebookProfile.name,
            "avatar_url": avatarUrl
        ]
        let localUserId = LocalUser.currentUser.value?.userId
        
        let serverConfig = ServerConfiguration()
        return serverConfig.responseJSON(.UserManager, method: .post, body: params, userId: localUserId)
            .map { userInfo -> LocalUser in
                return LocalUser(userInfo: userInfo as! [String: Any], serverConfig: serverConfig)
            }
    }

    fileprivate class func loadFBSDKProfile() -> SignalProducer<FBSDKProfile, NSError> {
        return SignalProducer { observer, disposable in
            FBSDKProfile.loadCurrentProfile { profile, error in
                if let profile = profile {
                    observer.send(value: profile)
                    observer.sendCompleted()
                } else {
                    observer.send(error: error! as NSError)
                }
            }
        }
    }
    
    fileprivate func facebookFriendsIds() -> SignalProducer<[String], NSError> {
        return SignalProducer { observer, disposable in
            //"limit": 500, "fields": "id"
            let requestParams: [AnyHashable: Any] = [:]
            let request = FBSDKGraphRequest(graphPath: "me/friends", parameters: requestParams)
            let connection = FBSDKGraphRequestConnection()
            connection.add(request, completionHandler: { connection, result, error in
                if let error = error {
                    observer.send(error: error as NSError)
                    
                } else if let data = (result as? [String: AnyObject])?["data"] as? [NSDictionary] {
                    let ids = data.map { $0["id"] as! String }
                    observer.send(value: ids)
                    observer.sendCompleted()
                    
                } else {
                    observer.sendCompleted()
                }
            })
            connection.start()
        }
    }
    
    public func facebookFriends() -> SignalProducer<[User], NSError> {
        return self.facebookFriendsIds().flatMap(FlattenStrategy.merge) { ids -> SignalProducer<[User], NSError> in
            return self.queryUsers(withIds: ids)
        }
    }
    
    fileprivate func updateDataFromFacebook() {
 
        if let profile = FBSDKProfile.current() {
            self.name = profile.name
            self.avatarURL = profile.imageURL(for: FBSDKProfilePictureMode.square, size: CGSize(width: 300, height: 300)).absoluteString
            self.facebookId = profile.userID
        }
    }
    
}
