//
//  JsonHead2HeadChallengeParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 27/12/15.
//  Copyright © 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonHead2HeadChallengeParser: Head2HeadChallengeParser {
    public struct Field {
        static let BetInfo = "bet_info"
        static let BetAmount = "bet_amount"
        static let BetUID = "id"
        static let BetTime = "time"
        static let BetTitle = "title"
        static let BetEventID = "event_id"
        
        static let UserResult = "result"
        static let UserResultDetails = "result_details"
        static let Users = "users"
    }
    
    /*
    json format: 
    {
        "bet_info": {},
        "users": [{}, {}],
    
        "match": {},
        "teams": [],
        "competitions": []
    }
    */
    open func parse(_ data: Any) -> [Head2HeadChallenge] {
        if let dataDict = data as? NSDictionary {
            return [parse(dataDict)]
        } else if let dataArrayDict = data as? [NSDictionary] {
            return dataArrayDict.map { dataDict -> Head2HeadChallenge in
                return self.parse(dataDict)
            }
        }
        return []
    }
    
    fileprivate func parse(_ dataDict: NSDictionary) -> Head2HeadChallenge {
        //parse match
        let match = JsonMatchParser().parse(dataDict).first
        
        //parse users
        let users = (dataDict[Field.Users] as! [NSDictionary]).map { userInfoDict -> Head2HeadUserInfo in
            return self.parseUserInfo(userInfoDict, match: match)
        }
        
        //parse bet info
        let betInfoDict = dataDict[Field.BetInfo] as! NSDictionary
        let betAmount = betInfoDict[Field.BetAmount] as! Int
        let betUID = betInfoDict[Field.BetUID] as! Int
        let betTime = ISO8601DateParser().parseDateTime(betInfoDict[Field.BetTime] as! String) ?? Date()
        let betTitle = betInfoDict[Field.BetTitle] as! String
        let betEventId = betInfoDict[Field.BetEventID] as! Int
        let betEvent = match?.eventWithId(betEventId)
        let betInfo = Head2HeadChallenge(uid: betUID, match: match, event: betEvent, title: betTitle, betAmount: betAmount, date: betTime, users: users)
        return betInfo
    }
    
    fileprivate func parseUserInfo(_ userInfo: NSDictionary, match: Game?) -> Head2HeadUserInfo {
        let user = JsonUserParser().parse(userInfo)
        let h2hUser = Head2HeadUserInfo(user: user!)
        h2hUser.result = userInfo[Field.UserResult] as? Int

        if let match = match, let resultDetails = userInfo["result_details"] as? [NSDictionary] {
            let events = JsonEventParser().parse(resultDetails, forMatch: match)
            match.addEvents(events)
            
            for event in events {
                //find main event
                if event.relatedEventId != nil || events.count == 1 {
                    h2hUser.resultDetails = event
                    break
                }
            }
        }
        
        return h2hUser
    }
}
