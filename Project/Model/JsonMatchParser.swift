//
//  JsonMatchParser.swift
//  Football
//
//  Created by Mikhail Shulepov on 12/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class JsonMatchParser: MatchParser {
    fileprivate struct Field {
        static let Competitions = "competitions"
        static let Teams = "teams"
        static let Matches = "matches"
        static let Match = "match"
        
        static let ID = "match_id"
        static let HomeTeamId = "home_team_id"
        static let AwayTeamId = "away_team_id"
        static let Date = "date"
        static let CompetitionId = "competition_id"
        static let Events = "events"
        static let Venue = "venue"
    }
    
    open let dateParser: DateParser = ISO8601DateParser()
    
    open func parse(_ data: Any) -> [Game] {
        if let jsonObject = data as? NSDictionary {
            let matchesInfo: [NSDictionary]
            if let matches = jsonObject[Field.Matches] as? [NSDictionary] {
                matchesInfo = matches
            } else if let matchInfo = jsonObject[Field.Match] as? NSDictionary {
                matchesInfo = [matchInfo]
            } else {
                return []
            }
            
            let competitionsParser = JsonCompetitionParser()
            let teamsParser = JsonTeamParser()
            
            let competitionsInfo = jsonObject[Field.Competitions]
            let teamsInfo = jsonObject[Field.Teams]!
            let competitions = competitionsInfo != nil ? competitionsParser.parse(competitionsInfo!) : []
            let teams = teamsParser.parse(teamsInfo)
            
            let matches = self.parse(matchesInfo, teams: teams, competitions: competitions)
            
            return matches
        }
        
        return []
    }
    
    fileprivate func parse(_ jsonArray: [NSDictionary], teams: [Team], competitions: [Competition]) -> [Game] {
        let teamWithId = { (teamId: Int) -> Team in
            return teams.filter { $0.uid! == teamId }.first!
        }
        
        let competitionWithId = { (competitionId: Int) -> Competition in
            return competitions.filter { $0.uid! == competitionId }.first!
        }
        
        return jsonArray.map { dict -> Game in
            let date = self.dateParser.parseDateTime(dict[Field.Date] as! String)
            let homeTeamId = dict[Field.HomeTeamId] as! Int
            let awayTeamId = dict[Field.AwayTeamId] as! Int
            let homeTeam = teamWithId(homeTeamId)
            let awayTeam = teamWithId(awayTeamId)
            let matchInfo = Game(homeTeam: homeTeam, awayTeam: awayTeam, date: date!)

            let competitionId = dict[Field.CompetitionId] as! Int
            let competition = competitionWithId(competitionId)
            matchInfo.competition = competition
            
            matchInfo.uid = dict[Field.ID] as? Int
            
            if let eventsData = dict[Field.Events] {
                let eventsParser = JsonEventParser()
                let events = eventsParser.parse(eventsData, forMatch: matchInfo)
                matchInfo.addEvents(events)
            }
            
            return matchInfo
        }
    }
    
    open func serialize(_ match: Game) -> Data {
        var dict = [String: Any]()
        dict[Field.HomeTeamId] = match.homeTeam.uid
        dict[Field.AwayTeamId] = match.awayTeam.uid
        if let competitionUid = match.competition?.uid {
            dict[Field.CompetitionId] = competitionUid
        }
        if let venue = match.venue {
            dict[Field.Venue] = venue as AnyObject?
        }
        dict[Field.Date] = ISO8601DateParser().dateTimeToString(match.gameDate)
        
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            return data
        } catch let error {
            NSLog("Unexpected events json writing error: \(error)")
            fatalError("Match serialization failed")
        }
    }
}
