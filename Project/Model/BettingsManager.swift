//
//  BettingsManager.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire

open class BettingsManager {
    fileprivate let userId: String
    fileprivate let serverConfig: ServerConfiguration
    
    public init(serverConfig: ServerConfiguration, userId: String) {
        self.userId = userId
        self.serverConfig = serverConfig
    }
    
    fileprivate func request(_ body: [String: Any]) -> SignalProducer<Any, NSError> {
        return self.serverConfig.responseJSON(.BettingsManager, method: .post, body: body, userId: self.userId, logging: false)
    }
       
    open func createBet(_ amount: Int, match: Game, events: [Event]) -> SignalProducer<Int, NSError> {
        var body = [String: Any]()
        body["bet_info"] = [
            "user_id": self.userId,
            "bet_amount": amount,
            "match_id": match.uid!
        ]
        let eventsData = JsonEventParser().toJsonWithExternalQualifications(events, atMatch: match)
        for (key, value) in eventsData {
            body[key] = value
        }
        body["method"] = "make_bet"
        
        return self.request(body).map { json in
            return (json as? Int) ?? -1
        }
    }
    
    open func getBets(_ limit: Int) -> SignalProducer<[BetInfo], NSError> {
        let body = [
            "user_id": self.userId,
            "limit": "\(limit)",
            "method": "get_bets_infos"
        ]
        return self.request(body)
            .observe(on: QueueScheduler())
            .map { json in
                return JsonBetInfoParser().parse(json)
            }
    }
}


open class BetInfo {
    open var uid: Int?
    open let betAmount: Int
    open let match: Game
    open let result: Float?
    open let sketchEvents: [Event]?
    open let realEvents: [Event]?

    public init(match: Game, betAmount: Int, sketchEvents: [Event]?, result: Float?, realEvents: [Event]?) {
        self.betAmount = betAmount
        self.match = match
        self.sketchEvents = sketchEvents
        self.result = result
        self.realEvents = realEvents
    }
}
