//
//  GameInfo.swift
//  Model
//
//  Created by Mihail Shulepov on 17/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class Game: NSObject {
    public enum `Type` {
        case soccer
        case basketball
    }
    
    public enum WeatherType {
        case clear
        case rainy
        case cloudy
        case unknown
    }
    
    open class PostMatchInfo {
        var weather: WeatherType = .unknown
        //stats - time, first half extra time, second half extra time, first half time, second half time
        //result type - Normal/Abandoned/Postponed
        //score (goals - time and player name?)
    }
    //state - finished / live / announced
    
    internal var uid: Int?
    open let type: Type
    
    open var competition: Competition?
//    public var round: Int? //e.g. 4
    open var season: String? //e.g. 'Season 2013/2014'
    open var venue: String? //meeting place
    open var postMatchInfo: PostMatchInfo?
    
    open var homeTeam, awayTeam: Team
    open var gameDate: Date

    fileprivate var maxEventId = 0
    fileprivate(set) open var events = [Event]()
    fileprivate var eventsMap = [Int: Event]()
        
    public init(homeTeam: Team, awayTeam: Team, date: Date) {
        self.type = .soccer
        self.homeTeam = homeTeam
        self.awayTeam = awayTeam
        self.gameDate = date
    }
    
    open func teamWithId(_ teamId: Int) -> Team? {
        if let homeTeamId = self.homeTeam.uid, homeTeamId == teamId {
            return self.homeTeam
        } else if let awayTeamId = self.awayTeam.uid, awayTeamId == teamId {
            return self.awayTeam
        }
        return nil
    }
       
    open func addEvents(_ events: [Event]) {
        for event in events {
            self.addEvent(event)
        }
    }
    
    open func addEvent(_ event: Event) {
        if event.uid == nil {
            event.uid = self.maxEventId + 1
        }
        
        self.maxEventId = max(self.maxEventId, event.uid)
        self.events.append(event)
        self.eventsMap[event.uid] = event
    }
    
    open func removeEvent(_ event: Event) {
        if let idx = self.events.index(of: event) {
            self.events.remove(at: idx)
            self.eventsMap.removeValue(forKey: event.uid)
        }
    }
    
    open func removeAllEvents() {
        self.events.removeAll(keepingCapacity: true)
        self.eventsMap.removeAll(keepingCapacity: true)
    }
    
    open func eventWithId(_ eventId: Int) -> Event? {
        return self.eventsMap[eventId]
    }
}
