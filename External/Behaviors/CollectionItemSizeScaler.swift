//
//  CollectionItemSizeScaler.swift
//  Football
//
//  Created by Mikhail Shulepov on 07/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class CollectionItemSizeScaler: NSObject {
    @IBOutlet var flowLayout: UICollectionViewFlowLayout! {
        didSet {
            self.refresh()
        }
    }
    
    @IBInspectable var iPadScale: CGFloat = 1.2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.refresh()
    }
    
    fileprivate func refresh() {
        if let flowLayout = self.flowLayout {
            if UIDevice.current.userInterfaceIdiom == .pad {
                let originalSize = flowLayout.itemSize
                flowLayout.itemSize = CGSize(width: originalSize.width * iPadScale, height: originalSize.height * iPadScale)
            }
        }
    }
}
