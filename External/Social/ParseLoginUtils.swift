//
//  FacebookSupport.swift
//  Football
//
//  Created by Mikhail Shulepov on 16/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import ReactiveSwift
import FBSDKLoginKit

open class ParseLoginUtils: NSObject {
    
    fileprivate let _currentUser: MutableProperty<PFUser?>
    open let currentUser: Property<PFUser?>
    
    fileprivate let _facebookLoggedIn: MutableProperty<Bool>
    open let facebookLoggedIn: Property<Bool>
    
    open class var shared: ParseLoginUtils {
        struct Static {
            static var Instance: ParseLoginUtils!
        }
        if Static.Instance == nil {
            Static.Instance = ParseLoginUtils()
        }
        return Static.Instance
    }
    
    open let loggedIn: Property<Bool>
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override init() {
        let pfUser = PFUser.current()
        if let pfUser = pfUser, pfUser.objectId == nil {
            _currentUser = MutableProperty(nil)
        } else {
            _currentUser = MutableProperty(pfUser)
        }
        currentUser = Property(_currentUser)
        
        
        let fbUserLoggedIn: Bool
        if let accessToken = FBSDKAccessToken.current() {
            let now = Date()
            fbUserLoggedIn = now.isBefore(accessToken.expirationDate)
        } else {
            fbUserLoggedIn = false
        }
        _facebookLoggedIn = MutableProperty(fbUserLoggedIn)
        facebookLoggedIn = Property(_facebookLoggedIn)
        
        loggedIn = Property(_facebookLoggedIn)
        
        super.init()
        
        self.prepareAnonymousUser().start()
        
        if (fbUserLoggedIn) {
            if let user = PFUser.current(), user.facebookId == nil {
                self.assignFacebookUserToParseUser(user)
            }
            if let user = PFUser.current(), user.displayName == nil {
                self.updateFacebookProfileDataForCurrentUser()
            }
        }
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "fbLoginStateChangedNotification",
        //    name: FBSDKAccessTokenDidChangeNotification, object: nil)
        
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        NotificationCenter.default.addObserver(self, selector: #selector(ParseLoginUtils.updateFacebookProfileDataForCurrentUser),
            name: NSNotification.Name.FBSDKProfileDidChange, object: nil)
    }
    
    internal func fbLoginStateChangedNotification() {
        _facebookLoggedIn.value = FBSDKAccessToken.current() != nil
        if let user = PFUser.current() {
            self.assignFacebookUserToParseUser(user)
        }
    }
    
    open func prepareAnonymousUser() -> SignalProducer<Bool, NSError> {
        if let pfUser = PFUser.current() {
            if pfUser.objectId != nil {
                return SignalProducer(value: true)
            }
            return SignalProducer { observer, compositeDisposable in
                pfUser.saveInBackground { result, error in
                    if let error = error {
                        NSLog("Error saving anonymous user: %@", (error as NSError).description)
                        observer.send(error: error as NSError)
                    } else {
                        self._currentUser.value = pfUser
                        observer.send(value: result)
                        observer.sendCompleted()
                    }
                }
            }
        } else {
            let info = [NSLocalizedDescriptionKey: "Current user is not set (anonymous users not enabled)"]
            let error = NSError(domain: "ParseLoginUtils", code: 9883, userInfo: info)
            return SignalProducer(error: error)
        }
    }
    
    open func linkOrLoginWithFacebook() -> SignalProducer<PFUser, NSError> {
        let producer: SignalProducer<PFUser, NSError>
   
        if let user = PFUser.current() {
            producer = self.linkWithFacebook(user).flatMapError { error -> SignalProducer<PFUser, NSError> in
                if error.code == 208 {
                    return self.linkNewUserWithFacebook()
                } else {
                    return SignalProducer(error: error)
                }
            }
        } else {
            producer = self.loginWithFacebook()
        }
        
        return producer.on (value: {user in
            self._facebookLoggedIn.value = true
            self.assignFacebookUserToParseUser(user)
            self.updateFacebookProfileDataForUser(user)
            self.didLogin(user)
        })
    }
    
    fileprivate var readPermissions: [String] {
        return ["public_profile", "user_friends"]
    }
    
    fileprivate func linkWithFacebook(_ user: PFUser) -> SignalProducer<PFUser, NSError> {
        return SignalProducer { observer, disposable in
            PFFacebookUtils.linkUser(inBackground: user, withReadPermissions: self.readPermissions, block: { succeeded, error  in
                if let error = error {
                    observer.send(error: error as NSError)
                } else {
                    observer.send(value: user)
                    observer.sendCompleted()
                }
            })
        }
    }
    
    fileprivate func linkNewUserWithFacebook() -> SignalProducer<PFUser, NSError> {
        return SignalProducer { observer, disposable in
            PFFacebookUtils.logInInBackground(with: FBSDKAccessToken.current(), block: { user, error in
                if let error = error {
                    observer.send(error: error as NSError)
                    
                } else if let user = user {
                    observer.send(value: user)
                    observer.sendCompleted()
                    
                } else {
                    NSLog("Login: complete without next")
                    observer.sendCompleted()
                }
            })
        }
    }
    
    fileprivate func loginWithFacebook() -> SignalProducer<PFUser, NSError> {
        NSLog("Try login")
        return SignalProducer { observer, disposable in
            PFFacebookUtils.logInInBackground(withReadPermissions: self.readPermissions, block: { user, error in
                if let error = error {
                    observer.send(error: error as NSError)
                    
                } else if let user = user {
                    observer.send(value: user)
                    observer.sendCompleted()
                    
                } else {
                    NSLog("Login: complete without next")
                    observer.sendCompleted()
                }
            })
        }
    }
    
    fileprivate func facebookFriendsIds() -> SignalProducer<[String], NSError> {
        return SignalProducer { observer, disposable in
            //"limit": 500, "fields": "id"
            let requestParams: [AnyHashable: Any] = [:]
            let request = FBSDKGraphRequest(graphPath: "me/friends", parameters: requestParams)
            let connection = FBSDKGraphRequestConnection()
            connection.add(request, completionHandler: { connection, result, error in
                if let error = error {
                    observer.send(error: error as NSError)
                    
                } else if let data = (result as? [String: AnyObject])?["data"] as? [NSDictionary] {
                    let ids = data.map { $0["id"] as! String }
                    observer.send(value: ids)
                    observer.sendCompleted()
                    
                } else {
                    observer.sendCompleted()
                }
            })
            connection.start()
        }
    }
       
    open func facebookFriends() -> SignalProducer<[PFUser], NSError> {
        return self.facebookFriendsIds().flatMap(FlattenStrategy.merge) { ids -> SignalProducer<[PFUser], NSError> in
            let query = PFUser.query()!
            query.whereKey(PFUser.FieldKey.FacebookId, containedIn: ids)
            return self.queryParseUsers(query)
        }
    }
    /*
    public func facebookFriends() -> SignalProducer<[PFUser], NSError> {
        return self.facebookFriendsIds() |> flatMap(.Merge) { ids in
            let query = PFUser.query()!
            query.whereKeyExists(PFUser.FieldKey.FacebookId)
            return self.queryParseUsers(query)
        }
    }*/
    
    fileprivate func assignFacebookUserToParseUser(_ parseUser: PFUser) {
        if PFFacebookUtils.isLinked(with: parseUser) {
            let facebookUserId = FBSDKAccessToken.current()?.userID
            if let oldFbId = parseUser.facebookId, oldFbId == facebookUserId {
                //nothing to do
            } else {
                parseUser.facebookId = facebookUserId
                parseUser.saveInBackground()
            }
        } else {
            NSLog("Not linked yet")
        }
    }
    
    internal func updateFacebookProfileDataForCurrentUser() {
        if let parseUser = PFUser.current() {
            self.updateFacebookProfileDataForUser(parseUser)
            self.didLogin(parseUser)
        }
    }
    
    fileprivate func updateFacebookProfileDataForUser(_ parseUser: PFUser) {
        if let profile = FBSDKProfile.current() {
            parseUser.displayName = profile.name
            parseUser.avatarURL = profile.imageURL(for: FBSDKProfilePictureMode.square, size: CGSize(width: 300, height: 300)).absoluteString
            parseUser.saveInBackground()
        }
    }
    
    fileprivate func queryParseUsers(_ query: PFQuery) -> SignalProducer<[PFUser], NSError> {
        return SignalProducer { observer, compositeDisposable in
            query.findObjectsInBackground { users, error in
                if let users = users as? [PFUser] {
                    observer.send(value: users)
                    observer.sendCompleted()
                } else if let error = error {
                    observer.send(error: error as NSError)
                } else {
                    observer.sendCompleted()
                }
            }
        }
    }
    
    fileprivate func didLogin(_ user: PFUser) {
        _currentUser.value = user
    }
    
    fileprivate func didLogout() {
        _currentUser.value = PFUser.current()!
    }
}

public extension PFUser {
    fileprivate struct FieldKey {
        static let AvatarUrl = "avatarUrl"
        static let FacebookId = "fbId"
        static let DisplayName = "displayName"
    }
    
    public var displayName: String? {
        get { return self.object(forKey: FieldKey.DisplayName) as? String }
        set {
            if let displayName = newValue {
                self.setObject(displayName, forKey: FieldKey.DisplayName)
            } else {
                self.remove(forKey: FieldKey.DisplayName)
            }
        }
    }
    
    public var avatarURL: String? {
        get { return self.object(forKey: FieldKey.AvatarUrl) as? String }
        set {
            if let avatarURL = newValue {
                self.setObject(avatarURL, forKey: FieldKey.AvatarUrl)
            } else {
                self.remove(forKey: FieldKey.AvatarUrl)
            }
        }
    }
    
    fileprivate(set) public var facebookId: String? {
        get {
            return self.object(forKey: FieldKey.FacebookId) as? String
        }
        set {
            if let facebookId = newValue {
                self.setObject(facebookId, forKey: FieldKey.FacebookId)
            } else {
                self.remove(forKey: FieldKey.FacebookId)
            }
        }
    }
}
