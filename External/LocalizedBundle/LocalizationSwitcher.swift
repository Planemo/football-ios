//
//  LocalizationSwitcher.swift
//  davinci
//
//  Created by Mihail Shulepov on 17/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

public class LocalizationSwitcher: NSObject {
    @IBInspectable
    public var storyboardName: String = "Main"
    
    public override init() {

    }
    
    @IBAction
    public func switchLanguage() {
        let languages = (NSBundle.availableLanguages() as! [String]).sorted{$0 < $1}
        let currentLanguage = NSBundle.language()
        var nextLanguage = languages.first!
        if let idx = find(languages, currentLanguage) {
            let nextIdx = (idx + 1) % languages.count
            nextLanguage = languages[nextIdx]
        }
        NSBundle.setLanguage(nextLanguage)
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: NSBundle.mainBundle())
        let initialVC = storyboard.instantiateInitialViewController() as! UIViewController
        let appDelegate = UIApplication.sharedApplication().delegate!
        appDelegate.window!?.rootViewController = initialVC
    }
}