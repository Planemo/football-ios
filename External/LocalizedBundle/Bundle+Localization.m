//
//  Bundle+Localization.m
//  davinci
//
//  Created by Mihail Shulepov on 17/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

#import <objc/runtime.h>
#import "Bundle+Localization.h"

static const char _bundle = 0;
static NSString * const SAVED_LANGUAGE_KEY = @"PLSavedLanguage";
NSString * const LanguageDidChangeNotification = @"PLLanguageDidChangeNotification";
NSString * const NotificationLanguageKey = @"PLLanguage";

@implementation BundleEx

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName {
    NSBundle *bundle = objc_getAssociatedObject(self, &_bundle);
    NSRange range = [tableName rangeOfString:@"~"];
    if (range.location != NSNotFound) {
        tableName = [tableName substringToIndex:range.location];
    }
    return bundle
        ? [bundle localizedStringForKey:key value:value table:tableName]
        : [super localizedStringForKey:key value:value table:tableName];
}

@end

@implementation NSBundle (Language)

+ (void)load {
    NSString *savedLanguage = [self language];
    if (savedLanguage) {
        [self setLanguage:savedLanguage];
    }
}

+ (void)setLanguage:(NSString *)language {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object_setClass([NSBundle mainBundle],[BundleEx class]);
    });

    if (language) {
        [[NSUserDefaults standardUserDefaults] setObject:language forKey:SAVED_LANGUAGE_KEY];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_LANGUAGE_KEY];
    }
    
    objc_setAssociatedObject([NSBundle mainBundle], &_bundle,
        language
            ? [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]]
            : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    NSDictionary *info = @{NotificationLanguageKey: language};
    [[NSNotificationCenter defaultCenter] postNotificationName:LanguageDidChangeNotification object:nil userInfo:info];
}

+ (NSString *)language {
    NSString *savedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:SAVED_LANGUAGE_KEY];
    if (savedLanguage) {
        return savedLanguage;
    }
    //Check whether user prefers language other than "en" (as it's often a default language, but not native)
    NSArray *availableLanguages = [self availableLanguages];
    NSArray *languages = [NSLocale preferredLanguages];
    NSArray *preferedLanguages = [languages subarrayWithRange:NSMakeRange(0, MIN(3, languages.count))];
    for (NSString *preferredLanguage in preferedLanguages) {
        if (![preferredLanguage isEqualToString:@"en"] && [availableLanguages containsObject:preferredLanguage]) {
            return preferredLanguage;
        }
    }
    if ([availableLanguages containsObject:@"en"]) {
        return @"en";
    }
    return availableLanguages.firstObject ?: preferedLanguages.firstObject;
}

+ (NSArray *)availableLanguages {
    NSArray *localizationBundles = [[NSBundle mainBundle] pathsForResourcesOfType:@"lproj" inDirectory:nil];
    NSMutableArray *result = [NSMutableArray array];
    for (NSString *path in localizationBundles) {
        NSString *name = path.lastPathComponent.stringByDeletingPathExtension;
        if ([name caseInsensitiveCompare:@"Base"] != NSOrderedSame) {
            [result addObject:name];
        }
    }
    return result;
}

@end