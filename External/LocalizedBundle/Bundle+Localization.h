//
//  Bundle+Localization.h
//  davinci
//
//  Created by Mihail Shulepov on 17/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const LanguageDidChangeNotification;
extern NSString * const NotificationLanguageKey;

@interface BundleEx : NSBundle
@end

@interface NSBundle(Language)

+ (void)setLanguage:(NSString *)language;
+ (NSString *)language;
+ (NSArray *)availableLanguages;

@end
